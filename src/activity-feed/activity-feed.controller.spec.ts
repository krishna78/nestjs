import { Test, TestingModule } from '@nestjs/testing';
import { ActivityFeedController } from './activity-feed.controller';

describe('ActivityFeed Controller', () => {
  let controller: ActivityFeedController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ActivityFeedController],
    }).compile();

    controller = module.get<ActivityFeedController>(ActivityFeedController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
