import { Module } from '@nestjs/common';
import { ActivityFeedController } from './activity-feed.controller';
import { ActivityFeedService } from './activity-feed.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ActivityFeedSchema } from './objects/activity-feed.schema';


@Module({
  imports: [  
     MongooseModule.forFeature([
    { name: "ActivityFeed", schema: ActivityFeedSchema },
  ])
],
  controllers: [ActivityFeedController],
  providers: [ActivityFeedService],
  exports: [ActivityFeedService]
})
export class ActivityFeedModule {}
