import { Injectable } from "@nestjs/common";
import { BaseService } from "../common/base/base.service";
import { IActivityFeed, ActivityFeed } from "./objects/activity-feed.schema";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CreateActivityFeedDto } from "./objects/activity-feed.dto";

@Injectable()
export class ActivityFeedService extends BaseService<IActivityFeed> {
  constructor(
    @InjectModel("ActivityFeed")
    private readonly activityFeedModel: Model<ActivityFeed>
  ) {
    super(activityFeedModel);
  }
  /*
   */
  /*
     async create(createDto: any,user) {
        let data = new CreateActivityFeedDto(createDto);
        
        console.log('datttttttttaaaaaaaaaaaaaaa',data);
        return super.create(
          data,
          user
          //user
         // createdBy: createDto.createdBy
        );
       

        
    }
    */

  async sponsorRegistration(createDto2, _user) {
    let data = new CreateActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async onboardingCompletion(createDto2, _user) {
    let data = new CreateActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async kYCVerificationNotificationsSuccessful(createDto3, model) {
    let data = new CreateActivityFeedDto(createDto3);
    return await super.create(data, model);
  }

  async kYCVerificationNotificationsPendingAndRequestingForDocuments(
    createDto5,
    user
  ) {
    let data = new CreateActivityFeedDto(createDto5);
    return await super.create(data, user);
  }

  async kYCVerificationNotificationsRejectedAndReasonForRejection(
    createDto5,
    model
  ) {
    let data = new CreateActivityFeedDto(createDto5);
    return await super.create(data, model);
  }

  async profileChangesEditingTheProfile(createDto7, model) {
    let data = new CreateActivityFeedDto(createDto7);
    await super.create(data, model);
  }

  async walletBalanceInitialBalance(createDto3, _user) {
    let data = new CreateActivityFeedDto(createDto3);
    return await super.create(data, _user);
  }

  async walletMoneyAdded(createDto8, _user) {
    let data = new CreateActivityFeedDto(createDto8);
    await super.create(data, _user);
  }

  async walletMoneyWithdrawn(createDto5, _user) {
    let data = new CreateActivityFeedDto(createDto5);
    await super.create(data, _user);
  }

  async projectsViewedBySponsor(createDto4, _user) {
    let data = new CreateActivityFeedDto(createDto4);
    await super.create(data, _user);
  }

  async projectsShownInterest(createDto4, _user) {
    let data = new CreateActivityFeedDto(createDto4);
    await super.create(data, _user);
  }

  async projectsAllottedSuccessfullyMatchedWithSponsor(createDto10, _user) {
    let data = new CreateActivityFeedDto(createDto10);
    await super.create(data, _user);
  }

  async projectsRejected(createDto9, _user) {
    let data = new CreateActivityFeedDto(createDto9);
    await super.create(data, _user);
  }

  async projectsPendingWithSuperAdmin(bidId: string, user) {}

  async projectsFunded(createDto11, _user) {
    let data = new CreateActivityFeedDto(createDto11);
    await super.create(data, _user);
  }

  // async amountFundedForProject(bidId: string, user) {}

  async repaymentBySME(bidId: string, user) {}

  /*
    async projectsRejected(bidId: string,user){
         // let bid  = await this.bidDetails.findOne(bidId);
         
          
         // super.create({bid,user})
    }
    */
}
