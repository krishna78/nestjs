import { Expose } from "class-transformer";
import { Reader, Creator } from "../../common/base/base.dto";
import { IsDefined, IsNotEmpty } from "class-validator";

export class ActivityFeedDto extends Reader {
  @Expose()
  readonly _priority: number = 0;

  @Expose()
  readonly type: string = "";

  @Expose()
  readonly setFields: any;

  @Expose()
  readonly itemId: string = "";

  @Expose()
  readonly optional: string = "";

  @Expose()
  readonly createdBy: string = "";
}

export class CreateActivityFeedDto extends Creator {
  constructor({ _priority, type, itemId, setFields, optional }) // createdBy
  {
    super(true);
    this._priority = _priority;
    this.type = type;
    this.itemId = itemId;
    this.setFields = setFields;
    this.optional = optional;
    // this.createdBy = createdBy
  }
  @IsDefined()
  @IsNotEmpty()
  readonly _priority: number = 0;

  @IsDefined()
  @IsNotEmpty()
  readonly type: string = "";

  @IsDefined()
  @IsNotEmpty()
  readonly optional: string = "";

  @IsDefined()
  @IsNotEmpty()
  readonly itemId: string = "";
  @IsDefined()
  @IsNotEmpty()
  readonly setFields: any;
}
