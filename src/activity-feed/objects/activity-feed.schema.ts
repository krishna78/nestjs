import { createModel, Entity, IEntity } from "../../common/base/base.model";
import { Schema } from "mongoose";
import { ACTIVITY_FEED_EVENTS_SPONSOR } from "../../common/constants/enum";

export class ActivityFeed extends Entity {
  _priority: number;
  type: string;
  itemId: string;
  setFields: any;
  optional: string;
}

export interface IActivityFeed extends ActivityFeed, IEntity {
  id: string;
}

export const ActivityFeedSchema: Schema = createModel("ActivityFeedTable", {
  _priority: { type: Number },
  type: {
    type: String,
    enum: Object.values({ ...ACTIVITY_FEED_EVENTS_SPONSOR }),
  },
  itemId: { type: String },
  setFields: { type: Object },
  optional: { type: String },
});
