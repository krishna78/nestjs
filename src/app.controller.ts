import { Controller, Get, Req } from "@nestjs/common";
import {
  INVESTOR,
  SME,
  USER_TYPES,
  KYC_VERIFICATION_STATUS,
  USER_STATUS,
  TIMELINE_PREFERENCE_0,
} from "./common/constants/enum";
import { success } from "./common/base/httpResponse.interface";

@Controller()
export class AppController {
  constructor() {}

  @Get()
  getHello(@Req() req): Object {
    return success({
      success: "Ok",
    });
  }

  @Get("/entity-types")
  getEntityTypes(): Object {
    return success({
      sme: Object.values(SME),
      investor: Object.values(INVESTOR),
    });
  }

  @Get("/user-types")
  getUserTypes(): Object {
    return success(Object.values({ ...USER_TYPES }));
  }

  @Get("/kyc-verification-status")
  getKycVerificationStatus(): Object {
    return success(Object({ ...KYC_VERIFICATION_STATUS }));
  }

  @Get("/user-status")
  getUserStatus(): Object {
    return success(Object({ ...USER_STATUS }));
  }
  /*
  @Get("/user-timeLine-preference")
  getUserTimeLinePreference(): Object {
  return success(Object({ ...TIMELINE_PREFERENCE }));
  }
*/
}
