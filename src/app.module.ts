import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { MongooseModule } from "@nestjs/mongoose";
import { CommonModule } from "./common/common.module";
import { AuthModule } from "./auth/auth.module";
import { CountryModule } from "./country/country.module";
import { MONGO_URI } from "./common/constants/config";
import { SmeEntityModule } from "./sme-entity/sme-entity.module";
import { FileUploadModule } from "./file-upload/file-upload.module";
import { SmeProjectModule } from "./sme-project/sme-project.module";
import { RolesModule } from "./roles/roles.module";
import { WalletTransactionsModule } from "./wallet-transactions/wallet-transactions.module";
import { BidDetailsModule } from "./bid-details/bid-details.module";
import { ActivityFeedModule } from "./activity-feed/activity-feed.module";
import { SmeActivityFeedModule } from "./sme-activity-feed/sme-activity-feed.module";
import { FavouriteProjectsModule } from "./favourite-projects/favourite-projects.module";
import { UserPortalRemarksModule } from './user-portal-remarks/user-portal-remarks.module';
import { BigchainDbModule } from './bigchain-db/bigchain-db.module';
@Module({
  imports: [
    MongooseModule.forRoot(MONGO_URI, { useNewUrlParser: true }),
    CommonModule,
    CountryModule,
    SmeEntityModule,
    AuthModule,
    FileUploadModule,
    SmeProjectModule,
    RolesModule,
    ActivityFeedModule,
    SmeActivityFeedModule,
    //WalletTransactionsModule,
    BidDetailsModule,
    FavouriteProjectsModule,
    BigchainDbModule,
    UserPortalRemarksModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
