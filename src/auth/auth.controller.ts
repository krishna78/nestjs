import {
  Controller,
  Post,
  Req,
  UseGuards,
  Get,
  Body,
  BadRequestException,
  Param,
  NotFoundException,
} from "@nestjs/common";
import { JwtAuthGuard } from "./auth.guard";
import { LoggedInToken } from "../users/objects/login-user.dto";
import { AuthService } from "./auth.service";
import * as speakeasy from "speakeasy";
import { optSecret } from "../common/constants/config";
import {
  UNKNOWN_PARAM,
  EMAIL_NOT_FOUND,
  OTP_ERROR,
  EXISTS,
  OTP_NOT_EXPIRED,
  NEW_PASSWORD_AND_CONFIRM_NEW_PASSWORD_ERROR,
  OTP_TIME_OUT,
  TOKEN_ALREADY_USED,
  EMAIL_ERROR,
  BLOCKED_ACCOUNT_ERROR,
} from "../common/constants/string";
import { plainToClass } from "class-transformer";
import { success } from "../common/base/httpResponse.interface";
import { UserDto } from "../users/objects/create-user.dto";
import { OtpEmail, UserCycloanAccountBlockedEmail } from "../users/objects/user.registered.email";
import {
  ForgetPasswordOtpEmail,
  PasswordChangedAlert,
} from "../users/objects/user.registered.email";
import { EmailService } from "../email/email.service";
import { OtpService } from "./otp/otp.service";
import { RequestUser } from "../common/utils/controller.decorator";
import { UsersService } from "../users/users.service";
import { EmailDto } from "../email/objects/email.dto";
import { OtpDto } from "./otp/otp.dto";
import { InjectModel } from "@nestjs/mongoose";
import { IOtp, Otp } from "./otp/otp.schema";
import { Model } from "mongoose";
import { ForgotPasswordOtpService } from "./forgot-password-otp/forgot-password-otp.service";
import { ForgotPasswordOtp } from "./forgot-password-otp/forgot-password-otp.schema";
import { ForgotPasswordOtpDto } from "./forgot-password-otp/forgot-password-otp.dto";
import { OtpIncorrectService } from "./otpIncorrect/otpIncorrect.service";
import { OtpIncorrect } from "./otpIncorrect/otpIncorrect.schema";
import { BlockedAccountService } from "./blockedAccounts/blockedAccounts.service";
import { IBlockedAccount } from "./blockedAccounts/blockedAccounts.schema";
import { OTP_RETRY_LIMIT, Status, ROLES_ACCESS_ACTION, BLOCKED_ACCOUNT_TYPE } from "../common/constants/enum";
import { RolesService } from "../roles/roles.service";
import { OtpIncorrectForgotPasswordService } from "./otpIncorrectForgotPassword/otpIncorrectForgotPassword.service";
import { OtpIncorrectForgotPassword } from "./otpIncorrectForgotPassword/otpIncorrectForgotPassword.schema";

//@UseGuards(JwtAuthGuard)
@Controller("auth/refresh")
export class AuthController {
  constructor(
    private authService: AuthService,
    private emailService: EmailService,
    private usersService: UsersService,
    private otpService: OtpService,
    private forgotPasswordOtpService: ForgotPasswordOtpService,
    @InjectModel("Otp") private readonly otpModel: Model<Otp>,
    @InjectModel("ForgotPasswordOtp")
    private readonly forgotPasswordotpModel: Model<ForgotPasswordOtp>,
    private readonly otpIncorrectService: OtpIncorrectService,
    @InjectModel("OtpIncorrect") private readonly otpIncorrectModel: Model<OtpIncorrect>,
    private readonly blockedAccountService: BlockedAccountService,
    @InjectModel("BlockedAccount") private readonly blockedAccountModel: Model<IBlockedAccount>,
    private rolesservice: RolesService,
    private otpIncorrectForgotPasswordService: OtpIncorrectForgotPasswordService,
    @InjectModel("OtpIncorrectForgotPassword") private readonly otpIncorrectForgotPasswordModel: Model<OtpIncorrectForgotPassword>,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  public async refresh(@Req() req): Promise<LoggedInToken> {
    return this.authService.createJwtPayLoad(req.user);
  }

  //Api For generating a secret and storing it in config.ts
  @Get("secret")
  async getSecret() {
    const secret = speakeasy.generateSecret({ length: 20 });
    return secret;
  }
  //Api For generating a 6 digit token using the secret

  @Post("generate")
  async getOtp(
    @Req() req,
    @Body() body: { email: string; firstName: string; lastName: string }
    //@RequestUser() user
  ) {
    debugger;
    let email = body.email;
    let firstName = body.firstName;
    let lastName = body.lastName;
    var token = speakeasy.totp({
      secret: optSecret,
      encoding: "base32",
    });

    let userToAttempt: any = await this.usersService.findOneByEmail(body.email);

    //Check for existing users
    if (!userToAttempt) {
     // let otpErrorCount: any = await this.otpIncorrectModel.find({ email: email });
     let _blocked: any = await this.blockedAccountService.findOneByQuery({email: email, type: BLOCKED_ACCOUNT_TYPE.USER_REGISTRATION})
     console.log('_blocked','_blocked .................._blocked',_blocked); 
     if(_blocked !== null && _blocked.otpCount > OTP_RETRY_LIMIT.MAXIMUM_OTP_RETRY_LIMIT){
        throw new BadRequestException(BLOCKED_ACCOUNT_ERROR(email))
      }

      let query = { email: email };
      console.log(query);
      let _otp: any = await this.otpService.findOneByQuery(query);
      let currentTime: number = Date.now();
      if (_otp) {
        let k: any = await this.otpModel
          .find({ email: email })
          .sort({ updatedTime: -1 })
          .limit(1);
        if (k !== undefined) {
          let diff = (currentTime - k[0].expiry) / 1000;

          let updateTime: number = Date.now();
          let createDto: any = {
            token: token,
            email: email,
            firstName: firstName,
            lastName: lastName,
            expiry: updateTime + 15 * 60 * 1000,
          };
          if (diff > 0) {
            let _otp: any = await this.otpService.create(createDto);
            let _data =
              "Otp sent to registered email " +
              body.email +
              " " +
              "token:" +
              token;
            await this.emailService.sendEmail(
              new OtpEmail(
                new EmailDto({
                  to: body.email,
                  metaData: { email, token, firstName, lastName },
                })
              )
            );
            return success(_data);
          } else {
            let errorData = "Otp sent yet to expire in" + diff + "seconds";
            throw new BadRequestException(OTP_NOT_EXPIRED(errorData));
          }
        }
      }
      //For users requesting for the first time
      let updateTime: number = Date.now();
      let createDto: any = {
        token: token,
        email: email,
        expiry: updateTime + 15 * 60 * 1000,
      };
      let _otp1: any = await this.otpService.create(createDto);
      await this.emailService.sendEmail(
        new OtpEmail(
          new EmailDto({
            to: body.email,
            metaData: { email, token, firstName, lastName },
          })
        )
      );
      let _data1 =
        "Otp sent to registered email " + body.email + " " + "token:" + token;
      return success(_data1);
    }
    throw new BadRequestException(EXISTS, "User exists");
  }

  @Post("forgotPassword/generate")
  async getForgotPasswordOtp(
    @Req() req,
    @Body() body: { email: string }
    // @RequestUser()User
  ) {
    debugger;
    let email = body.email;
    var token = speakeasy.totp({
      secret: optSecret,
      encoding: "base32",
    });

    let userToAttempt: any = await this.usersService.findOneByEmail(body.email);
    console.log('................................', userToAttempt)
    //Check for existing users
    if (userToAttempt !== null) {
    //if (userToAttempt.firstName) {
      let firstName = userToAttempt.firstName;
      let lastName = userToAttempt.lastName;
      let _blocked: any = await this.blockedAccountService.findOneByQuery({email: email, type: BLOCKED_ACCOUNT_TYPE.FORGOT_PASSWORD })
      console.log('_blocked','_blocked .................._blocked',_blocked); 
      if(_blocked !== null && _blocked.otpCount > OTP_RETRY_LIMIT.MAXIMUM_OTP_RETRY_LIMIT){
         throw new BadRequestException(BLOCKED_ACCOUNT_ERROR(userToAttempt.email))
       }
      let query = { email: email };
      console.log(query);
      //let _otp: any = await this.otpService.findOneByQuery(query);
      let _otp: any = await this.forgotPasswordOtpService.findOneByQuery(query);
      let currentTime: number = Date.now();
      if (_otp) {
        let k: any = await this.forgotPasswordotpModel
          .find({ email: email })
          .sort({ updatedTime: -1 })
          .limit(1);
        if (k !== undefined) {
          let diff = (currentTime - k[0].expiry) / 1000;

          let updateTime: number = Date.now();
          let createDto: any = {
            token: token,
            email: email,
            expiry: updateTime + 15 * 60 * 1000,
          };
          if (diff > 0) {
            let _otp: any = await this.forgotPasswordOtpService.create(
              createDto
            );
            let _data =
              "Otp for forgot password sent to registered email " + body.email+
              " " +
              "token:" +
              token;
            await this.emailService.sendEmail(
              new ForgetPasswordOtpEmail(
                new EmailDto({
                  to: body.email,
                  metaData: { email, token, firstName, lastName },
                })
              )
            );
            return success(_data);
          } else {
            let errorData =
              "Otp for forgot password sent yet to expire in" +
              diff +
              "seconds";
            throw new BadRequestException(OTP_NOT_EXPIRED(errorData));
          }
        }

        /**/

        /**/
      }

      //For users requesting for the first time
      let updateTime: number = Date.now();
      let createDto: any = {
        token: token,
        email: email,
        expiry: updateTime + 15 * 60 * 1000,
      };
      let _otp1: any = await this.forgotPasswordOtpService.create(createDto);
      await this.emailService.sendEmail(
        new ForgetPasswordOtpEmail(
          new EmailDto({
            to: body.email,
            metaData: { email, token, firstName, lastName },
          })
        )
      );
      let _data1 =
        "Otp for forgot password sent to registered email " + body.email +
        " " +
        "token:" +
        token;
      return success(_data1);
    } else {
      throw new BadRequestException(EMAIL_NOT_FOUND);
    }
    
  }

  //Api for verifying a 6 digit token using the secret
  @Post("otp/:emailOrMobile")
  async verifyOTP(
    @Param("emailOrMobile") emailOrMobile,
    @Body() body: { otp: string; email: string }
    //@RequestUser() user
  ) {
    debugger;
    let otp = body.otp;
    let email = body.email;
    let updateTime: number = Date.now();
    //query otp table with our otp
    //Check for expiry
    let update = {};
    let _blocked: any = await this.blockedAccountService.findOneByQuery({email: email, type: BLOCKED_ACCOUNT_TYPE.USER_REGISTRATION})
    console.log('_blocked','_blocked .................._blocked',_blocked); 
    if(_blocked !== null && _blocked.otpCount > OTP_RETRY_LIMIT.MAXIMUM_OTP_RETRY_LIMIT){
   // if(_blocked !== undefined){
      //throw new BadRequestException(BLOCKED_ACCOUNT_ERROR)
      throw new BadRequestException(BLOCKED_ACCOUNT_ERROR(email))
    }
    const userToAttempt: any = await this.usersService.findOneByEmail(email);
   // let firstName = userToAttempt.firstName;
  //  let lastName = userToAttempt.lastName;

    //let otpErrorCount: any = await this.otpIncorrectModel.find({ email: email });
   
    //let dCount = await this.smeProjectModel.count(
    //  _q,
    //);
  
    if (!userToAttempt) {
      let query = { token: otp, email: email };
      //let query = {token: otp,email:userToAttempt.email,$lte: updateTime}
      let _otp: any = await this.otpService.findOneByQuery(query);

      switch (emailOrMobile) {
        case "mobile":
          update = { mobile: true };
          break;
        case "email":
          var tokenValidates = speakeasy.totp.verify({
            secret: optSecret,
            encoding: "base32",
            token: otp,
            window: 30,
          });

          if (tokenValidates) {
            //return { email: true}
            update = {
              isVerified: true,
            };
          
          } else 
          {
                let updateTime: number = Date.now();
                let createDto: any = {
                  token: otp,
                  email: email,
                  //firstName: firstName,
                  //lastName: lastName,
                  //expiry: updateTime + 15 * 60 * 1000,
                };
                let createBlockedAccountDto: any = {
                  email: email,
                  type: BLOCKED_ACCOUNT_TYPE.USER_REGISTRATION,
                  otpCount: 1
                }
                //if (diff > 0) {
                //let _otp: any = await this.otpIncorrectService.create(createDto);
                //console.log('otp tokennnnnnnnnn errorrrr', _otp)
                //let otpErrorCount: any = await this.otpIncorrectModel.count({ email: email});
                //console.log('Otp error count',otpErrorCount, 'If the attempts of failure are greater than 10, block this account. Create blockedCollection.')
                //if(otpErrorCount > OTP_RETRY_LIMIT.MAXIMUM_OTP_RETRY_LIMIT){
              let _blocked: any = await this.blockedAccountService.findOneByQuery({email: email, type: BLOCKED_ACCOUNT_TYPE.USER_REGISTRATION})
              if(_blocked == null){
                let _blocked: any = await this.blockedAccountService.create(createBlockedAccountDto);
                //console.log('Your account is added to blocked list. BLOCKED LIST BLOCKED LIST BLOCKED LIST', _blocked);
               
              } else {
                let updateTime: number = Date.now();
                let _blockedUpdate: any = await this.blockedAccountService.edit(_blocked.code,{ otpCount: _blocked.otpCount + 1}, updateTime);
                      if(_blockedUpdate.otpCount > OTP_RETRY_LIMIT.MAXIMUM_OTP_RETRY_LIMIT){
                        await this.emailService.sendEmail(
                          new UserCycloanAccountBlockedEmail(
                            new EmailDto({
                              to: body.email,
                              metaData: { email, //firstName, lastName 
                              },
                            })
                          )
                        );
                      
                        console.log('Blocked Account email sent.................');
                        console.log('Your account is added to blocked list. BLOCKED LIST BLOCKED LIST BLOCKED LIST', _blocked);
                        throw new BadRequestException(BLOCKED_ACCOUNT_ERROR(email))
                      }
              //}
             
            }
            throw new BadRequestException(OTP_ERROR);
            
          }
          break;
        default:
          throw new BadRequestException(UNKNOWN_PARAM(emailOrMobile));
      }
      let updated = await this.otpService.edit(_otp.id, update, updateTime);
      const _data = plainToClass(OtpDto, updated, {
        excludeExtraneousValues: true,
      });
      return success(_data);
    }

  }
  
  @UseGuards(JwtAuthGuard)
  @Post("blockedAccounts")
  async unblockUserAccount(
    //@Param("emailOrMobile") emailOrMobile,
    @Body()
    body: {
      //otp: string;
      email: string;
      type: string;
      // newPassword: string;
      // confirmNewPassword: string;
    },
    @RequestUser() user
  ) {
    let _user = await this.rolesservice.findOneByQuery({roleName: user.type});
    console.log('userstype', user.type, _user)
    let hasAccess = _user.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.AUTH_CONTROLLER_UNBLOCK_USER_ACCOUNT
    );
    if(hasAccess) {
    
      debugger;
      //let otp = body.otp;
      let email = body.email;
      let type = body.type;
      let updateTime: number = Date.now();
      //let otpErrorCount: any = await this.otpIncorrectModel.count({ email: email });
      //let otpErrorList: any = await this.otpIncorrectModel.find({ email: email });
      /*
      for(var i = 0; i < otpErrorList.length; i++){
        //return await this.transactionsModel.find({code: idOrCode });
        //model.status = Status.DELETED;
        //await model.save();
        //otpErrorList[i].status = Status.DELETED
        let model = await this.otpIncorrectModel.findOne({ code: otpErrorList[i].code });
        model.status = Status.DELETED;
        await model.save();
        let _updatedModel = await this.otpIncorrectModel.findOne({ code: otpErrorList[i].code });
        console.log(_updatedModel)
      }
      console.log('Deleted all otp incorrect records for this email', 'email', email)
      */
      let _blocked: any = await this.blockedAccountService.findOneByQuery({email: email, type: type})
      if(_blocked !== null && _blocked.otpCount > OTP_RETRY_LIMIT.MAXIMUM_OTP_RETRY_LIMIT){
        let _block: any = await this.blockedAccountService.remove(_blocked.code);
        return _block
      }
    }
    throw new NotFoundException();
  }

async m(createDto, userToAttempt, createBlockedAccountDto){
  /*
  let _otp: any = await this.otpIncorrectForgotPasswordService.create(createDto);
  console.log('otp tokennnnnnnnnn errorrrr', _otp)
  let otpErrorCount: any = await this.otpIncorrectForgotPasswordModel.count({ email: userToAttempt.email});
  console.log('Otp error count',otpErrorCount, 'If the attempts of failure are greater than 10, block this account. Create blockedCollection.')
  if(otpErrorCount > OTP_RETRY_LIMIT.MAXIMUM_OTP_RETRY_LIMIT){
    */
    let _blocked: any = await this.blockedAccountService.findOneByQuery({email: userToAttempt.email, type: BLOCKED_ACCOUNT_TYPE.FORGOT_PASSWORD })
    if(_blocked == null){
      let _blocked: any = await this.blockedAccountService.create(createBlockedAccountDto);
      //console.log('Your account is added to blocked list. BLOCKED LIST BLOCKED LIST BLOCKED LIST', _blocked);
    } else {
      let updateTime: number = Date.now();
      let _blockedUpdate: any = await this.blockedAccountService.edit(_blocked.code,{ otpCount: _blocked.otpCount + 1}, updateTime);
          if(_blockedUpdate.otpCount > OTP_RETRY_LIMIT.MAXIMUM_OTP_RETRY_LIMIT){
          //if(_blocked.otpCount > OTP_RETRY_LIMIT.MAXIMUM_OTP_RETRY_LIMIT){
                let email = userToAttempt.email
                await this.emailService.sendEmail(
                  new UserCycloanAccountBlockedEmail(
                    new EmailDto({
                      to: userToAttempt.email,
                      metaData: { email, //firstName, lastName 
                      },
                    })
                  )
                );
                console.log('Blocked Account email sent.................');
                console.log('Your account is added to blocked list. BLOCKED LIST BLOCKED LIST BLOCKED LIST', _blocked);
                throw new BadRequestException(BLOCKED_ACCOUNT_ERROR(userToAttempt.email))
          }
            
      }
    //}
     
    
    
  //}
}
 

  //Api for verifying a 6 digit token using the secret
  @Post("forgotPasswordOtp/:emailOrMobile")
  async verifyForgotPasswordOTP(
    @Param("emailOrMobile") emailOrMobile,
    @Body()
    body: {
      otp: string;
      email: string;
      // newPassword: string;
      // confirmNewPassword: string;
    }
    //@RequestUser() user
  ) {
    debugger;
    let otp = body.otp;
    let email = body.email;
    let updateTime: number = Date.now();
    //query otp table with our otp
    //Check for expiry
    let update = {};
    let query = {
      token: otp,
      //, email: email
    };
   
    
    let _otp: any = await this.forgotPasswordOtpService.findOneByQuery(query);
    //if (_otp == undefined) {
   //   await this.m(createDto, userToAttempt, createBlockedAccountDto)
   //   throw new BadRequestException(EMAIL_ERROR);
   // }
    const userToAttempt: any = await this.usersService.findOneByEmail(
      email//_otp.email
    );

    var tokenValidates = speakeasy.totp.verify({
      secret: optSecret,
      encoding: "base32",
      token: otp,
      window: 30,
    });
/*
    if (tokenValidates) {
      //return { email: true}
      update = {
        isVerified: true,
      };
    } else {
      
     //await this.m(createDto, userToAttempt, createBlockedAccountDto)
      throw new BadRequestException(OTP_ERROR);
    }
   */
  
    if (userToAttempt) {
      // if (_otp) {
      switch (emailOrMobile) {
        case "mobile":
          update = { mobile: true };
          break;
        case "email":
          var tokenValidates = speakeasy.totp.verify({
            secret: optSecret,
            encoding: "base32",
            token: otp,
            window: 30,
          });

          if (tokenValidates) {
            //return { email: true}
            update = {
              isVerified: true,
            };
          } else {
            let createDto: any = {
              token: otp,
              email: userToAttempt.email,
              //firstName: firstName,
              //lastName: lastName,
              //expiry: updateTime + 15 * 60 * 1000,
            };
            let createBlockedAccountDto: any = {
              email: userToAttempt.email,
              type: BLOCKED_ACCOUNT_TYPE.FORGOT_PASSWORD,
              otpCount: 1
            } 
            let _blocked: any = await this.blockedAccountService.findOneByQuery({email: userToAttempt.email, type: BLOCKED_ACCOUNT_TYPE.FORGOT_PASSWORD })
            console.log('_blocked','_blocked .................._blocked',_blocked); 
            if(_blocked !== null && _blocked.otpCount > OTP_RETRY_LIMIT.MAXIMUM_OTP_RETRY_LIMIT){
               throw new BadRequestException(BLOCKED_ACCOUNT_ERROR(userToAttempt.email))
             }
            await this.m(createDto, userToAttempt, createBlockedAccountDto)
            throw new BadRequestException(OTP_ERROR);
          }
          break;
        default:
          throw new BadRequestException(UNKNOWN_PARAM(emailOrMobile));
      }

      let currentTime: number = Date.now();
      let diff = (currentTime - _otp.expiry) / 1000;
      if (!_otp.isVerified) {
        let updated = await this.forgotPasswordOtpService.edit(
          _otp.id,
          update,
          updateTime
        );

        //
        //  let firstName = userToAttempt.firstName;
        //let lastName = userToAttempt.lastName;
        //
        let email = userToAttempt.email;

        //if (body.newPassword == body.confirmNewPassword) {
        if (diff < 0) {
          /*
            let _update = { password: body.newPassword };
            let _updated = await this.usersService.edit(
              userToAttempt.id,
              _update,
              updateTime
            );
            */
          const _data = plainToClass(ForgotPasswordOtpDto, updated, {
            excludeExtraneousValues: true,
          });
          return success(_data);
        }
        throw new BadRequestException(OTP_TIME_OUT);
        // }
      } else {
        let errorData =
          "Token not verified.Otp for forgot password sent yet to expire in" +
          diff +
          "seconds";
        throw new BadRequestException(OTP_NOT_EXPIRED(errorData));
        //throw new BadRequestException(TOKEN_ALREADY_USED)
      }

      //}
    }
    throw new BadRequestException(EMAIL_NOT_FOUND);
  }

  @Post("forgotPassword")
  async verifyForgotPassword(
    //@Param("emailOrMobile") emailOrMobile,
    @Body()
    body: {
      otp: string;
      //email: string;
      newPassword: string;
      confirmNewPassword: string;
    }
    //@RequestUser() user
  ) {
    let otp = body.otp;
    //let email = body.email;
    let updateTime: number = Date.now();
    //query otp table with our otp
    //Check for expiry
    let update = {};
    let query = {
      token: otp,
      //, email: email
    };
    let _otp: any = await this.forgotPasswordOtpService.findOneByQuery(query);
    const userToAttempt: any = await this.usersService.findOneByEmail(
      _otp.email
    );
    let currentTime: number = Date.now();
    let diff = (currentTime - _otp.expiry) / 1000;
    if (userToAttempt) {
      if (_otp.isVerified) {
        //let updated = await this.forgotPasswordOtpService.edit(_otp.id, update, updateTime);
        if (body.newPassword == body.confirmNewPassword) {
          if (diff < 0) {
            let _update = { password: body.newPassword };
            let _updated = await this.usersService.edit(
              userToAttempt.id,
              _update,
              updateTime
            );
            let email = userToAttempt.email;
            let firstName = userToAttempt.firstName;
            let lastName = userToAttempt.lastName;
            await this.emailService.sendEmail(
              new PasswordChangedAlert(
                new EmailDto({
                  to: userToAttempt.email,
                  metaData: { email, firstName, lastName },
                })
              )
            );

            const _data = plainToClass(UserDto, _updated, {
              excludeExtraneousValues: true,
            });
            return success(_data);
          }
          throw new BadRequestException(OTP_TIME_OUT);
        } else {
          throw new BadRequestException(
            NEW_PASSWORD_AND_CONFIRM_NEW_PASSWORD_ERROR
          );
        }
      } else {
        let errorData =
          "Token already used and new password is set with the token.Otp for forgot password sent yet to expire in" +
          diff +
          "seconds";
        throw new BadRequestException(OTP_NOT_EXPIRED(errorData));
        //throw new BadRequestException(TOKEN_ALREADY_USED)
      }
    }
    throw new BadRequestException(EMAIL_NOT_FOUND);
  }
}
