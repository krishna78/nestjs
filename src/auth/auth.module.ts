import { Module } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { UsersModule } from "../users/users.module";
import { JwtStrategy } from "./jwt.strategy";
import { JwtModule } from "@nestjs/jwt";
import { JWT_SECRET_KEY, JWT_EXPIRY } from "../common/constants/config";
import { MongooseModule } from "@nestjs/mongoose";
import { UserSchema } from "../users/objects/user.schema";
import { AuthController } from "./auth.controller";
import { LoginModule } from "../login/login.module";
import { EmailModule } from "../email/email.module";
import { OtpService } from "./otp/otp.service";
import { OtpSchema } from "./otp/otp.schema";
import { ForgotPasswordOtpSchema } from "./forgot-password-otp/forgot-password-otp.schema";
import { ForgotPasswordOtpService } from "./forgot-password-otp/forgot-password-otp.service";
import { OtpIncorrectSchema } from "./otpIncorrect/otpIncorrect.schema";
import { OtpIncorrectService } from "./otpIncorrect/otpIncorrect.service";
import { BlockedAccountSchema } from "./blockedAccounts/blockedAccounts.schema";
import { BlockedAccountService } from "./blockedAccounts/blockedAccounts.service";
import { RolesSchema } from "../roles/objects/roles.schema";
import { RolesService } from "../roles/roles.service";
import { RolesModule } from "../roles/roles.module";
import { OtpIncorrectForgotPasswordSchema } from "./otpIncorrectForgotPassword/otpIncorrectForgotPassword.schema";
import { OtpIncorrectForgotPasswordService } from "./otpIncorrectForgotPassword/otpIncorrectForgotPassword.service";

@Module({
  imports: [
    JwtModule.register({
      secretOrPrivateKey: JWT_SECRET_KEY,
      signOptions: {
        expiresIn: JWT_EXPIRY,
      },
    }),
    MongooseModule.forFeature([{ name: "User", schema: UserSchema },
                               { name: "Otp", schema: OtpSchema},
                               { name: "ForgotPasswordOtp", schema: ForgotPasswordOtpSchema},
                               { name: "OtpIncorrect", schema: OtpIncorrectSchema},
                               { name: "BlockedAccount", schema: BlockedAccountSchema},
                               { name: "OtpIncorrectForgotPassword", schema: OtpIncorrectForgotPasswordSchema },
                               { name: "AdminRoles", schema: RolesSchema }
                              ]),
    LoginModule,EmailModule,  RolesModule,
  ],
  exports: [OtpService, ForgotPasswordOtpService, OtpIncorrectService, BlockedAccountService, OtpIncorrectForgotPasswordService],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, OtpService, ForgotPasswordOtpService, OtpIncorrectService, BlockedAccountService, RolesService, OtpIncorrectForgotPasswordService],
})
export class AuthModule {}
