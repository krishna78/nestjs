import { Injectable, UnauthorizedException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { UsersService } from "../users/users.service";
import { JwtPayLoad } from "./interfaces/jwt-payload.interface";
import { LoggedInToken } from "../users/objects/login-user.dto";
import { User, IUser } from "../users/objects/user.schema";
import { JWT_EXPIRY } from "../common/constants/config";
import { plainToClass } from "class-transformer";
import { UserDto } from "../users/objects/create-user.dto";

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private usersService: UsersService,
    @InjectModel("User") private readonly userModel: Model<IUser>
  ) {}

  async getUserFromJWT(payload: JwtPayLoad) {
    let user: any = await this.usersService.findOneByEmail(payload.email);
    if (user) {
      return plainToClass(UserDto, user, { excludeExtraneousValues: true });
    } else {
      throw new UnauthorizedException();
    }
  }

  async createJwtPayLoad(user: User): Promise<LoggedInToken> {
    let jwt = this.jwtService.sign(
      {
        email: user.email,
      },
      { expiresIn: JWT_EXPIRY }
    );
    return {
      expiresIn: JWT_EXPIRY,
      token: jwt,
      type:user.type,
      verification:user.verification
    };
  }
}
