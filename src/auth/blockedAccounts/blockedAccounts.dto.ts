import { Creator, Reader } from "../../common/base/base.dto";
import { IsDefined } from "class-validator";
import { Expose } from "class-transformer";

export class CreateBlockedAccountDto extends Creator {
    constructor({
        type,
        email,
        otpCount,
        unblock
    }) {
      super(true);
      this.type = type;
      this.email = email;
      this.otpCount = otpCount;
      this.unblock = unblock;
    }

    @IsDefined()
    readonly type: string;

    @IsDefined()
    readonly email: string;

    @IsDefined()
    readonly otpCount: number;
    
    @IsDefined()
    readonly unblock: boolean;
}

export class BlockedAccountDto extends Reader {
  @Expose()
  readonly type: string;
  
  @Expose()
  readonly email: string;
  
  //@Expose()
  //readonly expiry: number;
  
  @Expose()
  readonly unblock: boolean;
}
