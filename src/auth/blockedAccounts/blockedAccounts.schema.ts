import { createModel, Entity, IEntity } from "../../common/base/base.model";
import { Schema } from "mongoose";
import { OTP_EMAIL_VERIFICATION_STATUS } from "../../common/constants/enum";

export class BlockedAccount extends Entity {
  //token: string; 
  email: string;
  otpCount: number;
    unblock: boolean;
    type: string;
}

export interface IBlockedAccount extends BlockedAccount, IEntity {
    id: string;
  }

  export const BlockedAccountSchema: Schema = createModel("BlockedAccountTable", {
   
   // token: { type: String },
    email: { type: String },
    otpCount: { type: Number},
    unblock: { type: Boolean, default: OTP_EMAIL_VERIFICATION_STATUS.FALSE},
    type: { type: String}
  });