import { BaseService } from "../../common/base/base.service";
import { Injectable, BadRequestException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { IBlockedAccount } from "./blockedAccounts.schema";
import { CreateBlockedAccountDto } from "./blockedAccounts.dto";
import { Status, BLOCKED_ACCOUNT_TYPE, USER_STATUS } from "../../common/constants/enum";
import { OtpIncorrectForgotPassword } from "../otpIncorrectForgotPassword/otpIncorrectForgotPassword.schema";
import { BLOCKED_ACCOUNT_ERROR } from "../../common/constants/string";
//import { CreateOtpDto } from "./otp.dto";



@Injectable()
export class BlockedAccountService extends BaseService<IBlockedAccount> {
  constructor(
    @InjectModel("BlockedAccount") private readonly blockedAccountModel: Model<IBlockedAccount>,
    @InjectModel("OtpIncorrectForgotPassword") 
    private readonly otpIncorrectForgotPasswordModel: Model<OtpIncorrectForgotPassword>,
  ) {
    super(blockedAccountModel);
  }

  async create(createDto: any) {
      let data = new CreateBlockedAccountDto(createDto);
      return super.create(data);
  }

  async remove(idOrCode) {
    let model = await this.findOne(idOrCode);
   // if (this.PreDelete) {
   //   await this.PreDelete.doPreDelete(idOrCode, model);
   // }
    model.status = Status.DELETED;
    await model.save();
   // if (this.PostDelete) {
   //   await this.PostDelete.doPostDelete(idOrCode, model);
  //  }
    return model;
    //return await this.baseModel.insertMany(createDto);
   // let k = await this.baseModel.insertMany(createDto);
    //let check = await this.doPostCreate(k);
    //return k;
  }

  async verifyForBlockedAcccountEmail(userToAttempt){
    let _blocked: any = await this.blockedAccountModel.find({ email: userToAttempt.email, type: BLOCKED_ACCOUNT_TYPE.FORGOT_PASSWORD, status: USER_STATUS.ACTIVE })
    console.log('blocked account.............', _blocked)
    if(_blocked.length !== 0){
      throw new BadRequestException(BLOCKED_ACCOUNT_ERROR(userToAttempt.email))
    }
  }
}