import { createModel, Entity, IEntity } from "../../common/base/base.model";
import { Schema } from "mongoose";
import { OTP_EMAIL_VERIFICATION_STATUS } from "../../common/constants/enum";

export class ForgotPasswordOtp extends Entity {
    token: string; 
    email: string;
    expiry: number;
    isVerified: boolean;
}

export interface IForgotPasswordOtp extends ForgotPasswordOtp, IEntity {
    id: string;
  }

  export const ForgotPasswordOtpSchema: Schema = createModel("ForgotPasswordOtpTable", {
    token: { type: String },
    email: { type: String },
    expiry: { type: Number},
    isVerified: { type: Boolean, default: OTP_EMAIL_VERIFICATION_STATUS.FALSE}
  });