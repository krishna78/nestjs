import { IForgotPasswordOtp, ForgotPasswordOtp } from "./forgot-password-otp.schema";
import { BaseService } from "../../common/base/base.service";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";

import { CreateForgotPasswordOtpDto } from "./forgot-password-otp.dto";


@Injectable()
export class ForgotPasswordOtpService extends BaseService<IForgotPasswordOtp> {
  constructor(
    @InjectModel("ForgotPasswordOtp") private readonly forgotPasswordotpModel: Model<ForgotPasswordOtp>,
   
  ) {
    super(forgotPasswordotpModel);
  }

  async create(createDto: any) {
      let data = new CreateForgotPasswordOtpDto(createDto);
      return super.create(data);
  }
}