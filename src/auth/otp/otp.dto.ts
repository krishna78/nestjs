import { Creator, Reader } from "../../common/base/base.dto";
import { IsDefined } from "class-validator";
import { Expose } from "class-transformer";

export class CreateOtpDto extends Creator {
    constructor({
        token,
        email,
        expiry,
        isVerified
    }) {
      super(true);
      this.token = token;
      this.email = email;
      this.expiry = expiry;
      this.isVerified = isVerified;
    }

    @IsDefined()
    readonly token: string;

    @IsDefined()
    readonly email: string;

    @IsDefined()
    readonly expiry: number;
    
    @IsDefined()
    readonly isVerified: boolean;
}

export class OtpDto extends Reader {
  @Expose()
  readonly token: string;
  
  @Expose()
  readonly email: string;
  
  @Expose()
  readonly expiry: number;
  
  @Expose()
  readonly isVerified: boolean;
}
