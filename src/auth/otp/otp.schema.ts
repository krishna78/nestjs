import { createModel, Entity, IEntity } from "../../common/base/base.model";
import { Schema } from "mongoose";
import { OTP_EMAIL_VERIFICATION_STATUS } from "../../common/constants/enum";

export class Otp extends Entity {
  token: string; 
  email: string;
    expiry: number;
    isVerified: boolean;
}

export interface IOtp extends Otp, IEntity {
    id: string;
  }

  export const OtpSchema: Schema = createModel("OtpTable", {
   
     token: { type: String },
    email: { type: String },
    expiry: { type: Number},
    isVerified: { type: Boolean, default: OTP_EMAIL_VERIFICATION_STATUS.FALSE}
  });