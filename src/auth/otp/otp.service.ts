import { IOtp, Otp } from "./otp.schema";
import { BaseService } from "../../common/base/base.service";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CreateOtpDto } from "./otp.dto";


@Injectable()
export class OtpService extends BaseService<IOtp> {
  constructor(
    @InjectModel("Otp") private readonly otpModel: Model<Otp>,
   
  ) {
    super(otpModel);
  }

  async create(createDto: any) {
      let data = new CreateOtpDto(createDto);
      return super.create(data);
  }
}