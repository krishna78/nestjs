//import { IOtp, Otp } from "./otp.schema";
import { BaseService } from "../../common/base/base.service";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
//import { CreateOtpDto } from "./otp.dto";
import { IOtpIncorrect, OtpIncorrect } from "./otpIncorrect.schema";
import { CreateOtpIncorrectDto } from "./otpIncoreect.dto";


@Injectable()
export class OtpIncorrectService extends BaseService<IOtpIncorrect> {
  constructor(
    @InjectModel("OtpIncorrect") private readonly otpIncorrectModel: Model<OtpIncorrect>,
   
  ) {
    super(otpIncorrectModel);
  }

  async create(createDto: any) {
      let data = new CreateOtpIncorrectDto(createDto);
      return super.create(data);
  }
}