//import { IOtp, Otp } from "./otp.schema";
import { BaseService } from "../../common/base/base.service";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { OtpIncorrectForgotPassword, IOtpIncorrectForgotPassword } from "./otpIncorrectForgotPassword.schema";
import { CreateOtpIncorrectForgotPasswordDto } from "./otpIncorrectForgotPassword.dto";
//import { CreateOtpDto } from "./otp.dto";
//import { IOtpIncorrect, OtpIncorrect } from "./otpIncorrect.schema";
//import { CreateOtpIncorrectDto } from "./otpIncoreect.dto";


@Injectable()
export class OtpIncorrectForgotPasswordService extends BaseService<IOtpIncorrectForgotPassword> {
  constructor(
    @InjectModel("OtpIncorrectForgotPassword") private readonly otpIncorrectForgotPasswordModel: Model<OtpIncorrectForgotPassword>,
   
  ) {
    super(otpIncorrectForgotPasswordModel);
  }

  async create(createDto: any) {
      let data = new CreateOtpIncorrectForgotPasswordDto(createDto);
      return super.create(data);
  }
}