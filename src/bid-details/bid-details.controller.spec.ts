import { Test, TestingModule } from '@nestjs/testing';
import { BidDetailsController } from './bid-details.controller';

describe('BidDetails Controller', () => {
  let controller: BidDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BidDetailsController],
    }).compile();

    controller = module.get<BidDetailsController>(BidDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
