import {
  Controller,
  UseGuards,
  Get,
  Request,
  Query,
  NotFoundException,
  Post,
  Req,
  Body,
  BadRequestException,
  Put,
} from "@nestjs/common";
import {
  CreateBidDetailsDto,
  BidDetailsDto,
  UpdateBidDetailsDto,
  CreateTransactionDetailsDto,
  UpdateTransactionDetailsDto,
} from "./objects/bid-details.dto";
import {
  BASEROUTES,
  USER_TYPES,
  ACTIVITY_FEED_EVENTS_SPONSOR,
  USER_STATUS,
  PROJECT_STATUS,
  ACTIVITY_FEED_EVENTS_SME,
  TRANSACTION_TYPE,
  BID_STATUS,
  KYC_VERIFICATION_STATUS,
  ROLES_ACCESS_ACTION,
} from "../common/constants/enum";
import { abstractBaseControllerFactory } from "../common/base/base.controller";
import { BidDetailsService } from "./bid-details.service";
import { JwtAuthGuard } from "../auth/auth.guard";
import {
  RequestUser,
  IdOrCodeParser,
  LastUpdatedTime,
} from "../common/utils/controller.decorator";
import { success } from "../common/base/httpResponse.interface";
import { Model } from "mongoose";
import { IBidDetails, ITransactionDetails } from "./objects/bid-details.schema";
import { InjectModel } from "@nestjs/mongoose";
import { Paginate, normalizePaginateResult } from "../common/interfaces/pagination";
import {
  NOT_FOUND,
  ONLY_FOR_ADMIN,
  ONLY_FOR_SPONSOR,
  ONLY_FOR_APPROVED_PROJECTS,
  NO_FUNDED_BIDS,
  NO_BIDS_APPROVED,
  NO_BIDS_FOR_PROJECT_YET,
  BID_STATUS_ERROR,
  PROJECT_FINALIZE_BID_ONLY_FOR_APPROVED_STATUS,
  CANNOT_DELETE_FUNDED_PROJECTS,
  ONLY_FOR_SME,
  BID_CREATION_ERROR_SPONSOR_KYC_NOT_APPROVED,
  FINANCING_AMOUNT_ERROR,
  TRANSACTION_FOR_PROJECT_EXISTS,
  ONLY_FOR_FUNDED_PROJECTS_BIDS_APPROVED,
  BID_EDIT_ERROR_AFTER_APPROVAL,
} from "../common/constants/string";
import { UsersService } from "../users/users.service";
import { ActivityFeedService } from "../activity-feed/activity-feed.service";
import { AbstractClassTransformerPipe } from "../common/pipes/class-transformer.pipe";
import { SmeProjectService } from "../sme-project/sme-project.service";
import { ISO_8601 } from "moment";
import { SmeActivityFeedService } from "../sme-activity-feed/sme-activity-feed.service";
import { WalletTransactionsService } from "../wallet-transactions/wallet-transactions.service";
import { CreateWalletTransactionDto } from "../wallet-transactions/objects/wallet-transactions.dto";
import { RolesService } from "../roles/roles.service";
import { ProjectAllocationSponsorEmail, ProjectAmountFundedForProjectEmail } from "../users/objects/user.registered.email";
import { EmailDto } from "../email/objects/email.dto";
import { EmailService } from "../email/email.service";

const BaseController = abstractBaseControllerFactory<any>({
  DTO: BidDetailsDto,
  CreateDTO: CreateBidDetailsDto,
  UpdateDTO: UpdateBidDetailsDto,
  DisabledRoutes: [BASEROUTES.DETELEONE, BASEROUTES.PATCH],
});

export function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

@UseGuards(JwtAuthGuard)
@Controller("bid-details")
export class BidDetailsController extends BaseController {
  constructor(
    private bidDetailsService: BidDetailsService,
    private walletTransactionsService: WalletTransactionsService,
    private usersService: UsersService,
    private activityFeedService: ActivityFeedService,
    private smeactivityFeedService: SmeActivityFeedService,
    private smeProjectService: SmeProjectService,
    @InjectModel("BidDetails")
    private readonly bidDetailsModel: Model<IBidDetails>,
    private rolesservice: RolesService,
    @InjectModel("Transactions")
    private readonly transactionsModel: Model<ITransactionDetails>,
    private readonly emailService: EmailService,
  ) {
    super(bidDetailsService);
  }

  @Get()
  async findList(@Request() req, @Query() query, @RequestUser() user) {
    let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
    
    let hasAccessAdmin = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_FINDLIST
    );
    let hasAccessSme = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_FINDLIST_SME
    );
    let hasAccessSponsor = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_FINDLIST_SPONSOR
    );
   console.log('userstype', user.type, _userProfile, 'hasAccessAdmin, hasAccessSme, hasAccessSponsor', hasAccessAdmin, hasAccessSme, hasAccessSponsor);
   // if (user.type == USER_TYPES.ADMIN) {
    if(hasAccessAdmin){
      console.log("Adminnn queryyyy alllll bidsssss", query);
      // <--- only Admin can see all the bids list.
      const d = await super.findList(req, { ...query });
      return d;
    }// else if (user.type == USER_TYPES.SPONSOR) {
    else if(hasAccessSponsor){
      let t = { $or: [{ status: 1 }, { status: 2 }] };
        let userObject = { createdBy: user.code };
        let k = await super.findList(req, { ...query, ...userObject, ...t });
        return k;
      /*
      console.log(k)
      if(k !== undefined && k.data.total > 0 ){
        let projectId: string;
        var bidIdsArray: string[] = [];
    
      await k.data.docs.forEach(async (element) => {
       // let _query = { createdBy: user.code,status: 2 };
        let _project = await this.smeProjectService.findOne(element.projectId);
       // console.log('projectIdddddddd', _project);
        if(_project.status == 2){
          bidIdsArray.push(element.projectId);
        }
        console.log("bidDetailsssssArrayyyyyyyyy",bidIdsArray);
      });
     // let q = { projectId: bidIdsArray };
     // let _query = { createdBy: user.code,status: 1,q };
     // let _query =  { code: bidIdsArray} ;
      let q = { projectId: bidIdsArray };
      console.log("finalllll array", bidIdsArray);
      const d = await super.findList(req, { ...q, status: 1 });
      return d;
      */
      // const _bids = await super.findList(req, { ...q, status: 1 });
      /*
      let _bids = await this.bidDetailsService.findAll(_query, {
      limit: 30,
        page: 1,
        sort: "_id",
      });
      */
      // console.log('BBBIIIDDDSSS',_bids);
      /*
          let k: any = await this.bidDetailsModel
          .find({ createdBy: user.code })
          .sort({ updatedTime: -1 });
          */
      //      .limit(7);
      // let k1 = k.sort({ updatedTime: -1 })
      // return _bids;
      // }
      //  throw new BadRequestException(NO_FUNDED_BIDS)
    } //else if (user.type == USER_TYPES.SME) {
     else if(hasAccessSme){ 
     // let query = { createdBy: user.code, status: 2}
      let query = { createdBy: user.code, status: 2 };
      let projectId: string;
      var projectIdsArray: string[] = [];
      //Implement with smeProjectModel
      let _projects = await this.smeProjectService.findAll(query, {
        limit: 30,
        page: 1,
        sort: "_id",
      });
      console.log(_projects);
      await _projects.docs.forEach(async (element) => {
        projectIdsArray.push(element.code);
        console.log("projectssssssssssssArray", projectIdsArray);
      });

      let q = { projectId: projectIdsArray };
      console.log("finalllll array", projectIdsArray);
      const d = await super.findList(req, { ...q, status: 1 });
      return d;
      /*
        for (let i = 0; i < _projects.total; i++) {
          console.log('projectssssssssssssArray',_projects[i].code)
          projectIdsArray.push(_projects[i].code)
        }
        */
    }
    throw new BadRequestException(NOT_FOUND);
  }
  
  @Get("transactions")
  async findListTransactions(@Request() req, @Query() query, @RequestUser() user) {
    var options = {
      limit: 10,
      page: 1,
      sort: "_id",
      skip: query.page ? (query.page - 1) : 0
    };
 
    let k = await this.transactionsModel.find( query,
      {},  { sort: { _id: -1 },// skip: 0, limit: 30, 
      skip: options.skip * options.limit, limit: options.limit,projection: {}}
     // { sort: { _id: 1 }, skip: 0, limit: 30, projection: {} });
    )
    let dCount = await this.transactionsModel.count(
      query,
    );
    let pagination = normalizePaginateResult({
      total: dCount,//d.length,
      limit: options.limit,
      page: query.page,//options.page,
      pages: k.pages,
    });
    return { k, ...pagination };
  //return k;
  }

  @Post()
  public async create(
    @Request() req,
    @Body(AbstractClassTransformerPipe(CreateBidDetailsDto)) body: any,
    @Query() query,
    @RequestUser() user
  ) {
    //if (user.type == USER_TYPES.SPONSOR) {
      let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
    
      let hasAccess = _userProfile.rolesAccessAction.some(
        (e) => e === ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_CREATE
      );
      console.log('userstype', user.type, _userProfile, hasAccess)
    if (hasAccess) {
        if (
          user.verification.isProfileCompleted ==
            KYC_VERIFICATION_STATUS.PENDING ||
          user.verification.isProfileCompleted == KYC_VERIFICATION_STATUS.REJECTED
        ) {
          throw new BadRequestException(
            BID_CREATION_ERROR_SPONSOR_KYC_NOT_APPROVED
          );
        }
        let project = await this.smeProjectService.findOne(body.projectId);
        //Only if the project is approved, a bid can be created.
        if(body.financingAmount > project.fundsRequired){
          throw new BadRequestException(
            FINANCING_AMOUNT_ERROR
          );
        }
        if (
          project.status == PROJECT_STATUS.APPROVED &&
          project.hasMilestones == 1
        ) {
          let _body = {
            ...body,
            projectDuration: project.tenure
          //  tokensForSubmission: project.tokensRequired,
          //  financingAmount: project.tokensRequired * 100,
          /*
          perEmiAmount: +(
              (project.tokensRequired * 100) /
              body.noOfEmis
            ).toFixed(2),
            */
          };
          debugger;
          let data = await this.bidDetailsService.create(_body, user);
          return success(data);
        } else {
          throw new BadRequestException(ONLY_FOR_APPROVED_PROJECTS);
        }
     
    }
    throw new BadRequestException(ONLY_FOR_SPONSOR);
  }

  @Post("transaction")
  public async createTransaction(
    @Request() req,
    @Body(AbstractClassTransformerPipe(CreateTransactionDetailsDto)) body: any,
    @Query() query,
    @RequestUser() user
  ) {
    /*
      let projectId = body.projectId;
      let _query = { projectId: projectId, status: BID_STATUS.APPROVED };
      //let options: Paginate = { limit: 30, page: 1, sort: "_id" };
      let _project = await this.smeProjectService.findOne(projectId);
      //let _u = await this.usersService.findOne(_project.createdBy);
      //let _bid = await this.bidDetailsService.findOneByQuery(_query);
      let _bid = await this.bidDetailsModel.find(_query);
      console.log('_project, //_u, _bid', _project, //_u,
      _bid);
      
          
      let _approvedBids = await _bid.some((e) => e.status === 1);
      console.log(
        "approvedBidssssss",
        _approvedBids,
        _approvedBids.length,
        _bid.length
      );
      if (
        _project.status == PROJECT_STATUS.FUNDED &&
        _project.hasMilestones == 1 && (_approvedBids == true)
      ){
        let isTransactionExists = await this.transactionsModel.findOne({ projectId: projectId})
        
          //if (user.type == USER_TYPES.SPONSOR) {
            let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
          
            let hasAccess = _userProfile.rolesAccessAction.some(
              (e) => e === ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_CREATE_TRANSACTION
            );
            console.log('userstype', user.type, _userProfile, 'hasAccess',  hasAccess); 
        if (hasAccess) {
            if(!isTransactionExists){
                let _body = {
                  ...body,
                  sponsorId: _bid[0].createdBy,
                  smeId: _project.createdBy,
                  projectValue: _project.value,
                  fundsRequested: _project.fundsRequired,
                  projectValidationDate: _project.projectApprovedTime,
                  projectFinancingDate: _bid[0].bidApprovedTime,
                  fundsOffered: _bid[0].financingAmount
                  //projectDuration: project.tenure
                }
                let k = await this.transactionsModel.create(_body);
                return k;
              }
            throw new BadRequestException(TRANSACTION_FOR_PROJECT_EXISTS)
          }
        throw new BadRequestException(ONLY_FOR_ADMIN);
      }
      throw new BadRequestException(ONLY_FOR_FUNDED_PROJECTS_BIDS_APPROVED);
    */
  }


  @Post("finalizeBid")
  async completeBidProcess(
    @Req() req,
    @Body() body: { projectId: string },
    @RequestUser() user
  ) {
    debugger;
    let projectId = body.projectId;
    let query = { projectId: projectId };
    //let options: Paginate = { limit: 30, page: 1, sort: "_id" };
    let _project = await this.smeProjectService.findOne(projectId);
    let _u = await this.usersService.findOne(_project.createdBy);
    let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
    
    let hasAccess = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_COMPLETE_BID_PROCESS
    );
    console.log('userstype', user.type, _userProfile, hasAccess)
    //if (user.type == USER_TYPES.ADMIN) {
    if(hasAccess) {
        if (
           //_project.status == PROJECT_STATUS.FUNDED //&&
          _project.status == PROJECT_STATUS.APPROVED
        ) {
          //Implement with bidDetailsModel
          let bid = await this.bidDetailsModel.find(query);
          //let bid = await this.findList(req, query, user);
          console.log("ffffffffinnnnnnnn", bid, "alllBidddssss", bid.length);
          //Check for atleast one approved bid.
          let _approvedBids = await bid.some((e) => e.status === 1);
          console.log(
            "approvedBidssssss",
            _approvedBids,
            _approvedBids.length,
            bid.length
          );
          if (bid.length > 0) {
            if (_approvedBids == true) {
              //  await Promise.all(bid.data.docs.forEach(async element => {
              await this.bidDetailsService.editSmeProjectStatus(projectId, user);
              //  bid.forEach(async (element) => {
              await Promise.all(
                bid.map(async (element) => {
                  //switch element.status
                  if (element.status == 0 || element.status == 2) {
                    await this.bidDetailsService.rejectBidsAndReturnWalletMoney(
                      element,
                      _project
                    );
                  } else if (element.status == 1) {
                    let _user = await this.usersService.findOne(
                      element.createdBy
                    );
                    let updateDto = {
                      bidApprovedTime: Date.now(),
                      bidApprovedBy: user.code,
                      bidApprovedUserEmail: user.email
                    }
                    
                    await this.bidDetailsService.edit(element.code, updateDto, Date.now());
                    
                    console.log('Successfully added approvedBy user email credentialsss', updateDto)
                    
                    await this.bidDetailsService.activityFeedProjectFunded(
                      _project,
                      _u,
                      _user,
                      element
                    );
                    
                    await this.emailService.sendEmail(
                      new ProjectAllocationSponsorEmail(
                        new EmailDto({
                          to: _user.email,
                          metaData: { _project, _user, element, _u },
                        })
                      )
                    );

                    await this.emailService.sendEmail(
                      new ProjectAmountFundedForProjectEmail(
                        new EmailDto({
                          to: _u.email,
                          metaData: { _project, _user, element, _u },
                        })
                      )
                    );

                    await this.bidDetailsService.createTransactionTable(projectId, user);
                    await this.bidDetailsService.createBigChainTransaction(_u, _project, _user,
                      element);
                  }
                })
              );
          
            } else {
              throw new BadRequestException(NO_BIDS_APPROVED);
            }
          } else {
            throw new BadRequestException(NO_BIDS_FOR_PROJECT_YET);
          }
          //Implement with bidDetailsModel
          //let _bid = await this.findList(req, query, user);
          return await this.bidDetailsModel.find(query);
        } else if (
          _project.status == PROJECT_STATUS.FUNDED ||
          _project.status == PROJECT_STATUS.PENDING ||
          _project.status == PROJECT_STATUS.DOCUMENTSREQUIRED ||
          _project.status == PROJECT_STATUS.COMPLETED ||
          _project.status == PROJECT_STATUS.REJECTED
        ) {
          throw new BadRequestException(
            PROJECT_FINALIZE_BID_ONLY_FOR_APPROVED_STATUS
          );
        }
    }
    throw new BadRequestException(ONLY_FOR_ADMIN);
  }

  @Post("DeleteSmeProject")
  async rejectAllBidsDeleteProject(
    @Req() req,
    @Body() body: { projectId: string },
    @RequestUser() user
  ) {
    let projectId = body.projectId;
    let query = { projectId: projectId };
    //let options: Paginate = { limit: 30, page: 1, sort: "_id" };
  //  if (user.type == USER_TYPES.ADMIN) {
    let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
    
    let hasAccess = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_REJECT_ALL_BIDS_DELETE_PROJECT
    );
    console.log('userstype', user.type, _userProfile, hasAccess)
    if(hasAccess) {
      let _project = await this.smeProjectService.findOne(projectId);
      if (_project !== undefined) {
        let _u = await this.usersService.findOne(_project.createdBy);
        if (_project.status == PROJECT_STATUS.FUNDED) {
          throw new BadRequestException(CANNOT_DELETE_FUNDED_PROJECTS);
        } else if (_project.status == PROJECT_STATUS.APPROVED) {
          //Implement with bidDetailsModel
          let _bid = await this.bidDetailsModel.find(query);
          //let _bid = await this.findList(req, query, user);
          console.log("biddssssTotallllllArrrrraaayyyyy", _bid);
          if (_bid.length > 0) {
            // https://stackoverflow.com/questions/37576685/using-async-await-with-a-foreach-loop
            await Promise.all(
              _bid.map(async (element) => {
                if (
                  element.status == 1 ||
                  element.status == 0 ||
                  element.status == 2
                ) {
                  await this.bidDetailsService.rejectBidsAndReturnWalletMoney(
                    element,
                    _project
                  );
                } else {
                  throw new BadRequestException(BID_STATUS_ERROR);
                }
              })
            );
          }
          await this.bidDetailsService.smeActivityFeedDeleteSmeProject(
            _project,
            _u
          );
        }
        return await this.smeProjectService.remove(projectId);
      }
      throw new NotFoundException();
    }
    throw new BadRequestException(ONLY_FOR_ADMIN);
  }

  @Put("transaction/:idOrCode")
  public async updateTransaction(
    @IdOrCodeParser("idOrCode") idOrCode: string,
    @LastUpdatedTime() lastUpdatedTime: number,
    @Body(AbstractClassTransformerPipe(UpdateTransactionDetailsDto)) updateDto,
    @RequestUser() user
  ) {
   // Contact.update({_id: contact.id}, upsertData, {upsert: true}, function(err{...});
  
   //let data = new UpdateTransactionDetailsDto(updateDto);
   let k = await this.transactionsModel.update({code: idOrCode },updateDto,{upsert: true})
  // k = await this.bidDetailsService.edit(idOrCode, updateDto, Date.now());
    return await this.transactionsModel.find({code: idOrCode });
  }

  @Put(":idOrCode")
  public async update(
    @IdOrCodeParser("idOrCode") idOrCode: string,
    @LastUpdatedTime() lastUpdatedTime: number,
    @Body(AbstractClassTransformerPipe(UpdateBidDetailsDto)) updateDto,
    @RequestUser() user
  ) {
    console.log(
      "bid update method, user.type ,updateDto.status","_________________________________________________________________",
      user.type,
      updateDto.status
    );
    let k: any = {};
    let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
    
    let hasAccess = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_UPDATE
    );

    let hasAccessSponsor = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_UPDATE_SPONSOR
    );
    console.log('userstype', user.type, _userProfile, hasAccess)
    if(hasAccess) {
     // if (user.type == USER_TYPES.ADMIN) {
      let tcheck =
        updateDto.status == BID_STATUS.APPROVED ||
        updateDto.status == BID_STATUS.PENDING ||
        updateDto.status == BID_STATUS.REJECTED;
      if (updateDto.status && !tcheck) {
        throw new BadRequestException(BID_STATUS_ERROR);
      }
      let bid = await this.bidDetailsService.findOne(idOrCode);
      let _project = await this.smeProjectService.findOne(bid.projectId);
      console.log("project statusssss", _project.status);
      if (
        _project.status == PROJECT_STATUS.PENDING ||
        _project.status == PROJECT_STATUS.DOCUMENTSREQUIRED ||
        _project.status == PROJECT_STATUS.FUNDED ||
        _project.status == PROJECT_STATUS.COMPLETED ||
        _project.status == PROJECT_STATUS.REJECTED
      ) {
        throw new BadRequestException(ONLY_FOR_APPROVED_PROJECTS);
      }
      switch (updateDto.status) {
        case BID_STATUS.PENDING:
          k = await this.bidDetailsService.changeBidStatusToPending(idOrCode);
          break;
        case BID_STATUS.APPROVED:
          k = await this.bidDetailsService.changeBidStatusToApproved(idOrCode);
          break;
        case BID_STATUS.REJECTED:
          k = await this.bidDetailsService.changeBidStatusToRejected(idOrCode);
          break;
        default:
          throw new BadRequestException(BID_STATUS_ERROR);
      }
    }
    if(hasAccessSponsor){
      //if ((user.type == USER_TYPES.ADMIN)) {
        delete updateDto["status"];
        let bid = await this.bidDetailsService.findOne(idOrCode);
      let _project = await this.smeProjectService.findOne(bid.projectId);
      console.log("project statusssss", _project.status);
      if (
        _project.status == PROJECT_STATUS.PENDING ||
        _project.status == PROJECT_STATUS.DOCUMENTSREQUIRED ||
        _project.status == PROJECT_STATUS.FUNDED ||
        _project.status == PROJECT_STATUS.COMPLETED ||
        _project.status == PROJECT_STATUS.REJECTED
      ) {
        throw new BadRequestException(ONLY_FOR_APPROVED_PROJECTS);
      }
      switch (bid.status) {
        case BID_STATUS.PENDING:
          //k = await this.bidDetailsService.changeBidStatusToPending(idOrCode);
          if(updateDto.financingAmount > _project.fundsRequired){
            throw new BadRequestException(
              FINANCING_AMOUNT_ERROR
            );
          }
          break;
        case BID_STATUS.APPROVED:
          throw new BadRequestException(BID_EDIT_ERROR_AFTER_APPROVAL);
          //k = await this.bidDetailsService.changeBidStatusToApproved(idOrCode);
          break;
        case BID_STATUS.REJECTED:
          throw new BadRequestException(BID_EDIT_ERROR_AFTER_APPROVAL);
          //k = await this.bidDetailsService.changeBidStatusToRejected(idOrCode);
          //break;
        default:
          throw new BadRequestException(BID_STATUS_ERROR);
      }
        
      //}
    }
    k = await this.bidDetailsService.edit(idOrCode, updateDto, Date.now());
    return k;
  }

  @Get("sponsordashboard")
  async calculatebiddetails(
    @Req() req,
    // @Body() body: { projectId: string },
    @RequestUser() user
  ) {
    debugger;
    let dashboard = await this.bidDetailsService.calculateBidDetails(user);
    return success(dashboard);
  }

  @Get("funding-details")
  async getfundedProjects(@RequestUser() user) {
    let fundeddata = await this.bidDetailsService.getFundedProjects(user);
    return success(fundeddata);
  }
  /*
  @Get("funded-projects")
  async getfundedDetails(@RequestUser() user) {
    if(user.type == USER_TYPES.SME){
      let query = { createdBy: user.code, status: 2 };
      debugger;
      let data = await this.smeProjectService.findAll(query, {
        limit: 30,
        page: 1,
        sort: "_id",
      });
      console.log('funded-projects', data);
  
      let funds;
  
      for (let i = 0; i < data.total; i++) {
        let projectdata = data.docs[i];
        let projectId = projectdata.code;
        let bidData = await this.bidDetailsService.findOneByQuery({
          projectId: projectId,
        });
        console.log(bidData);
        if (bidData.status == 1) {
          funds = {
            formattedId: bidData.formattedId,
            financingAmount: bidData.financingAmount,
            fundsRequired: bidData.fundsRequired,
            status: bidData.status,
          };
        }
      }
      if (funds == undefined) {
        message: "No Projects are funded";
      }
  
      return success(funds);
    }
    throw new BadRequestException(ONLY_FOR_SME);
  }
  */
 
}
