import { Module, forwardRef, Global } from "@nestjs/common";
import { BidDetailsController } from "./bid-details.controller";
import { BidDetailsService } from "./bid-details.service";
import { BidDetailsSchema, TransactionDetailsSchema } from "./objects/bid-details.schema";
import { MongooseModule } from "@nestjs/mongoose";
import { WalletTransactionsService } from "../wallet-transactions/wallet-transactions.service";
import { WalletTransactionsModule } from "../wallet-transactions/wallet-transactions.module";
import { WalletTransactionsSchema } from "../wallet-transactions/objects/wallet-transactions.schema";
import { UsersModule } from "../users/users.module";
import { UserSchema } from "../users/objects/user.schema";
import { UsersService } from "../users/users.service";
import { EmailModule } from "../email/email.module";
import { SmeProjectSchema } from "../sme-project/objects/sme-project.schema";
import { SmeProjectModule } from "../sme-project/sme-project.module";
import { SmeProjectService } from "../sme-project/sme-project.service";
import { ActivityFeedModule } from "../activity-feed/activity-feed.module";
import { SmeActivityFeedModule } from "../sme-activity-feed/sme-activity-feed.module";
import { ProjectEmailNotificationService } from "../sme-project/project-emailnotification/project-emailnotification.service";
import { ProjectEmailNotificationSchema } from "../sme-project/project-emailnotification/project-emailnotification.schema";
import { BigchainDbModule } from "../bigchain-db/bigchain-db.module";
import { RolesSchema } from "../roles/objects/roles.schema";
import { RolesModule } from "../roles/roles.module";
import { RolesService } from "../roles/roles.service";

//@Global()
@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "BidDetails", schema: BidDetailsSchema },
      { name: "WalletTransaction", schema: WalletTransactionsSchema },
      { name: "User", schema: UserSchema },
      { name: "SmeProject", schema: SmeProjectSchema },
      {
        name: "ProjectEmailNotification",
        schema: ProjectEmailNotificationSchema,
      },
      { name: "AdminRoles", schema: RolesSchema },
      { name: "Transactions", schema: TransactionDetailsSchema}
    ]),
    WalletTransactionsModule,
    forwardRef(() => UsersModule),
    forwardRef(() => SmeProjectModule),
    EmailModule,
    //SmeProjectModule,
    ActivityFeedModule,
    SmeActivityFeedModule,
    BigchainDbModule,
    RolesModule,
  ],
  exports: [BidDetailsService, WalletTransactionsService],
  controllers: [BidDetailsController],
  providers: [BidDetailsService, WalletTransactionsService,
    RolesService
    //SmeProjectService, ProjectEmailNotificationService
  ],
})
export class BidDetailsModule {}
