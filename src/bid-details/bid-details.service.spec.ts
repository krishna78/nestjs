import { Test, TestingModule } from '@nestjs/testing';
import { BidDetailsService } from './bid-details.service';

describe('BidDetailsService', () => {
  let service: BidDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BidDetailsService],
    }).compile();

    service = module.get<BidDetailsService>(BidDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
