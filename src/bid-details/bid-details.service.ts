import {
  Injectable,
  BadRequestException,
  Inject,
  forwardRef,
} from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { BaseService } from "../common/base/base.service";
import { IBidDetails, ITransactionDetails } from "./objects/bid-details.schema";
import { CreateWalletTransactionDto } from "../wallet-transactions/objects/wallet-transactions.dto";
import {
  TRANSACTION_TYPE,
  USER_TYPES,
  ACTIVITY_FEED_EVENTS_SPONSOR,
  PROJECT_STATUS,
  ACTIVITY_FEED_EVENTS_SME,
  BID_STATUS,
  BIGCHAINDB_TRANSACTION_TYPE,
  ROLES_ACCESS_ACTION,
} from "../common/constants/enum";
import { WalletTransactionsService } from "../wallet-transactions/wallet-transactions.service";
import { UsersService } from "../users/users.service";
import {
  EXISTS,
  TOKENS_FOR_SUBMISSION_ERROR,
  ONLY_FOR_SPONSOR,
  NOT_FOUND,
  ONLY_FOR_APPROVED_PROJECTS,
  WALLET_BALANCE_FINANCING_AMOUNT_ERROR,
  TRANSACTION_FOR_PROJECT_EXISTS,
  ONLY_FOR_ADMIN,
  ONLY_FOR_FUNDED_PROJECTS_BIDS_APPROVED,
  SHOWN_INTEREST_TO_PROJECT_EARLIER,
} from "../common/constants/string";
import { IUser } from "../users/objects/user.schema";
import { SmeProjectService } from "../sme-project/sme-project.service";
import { HasPreCreate, HasPostCreate } from "../common/base/pre.post.action";
import { CanLoadVirtual } from "../common/interfaces/can.load.virtual";
import { TokensSubmittedSponsorEmail } from "../users/objects/user.registered.email";
import { EmailDto } from "../email/objects/email.dto";
import { EmailService } from "../email/email.service";
import { ActivityFeedService } from "../activity-feed/activity-feed.service";
import {
  UpdateBidDetailsDto,
  CreateBidDetailsDto,
  CreateTransactionDetailsDto,
} from "./objects/bid-details.dto";
import { create } from "domain";

import { SmeActivityFeedService } from "../sme-activity-feed/sme-activity-feed.service";
import { BigchainDbService } from "../bigchain-db/bigchain-db.service";
import { BIGCHAINDB_API_PATH } from "../common/constants/config";
import { EntityDetailsService } from "../entity-details/entity-details.service";
import { RolesService } from "../roles/roles.service";
const driver = require("bigchaindb-driver");

@Injectable()
export class BidDetailsService extends BaseService<IBidDetails>
  implements HasPreCreate, HasPostCreate, CanLoadVirtual {
  constructor(
    @InjectModel("BidDetails")
    private readonly bidDetailsModel: Model<IBidDetails>,
    private walletTransactionsService: WalletTransactionsService,
    private usersService: UsersService,
    private emailService: EmailService,
    // @Inject(forwardRef(() => SmeProjectService))
    private smeProjectService: SmeProjectService,
    private activityFeedService: ActivityFeedService,
    private smeactivityFeedService: SmeActivityFeedService,
    private readonly bigchaindbService: BigchainDbService,
    private entityDetailsService: EntityDetailsService,
    private rolesservice: RolesService,
    @InjectModel("Transactions")
    private readonly transactionsModel: Model<ITransactionDetails>,
  ) {
    super(bidDetailsModel);
  }
  formattedIdLength: number = 4;

  getQuery(createBidDetailsDto: CreateBidDetailsDto): Object {
    return null;
  }

  getPrefixForId(createBidDetailsDto: CreateBidDetailsDto): string {
    return "FU";
  }

  getVirtualForItem(): string[] {
    return ["formattedProjectIdForBid", "userDetails"
    //, "tuserDetails", "tformattedProjectIdForBid"
  ];
  }

  getVirtualForList(): string[] {
    return ["formattedProjectIdForBid", "userDetails"
    //,"tuserDetails", "tformattedProjectIdForBid" 
  ];

  }

  async doPreCreate(createDto) {
    let project = await this.smeProjectService.findOne(createDto.projectId);

    //    if(!(createDto.tokensForSubmission == project.tokensRequired)){
    //      throw new Error(TOKENS_FOR_SUBMISSION_ERROR);
    //    }
    let _user: IUser = await this.usersService.findOne(createDto.createdBy);
  /*
    if(_user.totalBalance < createDto.financingAmount){
      throw new BadRequestException(WALLET_BALANCE_FINANCING_AMOUNT_ERROR);
    }
    */
    if (_user.type == USER_TYPES.SPONSOR) {
      let query = {
        createdBy: createDto.createdBy,
        projectId: createDto.projectId,
      };
      let bid = await this.findOneByQuery(query);

      //Check if the user is investor and for particular project has already submitted the bid
      if (bid) {
        throw new Error(SHOWN_INTEREST_TO_PROJECT_EARLIER);
      }
    } else {
      throw new BadRequestException(ONLY_FOR_SPONSOR);
    }
  }

  async doPostCreate(bidModel: any, user): Promise<void> {
    let createWallet: any = {
      projectId: bidModel.projectId,
      bidId: bidModel.code,
      bidAmount: bidModel.financingAmount,
      //bidAmount: bidModel.tokensForSubmission,
      sponserId: bidModel.createdBy,
      transactionType: TRANSACTION_TYPE.DEBIT,
    };
    //Create a wallet transaction for every Post() request of the bid.
    let _createWallet = new CreateWalletTransactionDto(createWallet);
    let transaction = await this.walletTransactionsService.create(
      _createWallet
    );
    //projectId,
    //Equivavlent bid points,
    //tokens required,
    //tokens submitted,
    //remaining tokens
    let project = await this.smeProjectService.findOne(bidModel.projectId);
    let _user: IUser = await this.usersService.findOne(bidModel.createdBy);
    let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
    
    let hasAccess = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_CREATE
    );
    if (hasAccess) {
     //if (_user.type == USER_TYPES.SPONSOR) {
     /*
      console.log(
        project.equivalentBidPoints,
        project.tokensRequired,
        bidModel.tokensForSubmission,
        project.name,
        project.formattedId,
        _user.totalBalance
      );
      */
      await this.activityFeedProjectsShownInterest(bidModel, project, _user);
      await this.smeActivityFeedProjectsShownInterest(bidModel, project);
      if(_user.emailAlerts == true){
        await this.emailService.sendEmail(
          new TokensSubmittedSponsorEmail(
            new EmailDto({
              to: _user.email,
              metaData: { _user, project, bidModel },
            })
          )
        );
      }
    }
  }

  async doPostUpdate(
    idOrCode,
    editDto: UpdateBidDetailsDto,
    model
  ): Promise<void> {}

  async editSmeProjectStatus(projectId, user) {
    let project = await this.smeProjectService.findOne(projectId);
    let updateProject = await this.smeProjectService.edit(
      projectId,
      { status: PROJECT_STATUS.FUNDED },
      Date.now(),
      user
    );
    console.log("sme edit project to FUNDED.................");
    //await this.createTransactionTable(project.code, user)
  }

  async activityFeedProjectsShownInterest(bidModel, project, _user) {
    let createDto4: any = {
      _priority: 2,
      type: ACTIVITY_FEED_EVENTS_SPONSOR.PROJECTS_SHOWN_INTEREST,
      itemId: bidModel.code,
      setFields: {
        message: "Project shown interest",
        projectName: project.name,
      },
      optional: project.formattedId,
    };
    //PROJECTS_SHOWN_INTEREST
    let _activityFeed4: any = await this.activityFeedService.projectsShownInterest(
      createDto4,
      _user
    );
  }

  async smeActivityFeedProjectsShownInterest(bidModel, project) {
    let createDto4: any = {
      _priority: 2,
      type: ACTIVITY_FEED_EVENTS_SPONSOR.PROJECTS_SHOWN_INTEREST,
      itemId: bidModel.code,
      setFields: {
        message: "Project shown interest",
        projectName: project.name,
      },
      optional: project.formattedId,
    };
    //PROJECTS_SHOWN_INTEREST
    let _u = await this.usersService.findOne(project.createdBy);
    let _activityFeed2: any = await this.smeactivityFeedService.projectsShownInterest(
      createDto4,
      _u
    );
  }

  async activityFeedProjectsRejected(element, _project, _user) {
    let createDto9: any = {
      _priority: 1,
      type: ACTIVITY_FEED_EVENTS_SPONSOR.PROJECTS_REJECTED,
      itemId: element.code,
      setFields: element,
      optional: _project.formattedId,
    };
    let _activityFeed9: any = await this.activityFeedService.projectsRejected(
      createDto9,
      _user
    );
  }

  async rejectBidsAndReturnWalletMoney(element, _project) {
    console.log("eleeement", element); // let _user: IUser = await this.usersService.findOne(createDto.createdBy);
    let _user = await this.usersService.findOne(element.createdBy); //   if(_user){//  }
    if (_user.type == USER_TYPES.SPONSOR) {
      await this.activityFeedProjectsRejected(element, _project, _user);
      let createWallet: any = {
        projectId: _project.code,
        bidId: element.code,
        bidAmount: element.financingAmount,
        sponserId: element.createdBy,
        transactionType: TRANSACTION_TYPE.CREDIT,
      };
      //Create a wallet transaction for every Post() request of the bid.
      let _createWallet = new CreateWalletTransactionDto(createWallet);
      let transaction = await this.walletTransactionsService.create(
        _createWallet
      );
      await this.edit(element.code, { status: 2 }, Date.now());
    } else {
      throw new BadRequestException(NOT_FOUND);
    }
  }

  async activityFeedProjectFunded(_project, _u, _user, element) {
    let createDto10: any = {
      _priority: 1,
      type:
        ACTIVITY_FEED_EVENTS_SPONSOR.PROJECTS_ALLOTTED_SUCCESSFULLY_MATCHED_WITH_A_SPONSOR,
      itemId: element.code,
      setFields: {
        message: "project-allotted successfully mathced with sponsor",
        projectName: _project.name,
      },
      optional: _project.formattedId,
    };
    console.log("Bid Finalize method approved Uesrrrrr", _user.formattedId);

    let _activityFeed1: any = await this.activityFeedService.projectsAllottedSuccessfullyMatchedWithSponsor(
      createDto10,
      _user
    );
    let _activityFeed2: any = await this.smeactivityFeedService.projectsAllottedSuccessfullyMatchedWithSponsor(
      createDto10,
      _u
    );
    let createDto11: any = {
      _priority: 1,
      type: ACTIVITY_FEED_EVENTS_SPONSOR.PROJECTS_FUNDED,
      itemId: element.code,
      setFields: {
        message: "project-funded",
        projectName: _project.name,
      },
      optional: _project.formattedId + " " + element.financingAmount,
    };
    let _activityFeed11: any = await this.activityFeedService.projectsFunded(
      createDto11,
      _user
    );
    let createDto12: any = {
      _priority: 1,
      type: ACTIVITY_FEED_EVENTS_SME.PROJECTS_FUNDED,
      itemId: _project.code,
      setFields: {
        message: "project-funded",
        projectName: _project.name,
      },
      optional: _project.formattedId + " " + element.financingAmount,
    };
    let _activityFeed12: any = await this.smeactivityFeedService.projectsFunded(
      createDto11,
      _u
    );
  }

  async smeActivityFeedDeleteSmeProject(_project, _u) {
    let createDto12: any = {
      _priority: 1,
      type: ACTIVITY_FEED_EVENTS_SME.PROJECT_REJECTED,
      itemId: _project.code,
      setFields: {
        message: "project-rejected",
        projectName: _project.name,
      },
      optional: _project.formattedId,
    };
    let _activityFeed9Sme: any = await this.smeactivityFeedService.projectRejected(
      createDto12,
      _u
    );
  }

  async changeBidStatusToApproved(idOrCode) {
    let bid = await this.findOne(idOrCode);
    console.log("biddddddddddddddd1 object", bid);
    if (
      bid !== undefined &&
      (bid.status == BID_STATUS.PENDING ||
        bid.status == BID_STATUS.REJECTED ||
        bid.status == BID_STATUS.APPROVED)
    ) {
      //check whether the project is approved, i.e,status = 1.Only the,allow the bid edit.
      let _bid = await this.edit(
        idOrCode,
        { status: BID_STATUS.APPROVED },
        Date.now()
      );
      return _bid;
    }
    throw new BadRequestException(NOT_FOUND);
  }

  async changeBidStatusToPending(idOrCode) {
    let bid = await this.findOne(idOrCode);
    if (
      bid !== undefined &&
      (bid.status == BID_STATUS.PENDING ||
        bid.status == BID_STATUS.REJECTED ||
        bid.status == BID_STATUS.APPROVED)
    ) {
      let _bid = await this.edit(
        idOrCode,
        { status: BID_STATUS.PENDING },
        Date.now()
      );
      return _bid;
    }
    throw new BadRequestException(NOT_FOUND);
  }

  async changeBidStatusToRejected(idOrCode) {
    let bid = await this.findOne(idOrCode);
    if (
      bid !== undefined &&
      (bid.status == BID_STATUS.PENDING ||
        bid.status == BID_STATUS.REJECTED ||
        bid.status == BID_STATUS.APPROVED)
    ) {
      let _bid = await this.edit(
        idOrCode,
        { status: BID_STATUS.REJECTED },
        Date.now()
      );
      return _bid;
    }
    throw new BadRequestException(NOT_FOUND);
  }
  
  async createTransactionTable(projectId, user){
    let _query = { projectId: projectId, status: BID_STATUS.APPROVED };
    //let options: Paginate = { limit: 30, page: 1, sort: "_id" };
    let _project = await this.smeProjectService.findOne(projectId);
    //let _u = await this.usersService.findOne(_project.createdBy);
    //let _bid = await this.bidDetailsService.findOneByQuery(_query);
    let _bid = await this.bidDetailsModel.find(_query);
    console.log('_project, //_u, _bid', _project, //_u,
     _bid);
    
        
     let _approvedBids = await _bid.some((e) => e.status === 1);
     console.log(
      "approvedBidssssss",
      _approvedBids,
      _approvedBids.length,
      _bid.length
    );
     if (
      _project.status == PROJECT_STATUS.FUNDED &&
      _project.hasMilestones == 1 && (_approvedBids == true)
    ){
      let isTransactionExists = await this.transactionsModel.findOne({ projectId: projectId})
      
        //if (user.type == USER_TYPES.SPONSOR) {
          let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
        
          let hasAccess = _userProfile.rolesAccessAction.some(
            (e) => e === ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_CREATE_TRANSACTION
          );
          console.log('userstype', user.type, _userProfile, 'hasAccess',  hasAccess); 
      if (hasAccess) {
          if(!isTransactionExists){
              let _body = {
               // ...body,
                projectId: projectId,
                sponsorId: _bid[0].createdBy,
                smeId: _project.createdBy,
                projectValue: _project.value,
                fundsRequested: _project.fundsRequired,
                projectValidationDate: _project.projectApprovedTime,
                projectFinancingDate: _bid[0].bidApprovedTime,
                fundsOffered: _bid[0].financingAmount,
                sponsorformattedUserId: _bid[0].formattedUserId,
                projectformattedId: _project.formattedId,
                smeformattedUserId: _project.formattedUserId,
                transactionsSummary: '',
                repaymentMode: '',
                financingTenure: 0,
                interestRate: 0
                //projectDuration: project.tenure
              }
              //return await this.baseModel.insertMany(createDto);
              let data = new CreateTransactionDetailsDto(_body);
              //return super.create(req, data, user);
              let k = await this.transactionsModel.create(data);
              return k;
            }
          throw new BadRequestException(TRANSACTION_FOR_PROJECT_EXISTS)
        }
      throw new BadRequestException(ONLY_FOR_ADMIN);
     }
    throw new BadRequestException(ONLY_FOR_FUNDED_PROJECTS_BIDS_APPROVED);
  }


  async calculateBidDetails(user) {
    let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
    
    let hasAccess = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.BID_DETAILS_SERVICE_CALCULATE_BID_DETAILS
    );
    console.log('userstype', user.type, _userProfile, hasAccess)
    if(hasAccess) {
      let condition = { $or: [{ status: BID_STATUS.PENDING }, { status: BID_STATUS.APPROVED }] };
      let query = { createdBy: user.code, ...condition };
      //let query = { createdBy: user.code };
      //Implement with bidDetailsModel
      /*
      let data = await this.bidDetailsService.findAll(query, {
        limit: 30,
        page: 1,
        sort: "_id",
      });
      */
      let data = await this.bidDetailsModel.find(query);
      let dashboard;
      console.log("Data", data);
      let count = 0;
      let totalfundedamount = 0;
      for (let i = 0; i < data.length; i++) {
        let idorCode = data[i].projectId;
        let project = await this.smeProjectService.findOne(idorCode);
        if (project !== undefined) {
          if (project.status == PROJECT_STATUS.FUNDED) {
            totalfundedamount = totalfundedamount + data[i].financingAmount;
            count++;
          }
        }
      }
      // if()
      dashboard = {
        totalProjects: data.length,
        walletBalance: user.totalBalance,
        totalfundedProjects: count,
        totalfundedAmount: totalfundedamount,
      };
      return dashboard;
    }
  }

  async getFundedProjects(user) {
    let query = { createdBy: user.code, status: 1 };
    debugger;
    //Implement with bidDetailsModel
    /*
    let data = await this.bidDetailsService.findAll(query, {
      limit: 30,
      page: 1,
      sort: "_id",
    });
    */
    let data = await this.bidDetailsModel.find(query);
    console.log(data);

    let fundeddata;
    for (let i = 0; i < data.length; i++) {
      let fundata = data[i];
      console.log("fundData", fundata);
      let idOrCode = fundata.projectId;

      let smeproject = await this.smeProjectService.findOne(idOrCode);
      if (smeproject !== undefined) {
        if (smeproject.status == PROJECT_STATUS.FUNDED) {
          fundeddata = data[i];
        }
      }
    }
    if (fundeddata == undefined) {
      fundeddata = "Projects are not funded";
    }
    return fundeddata;
  }


  async createBigChainTransaction(userDetails, projectDetails, _user,
    bidDetails) {
    debugger;
    
    let bigchaindbkeys = await this.bigchaindbService.findOneByQuery({projectRef: projectDetails.code
    })
    
  /*
    let bigchaindbkeys = await this.bigchaindbService.findOneByQuery({
      createdBy: userDetails.code,
    });
    */
    //  console.log("companydetails", projectDetails._entityDetailsSponsor);
    console.log("userDetails", userDetails, bigchaindbkeys);
    // let username = model.firstName;
    const username = new driver.Ed25519Keypair();
    // console.log(username, model);
    let bigchaindbType = null;
    const API_PATH = BIGCHAINDB_API_PATH;
    bigchaindbType = BIGCHAINDB_TRANSACTION_TYPE.LOAN_SMART_CONTRACT;
    let loanSmartContractAssetData = {
      createdBy: "Cycloan",
      smeName: userDetails.formattedId,
      sponsorName: _user.formattedId,
      assetreferenceKey: bigchaindbkeys.transactionId,
     // loanAmount: ,
     // loanId: ,
      transactionId: bidDetails.code,
      transactionValue: bidDetails.financingAmount,
      transactionDate: bidDetails.createdTime
    };
   
   
    console.log("loanSmartContractAssetData", loanSmartContractAssetData);
    /*
     */
    let projectStatus = "";
    switch (projectDetails.status) {
      case 0:
        projectStatus = "Pending";
        break;
      case 1:
        projectStatus = "Approved";
        break;
      case 2:
        projectStatus = "Funded";
        break;
      case 3:
        projectStatus = "Rejected";
        break;
      case 4:
        projectStatus: "Completed";
        break;
      case -1:
        projectStatus = "Deleted";
        break;
      default:
        console.log("Nostatus");
        break;
    }
    debugger;
    
    let loanSmartContractMetaData = {
     // interestRate:,
     // installmentPeriod: projectDetails.tenure,
     // emi:,
     // totalNoOfPaidemis:,
     // loanRepaid:,
     // loanStatus:,
      transactionStatus: bidDetails.status,
      projectCompletionStatusCode: projectStatus
    };
    console.log("loanSmartContractMetaData", loanSmartContractMetaData);
    // Construct a transaction payload
    const tx = await driver.Transaction.makeCreateTransaction(
      { loanSmartContractAssetData },

      { loanSmartContractMetaData },

      // A transaction needs an output
      [
        driver.Transaction.makeOutput(
          driver.Transaction.makeEd25519Condition(username.publicKey)
        ),
      ],
      username.publicKey
    );
    console.log("ttttxxxxx", tx);

    // Sign the transaction with private keys
    const txSigned = driver.Transaction.signTransaction(
      tx,
      username.privateKey
    );
    console.log("txSigneddddd", txSigned);
    // Send the transaction off to BigchainDB

    const conn = new driver.Connection(API_PATH);

    conn.postTransactionCommit(txSigned).then((retrievedTx) => {
      let keys = {
        type: bigchaindbType,
        publickey: username.publicKey,
        privatekey: username.privateKey,
        transactionId: retrievedTx.id,
        projectRef: null,
        isDummyTransaction: false
        //"id",
      };
      return new Promise(async (resolve, reject) => {
        // <--- this line
        let bcdb = await this.bigchaindbService.SaveKeys(keys, userDetails);
        console.log("Transaction", retrievedTx.id, "successfully posted.");
      });
    });
    //  return new Promise(async (resolve, reject) => { // <--- this line
    //  let bcdb = await this.bigchaindbService.SaveKeys(keys, model);
    ////   console.log("Transaction", retrievedTx.id, "successfully posted.");

    //console.log(result);
    //
    // console.log(result.id)
    //   console.log(username.publicKey)

    //---bbbbbbbbbb
  }
}
