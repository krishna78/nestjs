import { Expose, Type } from "class-transformer";
import { Reader, Creator, Updater } from "../../common/base/base.dto";
import { IsDefined, IsNotEmpty, IsInt, IsOptional } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";
import { SmeProjectDto } from "../../sme-project/objects/sme-project.dto";
import { UserDto } from "../../users/objects/create-user.dto";

export class BidDetailsDto extends Reader{
   
    @Expose()
    readonly projectId: string = "";

    //@Expose()
    //readonly intRate: number;

    @Expose()
    readonly financingAmount: number;

    //@Expose()
    //readonly noOfEmis: number;

    //@Expose()
    //readonly perEmiAmount: number;

    //@Expose()
    //readonly tokensForSubmission: number;
    
    @Expose()
    readonly projectDuration: number;
    
    @Expose()
    readonly createdBy: string = "";
/**/ 
    @Expose()
    readonly formattedUserId: string = "";
/*    */
    @Expose()
    readonly formattedId: string = "";

    @Expose()
    @Type(() => UserDto)
    readonly userDetails: UserDto;
/**/ 
   @Expose()
   @Type(() => SmeProjectDto)
   readonly formattedProjectIdForBid: SmeProjectDto;

   @Expose()
  readonly bidApprovedTime: Date;

  @Expose()
  readonly bidApprovedBy: string = "";

  @Expose()
  readonly bidApprovedUserEmail: string = "";
 /*  */
}

export class CreateBidDetailsDto extends Creator {
  constructor() {
    super(true);
  }

  @IsDefined()
  @IsNotEmpty()
  readonly projectId: string;

  //@IsDefined()
  //@IsNotEmpty()
  //@IsInt()
  //readonly intRate: number;

   @IsDefined()
   @IsNotEmpty()
   @IsInt()
   readonly financingAmount: number;

   
  //@IsDefined()
  //@IsNotEmpty()
  //@IsInt()
  //readonly noOfEmis: number;

  //@IsDefined()
  //@IsNotEmpty()
  // @IsInt()
  // readonly perEmiAmount: number;

  // @IsDefined()
  // @IsNotEmpty()
  // @IsInt()
  // readonly tokensForSubmission: number;
}

export class UpdateBidDetailsDto extends Updater {
  //@IsOptional()
  //@IsNotEmpty()
  //@ApiModelProperty()
  //readonly intRate: number;

  @IsOptional()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly financingAmount: number;

  //@IsOptional()
  //@IsNotEmpty()
  //@ApiModelProperty()
  //readonly noOfEmis: number;

  //@IsOptional()
  //@IsNotEmpty()
  //@ApiModelProperty()
  //readonly perEmiAmount: number;

  //@IsOptional()
 // @IsNotEmpty()
 // @ApiModelProperty()
 // readonly tokensForSubmission: number;
}

export class TransactionDetailsDto extends Reader{
 
  @Expose()
  readonly projectId: string = "";
  
  @Expose()
  readonly smeId: string = "";
  
  @Expose()
  readonly sponsorId: string = "";
  
  @Expose()
  readonly projectValue: number;
  
  @Expose()
  readonly projectValidationDate: string = "";
  
  @Expose()
  readonly fundsRequested: number;
  
  @Expose()
  readonly fundsOffered: number;
  
  @Expose()
  readonly projectFinancingDate: string = "";
  
  @Expose()
  readonly financingTenure: number;
  
  @Expose()
  readonly interestRate: number;
  
  @Expose()
  readonly repaymentMode: string = "";
  
  @Expose()
  readonly transactionsSummary: string = "";
  
  @Expose()
  readonly sponsorformattedUserId: string = "";
  
  @Expose()
  readonly projectformattedId: string = "";
  
  @Expose()
  readonly smeformattedUserId: string = "";
 }
  
 


 export class CreateTransactionDetailsDto extends Creator {
  constructor({ projectId, smeId, sponsorId, projectValue, projectValidationDate, fundsRequested, fundsOffered, projectFinancingDate, financingTenure, interestRate,
    repaymentMode, transactionsSummary, sponsorformattedUserId, projectformattedId, smeformattedUserId }) {
    super(true);
    this.projectId = projectId; 
    this.smeId = smeId; 
    this.sponsorId = sponsorId;
    this.projectValue = projectValue; 
    this.projectValidationDate = projectValidationDate; 
    this.fundsRequested = fundsRequested;
    this.fundsOffered = fundsOffered;
    this.projectFinancingDate = projectFinancingDate; 
    this.financingTenure = financingTenure; 
    this.interestRate = interestRate;
    this.repaymentMode = repaymentMode; 
    this.transactionsSummary = transactionsSummary;
    this.sponsorformattedUserId = sponsorformattedUserId;
    this.projectformattedId = projectformattedId;
    this.smeformattedUserId = smeformattedUserId;
  }
  
  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly projectId: string;

  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly smeId: string;
  
  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly sponsorId: string;
  
  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly projectValue: number;
  
  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly projectValidationDate: string;
  
  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly fundsRequested: number;
  
  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly fundsOffered: number;
  
  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly projectFinancingDate: string;

  //@IsDefined()
  //@IsNotEmpty()
  //readonly fundsOffered: number;
  
  //@IsDefined()
  //@IsNotEmpty()
  //readonly projectFinancingDate: string;
  
  @IsDefined()
  @IsNotEmpty()
  readonly financingTenure: number;
  
  @IsDefined()
  @IsNotEmpty()
  readonly interestRate: number;
  
  @IsDefined()
  @IsNotEmpty()
  readonly repaymentMode: string;
  
  @IsDefined()
  @IsNotEmpty()
  readonly transactionsSummary: string;
 
  @IsDefined()
  @IsNotEmpty()
  readonly sponsorformattedUserId: string;
  
  @IsDefined()
  @IsNotEmpty()
  readonly projectformattedId: string;

  @IsDefined()
  @IsNotEmpty()
  readonly smeformattedUserId: string;
  
 }
  
 export class UpdateTransactionDetailsDto extends Updater {
  

  @IsOptional()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly interestRate: number;
  
  //@IsOptional()
  //@IsNotEmpty()
  //@ApiModelProperty()
  //readonly fundsOffered: number;
  
  //@IsOptional()
  //@IsNotEmpty()
  //@ApiModelProperty()
  //readonly projectFinancingDate: string = "";
  
  @IsOptional()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly financingTenure: number;
  
  @IsOptional()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly repaymentMode: string = "";
  
  @IsOptional()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly transactionsSummary: string = "";
 }
 