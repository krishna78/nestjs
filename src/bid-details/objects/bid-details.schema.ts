import { Schema } from "mongoose";
import { createModel, Entity, IEntity } from "../../common/base/base.model";

export class BidDetails extends Entity {
  projectId: string;
 // intRate: number;
  financingAmount: number;
 // noOfEmis: number;
 // perEmiAmount: number;
 // tokensForSubmission: number;
 projectDuration: number;
  formattedId: string;
  bidApprovedTime: string;
  bidApprovedBy: string;
  bidApprovedUserEmail: string;
}
export interface IBidDetails extends BidDetails, IEntity {
  id: string;
}
export const BidDetailsSchema: Schema = createModel("BidDetails", {
  projectId: { type: String, required: true },
//  intRate: { type: Number, required: true },
  financingAmount: { type: Number, required: true },
  projectDuration: { type: Number, required: true },
 // noOfEmis: { type: Number, required: true },
//  perEmiAmount: { type: Number, required: true },
 // tokensForSubmission: { type: Number, required: true },
  formattedId: { type: String },
  bidApprovedTime: { type: String },
  bidApprovedBy: { type: String },
  bidApprovedUserEmail: { type: String }
});



BidDetailsSchema.virtual("formattedProjectIdForBid", {
  ref: "SmeProject",
  localField: "projectId",
  foreignField: "code",
  justOne: true,
  /*
    options: {
      select: "mimetype + originalname",
    },
    */
});
BidDetailsSchema.virtual("userDetails", {
  ref: "User",
  localField: "createdBy",
  foreignField: "code",
  justOne: true,
  /*
    options: {
      select: "mimetype + originalname",
    },
    */
});


export class TransactionDetails extends Entity {
  projectId: string;
  smeId: string;
  sponsorId: string;
  projectValue: number;
  projectValidationDate: string;
  fundsRequested: number;
  fundsOffered: number;
  projectFinancingDate: string;
  financingTenure: number;
  interestRate: number;
  repaymentMode: string;
  transactionsSummary: string;
  sponsorformattedUserId: string;
  projectformattedId: string;
  smeformattedUserId: string;
 }
  
 export interface ITransactionDetails extends BidDetails, IEntity {
  id: string;
 }
 
 
 export const TransactionDetailsSchema: Schema = createModel("TransactionDetails", {
  projectId: { type: String, required: true },
  smeId: { type: String, required: true },
  sponsorId: { type: String, required: true },
  projectValue: { type: Number, required: true },
  projectValidationDate: { type: String, required: true },
  fundsRequested: { type: Number, required: true },
  fundsOffered: { type: Number, required: true },
  projectFinancingDate: { type: Number, required: true },
  financingTenure: { type: Number//, required: true
   },
  interestRate: { type: Number//, required: true 
  },
  repaymentMode: { type: String//, required: true 
  },
  transactionsSummary: { type: String//, required: true 
  },
  sponsorformattedUserId:  { type: String },
  projectformattedId:  { type: String },
  smeformattedUserId:  { type: String },

 });
 
/*
 TransactionDetailsSchema.virtual("tformattedProjectIdForBid", {
  ref: "SmeProject",
  localField: "projectId",
  foreignField: "code",
  justOne: true,
  /*
    options: {
      select: "mimetype + originalname",
    },
    */
//});
/*
TransactionDetailsSchema.virtual("tuserDetails", {
  ref: "User",
  localField: "sponsorId",
  foreignField: "code",
  justOne: true,
  /*
    options: {
      select: "mimetype + originalname",
    },
    */
//})
