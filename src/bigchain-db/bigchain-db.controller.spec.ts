import { Test, TestingModule } from '@nestjs/testing';
import { BigchainDbController } from './bigchain-db.controller';

describe('BigchainDb Controller', () => {
  let controller: BigchainDbController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BigchainDbController],
    }).compile();

    controller = module.get<BigchainDbController>(BigchainDbController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
