import { Controller } from '@nestjs/common';
import { BASEROUTES } from '../common/constants/enum';
import { abstractBaseControllerFactory } from '../common/base/base.controller';
import { BigchainDbDto } from './objects/bigchain-db.dto';
import { InjectModel } from '@nestjs/mongoose';
import { BigchainDb } from './objects/bigchain-db.schema';
import { Model } from "mongoose";
import { BigchainDbService } from './bigchain-db.service';

const BaseController = abstractBaseControllerFactory<any>({
    DTO: BigchainDbDto,
    //CreateDTO: CreateBidDetailsDto,
    //UpdateDTO: UpdateBidDetailsDto,
    DisabledRoutes: [BASEROUTES.DETELEONE, BASEROUTES.PATCH],
  });
  


@Controller('bigchain-db')
export class BigchainDbController extends BaseController {
    constructor(
        @InjectModel("BigchainDb")
        private readonly bigchaindbmodel: Model<BigchainDb>,
        private bigchainDbService: BigchainDbService,
    ){
        super(bigchainDbService);
      }
}
