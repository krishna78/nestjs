/*
import { Module } from '@nestjs/common';
import { BigchainDbController } from './bigchain-db.controller';
import { BigchainDbService } from './bigchain-db.service';

@Module({
  controllers: [BigchainDbController],
  providers: [BigchainDbService]
})

export class BigchainDbModule {}
*/
import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { BigchainDbController } from "./bigchain-db.controller";
import { BigchainDbService } from "./bigchain-db.service";
import { BigchaindDbSchema } from "./objects/bigchain-db.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "BigchainDb", schema: BigchaindDbSchema },
    ]),
  ],
  controllers: [BigchainDbController],
  providers: [BigchainDbService],
  exports: [BigchainDbService],
})
export class BigchainDbModule {}
