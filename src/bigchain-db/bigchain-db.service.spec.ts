import { Test, TestingModule } from '@nestjs/testing';
import { BigchainDbService } from './bigchain-db.service';

describe('BigchainDbService', () => {
  let service: BigchainDbService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BigchainDbService],
    }).compile();

    service = module.get<BigchainDbService>(BigchainDbService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
