
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";

import { BaseService } from "../common/base/base.service";
import { CreateBigchainDb } from "./objects/bigchain-db.dto";
import { BigchainDb, IBigchaindDb } from "./objects/bigchain-db.schema";

@Injectable()
export class BigchainDbService extends BaseService<IBigchaindDb> {
  constructor(
    @InjectModel("BigchainDb")
    private readonly bigchaindbmodel: Model<BigchainDb>
  ) {
    super(bigchaindbmodel);
  }

  async SaveKeys(keys, user) {
  
    let data = new CreateBigchainDb(keys);
    return await super.create(data, user);
  }
}
