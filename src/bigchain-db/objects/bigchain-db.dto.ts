import { Expose } from "class-transformer";
import { IsDefined, IsNotEmpty, IsString } from "class-validator";
import { Creator, Reader } from "../../common/base/base.dto";

export class CreateBigchainDb extends Creator {
  constructor(
    { type, privatekey, publickey, transactionId, projectRef, isDummyTransaction } // createdBy
  ) {
    super(true);
    this.privatekey = privatekey;
    this.type = type;
    this.publickey = publickey;
    this.transactionId = transactionId;
    this.projectRef = projectRef;
    this.isDummyTransaction = isDummyTransaction;
    // this.createdBy = createdBy
  }
  @IsDefined()
  @IsNotEmpty()
  readonly type: string = "";

  @IsDefined()
  @IsNotEmpty()
  readonly privatekey: string = "";

  @IsDefined()
  @IsNotEmpty()
  readonly publickey: string = "";

  @IsDefined()
  @IsNotEmpty()
  readonly transactionId: string = "";
  
  @IsDefined()
  @IsNotEmpty()
  readonly projectRef: string = "";

  @IsDefined()
  @IsNotEmpty()
  readonly isDummyTransaction: string = "";
}


export class BigchainDbDto extends Reader {
 
  @IsString()
  @Expose()
  readonly type: string = "";
 
  @IsString()
  @Expose()
  readonly privatekey: string = "";

  @IsString()
  @Expose()
  readonly publickey: string = "";

  @IsString()
  @Expose()
  readonly transactionId: string = "";
  
  @IsString()
  @Expose()
  readonly projectRef: string = "";

  @IsDefined()
  @IsNotEmpty()
  readonly isDummyTransaction: string = "";
 
}