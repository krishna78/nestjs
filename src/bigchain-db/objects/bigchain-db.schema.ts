import { createModel, Entity, IEntity } from "../../common/base/base.model";
import { Schema } from "mongoose";
import { ACTIVITY_FEED_EVENTS_SPONSOR } from "../../common/constants/enum";

export class BigchainDb extends Entity {
  type: string;
  privatekey: string;
  publickey: string;
  transactionId: string;
  projectRef: string;
  isDummyTransaction: boolean;
}
export interface IBigchaindDb extends BigchainDb, IEntity {
  id: string;
}

export const BigchaindDbSchema: Schema = createModel("BigchainDb", {
  type: { type: String },

  privatekey: { type: String },
  publickey: { type: Object },
  transactionId: { type: String },
  projectRef: { type: String },
  isDummyTransaction: { type: Boolean, default: false}
});

