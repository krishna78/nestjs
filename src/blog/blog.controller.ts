import { BlogService } from "./blog.service";
import { Controller, UseGuards } from "@nestjs/common";
import { BlogDto, CreateBlogDto, UpdateBlogDTO } from "./objects/blog.dto";
import { abstractBaseControllerFactory } from "../common/base/base.controller";
import { JwtAuthGuard } from "../auth/auth.guard";

const BaseController = abstractBaseControllerFactory<any>({
  DTO: BlogDto,
  CreateDTO: CreateBlogDto,
  UpdateDTO: UpdateBlogDTO,
});

@UseGuards(JwtAuthGuard)
@Controller("blog")
export class BlogController extends BaseController {
  constructor(private blogService: BlogService) {
    super(blogService);
  }
}
