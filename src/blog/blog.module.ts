import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { BlogSchema } from "./objects/blog.schema";
import { BlogController } from "./blog.controller";
import { BlogService } from "./blog.service";
import { UsersModule } from "../users/users.module";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "Blog", schema: BlogSchema }]),
    UsersModule,
  ],
  exports: [BlogService],
  providers: [BlogService],
  controllers: [BlogController],
})
export class BlogModule {}
