import { Injectable } from "@nestjs/common";
import { InjectModel, MongooseModule } from "@nestjs/mongoose";
import { BaseService } from "../common/base/base.service";
import { Blog, IBlog } from "./objects/blog.schema";
import { HasPreCreate, HasPreUpdate } from "../common/base/pre.post.action";
import { CreateBlogDto } from "./objects/blog.dto";
import { UsersService } from "../users/users.service";
import { CanLoadVirtual } from "../common/interfaces/can.load.virtual";

@Injectable()
export class BlogService extends BaseService<IBlog>
  implements HasPreCreate, HasPreUpdate, CanLoadVirtual {
  constructor(
    @InjectModel("Blog") private readonly model: MongooseModule,
    private readonly usersService: UsersService
  ) {
    super(model);
  }
  async doPreCreate(createDto: CreateBlogDto): Promise<void> {}

  async doPreUpdate(
    idOrCode: string,
    editDto: CreateBlogDto,
    model: Blog
  ): Promise<void> {}

  getVirtualForItem(): string[] {
    return ["editors", "contributed_by"];
  }

  getVirtualForList(): string[] {
    return ["contributed_by"];
  }
}
