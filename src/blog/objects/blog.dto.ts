import { Expose, Type } from "class-transformer";
import { Length, IsNotEmpty, IsDefined, Allow, IsArray } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";
import { Creator, Updater, Reader } from "../../common/base/base.dto";
import { UserDto } from "../../users/objects/create-user.dto";

export class CreateBlogDto extends Creator {
  constructor() {
    super(true);
  }

  @Length(1, 20)
  @IsDefined()
  @IsNotEmpty()

  // @IsDefined()
  @ApiModelProperty()
  readonly title: string;

  @Allow()
  @ApiModelProperty()
  readonly description: string;

  @Allow()
  @ApiModelProperty()
  readonly body: string;

  @ApiModelProperty()
  @Allow()
  readonly contributor: string;

  @ApiModelProperty()
  @IsArray()
  readonly editor: string[];
}

export class UpdateBlogDTO extends Updater {
  @Length(1, 20)
  @IsNotEmpty()
  readonly title: string;

  @Allow()
  @ApiModelProperty()
  readonly description: string;

  @Allow()
  @ApiModelProperty()
  readonly body: string;

  @ApiModelProperty()
  @Allow()
  readonly contributor: string;

  @ApiModelProperty()
  @IsArray()
  readonly editor: string[];
}

export class BlogDto extends Reader {
  @Expose()
  readonly title: string = "";

  @Expose()
  readonly description: string;

  @Expose()
  readonly body: string;

  @Expose()
  @Type(() => UserDto)
  readonly contributed_by: UserDto;

  @Expose()
  @Type(() => UserDto)
  readonly editors: UserDto[];
}
