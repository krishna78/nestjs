import { createModel, Entity, IEntity } from "../../common/base/base.model";
import { Schema } from "mongoose";

export class Blog extends Entity {}

export interface IBlog extends Blog, IEntity {
  id: string;
}

export const BlogSchema = createModel("Blogs", {
  title: {
    type: String,
  },
  description: String,
  body: String,
  contributor: {
    type: String,
  },
  editor: [
    {
      type: String,
    },
  ],
});

BlogSchema.virtual("contributed_by", {
  ref: "User",
  localField: "contributor",
  foreignField: "_id",
  justOne: true,
  options: {
    select: "email + socialLinks",
  },
});
BlogSchema.virtual("editors", {
  ref: "User",
  localField: "editor",
  foreignField: "_id",
  justOne: false,
  options: {
    select: "email",
  },
});
