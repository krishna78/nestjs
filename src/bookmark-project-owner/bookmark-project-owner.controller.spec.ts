import { Test, TestingModule } from "@nestjs/testing";
import { BookmarkProjectOwnerController } from "./bookmark-project-owner.controller";

describe("BookmarkProjectOwner Controller", () => {
  let controller: BookmarkProjectOwnerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BookmarkProjectOwnerController],
    }).compile();

    controller = module.get<BookmarkProjectOwnerController>(
      BookmarkProjectOwnerController
    );
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });
});
