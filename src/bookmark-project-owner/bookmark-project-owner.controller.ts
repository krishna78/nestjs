import { Controller, UseGuards, Post, Body, Query, Request, BadRequestException, Get, NotFoundException } from "@nestjs/common";
import { abstractBaseControllerFactory } from "../common/base/base.controller";
import {
  BMProjectOwnerDto,
  CreateBMProjectOwnerDto,
  CreateBMProjectDetailsDto,
} from "./objects/bookmark-project-owner.dto.";
import { BASEROUTES } from "../common/constants/enum";
import { BMProjectOwnerService } from "./bookmark-project-owner.service";
import { JwtAuthGuard } from "../auth/auth.guard";
import { AbstractClassTransformerPipe } from "../common/pipes/class-transformer.pipe";
import { RequestUser } from "../common/utils/controller.decorator";
import { EXISTS, CAN_BOOKMARK_CREATED_PROJECTS } from "../common/constants/string";
import { SmeProjectService } from "../sme-project/sme-project.service";
import { plainToClass } from "class-transformer";
import { success } from "../common/base/httpResponse.interface";

const BaseController = abstractBaseControllerFactory<any>({
  DTO: BMProjectOwnerDto,
  //Todo: Remove after creating records in Db.
  CreateDTO: CreateBMProjectOwnerDto,
  DisabledRoutes: [
    //Todo: Uncomment BASEROUTES.CREATE after creating records in Db.
    // BASEROUTES.CREATE,
    //BASEROUTES.DETELEONE,

    BASEROUTES.PATCH,
    BASEROUTES.UPDATEONE,
  ],
});
@UseGuards(JwtAuthGuard)
@Controller("bookmark-project-owner")
export class BookmarkProjectOwnerController extends BaseController {
  constructor(
    private bmprojectOwnerService: BMProjectOwnerService,
    private smeProjectService: SmeProjectService) {
    super(bmprojectOwnerService);
  }
  @Get()
  async findList(@Request() req, @Query() query, @RequestUser() user) {
    if (user.isAdmin) {
      // <--- only admin can see the user lists
      return await super.findList(req, { ...query });
    }
    let _query = { createdBy: user.code }
    return await super.findList(req, { ..._query });
    //throw new NotFoundException();
  }


  //@UseGuards(JwtAuthGuard)
  @Post()
  public async create(
    @Request() req,
    @Body(AbstractClassTransformerPipe(CreateBMProjectDetailsDto)) body: any,
    @RequestUser() user
  ) {

    let _milestones = await Promise.all(
    await body.bookmarks.map(async(milestone) => {
                let sp = await this.smeProjectService.findOne(milestone.projectId);
          console.log('createdByyyy',sp.createdBy,user.code)
          if(sp.createdBy == user.code){
              let _query = { projectId: milestone.projectId, createdBy: user.code }
              let bm = await this.bmprojectOwnerService.findOneByQuery(_query);
                  if((bm) == null || (bm.status == -1)){
                      console.log('bookmark....', bm) 

                            return {
                              ...milestone,
                              createdBy: user.code

                            };
                 } throw new BadRequestException(EXISTS, "Record exists")
           }throw new BadRequestException(CAN_BOOKMARK_CREATED_PROJECTS)
       })
    )
       console.log('mmmmmmmmmmm',_milestones)
       let createdItems = await this.bmprojectOwnerService.createD(_milestones);
       createdItems.map((milestone) => {
        return plainToClass(CreateBMProjectOwnerDto, milestone, {
          excludeExtraneousValues: true,
        });
      });
      return success(createdItems);
   /*
       let createdItems = await this.bmprojectOwnerService.createD(milestones);
    createdItems.map((milestone) => {
      return plainToClass(CreateBMProjectOwnerDto, milestone, {
        excludeExtraneousValues: true,
      });
    });
    return success(createdItems);
    */
  }
}
