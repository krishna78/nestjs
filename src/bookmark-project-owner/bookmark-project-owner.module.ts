import { Module, forwardRef } from "@nestjs/common";
import { BookmarkProjectOwnerController } from "./bookmark-project-owner.controller";
import { BMProjectOwnerService } from "./bookmark-project-owner.service";
import { MongooseModule } from "@nestjs/mongoose";
import { BMProjectOwnerSchema } from "./objects/bookmark-project-owner.schema";
import { SmeProjectModule } from "../sme-project/sme-project.module";
import { SmeProjectService } from "../sme-project/sme-project.service";
import { SmeProjectSchema } from "../sme-project/objects/sme-project.schema";
import { EmailService } from "../email/email.service";
import { UsersModule } from "../users/users.module";
import { EmailModule } from "../email/email.module";
import { ActivityFeedModule } from "../activity-feed/activity-feed.module";
import { SmeActivityFeedModule } from "../sme-activity-feed/sme-activity-feed.module";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "BMProjectOwner", schema: BMProjectOwnerSchema },
      { name: "SmeProject", schema: SmeProjectSchema },
    ]),
    UsersModule,
    EmailModule,
    forwardRef(() => SmeProjectModule),
    ActivityFeedModule,
    SmeActivityFeedModule,
  ],
  controllers: [BookmarkProjectOwnerController],
  providers: [BMProjectOwnerService
    //, SmeProjectService
  ],
})
export class BookmarkProjectOwnerModule {}
