import { Test, TestingModule } from "@nestjs/testing";
import { BookmarkProjectOwnerService } from "./bookmark-project-owner.service";

describe("BookmarkProjectOwnerService", () => {
  let service: BookmarkProjectOwnerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BookmarkProjectOwnerService],
    }).compile();

    service = module.get<BookmarkProjectOwnerService>(
      BookmarkProjectOwnerService
    );
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
