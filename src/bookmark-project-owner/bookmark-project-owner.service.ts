import { Injectable } from "@nestjs/common";
import { BaseService } from "../common/base/base.service";
import {
  BMProjectOwner,
  IBMProjectOwner,
} from "./objects/bookmark-project-owner.schema";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CanLoadVirtual } from "../common/interfaces/can.load.virtual";
import { CreateBMProjectOwnerDto } from "./objects/bookmark-project-owner.dto.";


@Injectable()
export class BMProjectOwnerService extends BaseService<IBMProjectOwner> implements CanLoadVirtual{
   constructor(
    @InjectModel("BMProjectOwner")
    private readonly bmprojectownermodel: Model<IBMProjectOwner>
  ) {
    super(bmprojectownermodel);
  }
/* */
  getVirtualForItem(): string[] {
    return ["favProjects"];
  }

  getVirtualForList(): string[] {
    return ["favProjects"];
  }

 // async createD(createDto: CreateBMProjectOwnerDto[]) {
  async createD(createDto: any) {
    //return await this.baseModel.insertMany(createDto);
    let k = await this.baseModel.insertMany(createDto);
    //let check = await this.doPostCreate(k)
    return k;

  }
}
