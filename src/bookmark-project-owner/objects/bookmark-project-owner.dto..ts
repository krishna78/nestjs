import { Expose, Type } from "class-transformer";
import { Reader, Creator } from "../../common/base/base.dto";
import { IsDefined, IsNotEmpty, Allow, ValidateNested, IsArray } from "class-validator";
import { SmeProjectDto } from "../../sme-project/objects/sme-project.dto";

export class BMProjectOwnerDto extends Reader {
  @Expose()
  readonly projectId: string = "";
  
  @Expose()
  @Type(() => SmeProjectDto)
  readonly favProjects: SmeProjectDto;

  @Expose()
  readonly createdBy: string = "";
  
  /*
  @Expose()
  readonly favProjects: string;
  */
}

//Todo: Remove after creating records in Db.
export class CreateBMProjectOwnerDto extends Creator {
  constructor() {
    super(true);
  }
  @IsDefined()
  @IsNotEmpty()
  readonly projectId: string;
}

export class CreateBMProjectDetailsDto {
  @Allow()
  @IsDefined()
  @IsNotEmpty()
  @IsArray()
  @Type(() => CreateBMProjectOwnerDto)
  @ValidateNested()
  bookmarks: CreateBMProjectOwnerDto[];
/*
  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  project: string;
  */
}