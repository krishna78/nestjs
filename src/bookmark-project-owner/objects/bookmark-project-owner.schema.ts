import { Entity, IEntity, createModel } from "../../common/base/base.model";
import { Schema } from "mongoose";
import { extend } from "lodash";

export class BMProjectOwner extends Entity {
  projectId: string;
  //OwnerName:string;
}
export interface IBMProjectOwner extends BMProjectOwner, IEntity {
  id: string;
}
export const BMProjectOwnerSchema: Schema = createModel("BMProjectOwner", {
  projectId: { type: String, required: true },
  // OwnerName: { type: String },
});


BMProjectOwnerSchema.virtual("favProjects", {
  ref: "SmeProject",
  localField: "projectId",
  foreignField: "_id",
  justOne: true
  //options: {
   // select: "owner"
  //}
});