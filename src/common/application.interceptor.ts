import {
  ExecutionContext,
  Injectable,
  NestInterceptor,
  CallHandler,
  UnprocessableEntityException,
  Logger,
} from "@nestjs/common";

import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";

@Injectable()
export class ApplicationExceptionInterceptor implements NestInterceptor {
  private readonly logger = new Logger(
    ApplicationExceptionInterceptor.name,
    true
  );

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      catchError((error) => {
        this.logger.error({ message: error.message, err: error });
        throw new UnprocessableEntityException(error.message);
      })
    );
  }
}
