import {
  Body,
  Delete,
  Get,
  Post,
  Put,
  Request,
  UseInterceptors,
  NotFoundException,
  Query,
} from "@nestjs/common";
import { AbstractControllerOptions } from "./base.interface";
import { AbstractClassTransformerPipe } from "../pipes/class-transformer.pipe";
import { plainToClass } from "class-transformer";
import { ApplicationExceptionInterceptor } from "../application.interceptor";
import {
  IdOrCodeParser,
  LastUpdatedTime,
  RequestUser,
} from "../utils/controller.decorator";
import { BaseService } from "./base.service";
import { IEntity } from "./base.model";
import { success } from "./httpResponse.interface";
import { BASEROUTES, Status } from "../constants/enum";
import { createMongoSearchQuery } from "../utils/helper";
import {
  normalizePaginateResult,
  normalizePagination,
  Paginate,
} from "../interfaces/pagination";
import * as dot from "dot-object";

export function abstractBaseControllerFactory<T>(
  options?: AbstractControllerOptions<T>
): any {
  const { DTO, CreateDTO, UpdateDTO, DisabledRoutes } = options;

  const routeAvailability = (routeTag: number): void => {
    if (DisabledRoutes && DisabledRoutes.includes(routeTag)) {
      throw new NotFoundException();
    }
  };

  @UseInterceptors(ApplicationExceptionInterceptor)
  abstract class AbstractController {
    protected _service: BaseService<IEntity>;

    protected constructor(service: any) {
      this._service = service;
    }

    @Get()
    async findList(@Request() req, @Query() query) {
      routeAvailability(BASEROUTES.FINDLIST);
      let options: Paginate = {
        limit: 30,
        page: 1,
        sort: "_id",
      };
      console.log("q1111111111", query);
      normalizePagination(query, options);
      console.log("q22222222222", query);
      //    query = query ? await createMongoSearchQuery(DTO,dot.object(query)) : {};
      if(query.status !== undefined && query.status == Status.DELETED){
       // if(!(query.status == -1)){
          console.log('For Deleteeee status = -1')
       // }
      } else {
        query = query ? await createMongoSearchQuery(DTO, query) : {};
        console.log("q33333333333", query, new DTO());
      }
     
     
      let d = await this._service.findAll(query, options);
      let pagination = normalizePaginateResult({
        total: d.total,
        limit: d.limit,
        page: d.page,
        pages: d.pages,
      });
      let data = d.docs.map((data) => {
        return plainToClass(DTO, data, { excludeExtraneousValues: true });
      });
      return success({
        docs: data,
        ...pagination,
      });
    }
    //Todo: Add  @Query() query the same way as done in FindList(@Request() req, @Query() query)
    @Get(":idOrCode")
    async findOne(@IdOrCodeParser("idOrCode") idOrCode: string, @Query() query) {
      routeAvailability(BASEROUTES.FINDONE);
      console.log('q55555555555555555',query)
      const d = await this._service.findOne(idOrCode, query);
      const _data = plainToClass(DTO, d, { excludeExtraneousValues: true });
      return success(_data);
    }

    @Post()
    public async create(
      @Request() req,
      @Body(AbstractClassTransformerPipe(CreateDTO)) body: any,
      @RequestUser() user
    ) {
      routeAvailability(BASEROUTES.CREATE);
      if (user) {
        body.createdBy = user.code;
        body.formattedUserId = user.formattedId;
      }
      let data = await this._service.create(body,user);
      data = plainToClass(DTO, data, { excludeExtraneousValues: true });
      return success(data);
    }

    @Put(":idOrCode")
    public async update(
      @IdOrCodeParser("idOrCode") idOrCode: string,
      @LastUpdatedTime() lastUpdatedTime: number,
      @Body(AbstractClassTransformerPipe(UpdateDTO)) updateDto,
      @RequestUser() user
    ) {
      if (user) {
        updateDto.updatedBy = user.code;
      }
      routeAvailability(BASEROUTES.UPDATEONE);
      let data = await this._service.edit(
        idOrCode,
        updateDto,
        lastUpdatedTime,
        user
      );
      data = plainToClass(DTO, data, { excludeExtraneousValues: true });
      return success(data);
    }

    @Delete(":idOrCode")
    public async delete(@IdOrCodeParser("idOrCode") idOrCode: string) {
      routeAvailability(BASEROUTES.DETELEONE);
      let data = await this._service.remove(idOrCode);
      data = plainToClass(DTO, data, { excludeExtraneousValues: true });
      return success(data);
    }
  }
  return AbstractController;
}
