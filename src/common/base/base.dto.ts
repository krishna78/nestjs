import { Expose } from "class-transformer";
import { IsNotEmpty, IsNumber } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";
import * as random from "randomstring";

// IsDefined -> required
// IsNotEmpty -> not null or empty if defined
export class Creator {
  constructor(autoGenerateTheCode: boolean = false) {
    if (autoGenerateTheCode) {
      this.code = random.generate({
        length: 12,
        capitalization: "lowercase",
        charset: "alphanumeric",
      });
    }
  }

  @IsNotEmpty()
  @ApiModelProperty()
  code: string = null;

  @ApiModelProperty()
  @IsNumber()
  status: number = 0;
}

export class Updater {
  @ApiModelProperty()
  @IsNumber()
  status: number;
}

export class Reader {
  @Expose()
  readonly id: string = "";

  @Expose()
  readonly status: number = 0;

  @Expose()
  readonly code: string = "";

  @Expose()
  readonly createdTime: Date;

  @Expose()
  readonly updatedTime: Date;
}
