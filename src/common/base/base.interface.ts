import { Type } from "@nestjs/common";
import { BASEROUTES } from "../constants/enum";

export interface AbstractControllerOptions<T> {
  DTO: Type<T>;
  CreateDTO?: Type<T>;
  UpdateDTO?: Type<T>;
  DisabledRoutes?: BASEROUTES[];
}
