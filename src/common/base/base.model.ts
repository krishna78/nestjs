// TODO: add a functionality to get deleted item, by overriding this.status: -1 in this.pre
import * as util from "util";
import { Document, Schema } from "mongoose";
import * as mongoose from "mongoose";
import { Status } from "../constants/enum";
import * as mongoosePaginate from "mongoose-paginate";

mongoose.set("debug", true);

const EntitySchema = function (...args: any[]) {
  Schema.apply(this, args);
  this.plugin(mongoosePaginate);
  this.pre("save", function (next) {
    this.set({ updatedTime: new Date() });
    next();
  });

  this.pre("find", function (next) {
   /*
    var _a = this;
    
    var conditions = this.getQuery()
    console.log('BeforeAll........',
    _a,
    conditions,conditions.status)
    if(_a._conditions !== undefined){
      if(_a._conditions.status !== undefined && _a._conditions.status !== 0){
        //_a.setQuery({ status: { $lte: Status.DISABLED }, ...this.getQuery() });
        _a._conditions.status = { $lte: Status.DISABLED }
        this.setOptions({ projection: {}})
       // _a.set({'$useProjection': false})
        console.log('afterre........',this.getQuery())
       return next()
        //this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });
      }
    }
    */
   // this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });

   //-- next();
  // -- this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });
    var user = this;
    //let model = {model:CountriesStates}
    //  console.log('Prefind..........',user,'nameeeeeeeee',user.model,user.schema.options.collection)
    // let tcheck = (user.model == "CountriesStates")
    let tcheck = user.schema.options.collection == "mTwo";
    //  if(user.options.collection == "CountriesStates"){
    console.log(tcheck);
    // if(user.model == "CountriesStates"){
    if (tcheck) {
      return next();
    }
    this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });
    next();
  });

  this.pre("findOne", function (next) {
    var _a = this;
    
    var conditions = _a.getQuery()
    console.log('Before........',
    //_a,
    conditions,conditions.status)
    if(_a._userProvidedFields !== undefined){
      if(_a._userProvidedFields.status == -1){
        _a.setQuery({ status: { $lte: Status.DISABLED }, ...this.getQuery() });
        this.setOptions({ projection: {}})
       // _a.set({'$useProjection': false})
        console.log('afterre........',this.getQuery())
       return next()
        //this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });
      }
    }
    this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });

    next();
  });

  this.pre("findOneAndDelete", function (next) {
    this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });
    this.set({ updatedTime: new Date() });
    next();
  });

  this.pre("findOneAndRemove", function (next) {
    this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });
    this.set({ updatedTime: new Date() });
    next();
  });

  this.pre("findOneAndUpdate", function (next) {
    this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });
    this.set({ updatedTime: new Date() });
    next();
  });

  this.pre("update", function (next) {
    this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });
    this.set({ updatedTime: new Date() });
    next();
  });

  this.pre("updateOne", function (next) {
    this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });
    this.set({ updatedTime: new Date() });
    next();
  });

  this.pre("updateMany", function (next) {
    this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });
    this.set({ updatedTime: new Date() });
    next();
  });
};

util.inherits(EntitySchema, Schema);

export const createModel = (collection: string, schema: Object) => {
  debugger;
  return new EntitySchema(
    {
      code: {
        type: String,
        unique: true,
      },
      createdTime: { type: Number, default: Date.now },
      updatedTime: { type: Number, default: Date.now },
      status: {
        type: Number,
        default: Status.DISABLED,
      },
      // accessGroups: {type: [String]}
      createdBy: {
        type: String,
        default: "system",
      },
      formattedUserId: {
        type: String,
      },
      ...schema,
      updatedBy: {
        type: String,
        default: "system",
      },
    },
    {
      collection,
      toObject: {
        getters: true,
      },
      toJSON: {
        getters: true,
        virtuals: true,
        versionKey: false,
        transform: (doc, ret) => {
          delete ret._id;
        },
      },
      timestamps: {
        createdAt: false,
        updatedAt: false,
      },
    }
  );
};

export class Entity {
  code: string;
  createdTime?: number;
  updatedTime?: number;
  status?: number;
}

export interface IEntity extends Entity, Document {}
