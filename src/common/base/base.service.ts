import { NotFoundException } from "@nestjs/common";
import { Model } from "mongoose";
import * as moment from "moment";
import { APP_ENV, Status } from "../constants/enum";
import { NODE_ENV } from "../constants/config";
import { NOT_FOUND, NOT_SYNCED } from "../constants/string";
import { UniqueValidator } from "../validator/unique.validator";
import * as _ from "lodash";
import * as flatter from "flat";

import {
  HasPreCreate,
  HasPostCreate,
  HasPreUpdate,
  HasPostUpdate,
  HasPreDelete,
  HasPostDelete,
  HasPreGetAll,
} from "./pre.post.action";
import { SetFormattedId, getFormatedId } from "./set.formattedId";
import { CanLoadVirtual } from "../interfaces/can.load.virtual";
import { Paginate } from "../interfaces/pagination";
import { User } from "src/users/objects/user.schema";
export abstract class BaseService<T> extends UniqueValidator {
  private PreCreate = !!(<HasPreCreate>(<any>this)).doPreCreate
    ? <HasPreCreate>(<any>this)
    : null;
  private PreGetAll = !!(<HasPreGetAll>(<any>this)).doPreGetAll
    ? <HasPreGetAll>(<any>this)
    : null;
  private PostCreate = !!(<HasPostCreate>(<any>this)).doPostCreate
    ? <HasPostCreate>(<any>this)
    : null;
  private PreUpdate = !!(<HasPreUpdate>(<any>this)).doPreUpdate
    ? <HasPreUpdate>(<any>this)
    : null;
  private PostUpdate = !!(<HasPostUpdate>(<any>this)).doPostUpdate
    ? <HasPostUpdate>(<any>this)
    : null;
  private PreDelete = !!(<HasPreDelete>(<any>this)).doPreDelete
    ? <HasPreDelete>(<any>this)
    : null;
  private PostDelete = !!(<HasPostDelete>(<any>this)).doPostDelete
    ? <HasPostDelete>(<any>this)
    : null;
  private CanLoadVirtual = !!(<CanLoadVirtual>(<any>this)).getVirtualForItem
    ? <CanLoadVirtual>(<any>this)
    : null;
  private SetFormattedId = !!(<SetFormattedId>(<any>this)).getPrefixForId
    ? <SetFormattedId>(<any>this)
    : null;

  private readonly validVirtual = [];

  protected constructor(baseModel: Model) {
    // debugger;
    super(baseModel);

    this.setVirtual();
  }

  async findAll(query: any, options?: Paginate) {
    if (this.PreGetAll) {
      await this.PreGetAll.doPreGetAll(query);
    }

    options.populate = this.CanLoadVirtual
      ? [...this.CanLoadVirtual.getVirtualForList()]
      : null;
    return await this.baseModel.paginate(query, options);
    // .populate(
  }

  async findOne(idOrCode: string,query?: string) {
    let findWithIdOrCodeQuery = {
      $or: [
        {
          _id: idOrCode,
        },
        {
          code: idOrCode,
        },
      ],
    } as any;

    const one = await this.baseModel
     // .findOne(findWithIdOrCodeQuery, {})
      .findOne(findWithIdOrCodeQuery, query)
      .populate(
        this.CanLoadVirtual
          ? [...this.CanLoadVirtual.getVirtualForItem()]
          : null
      );
    if (!one) {
      throw new NotFoundException(NOT_FOUND);
    }
    return one;
  }

  async create(createDto: any,user?: any) {
    if (user) {
      createDto.createdBy = user.code;
      createDto.formattedUserId = user.formattedId;
    }
    await this.validate(createDto);
    if (this.PreCreate) {
      await this.PreCreate.doPreCreate(createDto);
    }
    if (this.SetFormattedId) {
      createDto.formattedId = await getFormatedId(
        this.baseModel,
        this.SetFormattedId.getPrefixForId(createDto),
        this.SetFormattedId.getQuery(createDto),
        this.SetFormattedId.formattedIdLength
      );
    }

    let created = await this.baseModel(createDto);
    await created.save();
    if (this.PostCreate) {
      await this.PostCreate.doPostCreate(created, user);
    }
    return await this.findOne(created.code);
  }

  async edit(
    idOrCode: string,
    editDto: any,
    lastUpdatedTime?: number,
    user?: any
  ) {
    debugger;
    await this.validate(editDto, idOrCode);
    let model = await this.findOne(idOrCode);
    if (
      NODE_ENV !== APP_ENV.development &&
      !moment(model.updatedTime).isSame(moment(lastUpdatedTime))
    ) {
      // throw new Error(NOT_SYNCED);
    }

    if (this.PreUpdate) {
      debugger;
      await this.PreUpdate.doPreUpdate(idOrCode, editDto, model, user);
    }

    editDto = flatter.flatten(editDto);
    await this.baseModel.updateOne(
      {
        code: model.code,
      },
      {
        $set: {
          ...editDto,
        },
      }
    );
    model = await this.findOne(idOrCode);
    if (this.PostUpdate) {
      debugger;
      await this.PostUpdate.doPostUpdate(idOrCode, model, user);
    }
    return model;
  }

  async remove(idOrCode: string) {
    let model = await this.findOne(idOrCode);
    if (this.PreDelete) {
      await this.PreDelete.doPreDelete(idOrCode, model);
    }
    model.status = Status.DELETED;
    await model.save();
    if (this.PostDelete) {
      await this.PostDelete.doPostDelete(idOrCode, model);
    }
    return model;
  }

  async findOneByQuery(query) {
    // TODO :make strong type
    return this.baseModel.findOne(query);
  }

  private setVirtual() {
    if (!this.CanLoadVirtual) {
      return;
    }
    let key: string;
    let value: any;

    for ([key, value] of Object.entries(this.baseModel.schema.virtuals)) {
      if (value["options"]["localField"] && value["options"]["foreignField"]) {
        this.validVirtual.push(key);
      }
    }
    let virtualForItem = this.CanLoadVirtual.getVirtualForItem().concat(
      this.CanLoadVirtual.getVirtualForList()
    );
    let unWantedOrTypo = _.difference(virtualForItem, this.validVirtual);
    if (unWantedOrTypo[0]) {
      throw new Error(
        `Virtual ${unWantedOrTypo.toString()} doesnot exist in model ${
          this.baseModel.modelName
        }, Loaded from ${this.constructor.name}`
      );
    }
  }
}
