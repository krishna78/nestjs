import { Creator, Updater } from "./base.dto";
import { Entity } from "./base.model";

export interface HasPreGetAll {
  doPreGetAll(query: any): Promise<void>;
}

export interface HasPreCreate {
  doPreCreate(createDto: Partial<Creator>): Promise<void>;
}

export interface HasPostCreate {
  doPostCreate(model: Entity,user?: any): Promise<void>;
}

export interface HasPreUpdate {
  doPreUpdate(
    idOrCode: string,
    editDto: Partial<Updater>,
    model: Entity,
    user: any
  ): Promise<void>;
}

export interface HasPostUpdate {
  doPostUpdate(idOrCode: string, model: Entity, user: any): Promise<void>;
}

export interface HasPreDelete {
  doPreDelete(idOrCode: string, model: Entity): Promise<void>;
}

export interface HasPostDelete {
  doPostDelete(idOrCode: string, model: Entity): Promise<void>;
}
