import { ApiModelProperty } from "@nestjs/swagger";

export class ErrorDto {
  @ApiModelProperty()
  code: string;

  @ApiModelProperty()
  message: string;

  @ApiModelProperty()
  lang: string;

  @ApiModelProperty()
  exception: string;

  @ApiModelProperty()
  traceId: string;

  @ApiModelProperty()
  helpUrl: string;

  @ApiModelProperty()
  path: string;

  @ApiModelProperty()
  timestamp: string;
}
