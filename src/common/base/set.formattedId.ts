import { Model } from "mongoose";
import { Creator } from "./base.dto";

export interface SetFormattedId {
  getQuery(model: Partial<Creator>): Object | null;
  getPrefixForId(model: Partial<Creator>): string;
  formattedIdLength: number;
}

const pad = (n, width, z) => {
  z = z || "0";
  n = n + "";
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

export const getFormatedId = async (
  model: Model,
  prefix: string,
  query: Object | null,
  formattedIdLength: number
) => {
  // debugger;
  query = query || {};
  let count = await model.countDocuments(query);
  return prefix + pad(count + 1, formattedIdLength, 0);
};
