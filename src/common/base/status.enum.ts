export enum Status {
  DELETED = -1,
  DISABLED = 0,
  ACTIVE = 1,
}
