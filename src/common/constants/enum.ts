export const enum Status {
  DELETED = -1,
  DISABLED = 0,
  ACTIVE = 1,
}

export const enum APP_ENV {
  development = "development",
  production = "production",
}

export const enum BASEROUTES {
  FINDLIST = 1,
  FINDONE = 2,
  CREATE = 3,
  UPDATEONE = 4,
  DETELEONE = 5,
  PATCH = 6,
}

export const enum EMAILSTATUS {
  PENDING = 0,
  FAILED = -1,
  SUCCESS = 1,
}

export enum SME {
  TECH_SME_IN_SINGAPORE = "Tech SME Inside Singapore",
  TECH_SME_OUTSIDE_SINGAPORE = "Tech SME Outside Singapore",
}
export enum INVESTOR {
  TECH_COMPANY_IN_SINGAPORE = "Tech Company Inside Singapore",
  TECH_COMPANY_OUTSIDE_SINGAPORE = "Tech Company Outside Singapore",
  FINANCIAL_INSTITUTIONS = "Financial Institutions(VCs, Hedge Funds)",
  HIGH_NET_WORTH_INDIVIDUALS = "High Net Worth Individuals",
}

export enum USER_TYPES {
  SME = "Sme",
  SPONSOR = "Sponsor",
  //INVESTOR = "investor",
  ADMIN = "Admin",
  OPERATIONS_TEAM = "Operations"
}

export enum PROJECT_STATUS {
  DELETED = -1,
  PENDING = 0,
  APPROVED = 1,
  FUNDED = 2,
  REJECTED = 3,
  COMPLETED = 4,
  DOCUMENTSREQUIRED = 5,
}

export enum BLOCKED_ACCOUNT_TYPE {
  USER_REGISTRATION = "User registration",
  FORGOT_PASSWORD = "Forgot password"
}

export enum BID_STATUS {
  DELETED = -1,
  PENDING = 0,
  APPROVED = 1,
  REJECTED = 2,
}
//rolesAccessAction 
export enum ROLES_ACCESS_ACTION {
  USERS_CONTROLLER_FINDLIST_OPERATIONS = "users.controller.findList_operations",
  USERS_CONTROLLER_FINDLIST_ADMIN = "users.controller.findList_admin",
  USERS_CONTROLLER_FIND_ONE = "users.controller.findOne",
  USERS_CONTROLLER_KYC_FILTER = "users.controller.findListFilterKYCStatus",
  USERS_CONTROLLER_USER_STATUS_FILTER = "users.controller.findListFilterUserStatus",
  USERS_CONTROLLER_USER_UPDATE = "users.controller.update",
  USERS_CONTROLLER_DELETE = "users.controller.delete",
  USERS_SERVICE_CHECK_FOR_UPDATE_STATUS_ERROR = "users.service.checkForUpdateStatusError",
  USERS_SERVICE_CREATE = "users.service.create",
  USERS_SERVICE_UPDATE_USER_CRMID_AND_ENTITYDETAILCODE = "users.service.updateUserCrmIdAndEntityDetailCode",
  REMARKS_CONTROLLER_CREATE = "remarks.controller.create",
  REMARKS_CONTROLLER_FINDLIST = "remarks.controller.findList",
  REMARKS_CONTROLLER_FINDLIST_SME = "remarks.controller.findList_sme",
  REMARKS_CONTROLLER_FINDLIST_SPONSOR = "remarks.controller.findList_sponsor",
  SME_PROJECT_CONTROLLER_FINDLIST = "sme-project.controller.findList",
  SME_PROJECT_CONTROLLER_FINDONE = "sme-project.controller.findOne",
  SME_PROJECT_CONTROLLER_RECOMMENDED_PROJECTS = "sme-project.controller.getRecommendedProjects",
  SME_PROJECT_CONTROLLER_CREATE = "sme-project.controller.create",
  SME_PROJECT_CONTROLLER_SPONSOR_FILTER = "sme-project.controller.projectSponsorFilter",
  SME_PROJECT_CONTROLLER_UPDATE = "sme-project.controller.update",
  BID_DETAILS_CONTROLLER_FINDLIST = "bid-details.controller.findList",
  BID_DETAILS_CONTROLLER_FINDLIST_SME = "bid-details.controller.findList_sme",
  BID_DETAILS_CONTROLLER_FINDLIST_SPONSOR = "bid-details.controller.findList_sponsor",
  BID_DETAILS_CONTROLLER_CREATE =  "bid-details.controller.create",
  BID_DETAILS_CONTROLLER_COMPLETE_BID_PROCESS = "bid-details.controller.completeBidProcess",
  BID_DETAILS_CONTROLLER_REJECT_ALL_BIDS_DELETE_PROJECT = "bid-details.controller.rejectAllBidsDeleteProject",
  BID_DETAILS_CONTROLLER_UPDATE = "bid-details.controller.update",
  BID_DETAILS_CONTROLLER_UPDATE_SPONSOR = "bid-details.controller.update_sponsor",
  BID_DETAILS_SERVICE_CALCULATE_BID_DETAILS = "bid-details.controller.calculatebiddetails",
  BID_DETAILS_CONTROLLER_CREATE_TRANSACTION = "bid-details.controller.createTransaction",
  //BID_DETAILS_CONTROLLER_GET_FUNDED_PROJECTS = "bid-details.controller.getfundedProjects",
  USER_PORTAL_REMARKS_CONTROLLER_CREATE = "user-portal-remarks.controller.create",
  USER_PORTAL_REMARKS_CONTROLLER_FINDLIST = "user-portal-remarks.controller.findList",
  USER_PORTAL_REMARKS_CONTROLLER_FINDLIST_SME = "user-portal-remarks.controller.findList_sme",
  USER_PORTAL_REMARKS_CONTROLLER_FINDLIST_SPONSOR = "user-portal-remarks.controller.findList_sponsor",
  FILE_UPLOAD_DELETE = "file-upload.controller.delete",
  AUTH_CONTROLLER_UNBLOCK_USER_ACCOUNT = "auth.controller.unblockUserAccount"
}

export enum BID_RATIO {
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_1_10 = 0.1,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_2_10 = 0.2,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_3_10 = 0.3,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_4_10 = 0.4,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_5_10 = 0.5,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_6_10 = 0.6,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_7_10 = 0.7,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_8_10 = 0.8,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_9_10 = 0.9,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_1_1 = 1,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_2_1 = 2,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_3_1 = 3,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_4_1 = 4,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_5_1 = 5,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_6_1 = 6,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_7_1 = 7,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_8_1 = 8,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_9_1 = 9,
  EQUIVALENT_BID_POINTS_TO_FUNDS_REQUESTED_10_1 = 10,
}

export enum REMARKS {
  INTERNAL_REMARKS = "Internal Remarks",
  CUSTOMER_VIEW_REMARKS = "Customer View Remarks",
}

export enum OTP_RETRY_LIMIT {
  MAXIMUM_OTP_RETRY_LIMIT = 9
}

export enum REMARKS_SUBTYPE {
  STATUS_COLUMN = "Status Column",
  EDIT_COLUMN = "Edit Column",
}

export enum PROJECT_TENURE {
  LESS_THAN_SIX_MONTHS = 6,
  SIX_MONTHS_TO_ONE_YEAR = 12,
  MORE_THAN_ONE_YEAR = 12,
}

export enum KYC_VERIFICATION_STATUS {
  DELETED = -1,
  PENDING = 0,
  APPROVED = 1,
  REJECTED = 2,
  //IN_REVIEW = 0,
  //ACTIVE = 1,
  //KYC_INCOMPLETE = 2,
  //CLOSED = 3,
}

export enum USER_STATUS {
  DELETED = -1,
  ACTIVE = 0,
  IN_REVIEW = 2,
  CLOSED = 1,
  KYC_INCOMPLETE = 3,
}

export enum OWNER_VERIFIED_STATUS {
  DELETED = -1,
  PENDING = 0,
  APPROVED = 1,
}

export enum PRACTICE_AREA {
  HARDWARE = "Hardware",
  SOFTWARE = "Software",
  NETWORK = "Network",
  INFRASTRUCTURE = "Infrastructure",
  CYBER_SECURITY = "Cyber security",
  COMBINATION = "Combination",
}

export enum PROJECT_TYPE {
  GOVERNMENT = "Govt.",
  PRIVATE = "Private",
  OTHERS = "Others",
}

export enum PROJECT_COMPLEXITY {
  SIMPLE = "Simple",
  MEDIUM = "Medium",
  COMPLEX = "Complex",
}

export enum TRANSACTION_TYPE {
  CREDIT = "credit",
  DEBIT = "debit",
}
export enum DOCUMENTS_UPLOADS {
  CERTIFICATE_OF_INCORPORATION = "certificateOfIncorporation",
  COMPANY_CONSTITUTION = "companyConstitution",
  FINANCIAL_STATEMENTS = "financialStatements",
  MANAGEMENT_ACCOUNTS_FOR_THE_CURRENT_YEAR = "managementAccountsForTheCurrentYear",
  LATEST_ACRA_BUSINESS_PROFILE = "latestACRABusinessProfile",
  NRIC_AND_PASSPORT_OF_DIRECTORS_OR_SHAREHOLDERS = "nRICAndPassportOfDirectorsOrShareholders",
  PROOF_OF_ADDRESS_FOR_COMPANY_DIRECTORS = "proofOfAddressForCompanyDirectors",
  BANK_STATEMENT = "bankStatement",
  PROOF_OF_ADDRESS = "proofOfAddress",
  OTHERS = "others",
}

export enum OTP_EMAIL_VERIFICATION_STATUS {
  FALSE = "false",
  TRUE = "true",
}

export enum TIMELINE_PREFERENCE_0 {
  LESS_THAN_SIX_MONTHS = "< 6 months",
  SIX_MONTHS_TO_ONE_YEAR = "6 months to 1 year",
  GREATER_THAN_ONE_YEAR = "> 1 year",
}

export enum INVESTMENT_RANGE {
LESS_THAN_SGD_10000 = "Less than SGD 10000",
SGD_10000_TO_SGD_50000 = "SGD 10000 – SGD 50000",
SGD_50000_TO_SGD_100000 = "SGD 50000 – SGD 100000",
GREATER_THAN_SGD_100000 = "Greater than SGD 100000"
}

export enum ACTIVITY_FEED_EVENTS_SPONSOR {
  SPONSOR_REGISTRATION = "Sponsor Registration",
  ONBOARDING_COMPLETION = "Onboarding completion",
  KYC_VERIFICATION_NOTIFICATION_SUCCESSFUL = "KYC Verification  (Successful)",
  KYC_VERIFICATION_NOTIFICATION_PENDING = "KYC Verification  (Pending & requesting for documents)",
  KYC_VERIFICATION_NOTIFICATION_REJECTED = "KYC Verification (Rejected & reason for rejection)",
  PROFILE_CHANGES = "Profile Changes - editing the profile",
  WALLET_BALANCE_INITIAL_BALANCE = "Wallet Balance - Initial Balance",
  WALLET_MONEY_ADDED = "Wallet money added",
  WALLET_MONEY_WITHDRAWN = "Wallet money withdrawn",
  PROJECTS_VIEWED_BY_SPONSOR = "Projects Viewed by Sponsor",
  PROJECTS_SHOWN_INTEREST = "Projects shown interest",
  PROJECTS_ALLOTTED_SUCCESSFULLY_MATCHED_WITH_A_SPONSOR = "Projects allotted - Successfully matched with a sponsor",
  PROJECTS_REJECTED = "Projects rejected",
  PROJECTS_PENDING_WITH_SUPER_ADMIN = "Projects pending with super admin",
  PROJECTS_FUNDED = "Projects Funded",
  AMOUNT_FUNDED_FOR_PROJECT = "Amount funded for project",
  REPAYMENT_BY_SME = "Repayment by SME",
}

export enum ACTIVITY_FEED_EVENTS_SPONSOR_PRIORITY {
  SPONSOR_REGISTRATION = 2,
  ONBOARDING_COMPLETION = 1,
  KYC_VERIFICATION_NOTIFICATION_SUCCESSFUL = 1,
  KYC_VERIFICATION_NOTIFICATION_PENDING = 1,
  KYC_VERIFICATION_NOTIFICATION_REJECTED = 1,
  PROFILE_CHANGES = 3,
  WALLET_BALANCE_INITIAL_BALANCE = 2,
  WALLET_MONEY_ADDED = 1,
  WALLET_MONEY_WITHDRAWN = 1,
  PROJECTS_VIEWED_BY_SPONSOR = 3,
  PROJECTS_SHOWN_INTEREST = 2,
  PROJECTS_ALLOTTED_SUCCESSFULLY_MATCHED_WITH_A_SPONSOR = 1,
  PROJECTS_REJECTED = 1,
  PROJECTS_PENDING_WITH_SUPER_ADMIN = 2,
  PROJECTS_FUNDED = 1,
  AMOUNT_FUNDED_FOR_PROJECT = 1,
  REPAYMENT_BY_SME = 1,
}

export enum ACTIVITY_FEED_EVENTS_SME {
  SME_REGISTRATION = "Sme Registration",
  ONBOARDING_COMPLETION = "Onboarding completion",
  KYC_VERIFICATION_NOTIFICATION_SUCCESSFUL = "KYC Verification  (Successful)",
  KYC_VERIFICATION_NOTIFICATION_PENDING = "KYC Verification  (Pending & requesting for documents)",
  KYC_VERIFICATION_NOTIFICATION_REJECTED = "KYC Verification (Rejected & reason for rejection)",
  PROFILE_CHANGES = "Profile Changes - editing the profile",
  NEW_PROJECT = "Creating new project",
  PROJECT_STATUS = "Project Status",
  PROJECT_APPROVED_TO_MARKETPLACE = "Project approved to marketplace",
  PROJECT_REJECTED = "Project rejected",
  PROJECT_PENDING_WITH_SUPER_ADMIN = "Project pending",
  // PROJECTS_FUNDED = "Projects Funded",
  PROJECT_DOCUMENT_REQUIRED = "Project documents required",
  PROJECT_VIEWED_BY_SPONSOR = "Project viewed by sponsor",
  PROJECT_SHOWN_INTEREST = "Project shown interest",
  PROJECTS_ALLOTTED_SUCCESSFULLY_MATCHED_WITH_A_SPONSOR = "Projects allotted - Successfully matched with a sponsor",
  PROJECTS_FUNDED = "Projects Funded",
  AMOUNT_FUNDED_FOR_PROJECT = "Amount funded for project",
  REPAYMENT_BY_SME = "Repayment by SME",
}
export enum BIGCHAINDB_TRANSACTION_TYPE {
  CYCLOAN_MAIN_OWNER = "Cycloan-Sponsor",
  SME_VERIFIED = "Sme-Verified-User",
  SPONSOR_VERFIED = "Sponsor-Verified-User",
  PROJECT_VERIFIED = "Project-Verified",
  LOAN_SMART_CONTRACT = "Loan-Smart-Contract-Verified"
}

export enum ACTIVITY_FEED_EVENTS_SME_PRIORITY {
  SME_REGISTRATION = 2,
  ONBOARDING_COMPLETION = 1,
  KYC_VERIFICATION_NOTIFICATION_SUCCESSFUL = 1,
  KYC_VERIFICATION_NOTIFICATION_PENDING = 1,
  KYC_VERIFICATION_NOTIFICATION_REJECTED = 1,
  PROFILE_CHANGES = 3,
  NEW_PROJECT = 2,
  PROJECT_STATUS = 1,
  PROJECT_APPROVED = 1,
  PROJECTS_REJECTED = 1,
  PROJECTS_PENDING_WITH_SUPER_ADMIN = 2,
  //PROJECTS_FUNDED = 1,
  PROJECT_DOCUMENT_REQUIRED = 2,
  PROJECT_VIEWED_BY_SPONSOR = 2,
  PROJECT_SHOWN_INTEREST = 2,
  PROJECTS_ALLOTTED_SUCCESSFULLY_MATCHED_WITH_A_SPONSOR = 1,
  PROJECTS_FUNDED = 1,
  AMOUNT_FUNDED_FOR_PROJECT = 1,
  REPAYMENT_BY_SME = 1,
}
/*
export enum ACTIVITY_FEED_EVENTS_SPONSOR_PRIORITY {
  SPONSOR_REGISTRATION = 2,
  ONBOARDING_COMPLETION = 1,
  KYC_VERIFICATION_NOTIFICATION_SUCCESSFUL = 1,
  KYC_VERIFICATION_NOTIFICATION_PENDING = 1,
  KYC_VERIFICATION_NOTIFICATION_REJECTED = 1,
  PROFILE_CHANGES = 3,
  WALLET_BALANCE_INITIAL_BALANCE = 2,
  WALLET_MONEY_ADDED = 1,
  WALLET_MONEY_WITHDRAWN = 1,
  PROJECTS_VIEWED_BY_SPONSOR = 3,
  PROJECTS_SHOWN_INTEREST = 2,
  PROJECTS_ALLOTTED_SUCCESSFULLY_MATCHED_WITH_A_SPONSOR = 1,
  PROJECTS_REJECTED = 1,
  PROJECTS_PENDING_WITH_SUPER_ADMIN = 2,
  PROJECTS_FUNDED = 1,
  AMOUNT_FUNDED_FOR_PROJECT = 1,
  REPAYMENT_BY_SME = 1,
}
*/
