import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
} from "@nestjs/common";
import { Request, Response } from "express";
import { ErrorDto } from "../base/response.dto";

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    console.debug(
      "HttpExceptionFilter",
      exception.name,
      JSON.stringify(exception)
    );

    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    let status = HttpStatus.INTERNAL_SERVER_ERROR;
    let error = new ErrorDto();

    error.path = request.url;
    error.timestamp = new Date().toISOString();
    error.exception = JSON.stringify(exception);

    let message = exception.message as any;

    if (exception.getResponse && exception.getStatus) {
      status = exception.getStatus();
      error.message = message.message || message.error || message;
    } else {
      error.message =
        message.error || exception.message || "Internal Server Error";
    }

    // TODO: Find a better approach to set the error code
    // error.code = status === HttpStatus.BAD_REQUEST ? 'bad_request' : 'internal_error';
    switch (status) {
      case HttpStatus.BAD_REQUEST:
        error.code = "bad_request";
        break;
      case HttpStatus.UNAUTHORIZED:
        error.code = "unauthorized";
        break;
      case HttpStatus.NOT_FOUND:
        error.code = "not_found";
        break;
      default:
        error.code = "internal_error";
    }

    response.status(status).json({
      success: false,
      error,
    });
  }
}
