export interface CanLoadVirtual {
  getVirtualForList(): string[];
  getVirtualForItem(): string[];
}
