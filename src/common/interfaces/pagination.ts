import { INVALID_VALUE } from "../constants/string";

export interface Paginate {
  select?: string | Object;
  sort: string | Object;
  populate?: string | Object;
  // lean?: boolean,
  // leanWithId?: boolean,
  // offset: number
  limit?: number;
  page: number;
}

const paginated: Paginate = {
  select: "",
  sort: "",
  populate: "",
  limit: 1,
  page: 1,
};

export const normalizePagination = (query, options) => {
  Object.keys(query).forEach((q) => {
    Object.keys(paginated).forEach((key) => {
      if (key === q) {
        if (typeof paginated[key] === "number") {
          options[key] = Number(query[q]);
        } else {
          options[key] = query[q];
        }
        delete query[q];
      }
    });
  });
  if (options["page"] <= 0) {
    throw new Error(INVALID_VALUE("Page", "Zero or less"));
  }
};

export const normalizePaginateResult = (result: {
  total;
  limit;
  page;
  pages;
}) => {
  return {
    total: result.total,
    page: result.page,
    // Circular pagination for next and previous page
    nextPage: result.limit * result.page < result.total ? result.page + 1 : 1,
    prevPage:
      result.pages >= result.page && result.page !== 1
        ? result.page - 1
        : Math.ceil(result.total / result.limit),
  };
};
