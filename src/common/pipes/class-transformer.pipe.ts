import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  BadRequestException,
  Type,
  mixin,
  Logger,
} from "@nestjs/common";
import { validate } from "class-validator";
import { plainToClass } from "class-transformer";
import { asyncForEach, normalizeObject } from "../utils/helper";
import { memoize } from "lodash";

@Injectable()
export class ClassTransformerPipe implements PipeTransform<any> {
  protected readonly logger = new Logger(ClassTransformerPipe.name, true);

  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }

    const object = plainToClass(metatype, value, {
      excludeExtraneousValues: false,
    });

    const errors = await validate(object, {
      whitelist: true,
      forbidNonWhitelisted: true,
      forbidUnknownValues: true,
      skipUndefinedProperties: true,
    });
    let errorObject = {};
    await asyncForEach(errors, async (e) => {
      if (e.children && e.children.length > 0) {
        await asyncForEach(e.children, async (e2) => {
          let err = await this.recursiveChild(e.property, e2);
          errorObject = {
            ...errorObject,
            ...err,
          };
        });
      } else {
        this.logger.error("e", JSON.stringify(e));
        if (e.constraints) {
          errorObject[e.property] = Object.values(e.constraints)
            .join()
            .toLowerCase();
        }
      }
    });

    if (errors.length > 0) {
      throw new BadRequestException(JSON.stringify(errorObject));
    }
    return normalizeObject(object);
  }

  private async recursiveChild(parentKey: string, value): Promise<any> {
    this.logger.error("recursiveChild", JSON.stringify(value));
    let errorObject = {};
    errorObject[parentKey] = {};
    if (value.children && value.children.length > 0) {
      await asyncForEach(value.children, async (e) => {
        errorObject[parentKey][e.property] = Object.values(e.constraints)
          .join()
          .toLowerCase();
        return errorObject;
      });
    }
    if (value.constraints) {
      errorObject[parentKey][value.property] = Object.values(value.constraints)
        .join()
        .toLowerCase();
    }
    return errorObject;
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object];
    return !types.includes(metatype);
  }
}

function createClassTransformPipe<T>(itemType: Type<T>): Type<PipeTransform> {
  class AbstractionClassTransformerPipe extends ClassTransformerPipe
    implements PipeTransform<any> {
    async transform(values: any, metadata: ArgumentMetadata) {
      metadata = { ...metadata, ...{ metatype: itemType } };
      return super.transform(values, metadata);
    }
  }

  return mixin(AbstractionClassTransformerPipe);
}

export const AbstractClassTransformerPipe: <T>(
  itemType: Type<T>
) => Type<PipeTransform> = memoize(createClassTransformPipe);
