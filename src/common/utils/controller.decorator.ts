import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { isAdmin } from "../../users/objects/user.schema";

export const IdOrCodeParser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return request.params.idOrCode;
  }
);

export const LastUpdatedTime = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return Number(request.header("lastUpdated"));
  }
);

export const RequestUser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    let user = request.user;
    if (user) {
      user.isAdmin = isAdmin(user);
    }
    return request.user;
  }
);
