export async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

export function populateQuery(query, key, value, type) {
  //check if range
  if (typeof value == "object") {
    query[key] = value;
    return query;
  }
  if (typeof value === "string") {
    let checkRange = value.split("-");
    if (
      checkRange.length == 2 &&
      !isNaN(Number(checkRange[0])) &&
      !isNaN(Number(checkRange[1]))
    ) {
      //is checking numeric range;
      if (Number(checkRange[0]) > Number(checkRange[1])) {
        query[key] = {
          $lte: Number(checkRange[0]),
          $gte: Number(checkRange[1]),
        };
      } else {
        query[key] = {
          $lte: Number(checkRange[1]),
          $gte: Number(checkRange[0]),
        };
      }
      return query;
    }
  }
  if (type === "number") {
    if (!isNaN(Number(value))) {
      query[key] = Number(value);
    }
  } else if (type === "boolean") {
    return query;
  } else if (value.toString().split(",").length > 1) {
    query[key] = { $in: value.split(",") };
  } else if (!type && !isNaN(Number(value))) {
    query[key] = value;
  } else if (!type) {
    query[key] = new RegExp(`^${value}$`, "i");
  } else {
    query[key] = new RegExp(value, "i");
  }
  return query;
}

export async function createMongoSearchQuery(
  className,
  query
): Promise<object> {
  let a = new className();
  let properties: string[] = Object.getOwnPropertyNames(a);
  //only allow queries on fields with getters
  let search = {};
  if (query.any) {
    let orquery = [];
    for (let k of properties) {
      if (k.indexOf("_") == 0) {
        k = k.slice(1);
      }
      let anyObj = {};
      if (a[k] instanceof Object) {
        let b = a[k];
        if (!Array.isArray(a[k])) {
          let propertiesb: string[] = Object.keys(b);
          propertiesb.forEach((bb) => {
            anyObj = populateQuery({}, `${k}.${bb}`, query.any, typeof b[bb]);
            if (Object.keys(anyObj).length > 0) {
              orquery.push(anyObj);
              anyObj = {};
            }
          });
        } else {
          if (b.length == 0) {
            //an array of primitive type
            anyObj = populateQuery(anyObj, k, query.any, typeof a[k]);
          }
          b.forEach((barray) => {
            let propertiesb: string[] = Object.keys(barray);
            propertiesb.forEach((bb) => {
              anyObj = populateQuery({}, `${k}.${bb}`, query.any, typeof b[bb]);
              if (Object.keys(anyObj).length > 0) {
                orquery.push(anyObj);
                anyObj = {};
              }
            });
          });
        }
      } else {
        anyObj = populateQuery(anyObj, k, query.any, typeof a[k]);
      }
      if (Object.keys(anyObj).length > 0) {
        orquery.push(anyObj);
      }
    }
    if (orquery.length > 0) {
      search["$or"] = orquery;
    }
    delete query["any"];
  }
  for (let [k, v] of Object.entries<string>(a)) {
    if (query[k]) {
      search = populateQuery(search, k, query[k], typeof a[k]);
    }
  }

  return search;
}

export function normalizeObject(object) {
  // TODO: normaize with array
  Object.keys(object).forEach((key) => {
    if (object[key] && typeof object[key] === "object")
      normalizeObject(object[key]);
    else if (object[key] === undefined) delete object[key];
  });
  return object;
}
