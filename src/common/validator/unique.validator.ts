// TODO: add Types in each function
import { Status } from "../constants/enum";
import { EXISTS } from "../constants/string";
import * as dot from "dot-object";
export class UniqueValidator {
  private unique: string[];

  protected constructor(protected readonly baseModel: any) {
    debugger;
    this.unique = Object.keys(baseModel.schema.paths).filter((data) => {
      return baseModel.schema.paths[data].options.unique;
    });
  }

  protected async validate(data: any, idOrCode?: string): Promise<boolean> {
    let query = {
      status: {
        $gte: Status.DISABLED,
      },
    };
    if (idOrCode) {
      query["$and"] = [
        {
          _id: {
            $ne: idOrCode,
          },
          code: {
            $ne: idOrCode,
          },
        },
      ];
    }

    if (this.unique.length > 0) {
      //debugger;
      query["$or"] = [];
      this.unique.forEach((a) => {
        let param = {};
        if (data[a] != void 0) {
          param[a] = dot.pick(a, data);
          query["$or"].push(param);
        }
      });
    }
    if (query["$or"] && query["$or"].length === 0) {
      return true; // nothing to check further
    }

    let exist = await this.baseModel.findOne(query);
    if (!exist) {
      return true;
    }

    let error = [];
    this.unique.forEach((a) => {
      if (data[a] === exist[a]) {
        error.push(`{${a}: ${dot.pick(a, data)}}`);
      }
    });
    throw new Error(`${EXISTS}: ${error.toString()}`);
  }
}

export interface Interface {
  //     protected async preCreate(requestData): Promise<void> {
  //     console.log('pre create')
  // }
  //
  // protected async postCreate(savedData): Promise<void> {
  //     console.log('post create')
  // }
  //
  // protected async preDelete(idOrCode:string): Promise<void> {
  //     console.log('pre delete')
  // }
  //
  // protected async postDelete(idOrCode:string,deletedItem): Promise<void> {
  //     console.log('post delete')
  // }
  //
  
  //
  // protected async preUpdate(idOrCode:string,requestData): Promise<void> {
  //     console.log('pre update')
  // }
  //
  // protected async postUpdate(idOrCode:string,savedData): Promise<void> {
  //     console.log('post update')
  // }
  //
}
