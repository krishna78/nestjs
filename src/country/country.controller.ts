import { Controller, Get, Request, Query, Res } from "@nestjs/common";
import { abstractBaseControllerFactory } from "../common/base/base.controller";
import { CountryService } from "./country.service";
import { BASEROUTES } from "../common/constants/enum";
import { CountryDto } from "./objects/country.dto";
import { RequestUser } from "../common/utils/controller.decorator";

const BaseController = abstractBaseControllerFactory<any>({
  DTO: CountryDto,
  DisabledRoutes: [
    // BASEROUTES.CREATE,
    BASEROUTES.DETELEONE,
    BASEROUTES.PATCH,
    BASEROUTES.UPDATEONE,
  ],
});

@Controller("country")
export class CountryController extends BaseController {
  constructor(private countryService: CountryService) {
    super(countryService);
  }

  @Get()
  async findList(@Request() req, @Query() query, @RequestUser() user,@Res() res) {
   // if (user.isAdmin) {
      // <--- only admin can see the user lists
      //let k = await super.findList(req, { ...query });
     let _k = await this.countryService.findAll(res)
      console.log(_k)
   // }
   // throw new NotFoundException();
  }

  @Get('states')
  async findListStates(@Request() req, @Query() query, @RequestUser() user,@Res() res) {
   // if (user.isAdmin) {
      // <--- only admin can see the user lists
      //let k = await super.findList(req, { ...query });
     let _k = await this.countryService.findAllStates(res,query)
      console.log(_k)
   // }
   // throw new NotFoundException();
  }
}
