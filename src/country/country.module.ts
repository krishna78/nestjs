import { Module } from "@nestjs/common";
import { CountryService } from "./country.service";
import { CountryController } from "./country.controller";
import { MongooseModule } from "@nestjs/mongoose";
import { CountrySchema, CountryStatesSchema } from "./objects/country.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "Country", schema: CountrySchema },
                               { name: "CountriesStates", schema: CountryStatesSchema },]),
  ],
  providers: [CountryService],
  controllers: [CountryController],
})
export class CountryModule {}
