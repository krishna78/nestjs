import { Injectable, Res } from "@nestjs/common";
import { BaseService } from "../common/base/base.service";
import { Country, ICountry,CountryStates, ICountryStates  } from "./objects/country.schema";
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";

@Injectable()
//export class CountryService extends BaseService<ICountry> {
  export class CountryService extends BaseService<ICountryStates> {
  constructor(
    //@InjectModel("Country") private readonly countryModel: Model<Country>
      @InjectModel("CountriesStates") private readonly countryModel: Model<CountryStates>
    ) {
    super(countryModel);
  }

  async findAll(@Res() res){
  let t =  this.countryModel.find({}, function(err, result) {
      if (err) {
        console.log(err);
      } else {
        res.json(result);
      }
    });
    //let t = await this.countryModel.find();
    console.log(t)
  }

  async findAllStates(@Res() res, query){
    console.log(query)
    let t =  this.countryModel.find(query, function(err, result) {
        if (err) {
          console.log(err);
        } else {
          res.json(result);
        }
      });
      //let t = await this.countryModel.find();
      console.log(t)
    }

}
