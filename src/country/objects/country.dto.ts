import { Expose, Type } from "class-transformer";
import { Reader } from "../../common/base/base.dto";

class StateDTO {
  @Expose()
  readonly name: string = "";

  @Expose()
  @Type(() => CityDTO)
  readonly cities: CityDTO[] = [new CityDTO()];
}

class _StateDto {
      @Expose()
      readonly id: number;

      @Expose()
      readonly name: string;

      @Expose()
      readonly  state_code: string;
    
}

class CityDTO {
  @Expose()
  readonly name: string = "";

  @Expose()
  readonly pincode: string = "";
}
/*
export class CountryDto extends Reader {
  @Expose()
  readonly name: string = "";

  @Type(() => StateDTO)
  @Expose()
  readonly states: StateDTO[] = [new StateDTO()];
}
*/
export class CountryDto extends Reader {
  @Expose()
  readonly name: string = "";

  @Type(() => _StateDto)
  @Expose()
  readonly states: _StateDto[] = [new _StateDto()];
}