import { Schema } from "mongoose";
import { createModel, Entity, IEntity } from "../../common/base/base.model";

export class Country extends Entity {
  name: string;
  states: [
    {
      name: string;
      cities: [
        {
          name: string;
          pincode: string;
        }
      ];
    }
  ];
}
export class CountryStates extends Entity { 
       name: string;
       iso3: string;
       iso2: string;
       phone_code: string;
       capital: string;
       currency:  number;
       states: [
           {
               id: number;
               name: string;
               state_code: string;
           }]         
}
export interface ICountryStates extends CountryStates,IEntity {
  id: string;
}

export interface ICountry extends Country, IEntity {
  id: string;
}

export const CountryStatesSchema: Schema = createModel("mTwo", { 
   name: { type: String },
        iso3: { type: String },
        iso2: { type: String },
      phone_code: { type: String },
        capital: { type: String },
        currency: { type: Number },
        states: [
            {
                id: {
                  type: Number,
                },
                name: { type: String },
                state_code: { type: String },
            }]
             
});

export const CountrySchema: Schema = createModel("Countries", {
  name: { type: String },
  states: [
    {
      name: {
        type: String,
      },
      cities: [
        {
          name: {
            type: String,
          },
          pincode: {
            type: String,
          },
        },
      ],
    },
  ],
});
