import { Module } from "@nestjs/common";
import { EmailService } from "./email.service";
import { MongooseModule } from "@nestjs/mongoose";
import { EmailSchema } from "./objects/email.schema";
import { MailerModule, MailerOptions } from "@nestjs-modules/mailer";
import { EmailSenderService } from "./email.sender.service";
import {
  EMAIL_AUTH,
  EMAIL_HOST,
  EMAIL_PASSWORD,
  EMAIL_PORT,
  EMAIL_SECURED,
  AWS_CONFIG,
} from "../common/constants/config";
import * as nodemailer from 'nodemailer';


@Module({
  imports: [
    MongooseModule.forFeature([{ name: "Email", schema: EmailSchema }]),
   /* 
    MailerModule.forRootAsync({
      useFactory: async () => {
        return {
          transport: {
           // host: EMAIL_HOST,
            port: EMAIL_PORT,
            secure: EMAIL_SECURED,
           // auth: {
            //  user: EMAIL_AUTH,
            //  pass: EMAIL_PASSWORD,
            //},
            accessKeyId: AWS_CONFIG.key,
            secretAccessKey: AWS_CONFIG.secret,
            region: AWS_CONFIG.ses.region,
          },
        };
      },
    }),
  /*  */
  ],
  exports: [EmailService],
  providers: [EmailService, EmailSenderService],
})
export class EmailModule {}
