import { Injectable, Logger } from "@nestjs/common";
//import { MailerService } from "@nestjs-modules/mailer";
import { Interval, NestSchedule } from "nest-schedule";
import { EmailService } from "./email.service";
import { EMAILSTATUS } from "../common/constants/enum";
import AWS = require("aws-sdk");
import { AWS_CONFIG } from "../common/constants/config";
import * as nodemailer from "nodemailer";
import { Otp } from "src/auth/otp/otp.schema";

@Injectable()
export class EmailSenderService extends NestSchedule {
  private logger = new Logger("EmailSenderService", true);

  constructor(
    //  private readonly mailerService: MailerService,
    private readonly emailService: EmailService
  ) {
    super();
  }

  @Interval(30000)
  private async publish() {
    let emails = await this.emailService.findAll(
      {
        status: EMAILSTATUS.PENDING,
        sendAt: {
          $lte: Date.now(),
        },
      },
      {
        page: 1,
        sort: "_id",
      }
    );

    // TODO: this will braek;
    for (const email of emails.docs) {
      try {
        let sent = await this.send(email);
        await this.emailService.edit(email.id, {
          response: sent,
          status: EMAILSTATUS.SUCCESS,
        });
      } catch (e) {
        this.logger.error(e);
      }
    }
  }

  private async send(email: any): Promise<any> {
    // configure AWS SDK
    AWS.config.update({
      accessKeyId: AWS_CONFIG.key,
      secretAccessKey: AWS_CONFIG.secret,
      region: AWS_CONFIG.ses.region,
    });

    // create Nodemailer SES transporter
    let transporter = nodemailer.createTransport({
      SES: new AWS.SES({
        apiVersion: "2010-12-01",
      }),
    });
    // send some mail
    return await transporter.sendMail({
      to: email.to,
      from: email.from,
      subject: email.subject,
      html: email.content,
    });
    /* return await this.mailerService.sendMail({
      to: email.to,
      from: email.from,
      subject: email.subject,
      html: email.content
    });
   /* */
  }
}
