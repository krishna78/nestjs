import { Injectable } from "@nestjs/common";
import { InjectModel, MongooseModule } from "@nestjs/mongoose";
import { BaseService } from "../common/base/base.service";
import { HasPostCreate } from "../common/base/pre.post.action";
import { IEmail } from "./objects/email.schema";
import { AbstractEmail } from "./objects/abstract.email";
@Injectable()
export class EmailService extends BaseService<IEmail> implements HasPostCreate {
  constructor(@InjectModel("Email") private readonly model: MongooseModule) {
    super(model);
  }

  async sendEmail(email: AbstractEmail) {
    console.log("started");
    //debugger;
    console.log(email);
    console.log("ended");
    let emailDto = await email.createEmailDto();
    await this.create(emailDto);
  }

  async doPostCreate(model: any): Promise<void> {
    console.log(model);
    console.log("send logic and update status :) :)");
  }
}
