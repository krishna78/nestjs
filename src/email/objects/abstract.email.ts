// TODO: work on email template Module, KRISHNA
import * as Mustache from "mustache";
import { FROM_EMAIL } from "../../common/constants/config";
import { EmailDto } from "./email.dto";

export interface EmailTemplateAndSubject {
  view: string;
}

export abstract class AbstractEmail {
  protected abstract defaultEmailStatusWhenSent;
  protected abstract defaultTemplate: EmailTemplateAndSubject;

  protected abstract getDataForTemplate(meta: any);
  protected abstract getEmailSubject(meta: any);

  protected abstract emailTemplateCode: string; // query to find one email template
  private readonly;
  emailDto: EmailDto;

  constructor(emailDto: EmailDto) {
    this.emailDto = emailDto;
  }

  public async createEmailDto() {
    //debugger;
    const template = await this.getTemplate();
    const viewFiller = this.getDataForTemplate(this.emailDto.metaData);
    const result = await Mustache.render(template.view, viewFiller);
    let emailDto = this.emailDto;

    return {
      status: this.defaultEmailStatusWhenSent,
      from: this.getSenderEmail(),
      subject: this.getEmailSubject(this.emailDto.metaData),
      content: result,
      ...emailDto,
    };
  }

  private async getTemplate(): Promise<{ view: string; subject: string }> {
    let emailTemplate;
    let email;
    if (this.emailTemplateCode) {
      // emailTemplate = await this.emailTemplateModel.findOne({
      //   code: this.emailTemplateCode,
      // });
    }
    if (!emailTemplate) {
      email = this.defaultTemplate;
    } else {
      // TODO: verify after email template has been created
      email = {
        view: emailTemplate.content,
        subject: emailTemplate.subject,
      };
    }
    return email;
  }

  // Override if want to have unique sender
  protected getSenderEmail(): string {
    return String(FROM_EMAIL);
  }
}
