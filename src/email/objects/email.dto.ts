import { Creator } from "../../common/base/base.dto";

export class EmailDto extends Creator {
  constructor({ to, metaData }) {
    super(true);
    this.to = to;
    this.metaData = metaData;
  }

  metaData: any; // Any Data Used to fill up email Template
  to: string;
}
