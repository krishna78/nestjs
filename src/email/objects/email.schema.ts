import { createModel, Entity, IEntity } from "../../common/base/base.model";

export class Email extends Entity {}

export interface IEmail extends Email, IEntity {
  id: string;
}

export const EmailSchema = createModel("Emails", {
  from: {
    type: String,
  },
  to: {
    type: String,
  },
  subject: {
    type: String,
  },
  content: {
    type: String,
  },
  sendAt: {
    type: Number,
    default: new Date().getTime(),
  },
  response: {
    type: Object,
  },
});
