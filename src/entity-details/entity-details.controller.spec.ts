import { Test, TestingModule } from '@nestjs/testing';
import { EntityDetailsController } from './entity-details.controller';

describe('EntityDetails Controller', () => {
  let controller: EntityDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EntityDetailsController],
    }).compile();

    controller = module.get<EntityDetailsController>(EntityDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
