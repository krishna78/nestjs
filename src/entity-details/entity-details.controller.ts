import {
  Controller,
  Get,
  UseGuards,
  Put,
  Request,
  Body,
  Post,
  Query,
} from "@nestjs/common";
import { abstractBaseControllerFactory } from "../common/base/base.controller";
import {
  EntityDetailsDto,
  UpdateEntityDetailsDto,
  DocumentsUploadDto,
} from "./objects/entity-details.dto";
import { BASEROUTES, USER_TYPES } from "../common/constants/enum";
import { EntityDetailsService } from "./entity-details.service";
import { JwtAuthGuard } from "../auth/auth.guard";
import {
  IdOrCodeParser,
  RequestUser,
} from "../common/utils/controller.decorator";
import { plainToClass } from "class-transformer";
import { success } from "../common/base/httpResponse.interface";
import { NOT_FOUND, INVALID_VALUE } from "../common/constants/string";
import { AbstractClassTransformerPipe } from "../common/pipes/class-transformer.pipe";
import { FileUploadService } from "../file-upload/file-upload.service";
import { UsersService } from "../users/users.service";
import { InjectModel } from "@nestjs/mongoose";
import { IEntityDetails } from "./objects/entity-details.schema";
import { Model } from "mongoose";

const BaseController = abstractBaseControllerFactory<any>({
  DTO: EntityDetailsDto,
  // CreateDTO: CreateEntityDetailsDto,
  UpdateDTO: UpdateEntityDetailsDto,
  DisabledRoutes: [BASEROUTES.DETELEONE, BASEROUTES.PATCH, BASEROUTES.CREATE],
});

@UseGuards(JwtAuthGuard)
@Controller("entity-details")
export class EntityDetailsController extends BaseController {
  constructor(
    private entityDetailsService: EntityDetailsService,
    private fileUploadService: FileUploadService,
    private usersService: UsersService,
    @InjectModel("EntityDetails")
    private readonly entityDetailsModel: Model<IEntityDetails>
  ) {
    super(entityDetailsService);
  }

  @Get("m/d")
  async findMyEntity(
    @IdOrCodeParser("idOrCode") idOrCode: string,
    @RequestUser() user
  ) {
    // TODO: Delete this once users/profile -> entityDetailCode -> enriry-details/${code} is implemented
    const d = await this.entityDetailsService.findOneByQuery({
      createdBy: user.code,
    });
    debugger;
    const _data = plainToClass(EntityDetailsDto, d, {
      excludeExtraneousValues: true,
    });
    return success(_data);
  }

  @Put(":idOrCode")
  async update(
    @IdOrCodeParser("idOrCode") idOrCode: string,
    @Request() req,
    @Body(AbstractClassTransformerPipe(UpdateEntityDetailsDto)) updateDto,
    @RequestUser() user
  ) {
    debugger;
    let updateTime: number = Date.now();

    if (updateDto.documentsUpload) {
      let updated = await Promise.all(
        await updateDto.documentsUpload.map(async (element) => {
          let t: DocumentsUploadDto[];
          let _file = await this.fileUploadService.findOne(element.id);
          if (!_file) {
            throw INVALID_VALUE("fileId", element);
          }
          let name = element.name;

          delete element["name"];
          element["name"] = _file.originalname;
          return element;
        })
      );
      let data = await super.update(idOrCode, updateTime, updateDto, user);
      //call email method here
      return success(data);
    }
    let data = await super.update(idOrCode, updateTime, updateDto, user);
    //call email method here

    return success(data);
  }
}
