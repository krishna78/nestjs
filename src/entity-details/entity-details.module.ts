import { forwardRef, Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { EntityDetailsController } from "./entity-details.controller";
import { EntityDetailsService } from "./entity-details.service";
import { EntityDetailsSchema } from "./objects/entity-details.schema";
import { UsersModule } from "../users/users.module";
import { EmailModule } from "../email/email.module";
import { FileUploadSchema } from "../file-upload/objects/file-upload.schema";
import { FileUploadModule } from "../file-upload/file-upload.module";
import { FileUploadService } from "../file-upload/file-upload.service";
import { UsersService } from "../users/users.service";
import { UserSchema } from "../users/objects/user.schema";
import { EmailService } from "../email/email.service";
import { ActivityFeedModule } from "../activity-feed/activity-feed.module";
import { SmeActivityFeedModule } from "../sme-activity-feed/sme-activity-feed.module";
import { SmeProjectModule } from "../sme-project/sme-project.module";
import { SmeProjectSchema } from "../sme-project/objects/sme-project.schema";

@Module({
  imports: [
    forwardRef(() => UsersModule),
    MongooseModule.forFeature([
      { name: "EntityDetails", schema: EntityDetailsSchema },
      { name: "FileUpload", schema: FileUploadSchema },
      { name: "User", schema: UserSchema },
      { name: "SmeProject", schema: SmeProjectSchema },
    ]),
    /*
    forwardRef(() => 
    SmeProjectModule
    ),
    */
    FileUploadModule,
    EmailModule,
    ActivityFeedModule,
    SmeActivityFeedModule,
  ],
  controllers: [EntityDetailsController],
  providers: [EntityDetailsService, FileUploadService],

  exports: [
    EntityDetailsService,
    MongooseModule.forFeature([
      { name: "EntityDetails", schema: EntityDetailsSchema },
    ]),
  ],
})
export class EntityDetailsModule {}
