import { Test, TestingModule } from '@nestjs/testing';
import { EntityDetailsService } from './entity-details.service';

describe('EntityDetailsService', () => {
  let service: EntityDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EntityDetailsService],
    }).compile();

    service = module.get<EntityDetailsService>(EntityDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
