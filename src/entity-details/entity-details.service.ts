import { forwardRef, Inject, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { IEntityDetails } from "./objects/entity-details.schema";
import { BaseService } from "../common/base/base.service";
import { HasPreUpdate, HasPostUpdate } from "../common/base/pre.post.action";
import { UpdateEntityDetailsDto } from "./objects/entity-details.dto";
import { UsersService } from "../users/users.service";
import { CanLoadVirtual } from "../common/interfaces/can.load.virtual";
import {
  DOCUMENTS_UPLOADS,
  INVESTOR,
  SME,
  ACTIVITY_FEED_EVENTS_SPONSOR,
  ACTIVITY_FEED_EVENTS_SME,
  USER_TYPES,
} from "../common/constants/enum";
import * as request from "request-promise-native";
import {
  CRM_URL_HEADERS,
  CRM_URL_HEADERS_UPDATE,
} from "../common/constants/config";
import * as isEmpty from "lodash";
import {
  OnBoardingEmailAlert,
  OnBoardingCompletedForSponsor,
  OnBoardingCompletedForSme,
} from "../users/objects/user.registered.email";
import { EmailService } from "../email/email.service";
import { EmailDto } from "../email/objects/email.dto";
import { EmailNotificationService } from "../users/email-notification/email-notification.service";
import { ftruncate } from "fs";
import { ActivityFeedService } from "../activity-feed/activity-feed.service";
import { SmeActivityFeedService } from "../sme-activity-feed/sme-activity-feed.service";
import { IUser, User } from "../users/objects/user.schema";

@Injectable()
export class EntityDetailsService extends BaseService<IEntityDetails>
  implements HasPreUpdate, HasPostUpdate {
  constructor(
    @InjectModel("EntityDetails")
    private readonly entityDetailsModel: Model<IEntityDetails>,
    private readonly emailService: EmailService,
    private readonly emaillnotifcationService: EmailNotificationService,
    private readonly activityFeedService: ActivityFeedService,
    private readonly smeactivityFeedService: SmeActivityFeedService,
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
    @InjectModel("User") private readonly userModel: Model<User>,
  ) {
    super(entityDetailsModel);
  }

  async postSponsorcompanyDetails(model, k, user, aSponsor) {
    let query = { creator: model.createdBy };
    debugger;
    let data = await this.emaillnotifcationService.findOneByQuery(query);
    if (data.isOnboardingInitiated === false) {
      //sending email
      //let condition = { type: "Admin" };
      debugger;
      //Implement with usersModel
      let condition = { $or: [{ type: USER_TYPES.ADMIN }, { type: USER_TYPES.OPERATIONS_TEAM }] };
      debugger;
         ////Implement with usersModel
         let admins = await this.userModel.find(condition);
         
      /*
      let admins = await this.usersService.findAll(condition, {
        limit: 30,
        page: 1,
        sort: "_id",
      });
      */
     console.log(admins.length);
      //for (let i = 0; i < admins.total; i++) {
       // let email = admins.docs[i].email;
      for (let i = 0; i < admins.length; i++) {
        let email = admins[i].email;
          await this.emailService.sendEmail(
              new OnBoardingEmailAlert(
                new EmailDto({
                  to: email,
                  metaData: { user },
                })
              )
            );
      }
      debugger;
      console.log(data.code);
      // let update:boolean={IsOnboardingInitiated:true}
      data = await this.emaillnotifcationService.edit(data.code, {
        isOnboardingInitiated: true,
      });
      console.log(data);
      console.log("Updated");
    }

    let tCheckA = model.company.details.companyName;
    let tCheckB = model.company.details.name;
    let tCheckC = model.company.details.contactNumber;

    if (
      tCheckA !== undefined ||
      tCheckB !== undefined ||
      tCheckC !== undefined
    ) {
      //crm integration post call
      let d: any =
        '{ "id":' +
        '"' +
        user.id +
        '"' +
        ',"contacttype":"Sales Qualified Lead","assigned_user_id":"19x1"' +
        ',"lastname":' +
        '"' +
        user.lastName +
        '"' +
        ',"cf_contacts_role":' +
        '"' +
        user.type +
        '"' +
        ',"cf_contacts_cifid":' +
        '"' +
        user.formattedId +
        '"' +
        ',"cf_contacts_typeofsme":' +
        '"' +
        model.company.type +
        '"' +
        ',"cf_contacts_uennumber":' +
        '"' +
        aSponsor.uennumber +
        //model.company.details.uenNumber +
        '"' +
        ',"phone":' +
        '"' +
        aSponsor.phone +
        //model.company.details.telephoneNumber +
        '"' +
        ',"cf_contacts_companyname":' +
        '"' +
        model.company.details.name +
        '"' +
        ',"cf_contacts_country":' +
        '"' +
        model.company.address.country +
        '"' +
        ',"cf_contacts_state":' +
        '"' +
        model.company.address.state +
        '"' +
        ',"cf_contacts_city":' +
        '"' +
        model.company.address.cityOrTown +
        '"' +
        ',"cf_contacts_street":' +
        '"' +
        model.company.address.street +
        '"' +
        ',"cf_contacts_postalcode":' +
        '"' +
        model.company.address.postalCode +
        '"' +
        "}";

      var options3 = {
        ...CRM_URL_HEADERS_UPDATE,
        form: {
          element: d,
        },
      };

      /*3rd POST request to the Crm */

      const result3 = await request.post(options3);
      console.log(JSON.parse(result3));
    }
  }

  async postSponsorApplicantDetails(model, k) {
    let query = { creator: model.createdBy };
    debugger;
    let data = await this.emaillnotifcationService.findOneByQuery(query);
    //  if (model.verification.isOnboardingComplete === false) {
    let tCheckD = model.applicantDetails.firstName;
    if (tCheckD !== undefined) {
      let user = {
        id: k.crmId,
        lastName: k.lastName,
        type: k.type,
      };

      var crmUserApplicantDetails: any =
        '{"id":' +
        '"' +
        user.id +
        '"' +
        ',"contacttype":"Sales Qualified Lead","assigned_user_id":"19x1"' +
        ',"lastname":' +
        '"' +
        user.lastName +
        '"' +
        ',"cf_contacts_role":' +
        '"' +
        user.type +
        '"' +
        ',"cf_contacts_applicantfirstname":' +
        '"' +
        model.applicantDetails.firstName +
        '"' +
        ',"cf_contacts_applicantmiddename":' +
        '"' +
        model.applicantDetails.middleName +
        '"' +
        ',"cf_contacts_applicantlastname":' +
        '"' +
        model.applicantDetails.lastName +
        '"' +
        ',"cf_contacts_designation":' +
        '"' +
        model.applicantDetails.designation +
        '"' +
        ',"cf_contacts_nationality":' +
        '"' +
        model.applicantDetails.nationality +
        '"' +
        ',"cf_contacts_applicantcontactno":' +
        '"' +
        model.applicantDetails.contactNumber +
        '"' +
        ',"cf_contacts_passportnumber":' +
        '"' +
        model.applicantDetails.passportNumber +
        '"' +
        "}";

      var options4 = {
        ...CRM_URL_HEADERS_UPDATE,
        form: {
          element: crmUserApplicantDetails,
        },
      };

      /*4th POST request to the Crm */

      const result = await request.post(options4);
      console.log(
        result,
        result.success,
        "crm Sponsor applicant Details",
        options4
      );
    }
    //}
  }

  async postSmecompanyDetails(model, k, user, aSme) {
    debugger;

    let query = { creator: model.createdBy };
    debugger;
    let data = await this.emaillnotifcationService.findOneByQuery(query);
    console.log(data);
    if (data.isOnboardingInitiated === false) {
      let condition = { $or: [{ type: USER_TYPES.ADMIN }, { type: USER_TYPES.OPERATIONS_TEAM }] };
      debugger;
         ////Implement with usersModel
         let admins = await this.userModel.find(condition);
         
         /*
         let admins = await this.userService.findAll(condition, {
           limit: 30,
           page: 1,
           sort: "_id",
         });
         */
         console.log(admins.length);
       
      
    /*  
      let condition = { type: "Admin" };
      debugger;
      let admins = await this.usersService.findAll(condition, {
        limit: 30,
        page: 1,
        sort: "_id",
      });
      */
     // for (let i = 0; i < admins.total; i++) {
      //  let email = admins.docs[i].email;
      for (let i = 0; i < admins.length; i++) {
        let email = admins[i].email;
        await this.emailService.sendEmail(
          new OnBoardingEmailAlert(
            new EmailDto({
              to: email,
              metaData: { user },
            })
          )
        );
      }
      debugger;
      console.log(data.code);
      // let update:boolean={IsOnboardingInitiated:true}
      data = await this.emaillnotifcationService.edit(data.code, {
        isOnboardingInitiated: true,
      });
      console.log(data);
      console.log("Updated");

      debugger;

      // console.log('ModelSmeCompanyDetails',model);
      let tCheck1 = model.company.details.companyName;
      console.log(tCheck1);
      console.log(
        tCheck1,
        "Sme Compnay details .....",
        aSme,
        model.company,
        "Userrrrrrrr",
        user
      );
      debugger;
      let tCheckA = model.company.details.companyName;
      let tCheckB = model.company.type;
      // let tCheckC = model.company.details.contactNumber;

      if (
        tCheckA !== undefined ||
        tCheckB !== undefined
        // || tCheckC !== undefined
      ) {
        let dSme: any =
          '{ "id":' +
          '"' +
          user.id +
          '"' +
          ',"contacttype":"Sales Qualified Lead","assigned_user_id":"19x1"' +
          ',"lastname":' +
          '"' +
          user.lastName +
          '"' +
          ',"cf_contacts_role":' +
          '"' +
          user.type +
          '"' +
          ',"cf_contacts_cifid":' +
          '"' +
          user.formattedId +
          '"' +
          ',"cf_contacts_typeofsme":' +
          '"' +
          model.company.type +
          '"' +
          ',"cf_contacts_uennumber":' +
          '"' +
          aSme.uennumber +
          '"' +
          ',"phone":' +
          '"' +
          aSme.phone +
          '"' +
          ',"cf_contacts_companyname":' +
          '"' +
          model.company.details.companyName +
          '"' +
          ',"cf_contacts_country":' +
          '"' +
          model.company.address.country +
          '"' +
          ',"cf_contacts_state":' +
          '"' +
          model.company.address.state +
          '"' +
          ',"cf_contacts_city":' +
          '"' +
          model.company.address.cityOrTown +
          '"' +
          ',"cf_contacts_street":' +
          '"' +
          model.company.address.street +
          '"' +
          ',"cf_contacts_postalcode":' +
          '"' +
          model.company.address.postalCode +
          '"' +
          "}";
        // console.log(d);
        var options_Sme_CompanyDetails = {
          ...CRM_URL_HEADERS_UPDATE,
          form: {
            element: dSme,
          },
        };

        /*3rd POST request to the Crm */

        const result3 = await request.post(options_Sme_CompanyDetails);
        console.log(result3); //}//call email service here
        console.log(
          result3,
          result3.success,
          "crm Sme company Details",
          options_Sme_CompanyDetails
        );
      }
    }
  }

  async postSmeApplicantDetails(model, k) {
    debugger;
    let tCheck = model.applicantDetails.firstName;

    if (tCheck !== undefined) {
      let user = {
        id: k.crmId,
        lastName: k.lastName,
        type: k.type,
      };

      var crmUserApplicantDetails: any =
        '{"id":' +
        '"' +
        user.id +
        '"' +
        ',"contacttype":"Sales Qualified Lead","assigned_user_id":"19x1"' +
        ',"lastname":' +
        '"' +
        user.lastName +
        '"' +
        ',"cf_contacts_role":' +
        '"' +
        user.type +
        '"' +
        ',"cf_contacts_applicantfirstname":' +
        '"' +
        model.applicantDetails.firstName +
        '"' +
        ',"cf_contacts_applicantmiddename":' +
        '"' +
        model.applicantDetails.middleName +
        '"' +
        ',"cf_contacts_applicantlastname":' +
        '"' +
        model.applicantDetails.lastName +
        '"' +
        ',"cf_contacts_designation":' +
        '"' +
        model.applicantDetails.designation +
        '"' +
        ',"cf_contacts_nationality":' +
        '"' +
        model.applicantDetails.nationality +
        '"' +
        ',"cf_contacts_applicantcontactno":' +
        '"' +
        model.applicantDetails.contactNumber +
        '"' +
        ',"cf_contacts_passportnumber":' +
        '"' +
        model.applicantDetails.passportNumber +
        '"' +
        "}";

      var options4 = {
        ...CRM_URL_HEADERS_UPDATE,
        form: {
          element: crmUserApplicantDetails,
        },
      };

      /*4th POST request to the Crm */
      const result = await request.post(options4);

      // console.log(result,result.success,'crm applicant Details',options4);
    }
  }

  async SponsorOnboardingcompleted(model, k) {
    console.log("dkjdkdkjdk", k);
    console.log("djfkdfkdkfd", model);
    console.log("UserDetaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaails");
    debugger;
    let firstName = k.firstName;
    let lastName = k.lastName;
    let query = { creator: k.code };
    let createDto2: any = {
      //createdBy: k.code,
      _priority: 1,
      type: ACTIVITY_FEED_EVENTS_SPONSOR.ONBOARDING_COMPLETION,
      itemId: k.code,
      setFields: model,
    };
    //  let _activityFeed: any = await this.activityFeedService.onboardingCompletion(
    //    createDto2,
    //   k
    // );

    let data = await this.emaillnotifcationService.findOneByQuery(query);
    console.log(data.isDocumentsSubmitted);
    console.log(data);
    if (data.isDocumentsSubmitted === false) {
      //activity feeding for onboarding completion
      let _activityFeed: any = await this.activityFeedService.onboardingCompletion(
        createDto2,
        k
      );
      if(k.emailAlerts == true){
        await this.emailService.sendEmail(
          new OnBoardingCompletedForSponsor(
            new EmailDto({
              to: k.email,
              metaData: { firstName, lastName },
            })
          )
        );
      }
      debugger;
      let result = await this.emaillnotifcationService.edit(data.code, {
        isDocumentsSubmitted: true,
      });
      console.log(result);
    }
  }
  async SmeOnboadingcompleted(model, k) {
    console.log("dkjdkdkjdk", k);
    console.log("djfkdfkdkfd", model);
    console.log("UserDetaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaails");
    debugger;
    let firstName = k.firstName;
    let lastName = k.lastName;
    let query = { creator: k.code };
    let createDto2: any = {
      //createdBy: k.code,
      _priority: 1,
      type: ACTIVITY_FEED_EVENTS_SME.ONBOARDING_COMPLETION,
      itemId: k.code,
      setFields: model,
    };

    let data = await this.emaillnotifcationService.findOneByQuery(query);
    console.log(data.isDocumentsSubmitted);
    console.log(data);
    if (data.isDocumentsSubmitted === false) {
      let _smeactivityfeed = await this.smeactivityFeedService.onboardingCompletion(
        createDto2,
        k
      );
      if(k.emailAlerts == true){
        await this.emailService.sendEmail(
          new OnBoardingCompletedForSme(
            new EmailDto({
              to: k.email,
              metaData: { firstName, lastName },
            })
          )
        );
      }
      debugger;
      let result = await this.emaillnotifcationService.edit(data.code, {
        isDocumentsSubmitted: true,
      });
      console.log(result);
    }
  }

  async doPreUpdate(
    idOrCode,
    editDto: UpdateEntityDetailsDto,
    model
  ): Promise<void> {
    /*
    if (editDto.documentsUpload) {
      // TODO: doesnot seem to be a right idea to set isProfileCompleted, when document uploaded, not the correct business strategy
      await this.usersService.edit(
        model.createdBy,
        {
          verification: {
            isProfileCompleted: true,
          },
        },
        Date.now()
      );
    }*/
  }
  async doPostUpdate(
    idOrCode,
    //  editDto: UpdateEntityDetailsDto,
    model,
    _user
  ): Promise<void> {
    debugger;
    let k = await this.usersService.findOne(model.createdBy);
    console.log("User detailsssssss", k);
    let user = {
      id: k.crmId,
      firstName: k.firstName,
      lastName: k.lastName,
      type: k.type,
      email: k.email,
      mobile: k.mobileNumber,
      formattedId: k.formattedId,
    };
    debugger;
    switch (k.type) {
      case USER_TYPES.SPONSOR:
        let aSponsor = {
          uennumber: "",
          companyName: "",
          phone: "",
        };

        switch (model.company.type) {
          case INVESTOR.TECH_COMPANY_IN_SINGAPORE:
            aSponsor.uennumber = model.company.details.uenNumber;
            aSponsor.companyName = model.company.details.name;
            aSponsor.phone = model.company.details.telephoneNumber;
            break;
          case INVESTOR.TECH_COMPANY_OUTSIDE_SINGAPORE:
            aSponsor.uennumber = model.company.details.registrationNumber;
            aSponsor.companyName = model.company.details.name;
            aSponsor.phone = model.company.details.telephoneNumber;
            break;
          case INVESTOR.FINANCIAL_INSTITUTIONS:
            aSponsor.uennumber = model.company.details.registrationNumber;
            aSponsor.companyName = model.company.details.companyName;
            aSponsor.phone = model.company.details.telephoneNumber;
            break;
          case INVESTOR.HIGH_NET_WORTH_INDIVIDUALS:
            aSponsor.uennumber = "";
            aSponsor.companyName = "";
            aSponsor.phone = model.company.details.contactNumber;
            break;
        }
        console.log(aSponsor);
        if (
          k.verification.isProfileCompleted === 0 &&
          k.verification.isOnboardingComplete == false
        ) {
          let p1 = await this.postSponsorcompanyDetails(
            model,
            k,
            user,
            aSponsor
          );

          let p2 = await this.postSponsorApplicantDetails(model, k);

          // let p5 = await this.postSponsorAsCustomer(model, user);
        }
        let _allDocsUploaded = model.documentsUpload.some(
          (e) => e.type === DOCUMENTS_UPLOADS.PROOF_OF_ADDRESS
        );
        if (_allDocsUploaded) {
          let email = await this.SponsorOnboardingcompleted(model, k);
          // let email= SendEmailAfterDocumentsSub
          // let email=await this.SendEmailAfterDocumentsSubmission(model,k);mission(model,k)
          //if (model.documentsUpload.length > 0) {
          // TODO: doesnot seem to be a right idea to set isProfileCompleted, when document uploaded, not the correct business strategy.

          if (!(k.verification.isOnboardingComplete == true)) {
            await this.usersService.edit(
              model.createdBy,
              {
                verification: {
                  isOnboardingComplete: true,
                },
              },
              Date.now()
            );
          }
          // if requied want to update isDocumentsSubmitted to true
          //Crm integration ...
        }
        break;
      case USER_TYPES.SME:
        let allDocsUploaded =
          model.documentsUpload.some(
            (e) => e.type === DOCUMENTS_UPLOADS.CERTIFICATE_OF_INCORPORATION
          ) &&
          model.documentsUpload.some(
            (e) => e.type === DOCUMENTS_UPLOADS.COMPANY_CONSTITUTION
          ) &&
          model.documentsUpload.some(
            (e) => e.type === DOCUMENTS_UPLOADS.FINANCIAL_STATEMENTS
          ) &&
          model.documentsUpload.some(
            (e) =>
              e.type ===
              DOCUMENTS_UPLOADS.MANAGEMENT_ACCOUNTS_FOR_THE_CURRENT_YEAR
          ) &&
          model.documentsUpload.some(
            (e) => e.type === DOCUMENTS_UPLOADS.LATEST_ACRA_BUSINESS_PROFILE
          ) &&
          model.documentsUpload.some(
            (e) =>
              e.type ===
              DOCUMENTS_UPLOADS.NRIC_AND_PASSPORT_OF_DIRECTORS_OR_SHAREHOLDERS
          ) &&
          model.documentsUpload.some(
            (e) =>
              e.type ===
              DOCUMENTS_UPLOADS.PROOF_OF_ADDRESS_FOR_COMPANY_DIRECTORS
          ) &&
          model.documentsUpload.some(
            (e) => e.type === DOCUMENTS_UPLOADS.BANK_STATEMENT
          );
        /*
        let _user = {
          id: k.crmId,
          firstName: k.firstName,
          lastName: k.lastName,
          type: k.type,
          email: k.email,
          mobile: k.mobileNumber,
          formattedId: k.formattedId,
        };
        */

        let aSme = {
          uennumber: "",
          phone: "",
        };
        console.log(model.applicantDetails.firstName, user, aSme);

        switch (model.company.type) {
          case SME.TECH_SME_IN_SINGAPORE:
            aSme.uennumber = model.company.details.uenNumber;
            aSme.phone = model.company.details.companyTelephoneNumber;
            break;
          case SME.TECH_SME_OUTSIDE_SINGAPORE:
            aSme.uennumber = model.company.details.companyRegistrationNumber;
            aSme.phone = model.company.details.telephoneNumber;
            break;
        }
        if (k.verification.isProfileCompleted === 0) {
          let p3 = await this.postSmecompanyDetails(model, k, user, aSme);

          let p4 = await this.postSmeApplicantDetails(model, k);
        }
        debugger;
        console.log(allDocsUploaded);

        if (allDocsUploaded === true) {
          debugger;
          let email = await this.SmeOnboadingcompleted(model, k);
        }
        // let p6 = await this.postSmeAsCustomer(model, k);

        //if (model.documentsUpload.length > 7) {
        if (allDocsUploaded) {
          // TODO: doesnot seem to be a right idea to set isProfileCompleted, when document uploaded, not the correct business strategy.
          if (!(k.verification.isOnboardingComplete == true)) {
            await this.usersService.edit(
              model.createdBy,
              {
                verification: {
                  isOnboardingComplete: true,
                },
              },
              Date.now()
            );
          }
        }
        break;
      default:
        console.log("defaultinitiated");
    }
  } //End of doPostUpdate
} //End of EntityDetailsService
