import { Expose, Type } from "class-transformer";
import {
  ValidateNested,
  IsString,
  IsOptional,
  IsDefined,
  IsNotEmpty,
  IsIn,
  ValidateIf,
  IsBoolean,
} from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";
import { Reader, Creator, Updater } from "../../common/base/base.dto";
import { INVESTOR, SME, DOCUMENTS_UPLOADS } from "../../common/constants/enum";
import { FileUploadDto } from "../../file-upload/objects/file-upload.dto";

class DetailsDto {
  
  //@IsString()
  //@Expose()
  //@ApiModelProperty()
  //readonly aboutCompany: string; 
  //Todo: apply transform() to make other fields with no value as null

  /*
  Common field for 
  INVESTOR.TECH_COMPANY_OUTSIDE_SINGAPORE
  INVESTOR.TECH_COMPANY_IN_SINGAPORE
  */
 @ValidateIf((o) => o.type === (INVESTOR.TECH_COMPANY_IN_SINGAPORE || INVESTOR.TECH_COMPANY_OUTSIDE_SINGAPORE))
 @IsString()
 @Expose()
 @ApiModelProperty()
 readonly name: string;

 //Common Property starts here
 @ValidateIf((o) => o.type === (SME.TECH_SME_IN_SINGAPORE || SME.TECH_SME_OUTSIDE_SINGAPORE || INVESTOR.FINANCIAL_INSTITUTIONS))
 @IsString()
 @Expose()
 @ApiModelProperty()
 readonly companyName: string;
 
 @ValidateIf((o) => o.type === (SME.TECH_SME_IN_SINGAPORE || SME.TECH_SME_OUTSIDE_SINGAPORE))
 @IsString()
 @Expose()
 @ApiModelProperty()
 readonly entityType: string;
 //Common Property Ends here

 /* Common field for 
 TECH_SME_IN_SINGAPORE 
 INVESTOR.TECH_COMPANY_IN_SINGAPORE
 */
 @ValidateIf((o) => o.type === (SME.TECH_SME_IN_SINGAPORE || INVESTOR.TECH_COMPANY_IN_SINGAPORE))
 @IsString()
 @Expose()
 @ApiModelProperty()
 readonly uenNumber: string;

 @ValidateIf((o) => o.type === SME.TECH_SME_IN_SINGAPORE)
 @IsString()
 @Expose()
 @ApiModelProperty()
 readonly companyTelephoneNumber: string;
 //TECH_SME_IN_SINGAPORE ends here

 //TECH_SME_OUTSIDE_SINGAPORE starts here
 @ValidateIf((o) => o.type === SME.TECH_SME_OUTSIDE_SINGAPORE)
 @IsString()
 @Expose()
 @ApiModelProperty()
 readonly companyRegistrationNumber: string;

 /*
   Common field for 
   INVESTOR.TECH_COMPANY_OUTSIDE_SINGAPORE
   INVESTOR.FINANCIAL_INSTITUTIONS
   */
 @ValidateIf((o) => o.type === (INVESTOR.TECH_COMPANY_OUTSIDE_SINGAPORE || INVESTOR.FINANCIAL_INSTITUTIONS))
 @IsString()
 @Expose()
 @ApiModelProperty()
 readonly registrationNumber: string;

 @ValidateIf((o) => o.type === (SME.TECH_SME_OUTSIDE_SINGAPORE || INVESTOR.TECH_COMPANY_IN_SINGAPORE || INVESTOR.TECH_COMPANY_OUTSIDE_SINGAPORE || INVESTOR.FINANCIAL_INSTITUTIONS))
 @IsString()
 @Expose()
 @ApiModelProperty()
 readonly telephoneNumber: string;
 //TECH_SME_OUTSIDE_SINGAPORE ends here

 @IsString()
 @Expose()
 @ApiModelProperty()
 readonly concode: string;

  @ValidateIf((o) => o.type === INVESTOR.HIGH_NET_WORTH_INDIVIDUALS)
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly firstName: string;

  @ValidateIf((o) => o.type === INVESTOR.HIGH_NET_WORTH_INDIVIDUALS)
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly middleName: string;

  @ValidateIf((o) => o.type === INVESTOR.HIGH_NET_WORTH_INDIVIDUALS)
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly lastName: string;

  @ValidateIf((o) => o.type === INVESTOR.HIGH_NET_WORTH_INDIVIDUALS)
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly title: string;

  @ValidateIf((o) => o.type === INVESTOR.HIGH_NET_WORTH_INDIVIDUALS)
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly designation: string;

  @ValidateIf((o) => o.type === INVESTOR.HIGH_NET_WORTH_INDIVIDUALS)
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly nationality: string;

  @ValidateIf((o) => o.type === INVESTOR.HIGH_NET_WORTH_INDIVIDUALS)
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly passportNumber: string;

  @ValidateIf((o) => o.type === INVESTOR.HIGH_NET_WORTH_INDIVIDUALS)
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly contactNumber: string;

  @ValidateIf((o) => o.type === INVESTOR.HIGH_NET_WORTH_INDIVIDUALS)
  @Expose()
  @IsBoolean()
  @ApiModelProperty()
  readonly isAccreditedInvestor: boolean;
}

class CompanyAddressDto {
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly country: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly state: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly cityOrTown: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly street: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly postalCode: string;
}

class CompanyDto {
  // LOGIC to manipulate detail based on type value
  @IsDefined()
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => DetailsDto)
  @Expose()
  readonly details: DetailsDto;

  @Expose()
  @IsDefined()
  @IsNotEmpty()
  @IsIn(Object.values({ ...INVESTOR, ...SME }))
  readonly type: string;

  @IsDefined()
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CompanyAddressDto)
  @Expose()
  @ApiModelProperty()
  readonly address: CompanyAddressDto;
}

class ApplicantDetailsDto {
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly firstName: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly middleName: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly lastName: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly title: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly designation: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly nationality: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly passportNumber: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly contactNumber: string;
}

export class DocumentsUploadDto {
  @IsString()
  @Expose()
  @IsIn(Object.values({ ...DOCUMENTS_UPLOADS }))
  @ApiModelProperty()
  readonly type: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly id: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly name: string;

  /*
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly certificateOfIncorporation : string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly companyConstitution : string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly financialStatements : string; 

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly managementAccountsForTheCurrentYear : string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly latestACRABusinessProfile : string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly nRICAndPassportOfDirectorsOrShareholders : string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly proofOfAddressForCompanyDirectors : string;
  
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly bankStatement : string;
  
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly proofOfAddress : string;

  //@Expose()
 // @ApiModelProperty()
 // readonly others: string[];
 */
  } 

export class EntityDetailsDto extends Reader {
//  @Expose()
//  readonly isOnboardingComplete: boolean;

  @Expose()
  readonly createdBy: string = "";

  @Expose()
  @Type(() => CompanyDto)
  @ApiModelProperty()
  readonly company: CompanyDto;

  @Expose()
  @Type(() => ApplicantDetailsDto)
  @ApiModelProperty()
  readonly applicantDetails: ApplicantDetailsDto;
  
  @Expose()
  @Type(() => DocumentsUploadDto)
  @ApiModelProperty()
  readonly documentsUpload: DocumentsUploadDto[];
  
/*
  @IsOptional()
  @Expose()
  @ApiModelProperty()
  @Type(() => FileUploadDto)
  readonly _documentsUpload: FileUploadDto[];
  
  @IsOptional()
  @Expose()
  @ApiModelProperty()
  @Type(() => FileUploadDto)
  readonly _others: FileUploadDto[];
  */
}

export class CreateEntityDetailsDto extends Creator {
  constructor(private readonly createdBy) {
    super(true);
  }
}

export class UpdateEntityDetailsDto extends Updater {
  @ApiModelProperty()
  @IsOptional()
  @ValidateNested()
  @Type(() => CompanyDto)
  readonly company: CompanyDto;

  @ApiModelProperty()
  @IsOptional()
  @ValidateNested()
  @Type(() => ApplicantDetailsDto)
  readonly applicantDetails: ApplicantDetailsDto;
 
  @ApiModelProperty()
  @IsOptional()
  @ValidateNested()
  @Type(() => DocumentsUploadDto)
  readonly documentsUpload: DocumentsUploadDto[];
/*
  @ApiModelProperty()
  @IsOptional()
  readonly others: string[];
*/
}

