import { Entity, IEntity, createModel } from "../../common/base/base.model";
import { Schema } from "mongoose";
import { INVESTOR, SME } from "../../common/constants/enum";
import { DocumentsUploadDto } from "./entity-details.dto";


export class EntityDetails extends Entity {
  company: {
    details: {
      //aboutCompany: string;
      uenNumber: string;
      companyRegistrationNumber: string;
      registrationNumber: string;
      name: string;
      companyName: string;
      entityType: string;
      companyTelephoneNumber: string;
      telephoneNumber: string;
      concode: string;
      firstName: string;
      middleName: string;
      lastName: string;
      title: string;
      designation: string;
      nationality: string;
      passportNumber: string;
      contactNumber: string;
      isAccreditedInvestor: boolean;
    };
    address: {
      country: string;
      state: string;
      cityOrTown: string;
      street: string;
      postalCode: string;
    };
  };

  applicantDetails: {
    firstName: string;
    middleName: string;
    lastName: string;
    title: string;
    designation: string;
    nationality: string;
    passportNumber: string;
    contactNumber: string;
  };
  documentsUpload: DocumentsUploadDto [];
//  isOnboardingComplete: boolean;
  
}

export interface IEntityDetails extends EntityDetails, IEntity {
  id: string;
}

export const EntityDetailsSchema: Schema = createModel("EntityDetailsModel", {
  company: {
    type: { type: String, enum: Object.values({ ...INVESTOR, ...SME }) },
    details: {
      //aboutCompany: { type: String },
      uenNumber: { type: String },
      companyRegistrationNumber: { type: String },
      registrationNumber: { type: String },
      name: { type: String },
      companyName: { type: String },
      entityType: { type: String },
      companyTelephoneNumber: { type: String },
      telephoneNumber: { type: String },
      concode: { type: String },
      firstName: { type: String },
      middleName: { type: String },
      lastName: { type: String },
      title: { type: String },
      designation: { type: String },
      nationality: { type: String },
      passportNumber: { type: String },
      contactNumber: { type: String },
      isAccreditedInvestor: { type: Boolean },
    },
    address: {
      country: { type: String },
      state: { type: String },
      cityOrTown: { type: String },
      street: { type: String },
      postalCode: { type: String },
    },
  },
  applicantDetails: {
    firstName: { type: String },
    middleName: { type: String },
    lastName: { type: String },
    title: { type: String },
    designation: { type: String },
    nationality: { type: String },
    passportNumber: { type: String },
    contactNumber: { type: String },
  },
  documentsUpload: [
  
    {
    type: { type: String },
    id: { type: String },
    name: { type: String }
    }
  ],
 // isOnboardingComplete: { type: Boolean }
    
});


