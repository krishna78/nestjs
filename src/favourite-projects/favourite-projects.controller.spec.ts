import { Test, TestingModule } from "@nestjs/testing";
import { FavouriteProjectsController } from "./favourite-projects.controller";

describe("FavouriteProjects Controller", () => {
  let controller: FavouriteProjectsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FavouriteProjectsController],
    }).compile();

    controller = module.get<FavouriteProjectsController>(
      FavouriteProjectsController
    );
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });
});
