import { Controller, UseGuards, Get, Request, Query, BadRequestException, Post, Body } from "@nestjs/common";
import { abstractBaseControllerFactory } from "../common/base/base.controller";
import { BASEROUTES, USER_TYPES } from "../common/constants/enum";
import {
  CreateFavouriteProjectsDto,
  FavouriteProjectsDto,
} from "./objects/favourite-projects.dto";
import { FavouriteProjectsService } from "./favourite-projects.service";
import { JwtAuthGuard } from "../auth/auth.guard";
import { RequestUser } from "../common/utils/controller.decorator";
import { InjectModel } from "@nestjs/mongoose";
import { IFavouriteProjects } from "./objects/favourite-projects.schema";
import { Model } from "mongoose";
import { SmeProjectService } from "../sme-project/sme-project.service";
import { ISmeProject } from "../sme-project/objects/sme-project.schema";
import { plainToClass } from "class-transformer";
import { normalizePaginateResult } from "../common/interfaces/pagination";
import { SmeProjectDto } from "../sme-project/objects/sme-project.dto";
import { success } from "../common/base/httpResponse.interface";
import { ONLY_FOR_SPONSOR, EXISTS, EXISTS_ALREADY } from "../common/constants/string";
import { AbstractClassTransformerPipe } from "../common/pipes/class-transformer.pipe";

const BaseController = abstractBaseControllerFactory<any>({
  DTO: FavouriteProjectsDto,
  //Todo: Remove after creating records in Db.
  CreateDTO: CreateFavouriteProjectsDto,
  DisabledRoutes: [
    //Todo: Uncomment BASEROUTES.CREATE after creating records in Db.
    // BASEROUTES.CREATE,
    //BASEROUTES.DETELEONE,
  ]
})

@UseGuards(JwtAuthGuard)
@Controller('favourite-projects')
export class FavouriteProjectsController extends BaseController{
    constructor(
       private favouriteProjectsService: FavouriteProjectsService,
       @InjectModel("FavouriteProject")
       private readonly favouriteProjectsModel: Model<IFavouriteProjects>,
        //private bmprojectOwnerService: BMProjectOwnerService,
        private smeProjectService: SmeProjectService,
        @InjectModel("SmeProject")
        private readonly smeProjectModel: Model<ISmeProject>,
       ) {
        super(favouriteProjectsService);
      }
      
      @Get()
      async findList(@Request() req, @Query() query, @RequestUser() user) {
        debugger;
        if (user.isAdmin) {
          // <--- only admin and Sponsor can see all the project lists
          const _d = await super.findList(req, { ...query });
        }
        let userObject = { createdBy: user.code };
        console.log(userObject);
        const d = await super.findList(req, { ...query, ...userObject });
    
        console.log(d);
    
        return d;
      }

      @Get("favouriteObject")
      async findOne(
        //@IdOrCodeParser("idOrCode") idOrCode: string,
        @Query() query,
        @RequestUser() user
      ) {
        debugger;
        //let project = await super.findOne(idOrCode, query);
        let d = await this.favouriteProjectsModel.findOne(query);
        return d
      }


      @Get('projectList')
      async getRecommendedProjects(@Request() req, @Query() query, @RequestUser() user){
        if (user.type == USER_TYPES.SPONSOR) { 
          let query = { createdBy: user.code }
          var options = {
            limit: 100,
            page: 1,
            sort: "_id",
          }
          var smeProjectIdsArray: string[] = [];
          let k = await this.favouriteProjectsModel.find(query);
          console.log('favourite projectsss', k);
          await Promise.all(k.map(async (element) => {
            //let k = await this.usersService.findOne(element);
            //if(k.type == USER_TYPES.SME){
                await smeProjectIdsArray.push(element.projectId);
            //}
            console.log("smeeeeProjectIddsssArray", smeProjectIdsArray);
          }));
         // let projects = await this.smeProjectModel.find({"code": smeProjectIdsArray});
          let projects = await this.smeProjectService.findAll({"code": smeProjectIdsArray}, options);
          //return projects;
          console.log('projectssssss', projects)
         // await projects.map((data) => {
          await projects.docs.map((data) => {
            return plainToClass(SmeProjectDto, data, { excludeExtraneousValues: true });
          })
          let pagination = normalizePaginateResult({
            total: projects.length,
            limit: options.limit,
            page: options.page,
            pages: projects.pages,
          });
  
          //return success
          return success({projects, pagination});
        }
        //throw exception
        throw new BadRequestException(ONLY_FOR_SPONSOR)
      }

      @Get('projectIds')
      async getFavouritesProjectIds(@Request() req, @Query() query, @RequestUser() user){
        if (user.type == USER_TYPES.SPONSOR) { 
          let query = { createdBy: user.code }
          var options = {
            limit: 100,
            page: 1,
            sort: "_id",
          }
          let k = await this.favouriteProjectsModel.find(query);
          console.log('favourite projectsss', k);
          await k.map((data) => {
            return plainToClass(FavouriteProjectsDto, data, { excludeExtraneousValues: true });
          })
          let pagination = normalizePaginateResult({
            total: k.length,
            limit: options.limit,
            page: options.page,
            pages: k.pages,
          });
  
          //return success
          return success({k, pagination});
        }
      }
      @Post()
      public async create(
        @Request() req,
        @Body(AbstractClassTransformerPipe(CreateFavouriteProjectsDto)) body: any,
        @RequestUser() user
      ) {
        if (user.type == USER_TYPES.SPONSOR) { 
          let query = { createdBy: user.code, projectId: body.projectId, status: 0 }
        let k = await this.favouriteProjectsModel.find(query);
        console.log('favouritessssss', k)
          if(k.length > 0){
            throw new BadRequestException(EXISTS_ALREADY("Already marked as favourite."))
          }
          let data = await super.create(req, body, user);
          //  let _body = { ...body, entityDetailCode: user.entityDetailCode }
          //  let data = await super.create(_body,user);
          return success(data);
      }
    }
    

}

