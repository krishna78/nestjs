import { Module, forwardRef } from "@nestjs/common";
import { FavouriteProjectsController } from "./favourite-projects.controller";
import { FavouriteProjectsService } from "./favourite-projects.service";
import { MongooseModule } from "@nestjs/mongoose";
import { FavouriteProjectsSchema } from "./objects/favourite-projects.schema";
import { SmeProjectSchema } from "../sme-project/objects/sme-project.schema";
import { SmeProjectModule } from "../sme-project/sme-project.module";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "FavouriteProject", schema: FavouriteProjectsSchema },
      { name: "SmeProject", schema: SmeProjectSchema },
    ]),
    forwardRef(() => SmeProjectModule),
  ],
  controllers: [FavouriteProjectsController],
  providers: [FavouriteProjectsService],
})
export class FavouriteProjectsModule {}
