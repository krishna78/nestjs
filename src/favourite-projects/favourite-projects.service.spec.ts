import { Test, TestingModule } from "@nestjs/testing";
import { FavouriteProjectsService } from "./favourite-projects.service";

describe("FavouriteProjectsService", () => {
  let service: FavouriteProjectsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FavouriteProjectsService],
    }).compile();

    service = module.get<FavouriteProjectsService>(FavouriteProjectsService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
