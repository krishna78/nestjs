import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { IFavouriteProjects } from "./objects/favourite-projects.schema";
import { BaseService } from "../common/base/base.service";
@Injectable()
export class FavouriteProjectsService extends BaseService<IFavouriteProjects> {
  constructor(
    @InjectModel("FavouriteProject")
    private readonly favouriteProjectsModel: Model<IFavouriteProjects>
  ) {
    super(favouriteProjectsModel);
  }
}
