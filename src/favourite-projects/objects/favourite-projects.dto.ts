import { Expose, Type } from "class-transformer";
import { Reader, Creator } from "../../common/base/base.dto";
import {
  IsDefined,
  IsNotEmpty,
  Allow,
  ValidateNested,
  IsArray,
} from "class-validator";
import { SmeProjectDto } from "../../sme-project/objects/sme-project.dto";

export class FavouriteProjectsDto extends Reader {
  @Expose()
  readonly projectId: string = "";

  @Expose()
  readonly createdBy: string = "";

  /*
  @Expose()
  readonly favProjects: string;
  */
}

export class CreateFavouriteProjectsDto extends Creator {
  constructor() {
    super(true);
  }
  @IsDefined()
  @IsNotEmpty()
  readonly projectId: string;
}
