import { Entity, IEntity, createModel } from "../../common/base/base.model";
import { Schema } from "mongoose";
import { extend } from "lodash";

export class FavouriteProjects extends Entity {
  projectId: string;
  //OwnerName:string;
}
export interface IFavouriteProjects extends FavouriteProjects, IEntity {
  id: string;
}
export const FavouriteProjectsSchema: Schema = createModel(
  "FavouriteProjects",
  {
    projectId: { type: String, required: true },
    // OwnerName: { type: String },
  }
);

/*
FavouriteProjectsSchema.virtual("favProjects", {
  ref: "SmeProject",
  localField: "projectId",
  foreignField: "_id",
  justOne: true
  //options: {
   // select: "owner"
  //}
});
*/
