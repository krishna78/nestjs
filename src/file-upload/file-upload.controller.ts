import {
  Controller,
  Post,
  Body,
  UseInterceptors,
  Param,
  UploadedFile,
  Request,
  UploadedFiles,
  UseGuards,
  Delete,
  NotFoundException,
  Get,
  Req,
  Res,
} from "@nestjs/common";
import { FileInterceptor, FilesInterceptor } from "@nestjs/platform-express";
import { abstractBaseControllerFactory } from "../common/base/base.controller";
import { FileUploadDto, CreateFileUploadDto } from "./objects/file-upload.dto";
import { BASEROUTES, USER_TYPES, ROLES_ACCESS_ACTION } from "../common/constants/enum";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { FileUpload } from "./objects/file-upload.schema";
import { AbstractClassTransformerPipe } from "../common/pipes/class-transformer.pipe";
import { RequestUser, IdOrCodeParser } from "../common/utils/controller.decorator";
import { plainToClass } from "class-transformer";
import { MAX_FILE_LIMIT, PARAM_NOT_PROVIDED } from "../common/constants/string";
import { JwtAuthGuard } from "../auth/auth.guard";
import { success } from "../common/base/httpResponse.interface";
import { FileUploadService } from "./file-upload.service";
import { multerConfig } from "./objects/multer.config";
import { RolesService } from "../roles/roles.service";

const BaseController = abstractBaseControllerFactory<any>({
  DTO: FileUploadDto,
  // Create is not passsed, since we have override the create method in this controller
  //Update is not passed as file controller doesnot need update also.
  DisabledRoutes: [
    //BASEROUTES.DETELEONE,
    BASEROUTES.PATCH,
    //, BASEROUTES.FINDLIST
  ],
});

//@UseGuards(JwtAuthGuard)
@Controller("files")
export class FileUploadController extends BaseController {
  constructor(
    @InjectModel("FileUpload")
    private readonly fileUploadModel: Model<FileUpload>,
    private fileUploadService: FileUploadService,
    private rolesservice: RolesService
  ) {
    super(fileUploadService);
  }
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor("file", multerConfig))
  @Post()
  public async file(
    @UploadedFile()
    file: {
      encoding;
      originalname;
      mimetype;
      destination;
      filename;
      path;
      size;
    },
    @Request() req,
    @RequestUser() user
  ) {
    if (!file) {
      throw new Error(PARAM_NOT_PROVIDED("file"));
    }

    let data = new CreateFileUploadDto(file);
    return super.create(req, data, user);
  }
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FilesInterceptor("files", MAX_FILE_LIMIT, multerConfig))
  @Post("multiple")
  async files(
    @UploadedFiles()
    files: {
      encoding;
      originalname;
      mimetype;
      destination;
      filename;
      path;
      size;
    }[],
    @Request() req,
    @RequestUser() user
  ) {
    const _files = await files.map((file) => {
      return {
        createdBy: user.code,
        ...new CreateFileUploadDto(file),
      };
    });

    let createdItems = await this.fileUploadService.uploadMany(_files);
    var items = createdItems.map((fileMap) => {
      return plainToClass(FileUploadDto, fileMap, {
        excludeExtraneousValues: true,
      });
    });
    return success(items);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(":idOrCode")
  async delete(
  @IdOrCodeParser("idOrCode") idOrCode: string,
  @RequestUser() user
  ) {
    let _user = await this.rolesservice.findOneByQuery({roleName: user.type});
        
    let hasAccess  = _user.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.FILE_UPLOAD_DELETE
    );
    console.log('userstype', user.type, 'user', _user, hasAccess)
    if(hasAccess){
    //if (user.type == USER_TYPES.ADMIN) {
    // <--- only admin can delete a file
    return await super.delete(idOrCode, user);
    //return await this.fileUploadService.deleteFile(idOrCode);
  }
  throw new NotFoundException();
}
  @Get("download/:idOrCode")
  async find(@Req() req,@IdOrCodeParser("idOrCode") idOrCode: string,@Res()res){
  const d = await this.fileUploadService.findFile(req,idOrCode,res);
  return d;
 
  }

  @Get("profilePic/:idOrCode")
  async findProfilePic(@Req() req,@IdOrCodeParser("idOrCode") idOrCode: string,@Res()res){
  const d = await this.fileUploadService.findImage(req,idOrCode,res);
  return d;
 
  }

}
