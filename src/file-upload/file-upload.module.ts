import { Module } from '@nestjs/common';
import { FileUploadController } from './file-upload.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { FileUploadSchema } from './objects/file-upload.schema';
import { FileUploadService } from './file-upload.service';
import { RolesSchema } from '../roles/objects/roles.schema';
import { RolesModule } from '../roles/roles.module';
import { RolesService } from '../roles/roles.service';

@Module({
  imports:[  MongooseModule.forFeature([{ name: "FileUpload", schema: FileUploadSchema },
                                        { name: "AdminRoles", schema: RolesSchema }]),
                                        RolesModule,],
  controllers: [FileUploadController],
  providers: [FileUploadService, RolesService]
})
export class FileUploadModule {}
