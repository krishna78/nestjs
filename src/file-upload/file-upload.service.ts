import { Injectable, Res, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BaseService } from '../common/base/base.service';
import { CreateFileUploadDto } from './objects/file-upload.dto';
import { IFileUpload } from './objects/file-upload.schema';
import { Model } from "mongoose";
import { HasPreDelete } from '../common/base/pre.post.action';
import path = require('path');
import { NOT_FOUND } from '../common/constants/string';

@Injectable()
export class FileUploadService extends BaseService<IFileUpload> implements HasPreDelete{
    constructor(
        @InjectModel("FileUpload") private readonly fileUploadModel: Model<IFileUpload>
      ) {
        super(fileUploadModel);
      }
    
    async uploadMany(createDto: CreateFileUploadDto[]) {
      return await this.baseModel.insertMany(createDto);
      }
    
    async doPreDelete(idOrCode, model){
     /*
      console.log('filedeleteeeeeeee',idOrCode,model)
      let q = await this.findOne(idOrCode);
      console.log('filedeleteeeeeeee',q)
      
      if(!(q == null || q == undefined)){
        return
      }
      throw new BadRequestException(NOT_FOUND)
      */
      //Do nothing.
    }

    /*
    async deleteFile(idOrCode){
      return await this.remove(idOrCode);
    }
    */

    async findFile(req,idOrCode,@Res() response){
      let filepath : string;
      const fileProp = await this.findOne(idOrCode);
      filepath = fileProp.path;
      response.download(filepath,fileProp.originalname);
      }    
      
    async findImage(req,idOrCode,@Res() response){
      let filepath : string;
      const fileProp = await this.findOne(idOrCode);
      filepath = fileProp.path;
      response.sendFile(path.resolve(filepath))
    }  

}
