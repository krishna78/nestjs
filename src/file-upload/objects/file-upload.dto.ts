import { Reader, Creator } from "../../common/base/base.dto";
import { Allow, IsString } from "class-validator";
import { Expose } from "class-transformer";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateFileUploadDto extends Creator {
  constructor({
    originalname,
    mimetype,
    destination,
    filename,
    path,
    size,
    encoding,
  }) {
    super(true);
    this.originalname = originalname;
    this.mimetype = mimetype;
    this.destination = destination;
    this.filename = filename;
    this.path = path;
    this.size = size;
    this.encoding = encoding;
  }

  @Allow()
  readonly fieldname: string;

  @Allow()
  readonly originalname: string;

  @Allow()
  readonly encoding: string;

  @Allow()
  readonly mimetype: string;

  @Allow()
  readonly destination: string;

  @Allow()
  readonly filename: string;

  @Allow()
  readonly path: string;

  @Allow()
  readonly size: string;
}

export class FileUploadDto extends Reader {
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly originalname: string;

  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly mimetype: string;
}
