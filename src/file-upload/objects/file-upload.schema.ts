import { Entity, IEntity, createModel } from "../../common/base/base.model";

export class FileUpload extends Entity {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  destination: string;
  filename: string;
  path: string;
  size: string;
}

export interface IFileUpload extends FileUpload, IEntity {
  id: string;
}

export const FileUploadSchema = createModel("FileUploads", {
  fieldname: { type: String },
  originalname: { type: String },
  encoding: { type: String },
  mimetype: { type: String },
  destination: { type: String },
  filename: { type: String },
  path: { type: String },
  size: { type: String },
});
