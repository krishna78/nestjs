import { Controller, Post, Request, Body } from "@nestjs/common";
import { LoginService } from "./login.service";
import { success } from "../common/base/httpResponse.interface";
import { LoginUserDto, LoggedInToken } from "../users/objects/login-user.dto";
import { AbstractClassTransformerPipe } from "../common/pipes/class-transformer.pipe";
import { AuthService } from "../auth/auth.service";
import { plainToClass } from "class-transformer";

@Controller("login")
export class LoginController {
  constructor(
    private loginService: LoginService,
    private jwtAuthService: AuthService
  ) {}

  @Post()
  public async login(
    @Request() req,
    @Body(AbstractClassTransformerPipe(LoginUserDto)) loginUserDto: LoginUserDto
  ) {
    let loggedIn = await this.loginService.validateUserByEmailAndPassword(
      loginUserDto
    );
    let data = plainToClass(LoggedInToken, loggedIn, {
      excludeExtraneousValues: true,
    });
    return success(data);
  }
}
