import { Module, forwardRef } from "@nestjs/common";
import { LoginController } from "./login.controller";
import { LoginService } from "./login.service";
import { MongooseModule } from "@nestjs/mongoose";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";
import { UserSchema } from "../users/objects/user.schema";
import { AuthService } from "../auth/auth.service";
import { UsersService } from "../users/users.service";
import { JWT_SECRET_KEY, JWT_EXPIRY } from "../common/constants/config";
import { EmailModule } from "../email/email.module";
import { UsersModule } from "../users/users.module";
import { OtpService } from "../auth/otp/otp.service";
import { OtpSchema } from "../auth/otp/otp.schema";
import { AuthModule } from "../auth/auth.module";
import { CrmSchema } from "../users/crm/crm.schema";
import { CrmService } from "../users/crm/crm.service";
import { ActivityFeedModule } from "../activity-feed/activity-feed.module";
import { SmeActivityFeedModule } from "../sme-activity-feed/sme-activity-feed.module";
import { BigchainDbModule } from "../bigchain-db/bigchain-db.module";
import { RolesModule } from "../roles/roles.module";
import { RolesService } from "../roles/roles.service";
import { RolesSchema } from "../roles/objects/roles.schema";
import { BigchaindDbSchema } from "../bigchain-db/objects/bigchain-db.schema";
import { BlockedAccountSchema } from "../auth/blockedAccounts/blockedAccounts.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "User", schema: UserSchema },
      { name: "Otp", schema: OtpSchema },
      { name: "Crm", schema: CrmSchema },
      { name: "AdminRoles", schema: RolesSchema },
      { name: "BigchainDb", schema: BigchaindDbSchema },
      { name: "BlockedAccount", schema: BlockedAccountSchema},
    ]),
    JwtModule.register({
      secretOrPrivateKey: JWT_SECRET_KEY,
      signOptions: {
        expiresIn: JWT_EXPIRY,
      },
    }),
    EmailModule,
    UsersModule,
    forwardRef(() => AuthModule),
    //AuthModule,
    ActivityFeedModule,
    SmeActivityFeedModule,
    BigchainDbModule,
    RolesModule
  ],
  controllers: [LoginController],
  providers: [LoginService, AuthService, UsersService, OtpService, CrmService, RolesService],
  exports: [UsersService],
})
export class LoginModule {}
