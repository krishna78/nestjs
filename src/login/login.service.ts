import { Injectable, BadRequestException } from "@nestjs/common";
import { ExtractJwt } from "passport-jwt";
import { USERNAME_OR_PASSWORD_INCORRECT, BLOCKED_ACCOUNT_ERROR } from "../common/constants/string";
import { LoginUserDto } from "../users/objects/login-user.dto";
import { AuthService } from "../auth/auth.service";
import { UsersService } from "../users/users.service";
import { InjectModel } from "@nestjs/mongoose";
import { IUser } from "../users/objects/user.schema";
import { Model } from "mongoose";
import { CRM_URL_HEADERS_UPDATE } from "../common/constants/config";
import * as request from "request-promise-native";
import { BlockedAccountService } from "../auth/blockedAccounts/blockedAccounts.service";
import { IBlockedAccount } from "../auth/blockedAccounts/blockedAccounts.schema";
import { BLOCKED_ACCOUNT_TYPE, USER_STATUS } from "../common/constants/enum";

@Injectable()
export class LoginService {
  constructor(
    private usersService: UsersService,
    private jwtAuthService: AuthService,
    @InjectModel("User") private readonly userModel: Model<IUser>,
    private readonly blockedAccountService: BlockedAccountService,
    @InjectModel("BlockedAccount") private readonly blockedAccountModel: Model<IBlockedAccount>,
    
  ) {}

  async validateUserByEmailAndPassword(loginAttempt: LoginUserDto) {
    let error = USERNAME_OR_PASSWORD_INCORRECT;
    const userToAttempt: any = await this.usersService.findOneByEmail(
      loginAttempt.email
    );
    if (!userToAttempt) {
      throw new BadRequestException(error);
    }
    //let _blocked: any = await this.blockedAccountService.findOneByQuery({email: userToAttempt.email, type: BLOCKED_ACCOUNT_TYPE.USER_REGISTRATION})
    await this.blockedAccountService.verifyForBlockedAcccountEmail(userToAttempt)

    const isMatch = await userToAttempt.checkPassword(loginAttempt.password);
    if (isMatch) {

      //let lastLoginTime = new Date();
      let lastLoginTime: number = Date.now();
      var _date = new Date(lastLoginTime);
      let updated = await this.usersService.edit(userToAttempt.id, {
        lastLogin: lastLoginTime
      });
      /*
      {
        "id":"4x1508",
        "lastname" : "Rashid", "assigned_user_id":"19x1",
        "cf_contacts_lastlogin":"23-09-2020 3:30 p.m"
      }
      */
    
      let crmId = "";
      var crmUser: any =
        '{"firstname":' +
        '"' +
        userToAttempt.firstName +
        '"' +
        ',"lastname":' +
        '"' +
        userToAttempt.lastName +
        '"' +
        ',"cf_contacts_role":' +
        '"' +
        userToAttempt.type +
        '"' +
        ',"assigned_user_id":"19x1"' +
        ',"email":' +
        '"' +
        userToAttempt.email +
        '"' +
        ',"mobile":' +
        '"' +
        userToAttempt.mobileNumber +
        '"' +
        ',"id":' +
        '"' +
        userToAttempt.crmId +
        '"' +
        ',"cf_contacts_lastlogin":' +
        '"' +
        _date +
        '"' +
        "}";
  
        var options3 = {
          ...CRM_URL_HEADERS_UPDATE,
          form: {
            element: crmUser,
          },
        };
  
        /*3rd POST request to the Crm */
  
        const result3 = await request.post(options3);
        console.log(JSON.parse(result3));

        //let updateCrm = await this.usersService
      return this.jwtAuthService.createJwtPayLoad(userToAttempt);
    } else {
      throw new BadRequestException(error);
    }
  }
}
