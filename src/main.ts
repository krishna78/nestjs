import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { PORT } from "./common/constants/config";

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bodyParser: false,
  });
  var bodyParser = require("body-parser");
  app.setGlobalPrefix("api/v1");
  app.use(bodyParser.json({ limit: "10mb" }));
  app.enableCors();
  await app.listen(PORT);
  console.log("listening on port", PORT);
}
bootstrap();
