import {
  Body,
  Controller,
  Post,
  Request,
  UseGuards,
  Put,
  Query,
  HttpStatus,
  Res,
} from "@nestjs/common";
import { abstractBaseControllerFactory } from "../common/base/base.controller";
import {
  MilestoneDetailsDto,
  CreateMilestoneDetailsDto,
  UpdateMilestoneDetailsDto,
} from "./objects/milestone.dto";
import { BASEROUTES } from "../common/constants/enum";
import { MilestoneService } from "./milestone.service";
import { JwtAuthGuard } from "../auth/auth.guard";
import { AbstractClassTransformerPipe } from "../common/pipes/class-transformer.pipe";
import { RequestUser } from "../common/utils/controller.decorator";
import { plainToClass } from "class-transformer";
import { success } from "../common/base/httpResponse.interface";

const BaseController = abstractBaseControllerFactory<any>({
  DTO: MilestoneDetailsDto,
  DisabledRoutes: [
    BASEROUTES.DETELEONE,
     BASEROUTES.PATCH],
});

@UseGuards(JwtAuthGuard)
@Controller("milestone")
export class MilestoneController extends BaseController {
  constructor(private milestoneService: MilestoneService) {
    super(milestoneService);
  }

  @Post()
  public async create(
    @Request() req,
    @Body(AbstractClassTransformerPipe(CreateMilestoneDetailsDto))
    body: CreateMilestoneDetailsDto,
    @RequestUser() user
  ) {
    debugger;
    // TODO: verify if project exists with code
    let milestones = body.milestones.map((milestone) => {
      return {
        project: body.project,
        ...milestone,
      };
    });
    let createdItems = await this.milestoneService.create(milestones);
    createdItems.map((milestone) => {
      return plainToClass(MilestoneDetailsDto, milestone, {
        excludeExtraneousValues: true,
      });
    });
    return success(createdItems);
  }

  @Put()
  async update(
    @Request() req,
    @Body(AbstractClassTransformerPipe(UpdateMilestoneDetailsDto)) updateDto,
    @RequestUser() user
  ) {
    let updated = await Promise.all(
      await updateDto.milestones.map(async (element) => {
        let code = element.code;
        delete element["code"];
        let milestone = await this.milestoneService.edit(code, element);
        return plainToClass(MilestoneDetailsDto, milestone, {
          excludeExtraneousValues: true,
        });
      })
    );
    return success(updated);
  }
}
