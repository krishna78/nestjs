import { Module, forwardRef } from "@nestjs/common";
import { MilestoneController } from "./milestone.controller";
import { MilestoneService } from "./milestone.service";
import { MongooseModule } from "@nestjs/mongoose";
import { MilestoneSchema } from "./objects/milestone.schema";
import { SmeProjectSchema } from "../sme-project/objects/sme-project.schema";
import { SmeProjectModule } from "../sme-project/sme-project.module";
import { UsersService } from "../users/users.service";
import { EmailService } from "../email/email.service";
import { UsersModule } from "../users/users.module";
import { EmailModule } from "../email/email.module";
import { UserSchema } from "../users/objects/user.schema";
import { JwtService } from "@nestjs/jwt";
import { EmailNotificationService } from "../users/email-notification/email-notification.service";
import { EmailNotificationSchema } from "../users/email-notification/email-notification.schema";
import { SmeEntityModule } from "../sme-entity/sme-entity.module";
import { ProjectEmailNotificationService } from "../sme-project/project-emailnotification/project-emailnotification.service";
import { ProjectEmailNotificationSchema } from "../sme-project/project-emailnotification/project-emailnotification.schema";
import { SmeActivityFeedModule } from "../sme-activity-feed/sme-activity-feed.module";

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: "Milestone",
        schema: MilestoneSchema,
      },
      {
        name: "SmeProject",
        schema: SmeProjectSchema,
      },
      //{
      //name: "ProjectEmailNotification",
      //schema: ProjectEmailNotificationSchema,
      //},
    ]),
    UsersModule,
    EmailModule,
    SmeActivityFeedModule,
    //ProjectEmailNotificationService,
    // SmeProjectModule,

    // JwtService,

    forwardRef(() => SmeProjectModule),
  ],

  controllers: [MilestoneController],
  providers: [MilestoneService],
})
export class MilestoneModule {}
