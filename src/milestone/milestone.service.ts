import { Injectable } from "@nestjs/common";
import { BaseService } from "../common/base/base.service";
import { IMilestone, Milestone } from "./objects/milestone.schema";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { HasPreGetAll, HasPostCreate } from "../common/base/pre.post.action";
import { PARAM_NOT_PROVIDED } from "../common/constants/string";
import { ISmeProject } from "../sme-project/objects/sme-project.schema";
import { SmeProjectService } from "../sme-project/sme-project.service";
import { EmailService } from "../email/email.service";
import { EmailDto } from "../email/objects/email.dto";
import { ProjectCreationEmail } from "../users/objects/user.registered.email";
import { UsersService } from "../users/users.service";
import { EmailNotificationService } from "../users/email-notification/email-notification.service";
import { ProjectEmailNotificationService } from "../sme-project/project-emailnotification/project-emailnotification.service";
import {
  ACTIVITY_FEED_EVENTS_SPONSOR,
  ACTIVITY_FEED_EVENTS_SME,
} from "../common/constants/enum";
import { SmeActivityFeedService } from "../sme-activity-feed/sme-activity-feed.service";

@Injectable()
export class MilestoneService extends BaseService<IMilestone>
  implements HasPreGetAll, HasPostCreate {
  constructor(
    @InjectModel("Milestone")
    private readonly milestoneModel: Model<IMilestone>,
    private smeProjectService: SmeProjectService,
    private emailService: EmailService,
    private usersService: UsersService,
    private emailnotificationService: EmailNotificationService,
    private projectemailnotificationService: ProjectEmailNotificationService,
    private smeactivityFeedService: SmeActivityFeedService
  ) {
    super(milestoneModel);
  }

  async create(createDto: Milestone[]) {
    //return await this.baseModel.insertMany(createDto);
    let k = await this.baseModel.insertMany(createDto);
    let check = await this.doPostCreate(k);
    return k;
  }

  async doPreGetAll(query: any): Promise<void> {
    if (
      query.project === "/(?:)/i" ||
      query.project === null ||
      query.project == void 0
    ) {
      throw new Error(PARAM_NOT_PROVIDED("project"));
    }
  }

  async doPostCreate(model: ISmeProject): Promise<void> {
    debugger;
    console.log("Model", model, model[0].project);
    let idOrCode = model[0].project;
    let updateTime: number = Date.now();
    // readonly hasMilestones: boolean = false;
    let editSmeProject = await this.smeProjectService.edit(
      idOrCode,
      { hasMilestones: true },
      updateTime,
      model
    );

    debugger;

    console.log(editSmeProject);
    console.log("editSmeProjectttttttttttt", editSmeProject.createdBy);
    let _code = editSmeProject.createdBy;
    console.log(editSmeProject.name);
    let query = { projectcode: idOrCode };
    console.log(model);

    debugger;
    let notification = await this.projectemailnotificationService.findOneByQuery(
      query
    );
    if (notification.isProjectCreated === false) {
      console.log(notification);
      // let code=creator;
      console.log(_code);
      let data = await this.usersService.findOne(_code);
      let emaildata = {
        firstName: data.firstName,
        lastName: data.lastName,
        projectName: editSmeProject.name,
      };
      console.log("milestoneDataaaaaaa", emaildata, data);
      let email = data.email;
      if(data.emailAlerts == true){
        await this.emailService.sendEmail(
          new ProjectCreationEmail(
            new EmailDto({
              to: email,
              metaData: { emaildata },
            })
          )
        );
      }

      //sme activity feed while creating project
      let createDto3: any = {
        _priority: 2,
        type: ACTIVITY_FEED_EVENTS_SME.NEW_PROJECT,
        itemId: model.code,
        setFields: {
          message: "Project-Created",
          projectName: editSmeProject.name,
        },
        optional: editSmeProject.formattedId,
      };
      let _activityFeed: any = await this.smeactivityFeedService.creatingNewProject(
        createDto3,
        data
      );
      debugger;
      await this.projectemailnotificationService.edit(notification.code, {
        isProjectCreated: true,
      });
    }
  }
}
