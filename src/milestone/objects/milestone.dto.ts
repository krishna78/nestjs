import { Expose, Type } from "class-transformer";
import {
  IsNotEmpty,
  IsDefined,
  Allow,
  IsInt,
  Min,
  Max,
  ValidateNested,
  IsArray,
} from "class-validator";
import { Reader, Creator, Updater } from "../../common/base/base.dto";
import { ApiModelProperty } from "@nestjs/swagger";

class CreateSingleMilestoneDetailsDto extends Creator {
  constructor() {
    super(true);
  }

  @IsInt()
  @Min(1)
  @Max(99)
  @ApiModelProperty()
  readonly number: number;

  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly name: string;

  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly complexity: string;

  @IsDefined()
  @IsNotEmpty()
  @IsInt()
  @Min(0)
  @Max(100)
  @ApiModelProperty()
  readonly completion: number;
}

export class CreateMilestoneDetailsDto {
  @Allow()
  @IsDefined()
  @IsNotEmpty()
  @IsArray()
  @Type(() => CreateSingleMilestoneDetailsDto)
  @ValidateNested()
  milestones: CreateSingleMilestoneDetailsDto[];

  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  project: string;
}

export class MilestoneDetailsDto extends Reader {
  @Expose()
  @ApiModelProperty()
  readonly number: number;

  @Expose()
  @ApiModelProperty()
  readonly name: string = "";

  @Expose()
  @ApiModelProperty()
  readonly complexity: string;

  @Expose()
  @ApiModelProperty()
  readonly completion: number;

  @Expose()
  @ApiModelProperty()
  readonly project: string = "";
}

class UpdateSingleMilestoneDetailsDto extends Updater {
  @IsDefined()
  @IsNotEmpty()
  readonly code: string;

  @Allow()
  @ApiModelProperty()
  readonly number: number;

  @Allow()
  @ApiModelProperty()
  readonly name: string;

  @Allow()
  @ApiModelProperty()
  readonly complexity: string;

  @Allow()
  @ApiModelProperty()
  readonly completion: number;
}

export class UpdateMilestoneDetailsDto {
  @ValidateNested()
  @Type(() => UpdateSingleMilestoneDetailsDto)
  @ApiModelProperty()
  readonly milestones: UpdateSingleMilestoneDetailsDto[];
}
