import { Entity, IEntity, createModel } from "../../common/base/base.model";
import { Schema } from "mongoose";

export class Milestone extends Entity {
  project: string;
  number: number;
  name: string;
  complexity: string;
  completion: number;
}

export interface IMilestone extends Milestone, IEntity {
  id: string;
}

export const MilestoneSchema: Schema = createModel("Milestonesss", {
  project: { type: String },
  number: { type: Number },
  name: { type: String },
  complexity: { type: String },
  completion: { type: Number },
});
