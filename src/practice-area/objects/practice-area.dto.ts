import { Expose } from "class-transformer";
import { Reader, Creator } from "../../common/base/base.dto";
import { IsDefined, IsNotEmpty } from "class-validator";

export class PracticeAreaDto extends Reader {
 
  @Expose()
  readonly practiceArea: string;
}
//Todo: Remove after creating records in Db.
export class CreatePracticeAreaDto extends Creator {
    constructor() {
      super(true);
    }
    @IsDefined()
    @IsNotEmpty()
    readonly practiceArea: string;
  }