import { Schema } from "mongoose";
import { Entity, IEntity, createModel } from "../../common/base/base.model";


export class PracticeArea extends Entity {
    practiceArea: string;
}

export interface IPracticeArea extends PracticeArea, IEntity {
    id: string;
}

export const PracticeAreaSchema : Schema = createModel("PracticeAreas",{
    practiceArea: { type: String, required: true },
})