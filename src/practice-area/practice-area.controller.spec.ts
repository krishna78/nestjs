import { Test, TestingModule } from '@nestjs/testing';
import { PracticeAreaController } from './practice-area.controller';

describe('PracticeArea Controller', () => {
  let controller: PracticeAreaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PracticeAreaController],
    }).compile();

    controller = module.get<PracticeAreaController>(PracticeAreaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
