import { Controller, UseGuards } from '@nestjs/common';
import { abstractBaseControllerFactory } from '../common/base/base.controller';
import {  CreatePracticeAreaDto, PracticeAreaDto } from './objects/practice-area.dto';
import { BASEROUTES } from '../common/constants/enum';
import { PracticeAreaService } from './practice-area.service';
import { JwtAuthGuard } from '../auth/auth.guard';


const BaseController = abstractBaseControllerFactory<any>({
    DTO: PracticeAreaDto,
   //Todo: Remove after creating records in Db. 
   CreateDTO: CreatePracticeAreaDto,
    DisabledRoutes: [
      //Todo: Uncomment BASEROUTES.CREATE after creating records in Db.
      // BASEROUTES.CREATE,
      BASEROUTES.DETELEONE,
      BASEROUTES.PATCH,
     // BASEROUTES.UPDATEONE,
    ],
  });
  
@UseGuards(JwtAuthGuard)  
@Controller('practice-area')
export class PracticeAreaController extends BaseController{
    constructor(private practiceAreaService: PracticeAreaService) {
        super(practiceAreaService);
      }
}
