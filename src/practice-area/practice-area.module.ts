import { Module } from '@nestjs/common';
import { PracticeAreaController } from './practice-area.controller';
import { PracticeAreaService } from './practice-area.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PracticeAreaSchema } from './objects/practice-area.schema';

@Module({ imports: [
  MongooseModule.forFeature([{ name: "PracticeArea", schema: PracticeAreaSchema }]),
],
  controllers: [PracticeAreaController],
  providers: [PracticeAreaService]
})
export class PracticeAreaModule {}
