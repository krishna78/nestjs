import { Test, TestingModule } from '@nestjs/testing';
import { PracticeAreaService } from './practice-area.service';

describe('PracticeAreaService', () => {
  let service: PracticeAreaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PracticeAreaService],
    }).compile();

    service = module.get<PracticeAreaService>(PracticeAreaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
