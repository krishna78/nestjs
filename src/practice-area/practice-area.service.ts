import { Injectable } from '@nestjs/common';
import { BaseService } from '../common/base/base.service';
import { IPracticeArea } from './objects/practice-area.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";

@Injectable()
export class PracticeAreaService extends BaseService<IPracticeArea> {
    constructor(
        @InjectModel("PracticeArea") private readonly practiceAreaModel: Model<IPracticeArea>
      ) {
        super(practiceAreaModel);
      }
}
