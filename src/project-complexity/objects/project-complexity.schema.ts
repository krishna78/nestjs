import { Schema } from "mongoose";
import { createModel, Entity, IEntity } from "../../common/base/base.model";


export class ProjectComplexity extends Entity {
    complexity: string;
}

export interface IProjectComplexity extends ProjectComplexity,IEntity {
    id: string;
}

export const ProjectComplexitySchema: Schema = createModel("ProjectComplexities",{
    complexity: { type: String, required: true },
})

