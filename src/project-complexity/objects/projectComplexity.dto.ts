import { Expose } from "class-transformer";
import { Reader, Creator } from "../../common/base/base.dto";
import { IsDefined, IsNotEmpty } from "class-validator";

export class ProjectComplexityDto extends Reader {
 
  @Expose()
  readonly complexity: string;
}

//Todo: Remove after creating records in Db.
export class CreateProjectComplexityDto extends Creator {
    constructor() {
      super(true);
    }
    @IsDefined()
    @IsNotEmpty()
    readonly complexity: string;
  }
