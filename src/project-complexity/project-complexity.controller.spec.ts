import { Test, TestingModule } from '@nestjs/testing';
import { ProjectComplexityController } from './project-complexity.controller';

describe('DropDownComplexity Controller', () => {
  let controller: ProjectComplexityController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProjectComplexityController],
    }).compile();

    controller = module.get<ProjectComplexityController>(ProjectComplexityController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
