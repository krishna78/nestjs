import { Controller, UseGuards } from '@nestjs/common';
import { abstractBaseControllerFactory } from '../common/base/base.controller';
import {  ProjectComplexityDto, CreateProjectComplexityDto } from './objects/projectComplexity.dto';
import { BASEROUTES } from '../common/constants/enum';
import {  ProjectComplexityService } from './project-complexity.service';
import { JwtAuthGuard } from '../auth/auth.guard';

const BaseController = abstractBaseControllerFactory<any>({
    DTO: ProjectComplexityDto,
     //Todo: Remove after creating records in Db. 
    CreateDTO: CreateProjectComplexityDto,
    DisabledRoutes: [
      //Todo: Uncomment BASEROUTES.CREATE after creating records in Db.
      // BASEROUTES.CREATE,
     // BASEROUTES.DETELEONE,
      BASEROUTES.PATCH,
      BASEROUTES.UPDATEONE,
    ],
  });

@UseGuards(JwtAuthGuard)
@Controller('project-complexity')
export class ProjectComplexityController extends BaseController{
    constructor(private projectComplexityService: ProjectComplexityService) {
        super(projectComplexityService);
      }
}
