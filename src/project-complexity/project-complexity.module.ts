import { Module } from '@nestjs/common';
import {  ProjectComplexityController } from './project-complexity.controller';
import {  ProjectComplexityService } from './project-complexity.service';
import { MongooseModule } from '@nestjs/mongoose';
import {  ProjectComplexitySchema } from './objects/project-complexity.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "ProjectComplexity", schema: ProjectComplexitySchema }]),
  ],
  controllers: [ProjectComplexityController],
  providers: [ProjectComplexityService]
})
export class ProjectComplexityModule {}
