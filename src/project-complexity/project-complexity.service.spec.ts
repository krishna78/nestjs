import { Test, TestingModule } from '@nestjs/testing';
import { ProjectComplexityService } from './project-complexity.service';

describe('DropDownComplexityService', () => {
  let service: ProjectComplexityService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProjectComplexityService],
    }).compile();

    service = module.get<ProjectComplexityService>(ProjectComplexityService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
