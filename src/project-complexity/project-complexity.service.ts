import { Injectable } from '@nestjs/common';
import { BaseService } from '../common/base/base.service';
import { ProjectComplexity, IProjectComplexity } from './objects/project-complexity.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";
@Injectable()
export class ProjectComplexityService extends BaseService<IProjectComplexity> {
    constructor(
        @InjectModel("ProjectComplexity") private readonly projectComplexityModel: Model<IProjectComplexity>
      ) {
        super(projectComplexityModel);
      }
}
