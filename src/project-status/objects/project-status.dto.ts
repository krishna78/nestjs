import { Expose } from "class-transformer";
import { Reader, Creator } from "../../common/base/base.dto";
import { IsDefined, IsNotEmpty } from "class-validator";

export class ProjectStatusDto extends Reader {
  @Expose()
  readonly projectStatus: string;
 
}

//Todo: Remove after creating records in Db.
export class CreateProjectStatusDto extends Creator {
  constructor() {
    super(true);
  }
  @IsDefined()
  @IsNotEmpty()
  readonly projectStatus: string;
  
}