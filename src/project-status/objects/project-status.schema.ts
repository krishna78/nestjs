import { Schema } from "mongoose";
import { createModel, Entity, IEntity } from "../../common/base/base.model";

export class ProjectStatus extends Entity {
  projectStatus: string;
 
}

export interface IProjectStatus extends ProjectStatus, IEntity {
  id: string;
}

export const ProjectStatusSchema: Schema = createModel("ProjectStatus", {
  projectStatus: { type: String, required: true },
 
});

