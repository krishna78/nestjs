import { Controller, UseGuards } from '@nestjs/common';
import { abstractBaseControllerFactory } from '../common/base/base.controller';
import { ProjectStatusDto, CreateProjectStatusDto } from './objects/project-status.dto';
import { BASEROUTES } from '../common/constants/enum';
import { ProjectStatusService } from './project-status.service';
import { JwtAuthGuard } from '../auth/auth.guard';

const BaseController = abstractBaseControllerFactory<any>({
    DTO: ProjectStatusDto,
    //Todo: Remove after creating records in Db.
    CreateDTO: CreateProjectStatusDto,
    DisabledRoutes: [
      //Todo: Uncomment BASEROUTES.CREATE after creating records in Db.
      // BASEROUTES.CREATE,
      BASEROUTES.DETELEONE,
  
      BASEROUTES.PATCH,
      BASEROUTES.UPDATEONE,
    ],
  });

  @UseGuards(JwtAuthGuard)
@Controller('project-status')
export class ProjectStatusController extends BaseController {
    constructor(private projectstatusservice: ProjectStatusService) {
        super(projectstatusservice);
      }
}
