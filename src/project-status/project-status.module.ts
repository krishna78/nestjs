import { Module } from "@nestjs/common";
import { ProjectStatusController } from "./project-status.controller";
import { ProjectStatusService } from "./project-status.service";
import { MongooseModule } from "@nestjs/mongoose";
import { ProjectStatusSchema } from "./objects/project-status.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "ProjectStatus", schema: ProjectStatusSchema }]),
  ],
  controllers: [ProjectStatusController],
  providers: [ProjectStatusService],
})
export class ProjectStatusModule {}
