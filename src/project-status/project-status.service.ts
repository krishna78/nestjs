import { Injectable } from "@nestjs/common";
import { BaseService } from "../common/base/base.service";
import { ProjectStatus, IProjectStatus } from "./objects/project-status.schema";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
@Injectable()
export class ProjectStatusService extends BaseService<IProjectStatus> {
  constructor(
    @InjectModel("ProjectStatus") private readonly projectStatusModel: Model<IProjectStatus>
  ) {
    super(projectStatusModel);
  }
}