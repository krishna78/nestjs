import { IsInt, Min, Max, IsDefined, IsNotEmpty, Allow, IsArray, ValidateNested, IsIn } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";
import { Creator, Reader } from "../../common/base/base.dto";
import { Expose, Type } from "class-transformer";
import { REMARKS, REMARKS_SUBTYPE } from "../../common/constants/enum";

class CreateSingleRemarkDto extends Creator {
    constructor() {
      super(true);
    }
  

    @IsDefined()
    @IsNotEmpty()
    @IsIn(Object.values({ ...REMARKS}))
    @ApiModelProperty()
    readonly type: string;
  
    @IsDefined()
    @IsNotEmpty()
    @IsIn(Object.values({ ...REMARKS_SUBTYPE}))
    @ApiModelProperty()
    readonly subType: string;

    @IsDefined()
    @IsNotEmpty()
    @ApiModelProperty()
    readonly remark: string;

    //@IsDefined()
    //@IsNotEmpty()
    //@ApiModelProperty()
    //readonly createdBy: string;
  
}

export class CreateRemarksDto {
    @Allow()
    @IsDefined()
    @IsNotEmpty()
    @IsArray()
    @Type(() => CreateSingleRemarkDto)
    @ValidateNested()
    remarks: CreateSingleRemarkDto[];
  
    @IsDefined()
    @IsNotEmpty()
    @ApiModelProperty()
    project: string;
  }

  export class RemarkDto extends Reader {
    @Expose()
    @ApiModelProperty()
    readonly type: string = "";

    @Expose()
    @ApiModelProperty()
    readonly subType: string = "";

    @Expose()
    @ApiModelProperty()
    readonly remark: string;

    @Expose()
    @ApiModelProperty()
    readonly project: string = "";

    @Expose()
    @ApiModelProperty()
    readonly createdBy: string = "";
}