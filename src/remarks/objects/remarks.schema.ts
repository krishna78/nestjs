import { Entity, IEntity, createModel } from "../../common/base/base.model";
import { Schema } from "mongoose";

export class Remark extends Entity {
    project: string;
    type: string;
    subType: string;
    remark: string;
    createdBy: string;
}

export interface IRemark extends Remark, IEntity {
    id: string;
  }

export const RemarkSchema: Schema = createModel("Remarks", {
 project: { type: String },
 type: { type: String },
 subType: { type: String},
 remark: { type: String },
 createdBy:{ type: String }
})