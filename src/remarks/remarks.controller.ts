import { Controller, Post, Body, Request, NotFoundException, Query, UseGuards, Get, Param } from '@nestjs/common';
import { abstractBaseControllerFactory } from '../common/base/base.controller';
import { BASEROUTES, USER_TYPES, REMARKS_SUBTYPE, ROLES_ACCESS_ACTION } from '../common/constants/enum';
import { CreateRemarksDto, RemarkDto } from './objects/remarks.dto';
import { RemarksService } from './remarks.service';
import { RequestUser } from '../common/utils/controller.decorator';
import { AbstractClassTransformerPipe } from '../common/pipes/class-transformer.pipe';
import { plainToClass } from 'class-transformer';
import { success } from '../common/base/httpResponse.interface';
import { JwtAuthGuard } from '../auth/auth.guard';
import { SmeProjectService } from '../sme-project/sme-project.service';
import { RolesService } from '../roles/roles.service';


const BaseController = abstractBaseControllerFactory<any>({
    DTO: RemarkDto,
    CreateDTO: CreateRemarksDto,
    DisabledRoutes: [BASEROUTES.UPDATEONE,BASEROUTES.DETELEONE, BASEROUTES.PATCH],
  });

@UseGuards(JwtAuthGuard)
@Controller('remarks')
export class RemarksController extends BaseController {
    constructor(private remarksService: RemarksService,
      private smeProjectService: SmeProjectService,
      private rolesservice: RolesService) {
        super(remarksService);
      }


    @Get()
      async findList(
       // @Param("projectId") projectId,
        @Request() req, 
        @Query() query, 
        @RequestUser() user) {
        let projectId = query.projectId
        let smeProject = await this.smeProjectService.findOne(projectId);
        let _user = await this.rolesservice.findOneByQuery({roleName: user.type});
        
        let hasAccess  = _user.rolesAccessAction.some(
          (e) => e === ROLES_ACCESS_ACTION.REMARKS_CONTROLLER_FINDLIST
        );
        let _userSme = await this.rolesservice.findOneByQuery({roleName: user.type});
       
        let hasAccessSme = _userSme.rolesAccessAction.some(
          (e) => e === ROLES_ACCESS_ACTION.REMARKS_CONTROLLER_FINDLIST_SME
        );
        let _userSponsor = await this.rolesservice.findOneByQuery({roleName: user.type});
        
        let hasAccessSponsor = _userSponsor.rolesAccessAction.some(
          (e) => e === ROLES_ACCESS_ACTION.REMARKS_CONTROLLER_FINDLIST_SPONSOR
        );
        console.log('userstype', user.type, '_user, _userSme, _userSponsor, hasAccess, hasAccessSme, hasAccessSponsor', _user, _userSme, _userSponsor, hasAccess, hasAccessSme, hasAccessSponsor)

        if(smeProject){
          switch (smeProject.status){
            case 0:
             // if (user.type == USER_TYPES.SME) {
              if(hasAccessSme){
                let _query = { subType: REMARKS_SUBTYPE.STATUS_COLUMN, project: smeProject.code }
                console.log('_query,smeProject.status',_query,smeProject.status)
                return await super.findList(req, { ..._query });
              } //else if(user.type == USER_TYPES.SPONSOR){
                else if(hasAccessSponsor){
                 throw new NotFoundException();
              } //else if(user.type == USER_TYPES.ADMIN){
                else if(hasAccess){
              
                 // <--- only admin can see the user lists
                 let _query = { project: smeProject.code }
                 return await super.findList(req, { ..._query });
              }
              break;
            case 1:
             // if (user.type == USER_TYPES.SME) {
              if(hasAccessSme){
                let _query = { subType: REMARKS_SUBTYPE.EDIT_COLUMN, project: smeProject.code }
                return await super.findList(req, { ..._query });
              } //else if(user.type == USER_TYPES.SPONSOR){
              else if(hasAccessSponsor){
                let _query = { subType: REMARKS_SUBTYPE.EDIT_COLUMN, project: smeProject.code }
                  return await super.findList(req, { ..._query });
              } //else if(user.type == USER_TYPES.ADMIN){
              else if(hasAccess){
              // <--- only admin can see the user lists
                let _query = { project: smeProject.code }
                return await super.findList(req, { ..._query });
              }
              break;
            case 2:
             // if (user.type == USER_TYPES.SME) {
              if(hasAccessSme){
                let _query = { subType: REMARKS_SUBTYPE.EDIT_COLUMN, project: smeProject.code }
                return await super.findList(req, { ..._query });
              } //else if(user.type == USER_TYPES.SPONSOR){
              else if(hasAccessSponsor){
                let _query = { subType: REMARKS_SUBTYPE.EDIT_COLUMN, project: smeProject.code }
                  return await super.findList(req, { ..._query });
              } //else if(user.type == USER_TYPES.ADMIN){
              else if(hasAccess){
              // <--- only admin can see the user lists
                   let _query = { project: smeProject.code }
                return await super.findList(req, { ..._query });
              }
              break;
            case 3:
              //  if (user.type == USER_TYPES.SME) {
                if(hasAccessSme){
                  let _query = { subType: REMARKS_SUBTYPE.STATUS_COLUMN, project: smeProject.code }
                  console.log('_query,smeProject.status',_query,smeProject.status)
                  return await super.findList(req, { ..._query });
                } //else if(user.type == USER_TYPES.SPONSOR){
                else if(hasAccessSponsor){
                 throw new NotFoundException();
                } //else if(user.type == USER_TYPES.ADMIN){
                else if(hasAccess){
                // <--- only admin can see the user lists
                   let _query = { project: smeProject.code }
                   return await super.findList(req, { ..._query });
                }
              break;
            case 5:
              //  if (user.type == USER_TYPES.SME) {
                if(hasAccessSme){
                  let _query = { subType: REMARKS_SUBTYPE.STATUS_COLUMN, project: smeProject.code }
                  console.log('_query,smeProject.status',_query,smeProject.status)
                  return await super.findList(req, { ..._query });
                } //else if(user.type == USER_TYPES.SPONSOR){
                else if(hasAccessSponsor){
                  throw new NotFoundException();
                } //else if(user.type == USER_TYPES.ADMIN){
                else if(hasAccess){
                // <--- only admin can see the user lists
                   let _query = { project: smeProject.code }
                   return await super.findList(req, { ..._query });
                }
              break;   
          }
        }
        /*
       // switch (smeProject.status)
        if (user.type == USER_TYPES.ADMIN) {
          // <--- only admin can see the user lists
          return await super.findList(req, { ...query });
        }
       // throw new NotFoundException();
       */
      }
      


  @Post()
  public async create(
    @Request() req,
    @Body(AbstractClassTransformerPipe(CreateRemarksDto))
    body: CreateRemarksDto,
    @RequestUser() user
  ) {

    // TODO: verify if project exists with code
   // if (user.isAdmin) {
    let _user = await this.rolesservice.findOneByQuery({roleName: user.type});
    console.log('userstype', user.type, _user)
    let hasAccess = _user.rolesAccessAction.some(
      (e) => e ===  ROLES_ACCESS_ACTION.REMARKS_CONTROLLER_CREATE
    );
    console.log('userstype', user.type, _user, hasAccess)
 if(hasAccess) {
        // <--- only admin can see the remarks lists
        let remarks = body.remarks.map((remark) => {
            return {
              project: body.project,
              createdBy: user.email,
              ...remark,
            };
          });
          let createdItems = await this.remarksService.create(remarks);
          createdItems.map((milestone) => {
            return plainToClass(RemarkDto, milestone, {
              excludeExtraneousValues: true,
            });
          });
          return success(createdItems);
        
      }
      throw new NotFoundException();
 
  }
}
