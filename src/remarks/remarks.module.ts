import { Module, forwardRef } from '@nestjs/common';
import { RemarksController } from './remarks.controller';
import { RemarksService } from './remarks.service';
import { MongooseModule } from '@nestjs/mongoose';
import { RemarkSchema } from './objects/remarks.schema';
import { SmeProjectSchema } from '../sme-project/objects/sme-project.schema';
import { SmeProjectModule } from '../sme-project/sme-project.module';
import { RolesModule } from '../roles/roles.module';
import { RolesSchema } from '../roles/objects/roles.schema';
import { RolesService } from '../roles/roles.service';

@Module({
  imports: [
  MongooseModule.forFeature([{ name: "Remark", schema: RemarkSchema },
                             { name: "SmeProject", schema: SmeProjectSchema },
                             { name: "AdminRoles", schema: RolesSchema }]),
                             forwardRef(() => SmeProjectModule),
                             RolesModule,
],
  controllers: [RemarksController],
  providers: [RemarksService, RolesService]
})
export class RemarksModule {}
