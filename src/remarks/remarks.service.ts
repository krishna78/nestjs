import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BaseService } from '../common/base/base.service';
import { IRemark, Remark } from './objects/remarks.schema';
import { Model } from "mongoose";

@Injectable()
export class RemarksService extends BaseService<IRemark>{
    constructor(
        @InjectModel("Remark") private readonly remarkModel: Model<IRemark>
      ) {
        super(remarkModel);
      }

      async create(createDto: Remark[]) {
        return await this.baseModel.insertMany(createDto);
      }
}
