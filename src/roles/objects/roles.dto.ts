import { Expose } from "class-transformer";
import { Reader, Creator } from "../../common/base/base.dto";
import { IsDefined, IsNotEmpty } from "class-validator";

export class RolesDto extends Reader {
  @Expose()
  readonly roleName: string;
  
  @Expose()
  readonly type: string;
  
  @Expose()
  readonly roleCode: string;
  
  @Expose()
  readonly rolesAccessAction: string [];
}

//Todo: Remove after creating records in Db.
export class CreateRolesDto extends Creator {
  constructor() {
    super(true);
  }
  
  @IsDefined()
  @IsNotEmpty()
  readonly roleName: string;


  @IsDefined()
  @IsNotEmpty()
  readonly type: string;

  @IsDefined()
  @IsNotEmpty()
  readonly roleCode: string;
}
