import { Schema } from "mongoose";
import { createModel, Entity, IEntity } from "../../common/base/base.model";

export class Roles extends Entity {
  roleName: string;
  roleCode: string;
  type: string;
  rolesAccessAction: string[];
}

export interface IRoles extends Roles, IEntity {
  id: string;
}

export const RolesSchema: Schema = createModel("AdminRoles", {
  roleName: { type: String, required: true },
  roleCode: { type: String, required: true },
  type: { type: String, required: true},
  rolesAccessAction: [
    {
      type: String,
    },
  ]
});