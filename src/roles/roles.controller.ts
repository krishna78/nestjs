import { RolesDto, CreateRolesDto } from './objects/roles.dto';
import { abstractBaseControllerFactory } from '../common/base/base.controller';
import { BASEROUTES, USER_TYPES, ROLES_ACCESS_ACTION } from '../common/constants/enum';
import { RolesService } from './roles.service';
import { JwtAuthGuard } from '../auth/auth.guard';
import {
  Controller,
  Get,
  UseGuards,
  Request,
  Query,
  Put,
  Body,
  Post,
  BadRequestException,
  NotFoundException,
  Delete,
} from "@nestjs/common";
import { AbstractClassTransformerPipe } from '../common/pipes/class-transformer.pipe';
import { RequestUser } from '../common/utils/controller.decorator';
import { plainToClass } from 'class-transformer';
import { success } from '../common/base/httpResponse.interface';


const BaseController = abstractBaseControllerFactory<any>({
    DTO: RolesDto,
    //Todo: Remove after creating records in Db.
    CreateDTO: CreateRolesDto,
    DisabledRoutes: [
      //Todo: Uncomment BASEROUTES.CREATE after creating records in Db.
      // BASEROUTES.CREATE,
     // BASEROUTES.DETELEONE,
  
      BASEROUTES.PATCH,
     // BASEROUTES.UPDATEONE,
    ],
  });

@UseGuards(JwtAuthGuard)
@Controller('roles')
export class RolesController extends BaseController {
    constructor(private rolesservice: RolesService) {
        super(rolesservice);
      }
      @Post()
      public async create(
        @Request() req,
        @Body(AbstractClassTransformerPipe(CreateRolesDto)) body: any,
        @Query() query,
        @RequestUser() user
      ) {
         switch(body.roleName){
           case USER_TYPES.ADMIN:
             body.rolesAccessAction = [
              ROLES_ACCESS_ACTION.USERS_CONTROLLER_FINDLIST_ADMIN,
              ROLES_ACCESS_ACTION.USERS_CONTROLLER_FIND_ONE,
              ROLES_ACCESS_ACTION.USERS_CONTROLLER_KYC_FILTER,
              ROLES_ACCESS_ACTION.USERS_CONTROLLER_USER_STATUS_FILTER,
              ROLES_ACCESS_ACTION.USERS_CONTROLLER_USER_UPDATE,
              ROLES_ACCESS_ACTION.USERS_CONTROLLER_DELETE,
              ROLES_ACCESS_ACTION.USERS_SERVICE_CHECK_FOR_UPDATE_STATUS_ERROR,
              ROLES_ACCESS_ACTION.USERS_SERVICE_CREATE,
              ROLES_ACCESS_ACTION.USERS_SERVICE_UPDATE_USER_CRMID_AND_ENTITYDETAILCODE,
              ROLES_ACCESS_ACTION.REMARKS_CONTROLLER_CREATE,
              ROLES_ACCESS_ACTION.REMARKS_CONTROLLER_FINDLIST,
              ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_FINDLIST,
              ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_FINDONE,
              ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_UPDATE,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_FINDLIST,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_COMPLETE_BID_PROCESS,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_REJECT_ALL_BIDS_DELETE_PROJECT,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_UPDATE,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_CREATE_TRANSACTION,
              ROLES_ACCESS_ACTION.USER_PORTAL_REMARKS_CONTROLLER_CREATE,
              ROLES_ACCESS_ACTION.USER_PORTAL_REMARKS_CONTROLLER_FINDLIST,
              ROLES_ACCESS_ACTION.FILE_UPLOAD_DELETE,
              ROLES_ACCESS_ACTION.AUTH_CONTROLLER_UNBLOCK_USER_ACCOUNT
             ];
             break;
           case USER_TYPES.OPERATIONS_TEAM:
            body.rolesAccessAction = [
              ROLES_ACCESS_ACTION.USERS_CONTROLLER_FINDLIST_OPERATIONS,
              ROLES_ACCESS_ACTION.USERS_CONTROLLER_FIND_ONE,
              ROLES_ACCESS_ACTION.USERS_CONTROLLER_KYC_FILTER,
              ROLES_ACCESS_ACTION.USERS_CONTROLLER_USER_STATUS_FILTER,
              ROLES_ACCESS_ACTION.USERS_CONTROLLER_USER_UPDATE,
              ROLES_ACCESS_ACTION.USERS_CONTROLLER_DELETE,
              ROLES_ACCESS_ACTION.USERS_SERVICE_CHECK_FOR_UPDATE_STATUS_ERROR,
              ROLES_ACCESS_ACTION.USERS_SERVICE_CREATE,
              ROLES_ACCESS_ACTION.USERS_SERVICE_UPDATE_USER_CRMID_AND_ENTITYDETAILCODE,
              ROLES_ACCESS_ACTION.REMARKS_CONTROLLER_CREATE,
              ROLES_ACCESS_ACTION.REMARKS_CONTROLLER_FINDLIST,
              ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_FINDLIST,
              ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_FINDONE,
              ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_UPDATE,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_FINDLIST,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_COMPLETE_BID_PROCESS,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_REJECT_ALL_BIDS_DELETE_PROJECT,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_UPDATE,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_CREATE_TRANSACTION,
              ROLES_ACCESS_ACTION.USER_PORTAL_REMARKS_CONTROLLER_CREATE,
              ROLES_ACCESS_ACTION.USER_PORTAL_REMARKS_CONTROLLER_FINDLIST,
              ROLES_ACCESS_ACTION.FILE_UPLOAD_DELETE,
              ROLES_ACCESS_ACTION.AUTH_CONTROLLER_UNBLOCK_USER_ACCOUNT
            ];
             break;
           case USER_TYPES.SME:
            body.rolesAccessAction = [
              ROLES_ACCESS_ACTION.USERS_SERVICE_UPDATE_USER_CRMID_AND_ENTITYDETAILCODE,
              ROLES_ACCESS_ACTION.REMARKS_CONTROLLER_FINDLIST_SME,
              ROLES_ACCESS_ACTION.USER_PORTAL_REMARKS_CONTROLLER_FINDLIST_SME,
              ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_CREATE,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_FINDLIST_SME
            ];
           
             break;
           case USER_TYPES.SPONSOR:
            body.rolesAccessAction = [
              ROLES_ACCESS_ACTION.USERS_SERVICE_UPDATE_USER_CRMID_AND_ENTITYDETAILCODE,
              ROLES_ACCESS_ACTION.REMARKS_CONTROLLER_FINDLIST_SPONSOR,
              ROLES_ACCESS_ACTION.USER_PORTAL_REMARKS_CONTROLLER_FINDLIST_SPONSOR,
              ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_FINDLIST,
              ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_FINDONE,
              ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_RECOMMENDED_PROJECTS,
              ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_SPONSOR_FILTER,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_FINDLIST_SPONSOR,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_CREATE,
              ROLES_ACCESS_ACTION.BID_DETAILS_CONTROLLER_UPDATE_SPONSOR,
              ROLES_ACCESS_ACTION.BID_DETAILS_SERVICE_CALCULATE_BID_DETAILS
            ];
             break; 
          } 
          let roles =  await this.rolesservice.create(body);
          const _data = plainToClass(RolesDto, roles, { excludeExtraneousValues: true });
          return success(_data);
      }
}
