import { Module } from '@nestjs/common';
import { RolesController } from './roles.controller';
import { RolesService } from './roles.service';
import { MongooseModule } from '@nestjs/mongoose';
import { RolesSchema } from './objects/roles.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "AdminRoles", schema: RolesSchema }]),
  ],
  controllers: [RolesController],
  providers: [RolesService]
})
export class RolesModule {}
