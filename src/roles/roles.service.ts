import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { IRoles } from './objects/roles.schema';
import { BaseService } from '../common/base/base.service';
import { Model } from "mongoose";

@Injectable()
export class RolesService extends BaseService<IRoles>{
    constructor(
        @InjectModel("AdminRoles") private readonly rolesModel: Model<IRoles>
      ) {
        super(rolesModel);
      }
}
