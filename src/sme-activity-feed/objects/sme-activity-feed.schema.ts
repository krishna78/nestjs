import { createModel, Entity, IEntity } from "../../common/base/base.model";
import { Schema } from "mongoose";
import { ACTIVITY_FEED_EVENTS_SPONSOR } from "../../common/constants/enum";

export class SmeActivityFeed extends Entity {
  _priority: number;
  type: string;
  itemId: string;
  setFields: any;
  optional: string;
}

export interface ISmeActivityFeed extends SmeActivityFeed, IEntity {
  id: string;
}

export const SmeActivityFeedSchema: Schema = createModel(
  "SmeActivityFeedTable",
  {
    _priority: { type: Number },
    type: {
      type: String,
      // enum: Object.values({ ...ACTIVITY_FEED_EVENTS_SPONSOR }),
    },
    itemId: { type: String },
    setFields: { type: Object },
    optional: { type: String },
  }
);
