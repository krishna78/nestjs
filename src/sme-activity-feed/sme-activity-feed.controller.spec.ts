import { Test, TestingModule } from '@nestjs/testing';
import { SmeActivityFeedController } from './sme-activity-feed.controller';

describe('SmeActivityFeed Controller', () => {
  let controller: SmeActivityFeedController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SmeActivityFeedController],
    }).compile();

    controller = module.get<SmeActivityFeedController>(SmeActivityFeedController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
