import {
  Controller,
  Get,
  UseGuards,
  Request,
  Query,
  Put,
  Body,
  Post,
  BadRequestException,
  NotFoundException,
} from "@nestjs/common";
import { SmeActivityFeedService } from "./sme-activity-feed.service";
import { abstractBaseControllerFactory } from "../common/base/base.controller";
import { BASEROUTES, USER_TYPES } from "../common/constants/enum";
import { JwtAuthGuard } from "../auth/auth.guard";
import {
  SmeActivityFeedDto,
  CreateSmeActivityFeedDto,
} from "./objects/sme-activity-feed.dto";
import { RequestUser } from "../common/utils/controller.decorator";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { ISmeActivityFeed } from "./objects/sme-activity-feed.schema";
import { success } from "../common/base/httpResponse.interface";

const BaseController = abstractBaseControllerFactory<any>({
  DTO: SmeActivityFeedDto,
  //Todo: Remove after creating records in Db.
  CreateDTO: CreateSmeActivityFeedDto,
  DisabledRoutes: [
    //Todo: Uncomment BASEROUTES.CREATE after creating records in Db.
    // BASEROUTES.CREATE,
    //BASEROUTES.DETELEONE,

    BASEROUTES.PATCH,
    BASEROUTES.UPDATEONE,
  ],
});

@UseGuards(JwtAuthGuard)
@Controller("sme-activity-feed")
export class SmeActivityFeedController extends BaseController {
  constructor(
    private smeActivityFeedService: SmeActivityFeedService,
    @InjectModel("SmeActivityFeed")
    private readonly smeactivityFeedModel: Model<ISmeActivityFeed>
  ) {
    super(smeActivityFeedService);
  }

  @Get("latest")
  async getLatestActivityFeed(@RequestUser() user) {
    let k: any = await this.smeactivityFeedModel
      .find({ createdBy: user.code })
      .sort({ updatedTime: -1 })
      .limit(7);
    return success(k);
  }

  @Post("calenderActivity")
  async getCalendarActivityFeed(
    @Request() req,
    @Query() query,
    @RequestUser() user,
    @Body() body: { startDate: string }
  ) {
    debugger;
    let t = 0;
    let s = new Date(new Date(body.startDate).setHours(0, 0, 0));
    let s1 = new Date(new Date(body.startDate).setHours(23, 59, 59));
    let _query = {
      $lte: Number(s1),
      $gte: Number(s),
    };
    console.log(
      "start date and querrrryyyyyyyyyy",
      body.startDate,
      "s",
      s,
      "s1",
      s1,
      "querrry",
      _query
    );
    //const newLocal = 00;
    const d = await super.findList(req, { ...query, createdBy: user.code });
    console.log("ddddddddddddddd", d);
    let count = 0;
    await d.data.docs.forEach(async (element) => {
      if (element.createdTime > s && element.createdTime < s1) {
        count++;
      }
    });
    console.log("CCCCCCCCCCCCoooooouuuuunttttttttttttttttt", count);
    let k: any = await this.smeactivityFeedModel
      .find({ createdBy: user.code, createdTime: _query })
      .sort({ updatedTime: -1 })
      .limit(10);
    return success(k);
  }

  @Get()
  async getAllActivityFeed(
    @Request() req,
    @Query() query,
    @RequestUser() user
  ) {
    if (user.type == USER_TYPES.ADMIN) {
      // <--- only admin and Sponsor can see all the project lists
      const d = await super.findList(req, { ...query });
      return success(d);
    } else if (user.type == USER_TYPES.SME) {
      let userObject = { createdBy: user.code };
      console.log(userObject);
      const d = await super.findList(req, { ...query, ...userObject });
      console.log(d);
      return d;
    }
    throw new NotFoundException();
  }
}
