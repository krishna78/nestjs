import { Module } from "@nestjs/common";
import { SmeActivityFeedController } from "./sme-activity-feed.controller";
import { SmeActivityFeedService } from "./sme-activity-feed.service";
import { MongooseModule } from "@nestjs/mongoose";
import { SmeActivityFeedSchema } from "./objects/sme-activity-feed.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "SmeActivityFeed", schema: SmeActivityFeedSchema },
    ]),
  ],
  controllers: [SmeActivityFeedController],
  providers: [SmeActivityFeedService],
  exports: [SmeActivityFeedService],
})
export class SmeActivityFeedModule {}
