import { Test, TestingModule } from '@nestjs/testing';
import { SmeActivityFeedService } from './sme-activity-feed.service';

describe('SmeActivityFeedService', () => {
  let service: SmeActivityFeedService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SmeActivityFeedService],
    }).compile();

    service = module.get<SmeActivityFeedService>(SmeActivityFeedService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
