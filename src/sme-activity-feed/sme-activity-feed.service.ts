import { Injectable } from "@nestjs/common";
import {
  SmeActivityFeed,
  ISmeActivityFeed,
} from "./objects/sme-activity-feed.schema";
import { InjectModel } from "@nestjs/mongoose";
import { BaseService } from "../common/base/base.service";
import { IActivityFeed } from "../activity-feed/objects/activity-feed.schema";
import { Model } from "mongoose";
import { CreateSmeActivityFeedDto } from "./objects/sme-activity-feed.dto";

@Injectable()
export class SmeActivityFeedService extends BaseService<ISmeActivityFeed> {
  constructor(
    @InjectModel("SmeActivityFeed")
    private readonly smeactivityFeedModel: Model<SmeActivityFeed>
  ) {
    super(smeactivityFeedModel);
  }

  async smeRegistration(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async onboardingCompletion(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async kYCVerificationNotificationsSuccessful(createDto3, model) {
    let data = new CreateSmeActivityFeedDto(createDto3);
    return await super.create(data, model);
  }

  async kYCVerificationNotificationsPendingAndRequestingForDocuments(
    createDto5,
    user
  ) {
    let data = new CreateSmeActivityFeedDto(createDto5);
    return await super.create(data, user);
  }

  async kYCVerificationNotificationsRejectedAndReasonForRejection(
    createDto5,
    model
  ) {
    let data = new CreateSmeActivityFeedDto(createDto5);
    return await super.create(data, model);
  }

  async profileChangesEditingTheProfile(createDto7, model) {
    let data = new CreateSmeActivityFeedDto(createDto7);
    await super.create(data, model);
  }
  async creatingNewProject(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async projectStatus(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async projectApprovedToMarketplace(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async projectsPendingWithSuperAdmin(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async projectRejected(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async projectDocumentsRequired(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async projectCompleted(createDto2, _user){
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async projectsViewedBySponsor(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async projectsShownInterest(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async projectsAllottedSuccessfullyMatchedWithSponsor(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  async projectsFunded(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }

  /*
  async amountFundedForProject(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }
*/
  async repaymentBySME(createDto2, _user) {
    let data = new CreateSmeActivityFeedDto(createDto2);
    return await super.create(data, _user);
  }
}
