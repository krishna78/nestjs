import { Expose } from "class-transformer";
import { Reader } from "../../common/base/base.dto";

export class SmeEntityDto extends Reader {
  @Expose()
  readonly inSingapore: boolean;

  @Expose()
  readonly name: string;
}
