import { Schema } from "mongoose";
import { createModel, Entity, IEntity } from "../../common/base/base.model";

export class SmeEntity extends Entity {
  inSingapore: boolean;
  name: string;
}
export interface ISmeEntity extends SmeEntity, IEntity {
  id: string;
}
export const SmeEntitySchema: Schema = createModel("SmeEntity", {
  inSingapore: { type: Boolean, required: true },
  name: { type: String, required: true },
});
