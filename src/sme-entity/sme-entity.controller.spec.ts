import { Test, TestingModule } from "@nestjs/testing";
import { SmeEntityController } from "./sme-entity.controller";

describe("SmeEntity Controller", () => {
  let controller: SmeEntityController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SmeEntityController],
    }).compile();

    controller = module.get<SmeEntityController>(SmeEntityController);
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });
});
