// DROPDOWN while creating sme company detail
import { Controller } from "@nestjs/common";
import { abstractBaseControllerFactory } from "../common/base/base.controller";
import { SmeEntityDto } from "./objects/sme-entity.dto";
import { BASEROUTES } from "../common/constants/enum";
import { SmeEntityService } from "./sme-entity.service";


const BaseController = abstractBaseControllerFactory<any>({
  DTO: SmeEntityDto,
  DisabledRoutes: [
    // BASEROUTES.CREATE,
    BASEROUTES.DETELEONE,
    BASEROUTES.PATCH,
    BASEROUTES.UPDATEONE,
  ],
});

@Controller("sme-entity")
export class SmeEntityController extends BaseController {
  constructor(private smeEntityService: SmeEntityService) {
    super(smeEntityService);
  }
}
