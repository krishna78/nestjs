import { Module } from "@nestjs/common";
import { SmeEntityController } from "./sme-entity.controller";
import { SmeEntityService } from "./sme-entity.service";
import { MongooseModule } from "@nestjs/mongoose";
import { SmeEntitySchema } from "./objects/sme-entity.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "SmeEntity", schema: SmeEntitySchema }]),
  ],
  controllers: [SmeEntityController],
  providers: [SmeEntityService],
})
export class SmeEntityModule {}
