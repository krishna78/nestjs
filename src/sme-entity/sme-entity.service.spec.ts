import { Test, TestingModule } from "@nestjs/testing";
import { SmeEntityService } from "./sme-entity.service";

describe("SmeEntityService", () => {
  let service: SmeEntityService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SmeEntityService],
    }).compile();

    service = module.get<SmeEntityService>(SmeEntityService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
