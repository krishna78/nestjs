import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { BaseService } from "../common/base/base.service";
import { SmeEntity, ISmeEntity } from "./objects/sme-entity.schema";
@Injectable()
export class SmeEntityService extends BaseService<ISmeEntity> {
  constructor(
    @InjectModel("SmeEntity") private readonly smeEntityModel: Model<ISmeEntity>
  ) {
    super(smeEntityModel);
  }
}
