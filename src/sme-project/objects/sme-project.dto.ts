import { Expose, Type } from "class-transformer";
import {
  IsNotEmpty,
  IsDefined,
  MaxLength,
  IsOptional,
  ValidateNested,
  IsString,
  IsIn,
  IsInt,
} from "class-validator";
import { Reader, Creator, Updater } from "../../common/base/base.dto";
import { ApiModelProperty } from "@nestjs/swagger";
import { MilestoneDetailsDto } from "../../milestone/objects/milestone.dto";
import { FileUploadDto } from "../../file-upload/objects/file-upload.dto";
import {
  OWNER_VERIFIED_STATUS,
  PROJECT_TYPE,
  PRACTICE_AREA,
  TIMELINE_PREFERENCE_0,
  INVESTMENT_RANGE,
} from "../../common/constants/enum";
import { EntityDetailsDto } from "../../entity-details/objects/entity-details.dto";
import { RemarkDto } from "../../remarks/objects/remarks.dto";
import { UserDto } from "../../users/objects/create-user.dto";
import { BigchainDbDto } from "../../bigchain-db/objects/bigchain-db.dto";

class ProjectContractDto {
  @Expose()
  readonly fileId: string;
}

export class Owner {
  @Expose()
  readonly name: string;

  @Expose()
  readonly description: string;

  @IsIn(Object.values(OWNER_VERIFIED_STATUS))
  @Expose()
  readonly ownerVerified: number;
}

export class SmeProjectDto extends Reader {
  @Expose()
  readonly name: string = "";

  @Expose()
  readonly practiceArea: string;

  @Expose()
  readonly type: string;

  @Expose()
  readonly description: string;

  @Expose()
  readonly tenure: number;

  @Expose()
  @Type(() => MilestoneDetailsDto)
  readonly _milestoneDetails: MilestoneDetailsDto[];

  @Expose()
  @Type(() => RemarkDto)
  readonly _custRemarksDetails: RemarkDto[];

  @Expose()
  @Type(() => Owner)
  readonly owner: Owner;

  @Expose()
  readonly value: number;//string = "";

  @Expose()
  readonly fundsRequired: number;//string = "";

  @Expose()
  readonly equivalentBidPoints: number;

  @Expose()
  readonly tokensRequired: number;

  @Expose()
  @Type(() => FileUploadDto)
  readonly projectContractDetails: FileUploadDto[];

  @Expose()
  readonly createdBy: string = "";

  @Expose()
  readonly formattedId: string = "";

  @Expose()
  readonly projectApprovedTime: Date;

  @Expose()
  readonly projectApprovedBy: string = "";

  @Expose()
  readonly projectApprovedUserEmail: string = "";

  @Expose()
  readonly formattedUserId: string = "";

  @Expose()
  //readonly hasMilestones: boolean = false;
  readonly hasMilestones: number = 1;

  @Expose()
  @Type(() => EntityDetailsDto)
  readonly _entityDetailsSponsor: EntityDetailsDto;

  @Expose()
  readonly entityDetailCode: string;

  @Expose()
  @Type(() => UserDto)
  readonly userDetails: UserDto;

  @Expose()
  readonly favourite: string;

  @Expose()
  @Type(() => BigchainDbDto)
  readonly _bigChainDetails: BigchainDbDto;
}

export class CreateSmeProjectDto extends Creator {
  constructor() {
    super(true);
  }

  @IsDefined()
  @IsNotEmpty()
  @MaxLength(255, { message: "ProjectName is too long" })
  @ApiModelProperty()
  readonly name: string;

  @IsDefined()
  @IsNotEmpty()
  @IsIn(Object.values(PRACTICE_AREA))
  @ApiModelProperty()
  readonly practiceArea: string;

  @IsDefined()
  @IsNotEmpty()
  @IsIn(Object.values(PROJECT_TYPE))
  @ApiModelProperty()
  readonly type: string;

  @IsDefined()
  @IsNotEmpty()
  @MaxLength(255, { message: "ProjectDescription is too long" })
  @ApiModelProperty()
  readonly description: string;

  @IsDefined()
  @IsNotEmpty()
  @IsInt()
  //@MaxValue(50, { message: "ProjectTenure is too long" })
  @ApiModelProperty()
  readonly tenure: number;

  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  @Type(() => Owner)
  readonly owner: Owner;

  @IsDefined()
  @IsNotEmpty()
  @IsInt()
  //@IsString()
  //@MaxLength(20, { message: "ProjectValue is too long" })
  //@IsString()
  @ApiModelProperty()
  readonly value: number;

  @IsDefined()
  @IsInt()
  //@IsNotEmpty()
  //@IsString()
  //@MaxLength(20, { message: "FundsRequired is too long" })
  @ApiModelProperty()
  readonly fundsRequired: number;

  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  //@Type(() => ProjectContractDto)
  //readonly projectContract: ProjectContractDto [];
  readonly projectContract: string[];
}

export class UpdateSmeProjectDto extends Updater {
  @IsOptional()
  @IsNotEmpty()
  @MaxLength(255, { message: "ProjectName is too long" })
  @ApiModelProperty()
  readonly name: string;

  @IsOptional()
  @IsNotEmpty()
  @IsIn(Object.values(PRACTICE_AREA))
  @ApiModelProperty()
  readonly practiceArea: string;

  @IsOptional()
  @IsNotEmpty()
  @IsIn(Object.values(PROJECT_TYPE))
  @ApiModelProperty()
  readonly type: string;

  @IsOptional()
  @IsNotEmpty()
  @IsInt()
  //@MaxLength(50, { message: "ProjectTenure is too long" })
  @ApiModelProperty()
  readonly tenure: number;

  @IsOptional()
  @IsNotEmpty()
  @MaxLength(255, { message: "ProjectDescription is too long" })
  @ApiModelProperty()
  readonly description: string;

  @IsOptional()
  @ApiModelProperty()
  @Type(() => Owner)
  readonly owner: Owner;

  @IsOptional()
  @IsNotEmpty()
  @IsInt()
  //@IsString()
  //@MaxLength(20, { message: "ProjectValue is too long" })
  //@IsString()
  @ApiModelProperty()
  readonly value: number;

  @IsOptional()
  @IsNotEmpty()
  @IsInt()
  //@IsNotEmpty()
  //@IsString()
  //@MaxLength(20, { message: "FundsRequired is too long" })
  @ApiModelProperty()
  readonly fundsRequired: number;


  @IsOptional()
  @IsNotEmpty()
  @IsInt()
  @ApiModelProperty()
  readonly tokensRequired: number;

  @IsOptional()
  @IsNotEmpty()
  @ApiModelProperty()
  //@Type(() => ProjectContractDto)
  //readonly projectContract: ProjectContractDto [];
  readonly projectContract: string[];
}
export class SponsorProjectFilterDto {

  @IsOptional()
 // @IsNotEmpty()
  @IsIn(Object.values(PRACTICE_AREA))
  @ApiModelProperty()
  readonly practiceArea: string;

  @IsOptional()
 // @IsNotEmpty()
  @IsIn(Object.values(PROJECT_TYPE))
  @ApiModelProperty()
  readonly type: string;

  @IsOptional()
  //@IsNotEmpty()
 // @IsInt()
  @IsIn(Object.values(TIMELINE_PREFERENCE_0))
  //@MaxLength(50, { message: "ProjectTenure is too long" })
  @ApiModelProperty()
  readonly tenure: string;

  @IsOptional()
  //@IsNotEmpty()
  @IsInt()
  //@IsNotEmpty()
  //@IsString()
  //@MaxLength(20, { message: "FundsRequired is too long" })
  @ApiModelProperty()
  readonly fundsRequired: number;
  
  @IsOptional()
  @IsString()
  @Expose()
  @ApiModelProperty()
  readonly country: string;

  @IsOptional()
  //@IsNotEmpty()
  //@IsInt()
  //@IsNotEmpty()
  //@IsString()
  //@MaxLength(20, { message: "FundsRequired is too long" })
  @ApiModelProperty()
  readonly lessThanFundsRequired: boolean;


  @IsOptional()
  //@IsNotEmpty()
  //@IsInt()
  //@IsNotEmpty()
  //@IsString()
  //@MaxLength(20, { message: "FundsRequired is too long" })
  @IsIn(Object.values(INVESTMENT_RANGE))
  @ApiModelProperty()
  readonly rangeFundsRequired: string;
}