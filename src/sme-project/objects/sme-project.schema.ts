import { Entity, IEntity, createModel } from "../../common/base/base.model";
import { Schema } from "mongoose";
import {
  PROJECT_STATUS,
  OWNER_VERIFIED_STATUS,
  Status,
} from "../../common/constants/enum";

class Owner {
  name: string;
  description: string;
  ownerVerified: number;
}

export class ProjectContract {
  fileId: string;
}

export class SmeProject extends Entity {
  entityDetailCode: string;
  name: string;
  practiceArea: string;
  type: string;

  description: string;
  //milestoneDetails: Milestone [];
  owner: Owner;
  tenure: number;
  value: number;
  fundsRequired: number;
  projectContract: ProjectContract;
  // equivalentBidPoints: number;
  tokensRequired: number;
  formattedId: string;
  hasMilestones: number;
  projectApprovedTime: string;
  projectApprovedBy: string;
  projectApprovedUserEmail: string;
}

export interface ISmeProject extends SmeProject, IEntity {
  id: string;
}

export const SmeProjectSchema: Schema = createModel("SmeProjects", {
  name: { type: String },
  practiceArea: { type: String },
  type: { type: String },
  description: { type: String },
  tenure: { type: Number },
  status: { type: Number, default: PROJECT_STATUS.PENDING },
  owner: {
    name: { type: String },
    description: { type: String },
    ownerVerified: {
      type: Number,
      enum: Object.values({ ...OWNER_VERIFIED_STATUS }),
      default: OWNER_VERIFIED_STATUS.PENDING,
    },
  },
  value: { type: Number },
  fundsRequired: { type: Number },
  //equivalentBidPoints: { type: Number },
  tokensRequired: { type: Number },
  projectContract: [
    {
      type: String,
      //type: Schema.Types.ObjectId
      //fileId: { type: String },
    },
  ],
  formattedId: { type: String },
  entityDetailCode: { type: String },
  hasMilestones: { type: Number, default: 0 },
  projectApprovedTime: { type: String },
  projectApprovedBy: { type: String },
  projectApprovedUserEmail: { type: String }
});

SmeProjectSchema.virtual("_milestoneDetails", {
  ref: "Milestone",
  localField: "code",
  foreignField: "project",
  justOne: false,
  /*
  options: {
    select: "number + name + complexity + completion",
  },
  */
});

SmeProjectSchema.virtual("_custRemarksDetails", {
  ref: "Remark",
  localField: "code",
  foreignField: "project",
  justOne: false,
  /*
  options: {
    select: "number + name + complexity + completion",
  },
  */
});

SmeProjectSchema.virtual("projectContractDetails", {
  ref: "FileUpload",
  localField: "projectContract",
  foreignField: "_id",
  justOne: false,
  options: {
    select: "mimetype + originalname",
  },
});

SmeProjectSchema.virtual("_entityDetailsSponsor", {
  ref: "EntityDetails",
  localField: "entityDetailCode",
  foreignField: "code",
  justOne: true,
  /*
  options: {
    select: "mimetype + originalname",
  },
  */
});

SmeProjectSchema.virtual("_bigChainDetails", {
  ref: "BigchainDb",
  localField: "code",
  foreignField: "projectRef",
  justOne: true,
  /*
  */
  options: {
    select: "transactionId + publickey + type + isDummyTransaction",
  },
  
  /**/
});


SmeProjectSchema.virtual("userDetails", {
  ref: "User",
  localField: "createdBy",
  foreignField: "code",
  justOne: true,
  /*
   */
  options: {
    select: "firstName + lastName + email",
  },
  /*
   */
});
SmeProjectSchema.virtual("equivalentBidPoints").get(function () {
  return Math.round(this.fundsRequired / 100);
  // return Math.round(+this.fundsRequired.replace(/\,/g, "") / 100);
});

SmeProjectSchema.pre<ISmeProject>("find", function (next) {
  /* 
  var _a = this;
    
  var conditions = this.getQuery()
  console.log('BeforeAll........',
  _a,
  conditions,conditions.status)
  if(_a._conditions !== undefined){
    if(_a._conditions.status !== undefined && _a._conditions.status !== 0){
      //_a.setQuery({ status: { $lte: Status.DISABLED }, ...this.getQuery() });
      _a._conditions.status = { $lte: Status.DISABLED }
      this.setOptions({ projection: {}})
     // _a.set({'$useProjection': false})
      console.log('afterre........',this.getQuery())
     return next()
      //this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });
    }
  }
  */

  this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });
  this.set({ updatedTime: new Date() });
  next();
});
/*
SmeProjectSchema.pre<ISmeProject>('findOne', function(next) { 
  var _a = this;
  console.log('findOneeeeeeeee',_a)
  this.setQuery({ status: { $gte: Status.DISABLED }, ...this.getQuery() });
  this.set({ updatedTime: new Date() });
  next();
 });
 */
