import { Creator } from "../../common/base/base.dto";
import { IsDefined, IsNotEmpty } from "class-validator";
import { ProjectStatus } from "src/project-status/objects/project-status.schema";
import { Expose, Type } from "class-transformer";

export class SmeProjectStatus {
  
  @Expose()
  readonly isStatusZero: boolean;
  
  @Expose()
  readonly isStatusOne: boolean;
  
  @Expose()
  readonly isStatusTwo: boolean;
 
  @Expose()
  readonly isStatusThree: boolean;
}

export class CreateProjectEmailNotification extends Creator {
  constructor({ projectcode }) {
    super(true);
    /*this.creator = creator;
    this.isOnboardingInitiated=isOnboardingInitiated,
    this.isKycApproved=isKycApproved,
    this.isKycRejected=isKycRejected,
    this.isKycPending=isKycPending,
    this.isDocumentsSubmitted=isDocumentsSubmitted*/
    this.projectcode = projectcode;

    debugger;
  }

  @IsDefined()
  @IsNotEmpty()
  readonly isProjectCreated: boolean;

  @IsDefined()
  @IsNotEmpty()
  readonly isProjectPending: boolean;

  @IsDefined()
  @IsNotEmpty()
  readonly isProjectApproved: boolean;
  
  @IsDefined()
  @IsNotEmpty()
  readonly isProjectDocumentsRequired: boolean;

  @IsDefined()
  @IsNotEmpty()
  readonly isProjectCompleted: boolean;

  @IsDefined()
  @IsNotEmpty()
  readonly projectcode: string;
  
  @Type(() => SmeProjectStatus)
  readonly _smeprojectStatus: SmeProjectStatus
}


