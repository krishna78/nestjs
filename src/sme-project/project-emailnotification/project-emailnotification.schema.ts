import { Schema } from "mongoose";
import { createModel, Entity, IEntity } from "../../common/base/base.model";

export class ProjectEmailNotification extends Entity {
  isProjectCreated: boolean;
  isProjectPending: boolean;
  isProjectApproved: boolean;
  isProjectRejected: boolean;
  isProjectDocumentsRequired: boolean;
  isProjectCompleted: boolean;
  projectcode: string;
  smeProjectStatus:{
    isStatusZero: boolean;
    isStatusOne: boolean;
    isStatusTwo: boolean;
    isStatusThree: boolean;
  }
}

export interface IProjectEmailNotification
  extends ProjectEmailNotification,
    IEntity {
  id: string;
}

export const ProjectEmailNotificationSchema: Schema = createModel(
  "ProjectEmailNotification",
  {
    isProjectCreated: { type: Boolean, default: false },
    isProjectPending: { type: Boolean, default: false },
    isProjectApproved: { type: Boolean, default: false },
    isProjectRejected: { type: Boolean, default: false },
    isProjectDocumentsRequired: { type: Boolean, default: false },
    isProjectCompleted: { type: Boolean, default: false },
    projectcode: { type: String },
    smeProjectStatus:{
      isStatusZero: { type: Boolean, default: true },
      isStatusOne: { type: Boolean, default: false },
      isStatusTwo: { type: Boolean, default: false },
      isStatusThree: { type: Boolean, default: false },
    }
  }
);
