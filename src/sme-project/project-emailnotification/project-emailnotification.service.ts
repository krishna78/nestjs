import { Injectable } from "@nestjs/common";
import { BaseService } from "../../common/base/base.service";
import {
  IProjectEmailNotification,
  ProjectEmailNotification,
} from "../project-emailnotification/project-emailnotification.schema";
import { CreateProjectEmailNotification } from "../project-emailnotification/project-emailnotification.dto";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { from } from "rxjs";

@Injectable()
export class ProjectEmailNotificationService extends BaseService<
  IProjectEmailNotification
> {
  constructor(
    @InjectModel("ProjectEmailNotification")
    private readonly projectemailNoticationModel: Model<
      ProjectEmailNotification
    >
  ) {
    super(projectemailNoticationModel);
  }

  async create(createDto: any, user) {
    let data = new CreateProjectEmailNotification(createDto);

    console.log(data);

    return super.create(data, user);
  }
}
