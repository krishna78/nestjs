import { Test, TestingModule } from '@nestjs/testing';
import { SmeProjectController } from './sme-project.controller';

describe('SmeProject Controller', () => {
  let controller: SmeProjectController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SmeProjectController],
    }).compile();

    controller = module.get<SmeProjectController>(SmeProjectController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
