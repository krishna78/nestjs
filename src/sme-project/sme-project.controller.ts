import {
  Controller,
  Get,
  UseGuards,
  Request,
  Query,
  Put,
  Body,
  Post,
  BadRequestException,
  NotFoundException,
  Delete,
} from "@nestjs/common";
import {
  SmeProjectDto,
  CreateSmeProjectDto,
  UpdateSmeProjectDto,
  SponsorProjectFilterDto,
} from "./objects/sme-project.dto";
import {
  BASEROUTES,
  USER_TYPES,
  ACTIVITY_FEED_EVENTS_SME,
  ACTIVITY_FEED_EVENTS_SPONSOR,
  PROJECT_STATUS,
  Status,
  KYC_VERIFICATION_STATUS,
  PROJECT_TENURE,
  TIMELINE_PREFERENCE_0,
  ROLES_ACCESS_ACTION,
  INVESTMENT_RANGE,
} from "../common/constants/enum";
import { abstractBaseControllerFactory } from "../common/base/base.controller";
import { SmeProjectService } from "./sme-project.service";
import {
  RequestUser,
  IdOrCodeParser,
} from "../common/utils/controller.decorator";
import { JwtAuthGuard } from "../auth/auth.guard";
import { success } from "../common/base/httpResponse.interface";
import { AbstractClassTransformerPipe } from "../common/pipes/class-transformer.pipe";
import {
  ONLY_FOR_SME,
  PROJECT_EDIT_ERROR,
  PROJECT_APPROVED_STATUS_CHANGE_ERROR,
  PROJECT_FUNDED_STATUS_CHANGE_ERROR,
  PROJECT_COMPLETED_STATUS_CHANGE_ERROR,
  PROJECT_PENDING_STATUS_CHANGE_ERROR,
  PROJECT_REJECTED_STATUS_CHANGE_ERROR,
  PROJECT_DOCUMENTS_REQUIRED_STATUS_CHANGE_ERROR,
  ONLY_FOR_CREATED_BY_SME_OR_ADMIN,
  PROJECT_APPROVED_STATUS_CHANGE_ERROR1,
  FUNDS_REQUESTED_ERROR,
  CANNOT_DELETE_FUNDED_PROJECTS,
  PROJECT_PENDING_STATUS_CHANGE_ERROR_SME_KYC_NOT_APPROVED,
  ONLY_FOR_SPONSOR,
  USER_PREFERENCES_NOT_FILLED,
  PROJECT_FUNDED_STATUS_CHANGE_PENDING_ERROR,
  PROJECT_FUNDED_STATUS_CHANGE_REJECTED_ERROR,
  PROJECT_FUNDED_STATUS_CHANGE_DOCUMENTS_REQUIRED_ERROR,
  PROJECT_PENDING_TO_FUNDED_ERROR,
} from "../common/constants/string";
import { SmeActivityFeedService } from "../sme-activity-feed/sme-activity-feed.service";
import { use } from "passport";
import { ActivityFeedService } from "../activity-feed/activity-feed.service";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { ISmeProject } from "./objects/sme-project.schema";
import { BidDetailsService } from "../bid-details/bid-details.service";
import { IBidDetails } from "../bid-details/objects/bid-details.schema";
import { UsersService } from "../users/users.service";
import { plainToClass } from "class-transformer";
import { normalizePaginateResult } from "../common/interfaces/pagination";
import { IEntityDetails } from "../entity-details/objects/entity-details.schema";
import { IFavouriteProjects } from "../favourite-projects/objects/favourite-projects.schema";
import { RolesService } from "../roles/roles.service";

const BaseController = abstractBaseControllerFactory<any>({
  DTO: SmeProjectDto,
  CreateDTO: CreateSmeProjectDto,
  UpdateDTO: UpdateSmeProjectDto,
  DisabledRoutes: [
    //BASEROUTES.DETELEONE,
    BASEROUTES.PATCH,
  ],
});

@UseGuards(JwtAuthGuard)
@Controller("sme-project")
export class SmeProjectController extends BaseController {
  constructor(
    private smeProjectService: SmeProjectService,
    private smeactivityFeedService: SmeActivityFeedService,
    private activityFeedService: ActivityFeedService,
    private usersService: UsersService,
    //  private bidDetailsService: BidDetailsService,
    //  @InjectModel("BidDetails")
    //  private readonly bidDetailsModel: Model<IBidDetails>,
    @InjectModel("SmeProject")
    private readonly smeProjectModel: Model<ISmeProject>,
    @InjectModel("EntityDetails")
    private readonly entityDetailsModel: Model<IEntityDetails>,
    @InjectModel("FavouriteProject")
    private readonly favouriteProjectsModel: Model<IFavouriteProjects>,
    private rolesservice: RolesService
  ) {
    super(smeProjectService);
  }

  @Get()
  async findList(@Request() req, @Query() query, @RequestUser() user) {
    console.log('yesssssssss');
    debugger;
  //  if (user.isAdmin || user.type == USER_TYPES.SPONSOR) {
    let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
    
    let hasAccess = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_FINDLIST
    );
   console.log('userstype', user.type, _userProfile, hasAccess)
   if(hasAccess) {
     // <--- only admin and Sponsor can see all the project lists
      const _d = await super.findList(req, { ...query, sort: { _id: -1 }});
      let dPage ={

       total: _d.data.total,
          page: _d.data.page,
          nextPage: _d.data.nextPage,
           prevPage: _d.data.prevPage
      }
      //console.log('Smeprojectsss', _d)
      let _query = { createdBy: user.code }
      var options = {
        limit: 100,
        page: 1,
        sort: "_id",
      }
      let k = await this.favouriteProjectsModel.find(_query);
      //console.log('favourite projectsss', k);
      /*
      let updated = await Promise.all(
        await updateDto.milestones.map(async (element) => {
          let code = element.code;
          delete element["code"];
          let milestone = await this.milestoneService.edit(code, element);
          return plainToClass(MilestoneDetailsDto, milestone, {
            excludeExtraneousValues: true,
          });
        })
      );
      */
     let d;
     if(k.length == 0){
       d = await Promise.all( 
        await _d.data.docs.map(async(element) => {
          element = {
            ...element,
            favourite: false
          }
          return plainToClass(SmeProjectDto, element, {
            excludeExtraneousValues: true,
          });
        }));
     }
     else {
        d = await Promise.all( 
          _d.data.docs.map(async(element) => {
            let fav = k.find((e) => {
                return e.projectId == element.code
              })
              if(fav){
                element = {
                  ...element,
                  favourite: true
                }
              } else {
                element = {
                  ...element,
                  favourite: false
                }
              }
              return plainToClass(SmeProjectDto, element, {
                excludeExtraneousValues: true,
              });

          /*
          await _d.data.docs.map(async(element) => {
            await k.map((e) => {
                  if(element.code == e.projectId){
                    element = {
                      ...element,
                      favourite: true
                    }
                  } else {
                  element = {
                      ...element,
                      favourite: false
                    }
                  }
                  //console.log('elememt', element)
                
            })
            return plainToClass(SmeProjectDto, element, {
              excludeExtraneousValues: true,
            });
            */
        }));
      
      }
     //console.log('Favvvvvvvvvvvvvvv',d);
     return success({d,dPage});
    }
    let userObject = { createdBy: user.code };
    console.log(userObject);
    const d = await super.findList(req, { ...query, sort: { _id: -1 }, ...userObject });

    console.log(d);

    return d;
  }

  @Get(":idOrCode")
  async findOne(
    @IdOrCodeParser("idOrCode") idOrCode: string,
    @Query() query,
    @RequestUser() user
  ) {
    debugger;
    let project = await super.findOne(idOrCode, query);
    let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
    
    let hasAccess = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_FINDONE
    );
   console.log('userstype', user.type, _userProfile, hasAccess)
   //if(hasAccess) {
    if (
      hasAccess ||
      user.code === project.data.createdBy
    ) {
      if (user.type === USER_TYPES.SPONSOR) {
        let data = await this.activityFeedService.findOneByQuery({
          itemId: idOrCode,
        });
        if (data == undefined) {
          //activity feed for sponsor dashboard
          let createDto3: any = {
            _priority: 2,
            type: ACTIVITY_FEED_EVENTS_SPONSOR.PROJECTS_VIEWED_BY_SPONSOR,
            itemId: idOrCode,
            setFields: {
              message: "Project viewed ",
              projectName: project.data.name,
            },
            optional: project.data.formattedId,
          };
          debugger;
          let _activityFeed: any = await this.activityFeedService.projectsViewedBySponsor(
            createDto3,
            user
          );
          //activity feed for sme dashboard
          user.code = project.data.createdBy;
          let createDto2: any = {
            _priority: 2,
            type: ACTIVITY_FEED_EVENTS_SME.PROJECT_VIEWED_BY_SPONSOR,
            itemId: idOrCode,
            setFields: {
              message: "Project viewed by sponsor",
              projectName: project.data.name,
            },
            optional: project.data.formattedId,
          };
          debugger;
          let _activityFeed1: any = await this.smeactivityFeedService.projectsViewedBySponsor(
            createDto2,
            user
          );
        }
      }
      return project;
    }
    throw new NotFoundException();
  }
/*
  @Get("archive/projects")
  async getDeletedProjects(
    @Request() req,
    @Query() query,
    @RequestUser() user
  ) {
    if (user.type == USER_TYPES.ADMIN || user.type == USER_TYPES.SPONSOR) {
      if (query.status !== undefined && query.status == Status.DELETED) {
        let k: any = await this.smeProjectModel
          .find({ status: -1 })
          .sort({ updatedTime: -1 })
          .limit(1000);
        console.log("Deleted items", k);
        return success(k);
      }
    }
    throw new NotFoundException();
  }
*/
  @Get("recommended/projects")
  async getRecommendedProjects(
    @Request() req,
    @Query() query,
    @RequestUser() user
  ) {
   // if (user.type == USER_TYPES.SPONSOR) {
    let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
    
    let hasAccess = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_RECOMMENDED_PROJECTS
    );
   console.log('userstype', user.type, _userProfile, hasAccess)
   if(hasAccess) {
      let preferences = user.preferences;
      console.log("preferencesss", preferences);
      let d;
      let queryObject: any;
      if (
        preferences.timeline == undefined ||
        preferences.investmentRange == undefined ||
        preferences.projectType == undefined ||
        preferences.practiceArea == undefined
      ) {
        throw new BadRequestException(USER_PREFERENCES_NOT_FILLED);
      }
      
      
      let t;
      /*
      var options = {
        limit: 30,
        page: 1,
        sort: "_id",
      };
      */
      var options = {
        limit: 10,
        page: 1,
        sort: "_id",
        skip: query.page ? (query.page - 1) : 0
      };
      queryObject = {
        type: preferences.projectType,
        practiceArea: preferences.practiceArea,
      };
      if (preferences.timeline !== undefined) {
        switch(preferences.timeline){
          case(TIMELINE_PREFERENCE_0.LESS_THAN_SIX_MONTHS):
            t = { tenure: { $lte: 6 } };
            queryObject = { ...t, ...queryObject };
            break;
          case(TIMELINE_PREFERENCE_0.SIX_MONTHS_TO_ONE_YEAR):
            t = { $and: [{ tenure: { $lte: 12 } }, { tenure: { $gt: 6 } }] };
            queryObject = { ...t, ...queryObject };
            break;
          case(TIMELINE_PREFERENCE_0.GREATER_THAN_ONE_YEAR):
            t = { tenure: { $gt: 12 } };
            queryObject = { ...t, ...queryObject };
            break;
        }
      }

      if (preferences.investmentRange !== undefined) {
        t = { fundsRequired: { $lte: preferences.investmentRange } };
        queryObject = { ...t, ...queryObject };
      }
      
      queryObject = { ...queryObject, status: PROJECT_STATUS.APPROVED };
     
      d = await this.smeProjectModel.find(
        queryObject,
        //{
        //status: PROJECT_STATUS.APPROVED
        //},
        {},
        { sort: { _id: 1 },// skip: 0, limit: 30, 
       skip: options.skip * options.limit, limit: options.limit,projection: {} }
      );
    
      let dCount = await this.smeProjectModel.count(
        queryObject
        //{ status: PROJECT_STATUS.APPROVED },
      );
      console.log('dddddddddddddddddddddddddddd', d, dCount)

      var projectIdsArray: string[] = [];
      await d.map(async (data) => {
        await projectIdsArray.push(data.code);
        console.log("projectIdsssArray", projectIdsArray);
       // return plainToClass(SmeProjectDto, data, {
       //   excludeExtraneousValues: true,
       // });
      });
      let pagination = normalizePaginateResult({
        total: dCount,//d.length,
        limit: options.limit,
        page: query.page,//options.page,
        pages: d.pages,
      });
      let q = { code: projectIdsArray}
      let k = await this.findList(req, q, user );
      return { k, ...pagination };
    }
    throw new BadRequestException(ONLY_FOR_SPONSOR);
  }

  @Post()
  public async create(
    @Request() req,
    @Body(AbstractClassTransformerPipe(CreateSmeProjectDto)) body: any,
    @Query() query,
    @RequestUser() user
  ) {
   // if (user.type == USER_TYPES.SME) {
    let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
    
    let hasAccess = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_CREATE
    );
   console.log('userstype', user.type, _userProfile, hasAccess)
   if(hasAccess) {
     let conditional = body.value > body.fundsRequired
       // Number(body.value.replace(/\,/g, "")) >
       // Number(body.fundsRequired.replace(/\,/g, ""));
      console.log("Conditionallllllll", conditional);
      if (!conditional) {
        throw new BadRequestException(FUNDS_REQUESTED_ERROR);
      }

      let _body = {
        ...body,
        entityDetailCode: user.entityDetailCode,
        createdBy: user.code,
        formattedUserId: user.formattedId,
      };
      debugger;
      delete _body["status"];
      let data = await this.smeProjectService.create(_body);
      //  let _body = { ...body, entityDetailCode: user.entityDetailCode }
      //  let data = await super.create(_body,user);
      return success(data);
    }
    throw new BadRequestException(ONLY_FOR_SME);
  }

  /*
   
  @UseGuards(JwtAuthGuard)
  @Post("filter/projectCountryFilter")
  async findListFilterprojectCountry(
    @Request() req,
    @Query() query,
    @RequestUser() user,
    @Body() body: { country: string }
  ) {
    var options = {
      limit: 30,
      page: 1,
      sort: "_id",
    };

    //let _query = { createdBy: user.code, status: 2 };
    let projectId: string;
    var userIdsArray: string[] = [];
    var smeUserIdsArray: string[] = [];
    let _entityDetails = await this.entityDetailsModel.find(
      { "company.address.country": body.country },
      {},
      {
        sort: { _id: 1 },
        skip: 0,
        // limit: 30,
        projection: {},
      }
    );
    console.log("_entityDetails", _entityDetails);
    // await Promise.all(bid.map(async (element) => {
    await Promise.all(
      _entityDetails.map(async (element) => {
        await userIdsArray.push(element.createdBy);
        console.log("userssssssssArray", userIdsArray);
      })
    );
    await Promise.all(
      userIdsArray.map(async (element) => {
        let k = await this.usersService.findOne(element);
        if (k.type == USER_TYPES.SME) {
          await smeUserIdsArray.push(k.code);
        }
        // await userIdsArray.push(element.createdBy);
        console.log("smeeeeArray", smeUserIdsArray);
      })
    );
    let q = { createdBy: smeUserIdsArray };
    const d = await this.findList(req, q, user);
    //const d = await this.smeProjectService.findAll( q , options);
    return d;
    //return await this.smeProjectModel.find(q);
  }
   */

  @UseGuards(JwtAuthGuard)
  @Post("filter/projectSponsorFilter")
  async projectSponsorFilter(
    @Request() req,
    @Query() query,
    @RequestUser() user,
    @Body(AbstractClassTransformerPipe(SponsorProjectFilterDto)) body: any,
    //@Body() body: { tenure: string; country: string; fundsRequired: number; lessThanFundsRequired: boolean; }
  ) {
    //if (user.type == USER_TYPES.ADMIN || user.type == USER_TYPES.SPONSOR) {
      let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
    
    let hasAccess = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_SPONSOR_FILTER
    );
   console.log('userstype', user.type, _userProfile, hasAccess)
   if(hasAccess) {
     /*
      var options = {
        limit: 30,
        page: 1,
        sort: "_id",
      };
      */
      var options = {
        limit: 10,
        page: 1,
        sort: "_id",
        skip: query.page ? (query.page - 1) : 0
      };
      let d: any = [];
      let _q: any;
     /*
      d = await this.smeProjectModel.find(
        { status: PROJECT_STATUS.APPROVED },
        {},
        { sort: { _id: 1 }, skip: 0, limit: 30, projection: {} }
      );
      */
     
      // <--- only admin and Sponsor can see all the USER lists
      // const d = await this.findList(req, { ...query });

      //const d =  await this.findList(req,  query,  user);
      //let d = await this.usersService.findAll(query, options);
      //let d = await this.usersModel.find({"verification.isProfileCompleted": body.isProfileCompleted}, options);
      

      
      
      if (body.tenure !== undefined) {
        switch(body.tenure){
          case(TIMELINE_PREFERENCE_0.LESS_THAN_SIX_MONTHS):
            _q = { tenure: { $lte: 6 } };
            
            break;
          case(TIMELINE_PREFERENCE_0.SIX_MONTHS_TO_ONE_YEAR):
            let t = { $and: [{ tenure: { $lte: 12 } }, { tenure: { $gt: 6 } }] };
            _q = { ...t };
            break;
          case(TIMELINE_PREFERENCE_0.GREATER_THAN_ONE_YEAR):
            _q = { tenure: { $gt: 12 } };
            break;

        }
      }
      /*
      if (body.tenure !== undefined) {
        if (body.tenure < PROJECT_TENURE.LESS_THAN_SIX_MONTHS) {
          _q = { tenure: { $lt: 6 } };
          //d = await this.smeProjectModel.find({"tenure": { $lt: 6 }}, {},{ sort: { _id: 1 }, skip: 0, limit: 30, projection: {} });
        } else if (
          body.tenure > PROJECT_TENURE.LESS_THAN_SIX_MONTHS &&
          body.tenure <= PROJECT_TENURE.SIX_MONTHS_TO_ONE_YEAR
        ) {
          _q = { tenure: { $lte: 12 } };
          //d = await this.smeProjectModel.find({"tenure": { $lte: 12 }}, {},{ sort: { _id: 1 }, skip: 0, limit: 30, projection: {} });
        } else if (body.tenure > PROJECT_TENURE.MORE_THAN_ONE_YEAR) {
          _q = { tenure: { $gt: 12 } };
          //d = await this.smeProjectModel.find({"tenure": { $gt: 12 }}, {},{ sort: { _id: 1 }, skip: 0, limit: 30, projection: {} });
        }
      }
      */

      if (body.practiceArea !== undefined) {
        _q = { practiceArea: body.practiceArea, ..._q };
        //d = await this.smeProjectModel.find({"practiceArea": query.practiceArea}, {},{ sort: { _id: 1 }, skip: 0, limit: 30, projection: {} });
      }
      
      if (body.type !== undefined) {
        //_q = { type: body.type , ..._q};
        //d = await this.smeProjectModel.find({"type": query.type}, {},{ sort: { _id: 1 }, skip: 0, limit: 30, projection: {} });
      }

      if (body.rangeFundsRequired !== undefined 
        //&& body.lessThanFundsRequired !== undefined
        ) {
          let a = body.rangeFundsRequired == INVESTMENT_RANGE.LESS_THAN_SGD_10000
          let b = body.rangeFundsRequired == INVESTMENT_RANGE.SGD_10000_TO_SGD_50000
          let c = body.rangeFundsRequired == INVESTMENT_RANGE.SGD_50000_TO_SGD_100000
          let d = body.rangeFundsRequired == INVESTMENT_RANGE.GREATER_THAN_SGD_100000
          console.log(a, b, c, d)
      //  switch(body.rangeFundsRequired){
       //   case a:
      
     //  let t = { $and: [{ tenure: { $lte: 12 } }, { tenure: { $gt: 6 } }] };
       let t1 =   { $and: [{ "fundsRequired": { $lte: 50000 } }, { "fundsRequired": { $gt: 10000 } }] }
       let t2 = { $and: [{ fundsRequired: { $lte: 100000 } }, { fundsRequired: { $gt: 50000 } }] }
       let t3 = { $gt: 100000 }
       console.log('t1,t2,t3' , t1, t2, t3)
       if(a){
        _q = { "fundsRequired":  { $lt: 10000 } , ..._q  };
       }
    
           // break;
          //case b:
          if(b){
            _q = {   $and: [{ "fundsRequired": { $lte: 50000 } }, { "fundsRequired": { $gt: 10000 } }] , ..._q  };
          }
          
          //  break;
         // case c:
         if(c){
          _q = {  $and: [{ fundsRequired: { $lte: 100000 } }, { fundsRequired: { $gt: 50000 } }], ..._q  };
         }
            
          //  break;
          //case d:
          if(d){
            _q = { "fundsRequired":  t3 , ..._q  };
          }
       
          //  break;    

       // }
        
       /*
        switch(body.lessThanFundsRequired){
          case false:
            _q = { fundsRequired: { $gt: body.fundsRequired }, ..._q  };
            break;
          case true:
            _q = { fundsRequired: { $lte: body.fundsRequired }, ..._q  }; 
        }
        */
      }
      /*
       */
      if (body.country !== undefined) {
        let projectId: string;
        var userIdsArray: string[] = [];
        var smeUserIdsArray: string[] = [];
        let _entityDetails = await this.entityDetailsModel.find(
          { "company.address.country": body.country },
          {},
          {
            sort: { _id: 1 },
            //skip: 0,
            // limit: 30,
            projection: {},
          }
        );
        console.log("_entityDetails", _entityDetails);
        // await Promise.all(bid.map(async (element) => {
        await Promise.all(
          _entityDetails.map(async (element) => {
            await userIdsArray.push(element.createdBy);
            console.log("userssssssssArray", userIdsArray);
          })
        );
        await Promise.all(
          userIdsArray.map(async (element) => {
            let k = await this.usersService.findOne(element);
            if (k.type == USER_TYPES.SME) {
              await smeUserIdsArray.push(k.code);
            }
            // await userIdsArray.push(element.createdBy);
            console.log("smeeeeArray", smeUserIdsArray);
          })
        );
        let q = { createdBy: smeUserIdsArray };
        _q = { ...q, ..._q };
      }
      _q = { status: PROJECT_STATUS.APPROVED, ..._q };
     // d = await this.smeProjectModel.find()
      d = await this.smeProjectModel.find(
        _q,
        //{
        //status: PROJECT_STATUS.APPROVED
        //},
        {},  { sort: { _id: 1 },// skip: 0, limit: 30, 
        skip: options.skip * options.limit, limit: options.limit,projection: {}}
       // { sort: { _id: 1 }, skip: 0, limit: 30, projection: {} }
      );
      
      let dCount = await this.smeProjectModel.count(
        _q,
      );
     
      //DcOUNT IMPLEMENTED HERE.
      console.log(
        "filterrrrrrrrrrr",
        _q,
        query,
        "body.tenure",
        body.tenure,
        "body.practiceArea",
        query.practiceArea,
        "body.type",
        query.type,
        "dCount",
        dCount
      );
      var projectIdsArray: string[] = [];
      await d.map(async (data) => {
        await projectIdsArray.push(data.code);
        console.log("projectIdsssArray", projectIdsArray);
       // return plainToClass(SmeProjectDto, data, {
       //   excludeExtraneousValues: true,
       //   });
      });
      let q = { code: projectIdsArray}
      let k = await this.findList(req, q, user );
      let pagination = normalizePaginateResult({
        total: dCount,//d.length,
        limit: options.limit,
        page: query.page,//options.page,
        pages: d.pages,
      });
      return { k, ...pagination };
      //return k;
      /*
       d = await this.findList(req, q, user)
      */
      /*
      switch(body.tenure){
        case (body.tenure < PROJECT_TENURE.LESS_THAN_SIX_MONTHS):

          break;
        case PROJECT_TENURE.SIX_MONTHS_TO_ONE_YEAR:

          break;
        case PROJECT_TENURE.MORE_THAN_ONE_YEAR:

          break;  
      }
      */
      //let d = await this.smeProjectModel.find({"tenure": body.tenure}, {},{ sort: { _id: 1 }, skip: 0, limit: 30, projection: {} });
      //console.log('dddddddddddddddddddddddddddddd', d.length)
      await d.map((data) => {
        return plainToClass(SmeProjectDto, data, {
          excludeExtraneousValues: true,
        });
      });
      let _pagination = normalizePaginateResult({
        total: d.length,
        limit: options.limit,
        page: options.page,
        pages: d.pages,
      });

      //return success
      return success({ d, pagination });
    }
  }

  @Put(":idOrCode")
  async update(
    @IdOrCodeParser("idOrCode") idOrCode: string,
    @Request() req,
    @Body(AbstractClassTransformerPipe(UpdateSmeProjectDto)) updateDto,
    @RequestUser() user
  ) {
    let project = await this.smeProjectService.findOne(idOrCode);
    console.log("_projectttttt", project, user.isAdmin);
   // if (user.isAdmin || user.code === project.createdBy) {
    let _userProfile = await this.rolesservice.findOneByQuery({roleName: user.type});
    
    let hasAccess = _userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.SME_PROJECT_CONTROLLER_UPDATE
    );
    let tCheckAllFields =
    updateDto.name ||
    updateDto.practiceArea ||
    updateDto.type ||
    updateDto.tenure ||
    updateDto.description ||
    updateDto.owner ||
    updateDto.value ||
    updateDto.fundsRequired ||
    updateDto.tokensRequired;

   console.log('userstype', user.type, _userProfile, hasAccess)
   if(hasAccess 
    //|| user.code === project.createdBy
    ) {
      debugger;
    
      switch (project.status) {
        case PROJECT_STATUS.PENDING: {
          if (
            updateDto.fundsRequired !== undefined &&
            updateDto.value !== undefined
          ) {
            let conditional = updateDto.value > updateDto.fundsRequired
            console.log(
              "Conditionallllllll111111111",
              conditional,
              updateDto.value, updateDto.fundsRequired
            );
            if (!conditional) {
              throw new BadRequestException(FUNDS_REQUESTED_ERROR);
            }
          } else if (
            updateDto.value == undefined &&
            updateDto.fundsRequired !== undefined
          ) {
            let conditional = project.value > updateDto.fundsRequired
            console.log("Conditionallllllll2222222222222", conditional);
            if (!conditional) {
              throw new BadRequestException(FUNDS_REQUESTED_ERROR);
            }
          } else if (
            updateDto.fundsRequired == undefined &&
            updateDto.value !== undefined
          ) {
            let conditional = updateDto.value > project.fundsRequired
            console.log("Conditionallllllll33333333333", conditional);
            if (!conditional) {
              throw new BadRequestException(FUNDS_REQUESTED_ERROR);
            }
          }
         // if (!user.isAdmin) {
          
          //if (user.type == USER_TYPES.ADMIN) {
            let s = updateDto.status;
            if (updateDto.status == undefined) {
              console.log(
                "__________________________No status field___________________________"
              );
            } else {
              //if(updateDto.status){
              console.log("updateDTooooo.status", updateDto.status, s);
              let _user = await this.usersService.findOne(project.createdBy);
              if (
                _user.verification.isProfileCompleted !==
                KYC_VERIFICATION_STATUS.APPROVED
              ) {
                throw new BadRequestException(
                  PROJECT_PENDING_STATUS_CHANGE_ERROR_SME_KYC_NOT_APPROVED
                );
              }
             
              if (updateDto.status == PROJECT_STATUS.APPROVED) {
               //Add approved fields for tracking Admins who have approved projects.
                updateDto.projectApprovedTime = Date.now(),
                updateDto.projectApprovedBy = user.code,
                updateDto.projectApprovedUserEmail = user.email
              }
              if (
                !(
                  updateDto.status == PROJECT_STATUS.PENDING ||
                  updateDto.status == PROJECT_STATUS.APPROVED ||
                  updateDto.status == PROJECT_STATUS.DOCUMENTSREQUIRED ||
                  updateDto.status == PROJECT_STATUS.REJECTED
                )
              ) {
                throw new BadRequestException(
                  PROJECT_PENDING_TO_FUNDED_ERROR
                  //PROJECT_PENDING_STATUS_CHANGE_ERROR

                );
              }
              
              //}
            }
          //}

          break;
        }
        case PROJECT_STATUS.APPROVED: {
          if (!tCheckAllFields) {
            //if (user.type == USER_TYPES.ADMIN) {
              let s = updateDto.status;
              if (updateDto.status == undefined) {
                console.log(
                  "__________________________No status field___________________________"
                );
              } else {
                console.log("updateDTooooo.status", updateDto.status);
                // if(updateDto.status){

                if (
                  !(
                    updateDto.status == PROJECT_STATUS.APPROVED ||
                    updateDto.status == PROJECT_STATUS.FUNDED
                  )
                ) {
                  throw new BadRequestException(
                    PROJECT_APPROVED_STATUS_CHANGE_ERROR
                  );
                }

                if (updateDto.status == PROJECT_STATUS.FUNDED) {
                  throw new BadRequestException(
                    PROJECT_APPROVED_STATUS_CHANGE_ERROR1
                  );
                }
                // }
              }
            //}
          } else {
            throw new BadRequestException(PROJECT_EDIT_ERROR);
          }

          break;
        }
        case PROJECT_STATUS.FUNDED: {
          if (!tCheckAllFields) {
            //if (user.type == USER_TYPES.ADMIN) {
              let s = updateDto.status;
              if (updateDto.status == undefined) {
                console.log(
                  "__________________________No status field___________________________"
                );
              } else {
                console.log("updateDTooooo.status", updateDto.status);
                //  if(updateDto.status){

                if (
                  !(
                    updateDto.status == PROJECT_STATUS.FUNDED ||
                    updateDto.status == PROJECT_STATUS.COMPLETED
                  )
                ) {
                  if( updateDto.status == PROJECT_STATUS.PENDING){
                    throw new BadRequestException(PROJECT_FUNDED_STATUS_CHANGE_PENDING_ERROR) 
                  } else if(updateDto.status == PROJECT_STATUS.REJECTED){
                    throw new BadRequestException(PROJECT_FUNDED_STATUS_CHANGE_REJECTED_ERROR) 
                  } else if(updateDto.status == PROJECT_STATUS.DOCUMENTSREQUIRED){
                    throw new BadRequestException(PROJECT_FUNDED_STATUS_CHANGE_DOCUMENTS_REQUIRED_ERROR)
                  }
                 /*
                 */
                 else{
                  throw new BadRequestException(
                    PROJECT_FUNDED_STATUS_CHANGE_ERROR
                  );
                 }
                  /**/
                }
                //  }
              }
            //}
          }
          /**? */
           else {
            throw new BadRequestException(PROJECT_EDIT_ERROR);
          }
         /* */

          break;
        }
        case PROJECT_STATUS.REJECTED: {
          if (!tCheckAllFields) {
           
            //if (user.type == USER_TYPES.ADMIN) {
              let s = updateDto.status;
              if (updateDto.status == undefined) {
                console.log(
                  "__________________________No status field___________________________"
                );
              } else {
                // if(updateDto.status){
                console.log("updateDTooooo.status", updateDto.status);
                //if (updateDto.status == PROJECT_STATUS.REJECTED) {
                  let _user = await this.usersService.findOne(project.createdBy);
                  
                  if (
                    _user.verification.isProfileCompleted !==
                    KYC_VERIFICATION_STATUS.APPROVED
                  ) {
                    throw new BadRequestException(
                      PROJECT_PENDING_STATUS_CHANGE_ERROR_SME_KYC_NOT_APPROVED
                    );
                  }
                //}
                if (
                  !(
                    //updateDto.status == PROJECT_STATUS.PENDING || updateDto.status == PROJECT_STATUS.APPROVED || updateDto.status == PROJECT_STATUS.DOCUMENTSREQUIRED ||
                    (updateDto.status == PROJECT_STATUS.REJECTED)
                  )
                ) {
                  throw new BadRequestException(
                    PROJECT_REJECTED_STATUS_CHANGE_ERROR
                  );
                }
                // }
              }
            //}
          }
          else {
            throw new BadRequestException(PROJECT_EDIT_ERROR);
          }
          break;
        }
        case PROJECT_STATUS.COMPLETED: {
          if (!tCheckAllFields) {
            //if (user.type == USER_TYPES.ADMIN) {
              let s = updateDto.status;
              if (updateDto.status == undefined) {
                console.log(
                  "__________________________No status field___________________________"
                );
              } else {
                //  if(updateDto.status){
                  console.log("updateDTooooo.status", updateDto.status);
                if (!(updateDto.status == PROJECT_STATUS.COMPLETED)) {
                  throw new BadRequestException(
                    PROJECT_COMPLETED_STATUS_CHANGE_ERROR
                  );
                }
                //  }
              }
            //}
          } else {
            throw new BadRequestException(PROJECT_EDIT_ERROR);
          }
          break;
        }
        case PROJECT_STATUS.DOCUMENTSREQUIRED: {
          //if (user.type == USER_TYPES.ADMIN) {
            let s = updateDto.status;
            if (updateDto.status == undefined) {
              console.log(
                "__________________________No status field___________________________"
              );
            } else {
              console.log("updateDTooooo.status", updateDto.status);
              //  if(updateDto.status){
                //if (updateDto.status == PROJECT_STATUS.DOCUMENTSREQUIRED) {
                  let _user = await this.usersService.findOne(project.createdBy);
                  if (updateDto.status == PROJECT_STATUS.APPROVED) {
                  //Add approved fields for tracking Admins who have approved projects.
                    updateDto.projectApprovedTime = Date.now(),
                    updateDto.projectApprovedBy = user.code,
                    updateDto.projectApprovedUserEmail = user.email
                  }
                  
                  if (
                    _user.verification.isProfileCompleted !==
                    KYC_VERIFICATION_STATUS.APPROVED
                  ) {
                    throw new BadRequestException(
                      PROJECT_PENDING_STATUS_CHANGE_ERROR_SME_KYC_NOT_APPROVED
                    );
                  }
                //}
              if (
                !(
                  updateDto.status == PROJECT_STATUS.PENDING ||
                  updateDto.status == PROJECT_STATUS.APPROVED ||
                  updateDto.status == PROJECT_STATUS.DOCUMENTSREQUIRED ||
                  updateDto.status == PROJECT_STATUS.REJECTED
                )
              ) {
                throw new BadRequestException(
                  PROJECT_PENDING_TO_FUNDED_ERROR
                  //PROJECT_DOCUMENTS_REQUIRED_STATUS_CHANGE_ERROR
                );
              }
              //  }
            }
          //}
          break;
        }
      }

      let updateTime: number = Date.now();
      let data = await super.update(idOrCode, updateTime, updateDto, user);
      return success(data);

    } else if (!hasAccess && user.code === project.createdBy){
      
      switch (project.status) {
        case PROJECT_STATUS.PENDING:
          delete updateDto["status"];
          if (
            updateDto.fundsRequired !== undefined &&
            updateDto.value !== undefined
          ) {
            let conditional = updateDto.value > updateDto.fundsRequired
            console.log(
              "Conditionallllllll111111111",
              conditional,
              updateDto.value, updateDto.fundsRequired
            );
            if (!conditional) {
              throw new BadRequestException(FUNDS_REQUESTED_ERROR);
            }
          } else if (
            updateDto.value == undefined &&
            updateDto.fundsRequired !== undefined
          ) {
            let conditional = project.value > updateDto.fundsRequired
            console.log("Conditionallllllll2222222222222", conditional);
            if (!conditional) {
              throw new BadRequestException(FUNDS_REQUESTED_ERROR);
            }
          } else if (
            updateDto.fundsRequired == undefined &&
            updateDto.value !== undefined
          ) {
            let conditional = updateDto.value > project.fundsRequired
            console.log("Conditionallllllll33333333333", conditional);
            if (!conditional) {
              throw new BadRequestException(FUNDS_REQUESTED_ERROR);
            }
          }
          //if (!hasAccess) {
            delete updateDto["status"];
            delete updateDto["tokensRequired"];

            if (updateDto.owner) {
              delete updateDto.owner["ownerVerified"];
            }
          //}
          break;
        case PROJECT_STATUS.APPROVED:
          if (tCheckAllFields) {
            throw new BadRequestException(PROJECT_EDIT_ERROR);
          }
          delete updateDto["status"];
          break;  
        case PROJECT_STATUS.FUNDED:
          if (tCheckAllFields) {
            throw new BadRequestException(PROJECT_EDIT_ERROR);
          }
          delete updateDto["status"];
          break;  
        case PROJECT_STATUS.REJECTED:
          //if (!user.isAdmin) {
            //cannot change fields of rejected project.
            delete updateDto["status"];
            delete updateDto["tokensRequired"];

            if (updateDto.owner) {
              delete updateDto.owner["ownerVerified"];
            }
          //}
            if (tCheckAllFields) {
              throw new BadRequestException(PROJECT_EDIT_ERROR);
            }
          break;
        case PROJECT_STATUS.COMPLETED:
          delete updateDto["status"];
          if (tCheckAllFields) {
            throw new BadRequestException(PROJECT_EDIT_ERROR);
          }
          break;
        case PROJECT_STATUS.DOCUMENTSREQUIRED: 
          //if (!user.isAdmin) {
            delete updateDto["status"];
            delete updateDto["tokensRequired"];

            if (updateDto.owner) {
              delete updateDto.owner["ownerVerified"];
            }
          //}
           // if (tCheckAllFields) {
           //   throw new BadRequestException(PROJECT_EDIT_ERROR);
           // }
        break;
      }
      let updateTime: number = Date.now();
      let data = await super.update(idOrCode, updateTime, updateDto, user);
      return success(data);
    }
    throw new BadRequestException(ONLY_FOR_CREATED_BY_SME_OR_ADMIN);
  }

  @Get("projectcount/smedashboard")
  async calculateprojects(@Request() req, @Query() query, @RequestUser() user) {
    debugger;
    let createdBy = { createdBy: user.code };
    let data = await this.smeProjectModel.find(createdBy);
    /*
    let data = await this.smeProjectService.findAll(createdBy, {
      limit: 30,
      page: 1,
      sort: "_id",
    });
    */
    //Implement with smeProjectModel
    //console.log('projects DATA', data)
    let pending = 0;
    let approved = 0;
    let funded = 0;
    let rejected = 0;
    let completed = 0;
    let documentsRequired = 0;
    let deleted = 0;
    await Promise.all(
      data.map(async (element) => {
        switch (element.status) {
          //for (let i = 0; i < data.length; i++) {}
          //switch (data.docs[i].status) {}
          case -1: {
            deleted++;
            break;
          }
          case 0: {
            pending++;
            break;
          }
          case 1: {
            approved++;
            break;
          }
          case 2: {
            funded++;
            break;
          }
          case 3: {
            rejected++;
            break;
          }
          case 4: {
            completed++;
            break;
          }
          case 5: {
            documentsRequired++;
            break;
          }
          default: {
            console.log("This Project status may be other than -1,0,1,2,3,4,5");
          }
        }
      })
    );
    let dahsboard: any = {
      created: data.length,
      pending: pending,
      approved: approved,
      funded: funded,
      rejected: rejected,
      completed: completed, //the flow is not in place for completed
      documentsRequired: documentsRequired,
    };
    // console.log(data);
    return success(dahsboard);
  }


  
  /*
  @Delete(":idOrCode")
  async delete(
  @IdOrCodeParser("idOrCode") idOrCode: string,
  @RequestUser() user
  ) {
    if (user.type == USER_TYPES.ADMIN) {
      let project = await this.smeProjectService.findOne(idOrCode);
      let _u = await this.usersService.findOne(project.createdBy);
      let query = { projectId: project.code };
      if(project.status == PROJECT_STATUS.FUNDED){
        throw new BadRequestException(CANNOT_DELETE_FUNDED_PROJECTS)
      } 
      else if(project.status == PROJECT_STATUS.APPROVED){
        let bid = await this.bidDetailsModel
                        //  .find()
                        .find({ projectId: project.code })
                        .sort({ updatedTime: -1 })
        console.log('bidssssssssss', bid)
        //let bid = await this.bidDetailsService.findAll(query, null);
       /*
       */
  /*
        if (bid.length > 0) { 
          bid.forEach(async (element) => {
            this.smeProjectService.doTest();
            if (element.status !== 1 && (element.status == 0 || element.status == 2)) { 
              await this.bidDetailsService.rejectBidsAndReturnWalletMoney(element, project)
              let _bid = await this.bidDetailsService.edit(
                element.code,
                { status: 2 },
                Date.now()
              );
            } 
            else if(element.status == 1){
              let _bid = await this.bidDetailsService.edit(
                element.code,
                { status: 2 },
                Date.now()
              );
              let _user = await this.usersService.findOne(element.createdBy);
              let createDto9: any = {
                _priority: 1,
                type: ACTIVITY_FEED_EVENTS_SPONSOR.PROJECTS_REJECTED,
                itemId: element.code,
                setFields: _bid,
                optional: project.formattedId
              };
              let _activityFeed9: any = await this.activityFeedService.projectsRejected(
                createDto9,
                _user
              );
            }
            let createDto12: any = {
              _priority: 1,
              type: ACTIVITY_FEED_EVENTS_SME.PROJECT_REJECTED,
              itemId: project.code,
              setFields: {
                message: "project-rejected",
                projectName: project.name,
              },
              optional:
                project.formattedId,
            };
            let _activityFeed9Sme: any = await this.smeactivityFeedService.projectRejected(
              createDto12,
              _u
            );
          
        }
        */
  /*
   */
  /*
        let createDto12: any = {
          _priority: 1,
          type: ACTIVITY_FEED_EVENTS_SME.PROJECT_REJECTED,
          itemId: project.code,
          setFields: {
            message: "project-rejected",
            projectName: project.name,
          },
          optional:
            project.formattedId,
        };
        let _activityFeed9Sme: any = await this.smeactivityFeedService.projectRejected(
          createDto12,
          _u
        );
      } 
      // <--- only admin can delete a file
    //  return await super.delete(idOrCode, user);
      //return await this.fileUploadService.deleteFile(idOrCode);
    }
    throw new NotFoundException();
  }
  */
}
