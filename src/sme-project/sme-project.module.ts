import { Module, forwardRef, Global } from "@nestjs/common";
import { SmeProjectController } from "./sme-project.controller";
import { SmeProjectService } from "./sme-project.service";
import { MongooseModule } from "@nestjs/mongoose";
import { SmeProjectSchema } from "./objects/sme-project.schema";
import { ProjectComplexityModule } from "../project-complexity/project-complexity.module";
import { PracticeAreaModule } from "../practice-area/practice-area.module";
import { MilestoneModule } from "../milestone/milestone.module";
import { RemarksModule } from "../remarks/remarks.module";
import { BidDetailsModule } from "../bid-details/bid-details.module";
import { ProjectStatusModule } from "../project-status/project-status.module";
import { BookmarkProjectOwnerModule } from "../bookmark-project-owner/bookmark-project-owner.module";
import { EmailModule } from "../email/email.module";
import { UsersModule } from "../users/users.module";
import { EmailService } from "../email/email.service";
import { UserSchema } from "../users/objects/user.schema";
import { EmailSchema } from "../email/objects/email.schema";
import { UsersService } from "../users/users.service";
import { ProjectEmailNotificationSchema } from "./project-emailnotification/project-emailnotification.schema";
import { ProjectEmailNotificationService } from "./project-emailnotification/project-emailnotification.service";
import { ActivityFeedModule } from "../activity-feed/activity-feed.module";
import { SmeActivityFeedModule } from "../sme-activity-feed/sme-activity-feed.module";
import { BidDetailsSchema, TransactionDetailsSchema } from "../bid-details/objects/bid-details.schema";
import { BidDetailsService } from "../bid-details/bid-details.service";
import { WalletTransactionsService } from "../wallet-transactions/wallet-transactions.service";
import { EntityDetailsSchema } from "../entity-details/objects/entity-details.schema";
import { EntityDetailsModule } from "../entity-details/entity-details.module";
import { FavouriteProjectsSchema } from "../favourite-projects/objects/favourite-projects.schema";
import { BigchainDbModule } from "../bigchain-db/bigchain-db.module";
import { RolesModule } from "../roles/roles.module";
import { RolesService } from "../roles/roles.service";
import { RolesSchema } from "../roles/objects/roles.schema";

//@Global()
@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "SmeProject", schema: SmeProjectSchema },
      {
        name: "ProjectEmailNotification",
        schema: ProjectEmailNotificationSchema,
      },
      { name: "BidDetails", schema: BidDetailsSchema },
      { name: "EntityDetails", schema: EntityDetailsSchema },
      { name: "FavouriteProject", schema: FavouriteProjectsSchema },
      { name: "AdminRoles", schema: RolesSchema },
      { name: "Transactions", schema: TransactionDetailsSchema}
      // {name: "User",schema: UserSchema},
    ]),
    EntityDetailsModule,
    ProjectComplexityModule,
    PracticeAreaModule,
    MilestoneModule,
    ProjectStatusModule,
    RemarksModule,
    //  BidDetailsModule,
    BookmarkProjectOwnerModule,
    UsersModule,
    EmailModule,
    ActivityFeedModule,
    SmeActivityFeedModule,
    BigchainDbModule,
    RolesModule,
    //BidDetailsModule,
    //forwardRef(() => BidDetailsModule)
  ],
  exports: [SmeProjectService, ProjectEmailNotificationService],
  controllers: [SmeProjectController],
  providers: [
    SmeProjectService,
    ProjectEmailNotificationService,
    RolesService
    //BidDetailsService, WalletTransactionsService
  ],
})
export class SmeProjectModule {}
