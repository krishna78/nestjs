import { Test, TestingModule } from '@nestjs/testing';
import { SmeProjectService } from './sme-project.service';

describe('SmeProjectService', () => {
  let service: SmeProjectService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SmeProjectService],
    }).compile();

    service = module.get<SmeProjectService>(SmeProjectService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
