import { Injectable, BadRequestException } from "@nestjs/common";
import { Model } from "mongoose";
import { BaseService } from "../common/base/base.service";
import { ISmeProject } from "./objects/sme-project.schema";
import { InjectModel } from "@nestjs/mongoose";
import { CanLoadVirtual } from "../common/interfaces/can.load.virtual";
import { CreateSmeProjectDto } from "./objects/sme-project.dto";
import {
  HasPostUpdate,
  HasPostCreate,
  HasPreUpdate,
} from "../common/base/pre.post.action";
import { EmailService } from "../email/email.service";
import {
  ProjectPendingEmail,
  ProjectApprovedEmail,
  ProjectRejectedEmail,
  ProjectCompletedEmail,
  ProjectDocumentsRequiredEmail,
} from "../users/objects/user.registered.email";
import { EmailDto } from "../email/objects/email.dto";
import { UsersService } from "../users/users.service";
import { EmailNotificationService } from "../users/email-notification/email-notification.service";
import { ProjectEmailNotificationService } from "./project-emailnotification/project-emailnotification.service";
import {
  USER_STATUS,
  USER_TYPES,
  ACTIVITY_FEED_EVENTS_SPONSOR,
  ACTIVITY_FEED_EVENTS_SME,
  BIGCHAINDB_TRANSACTION_TYPE,
  SME,
  PROJECT_STATUS,
} from "../common/constants/enum";
import { ActivityFeedService } from "../activity-feed/activity-feed.service";
import { SmeActivityFeedService } from "../sme-activity-feed/sme-activity-feed.service";
import {
  TOKENS_REQUIRED_FIELD_NULL,
  TOKENS_REQUIRED_VALUE_ERROR,
} from "../common/constants/string";
import { BidDetailsService } from "../bid-details/bid-details.service";
import { BigchainDbService } from "../bigchain-db/bigchain-db.service";
import { BIGCHAINDB_API_PATH, CRM_URL_HEADERS_UPDATE } from "../common/constants/config";
import { ProjectStatus } from "../project-status/objects/project-status.schema";
import { parse } from "path";
import { EntityDetailsService } from "../entity-details/entity-details.service";
const driver = require("bigchaindb-driver");
import * as request from "request-promise-native";
import { IUser, User } from "../users/objects/user.schema";
import { ITransactionDetails } from "src/bid-details/objects/bid-details.schema";

@Injectable()
export class SmeProjectService extends BaseService<ISmeProject>
  implements CanLoadVirtual, HasPreUpdate, HasPostUpdate, HasPostCreate {
  constructor(
    @InjectModel("SmeProject")
    private readonly smeProjectModel: Model<ISmeProject>,
    private readonly emailService: EmailService,
    private userService: UsersService,
    private readonly projectemailnotificationService: ProjectEmailNotificationService,
    private activityFeedService: ActivityFeedService,
    private smeactivityFeedService: SmeActivityFeedService,
    private readonly bigchaindbService: BigchainDbService,
    private entityDetailsService: EntityDetailsService,
    @InjectModel("User") private readonly userModel: Model<User>,
    @InjectModel("Transactions")
    private readonly transactionsModel: Model<ITransactionDetails>,
  ) //private bidDetailsService: BidDetailsService
  {
    super(smeProjectModel);
  }
  formattedIdLength: number = 4;

  getQuery(createSmeProjectDto: CreateSmeProjectDto): Object {
    return null;
  }

  getPrefixForId(createSmeProjectDto: CreateSmeProjectDto): string {
    return "PR";
  }

  getVirtualForItem(): string[] {
    return [
      "_milestoneDetails",
      "projectContractDetails",
      "_entityDetailsSponsor",
      "_custRemarksDetails",
      "_bigChainDetails",
    ];
  }

  getVirtualForList(): string[] {
    return [
      "_milestoneDetails",
      "_entityDetailsSponsor",
      "_custRemarksDetails",
      "userDetails",
      "_bigChainDetails"
    ];
  }

  async doPreUpdate(idOrCode, editDto, model, user): Promise<void> {
    if (user.type == USER_TYPES.ADMIN) {
      //console.log('smeProjecttttt', model, /* editDto, model, user, */ editDto.tokensRequired)
    /*
      if (editDto.tokensRequired) {
        if (editDto.tokensRequired !== model.equivalentBidPoints) {
          throw new BadRequestException(TOKENS_REQUIRED_VALUE_ERROR);
        }
      }
      if (editDto.status == 1 && model.tokensRequired == undefined) {
        throw new BadRequestException(TOKENS_REQUIRED_FIELD_NULL);
      }
    */
    }
  }

  async doPostCreate(model, user) {
    debugger;
    console.log(model);
    let createDto: any = {
      projectcode: model.code,
    };
    let data = await this.projectemailnotificationService.create(
      createDto,
      user
    );
    console.log(data);
  }
  async doPostUpdate(idorcode: any, model: any, user?: any) {
    /* */
    debugger;
    console.log("rrrrrrrrrrrrrrrrrrrr", user);
    console.log(model);

    let registerduser = await this.userService.findOneByQuery({
      code: model.createdBy,
    });
    //  console.log(model.project, model.name);console.log(model.code);console.log(user.email, registerduser.email);

    let query = { projectcode: model.code };
    let notification = await this.projectemailnotificationService.findOneByQuery(
      query
    );
    console.log(model.project);

    let data = {
      firstName: registerduser.firstName,
      lastName: registerduser.lastName,
      projectName: model.name,
    };
    debugger;
    console.log(data);

    debugger;
    console.log("Do Post Update has Invoked");
    switch (model.status) {
      case PROJECT_STATUS.PENDING: {
        console.log("Sending Pending Email to all  Admin");
        if (notification.isProjectPending === false) {
       //   let condition = { type: "Admin" };
       let condition = { $or: [{ type: USER_TYPES.ADMIN }, { type: USER_TYPES.OPERATIONS_TEAM }] };
       debugger;
          ////Implement with usersModel
          let admins = await this.userModel.find(condition);
          
          /*
          let admins = await this.userService.findAll(condition, {
            limit: 30,
            page: 1,
            sort: "_id",
          });
          */
          console.log(admins.length);
          for (let i = 0; i < admins.length; i++) {
            let email = admins[i].email;
            await this.emailService.sendEmail(
              new ProjectPendingEmail(
                new EmailDto({
                  to: email,
                  metaData: { data },
                })
              )
            );
          }
          let notify = await this.projectemailnotificationService.edit(
            notification.code,
            { isProjectPending: true }
          );
          debugger;

          let createDto3: any = {
            _priority: 2,
            type: ACTIVITY_FEED_EVENTS_SME.PROJECT_PENDING_WITH_SUPER_ADMIN,
            itemId: model.code,
            setFields: {
              message: "Project is pending",
              projectName: model.name,
            },
            optional: model.formattedId,
          };
          debugger;
          let _activityFeed: any = await this.smeactivityFeedService.projectsPendingWithSuperAdmin(
            createDto3,
            registerduser
          );

          let crmId = "";
          var crmUser: any =
            '{"firstname":' +
            '"' +
            registerduser.firstName +
            '"' +
            ',"lastname":' +
            '"' +
            registerduser.lastName +
            '"' +
            ',"cf_contacts_role":' +
            '"' +
            registerduser.type +
            '"' +
            ',"assigned_user_id":"19x1"' +
            ',"contacttype":"Customer"' +
            ',"cf_contacts_projectcreationstatus":' +
            '"' +
            "project-created" +
            '"' +
            ',"id":' +
            '"' +
            registerduser.crmId +
            '"' +
            ',"cf_contacts_projectcreateddatetime":' +
            '"' +
            new Date(model.createdTime) +
            '"' +
            "}";
      
            var options3 = {
              ...CRM_URL_HEADERS_UPDATE,
              form: {
                element: crmUser,
              },
            };
           console.log('crm project crm project crm pproject', options3)
            /*3rd POST request to the Crm */
      
            const result3 = await request.post(options3);
            console.log(JSON.parse(result3));
        }
        break;
      }
      case PROJECT_STATUS.APPROVED: {
        console.log("Sending Project  Approved Email to User");
       
  
        if (notification.isProjectApproved === false) {
          if(registerduser.emailAlerts == true){
            await this.emailService.sendEmail(
              new ProjectApprovedEmail(
                new EmailDto({
                  to: registerduser.email,
                  metaData: { data },
                })
              )
            );
          }

          let notify = await this.projectemailnotificationService.edit(
            notification.code,
            {
              isProjectApproved: true,
              // isProjectPending:false,
              isProjectRejected: false,
            }
          );
          // smeactivity when project is approved
          let createDto3: any = {
            _priority: 2,
            type: ACTIVITY_FEED_EVENTS_SME.PROJECT_APPROVED_TO_MARKETPLACE,
            itemId: model.code,
            setFields: {
              message: "Project is approved",
              projectName: model.name,
            },
            optional: model.formattedId,
          };
          let _activityFeed: any = await this.smeactivityFeedService.projectApprovedToMarketplace(
            createDto3,
            registerduser
          );
          
          await this.createBigChainTransaction(registerduser, model);
          /*
          let crmId = "";
          var crmUser: any =
            '{"firstname":' +
            '"' +
            registerduser.firstName +
            '"' +
            ',"lastname":' +
            '"' +
            registerduser.lastName +
            '"' +
            ',"cf_contacts_role":' +
            '"' +
            registerduser.type +
            '"' +
            ',"assigned_user_id":"19x1"' +
            ',"contacttype":"Customer"' +
            ',"cf_contacts_projectcreationstatus":' +
            '"' +
            model.status +
            '"' +
            ',"id":' +
            '"' +
            registerduser.crmId +
            '"' +
            ',"cf_contacts_projectcreateddatetime":' +
            '"' +
            model.createdTime +
            '"' +
            "}";
      
            var options3 = {
              ...CRM_URL_HEADERS_UPDATE,
              form: {
                element: crmUser,
              },
            };
           console.log('crm project crm project crm pproject', options3)
           */
           /*3rd POST request to the Crm */
           /*
            const result3 = await request.post(options3);
            console.log(JSON.parse(result3));
          */
        }

        if (user.type == USER_TYPES.ADMIN || user.type == USER_TYPES.OPERATIONS_TEAM) {
          if (notification.smeProjectStatus.isStatusOne === false) {
            let _obj = {
              smeProjectStatus: {
                isStatusZero: false,
                isStatusOne: true,
                isStatusTwo: false,
                isStatusThree: false,
              },
            };
            let updateTime: number = Date.now();
            let notify_ = await this.projectemailnotificationService.edit(
              notification.code,
              _obj,
              updateTime
            );
          }

          //  let createDto6: any = {
          //   _priority: 2,
          //   type: ACTIVITY_FEED_EVENTS_SPONSOR.PROJECTS_SHOWN_INTEREST,
          //   setFields: bidModel
          // }
          //PROJECTS_SHOWN_INTEREST
          // let _activityFeed4: any = await this.activityFeedService.create(createDto6, _user);
        }
        // await this.createBigChainTransaction(registerduser, model);
        break;
      }
      case PROJECT_STATUS.REJECTED: {
        console.log("Sending Project  Rejected Email to Admin");

        if (notification.isProjectRejected === false) {
         // let condition = { type: "Admin" };
         let condition = { $or: [{ type: USER_TYPES.ADMIN }, { type: USER_TYPES.OPERATIONS_TEAM }] };
         debugger;
          //Implement with usersModel
          /*
          let admins = await this.userService.findAll(condition, {
            limit: 30,
            page: 1,
            sort: "_id",
          });
          */
          let admins = await this.userModel.find(condition);

          for (let i = 0; i < admins.length; i++) {
          //for (let i = 0; i < admins.total; i++) {
            let email = admins[i].email;
            await this.emailService.sendEmail(
              new ProjectRejectedEmail(
                new EmailDto({
                  to: email,
                  metaData: { data },
                })
              )
            );
          }

          let notify = await this.projectemailnotificationService.edit(
            notification.code,
            {
              isProjectApproved: false,
              // isProjectPending:false,
              isProjectRejected: true,
            }
          );
        }
        // smeactivity when project is pending
        let createDto3: any = {
          _priority: 2,
          type: ACTIVITY_FEED_EVENTS_SME.PROJECT_REJECTED,
          itemId: model.code,
          setFields: {
            message: "Project is rejected",
            projectName: model.name,
          },
          optional: model.formattedId,
        };
        let _activityFeed: any = await this.smeactivityFeedService.projectRejected(
          createDto3,
          registerduser
        );
        break;
      }
      case PROJECT_STATUS.DOCUMENTSREQUIRED: {
        // smeactivity when project is documents is required
        let createDto3: any = {
          _priority: 2,
          type: ACTIVITY_FEED_EVENTS_SME.PROJECT_DOCUMENT_REQUIRED,
          itemId: model.code,
          setFields: {
            message: "Project documents required",
            projectName: model.name,
          },
          optional: model.formattedId,
        };

        if (notification.isProjectDocumentsRequired === false) {
       
        
          let _activityFeed: any = await this.smeactivityFeedService.projectDocumentsRequired(
            createDto3,
            registerduser
          );

          await this.emailService.sendEmail(
            new ProjectDocumentsRequiredEmail(
              new EmailDto({
                to: registerduser.email,
                metaData: { data },
              })
            )
          );
          let notify = await this.projectemailnotificationService.edit(
            notification.code,
            {
              //isProjectApproved: false,
              // isProjectPending:false,
              //isProjectRejected: false,
              //isProjectCompleted: true,
              isProjectDocumentsRequired: true
            }); 
        }
        break;
      }
      case PROJECT_STATUS.COMPLETED:
        if (notification.isProjectCompleted === false) {
        
          let createDto3: any = {
            _priority: 2,
            type: ACTIVITY_FEED_EVENTS_SME.PROJECT_DOCUMENT_REQUIRED,
            itemId: model.code,
            setFields: {
              message: "Project completed",
              projectName: model.name,
            },
            optional: model.formattedId,
          };
         
          let _activityFeed: any = await this.smeactivityFeedService.projectCompleted(
            createDto3,
            registerduser
          );
  
          let k = await this.transactionsModel.find({ projectId: model.code })    
          console.log('Transaction details...................', k)
          let currentLoginTime: number = Date.now();
          var _currentLoginTime = new Date(currentLoginTime)
          //var _date = new Date(lastLoginTime);
         let projectFinancingDate = new Date(k[0].projectFinancingDate);
         //let projectFinancingDate: number = k.projectFinancingDate
         const unixTime = k.projectFinancingDate;
         const _date = new Date(unixTime*1000);
         var financingTenure = k[0].financingTenure
         // const unixtime = projectFinancingDate.valueOf();
          console.log('projectFinancingDate', projectFinancingDate, k[0].projectFinancingDate,'_currentLoginTime',  _currentLoginTime, 'Date', _date)
/*
*/
          await this.emailService.sendEmail(
                new ProjectCompletedEmail(
                  new EmailDto({
                    to: registerduser.email,
                    metaData: { registerduser, model, financingTenure, projectFinancingDate, _currentLoginTime },
                  })
                )
              );
/*
              */
              let notify = await this.projectemailnotificationService.edit(
                notification.code,
                {
                  //isProjectApproved: false,
                  // isProjectPending:false,
                  //isProjectRejected: false,
                  isProjectCompleted: true
                }
              );   
        }
      
     
       
        break;
      default: {
        console.log("Default Case");
        break;
      }
    }
    /* */
  }
  async createBigChainTransaction(userDetails, projectDetails) {
    debugger;
    let bigchaindbkeys = await this.bigchaindbService.findOneByQuery({
      createdBy: userDetails.code, isDummyTransaction: false
    });
    if (userDetails.entityDetailCode !== undefined) {
      let _model = await this.entityDetailsService.findOne(
        userDetails.entityDetailCode
      );
      let _uenNumber = "";
      let _companyName = "";
      let _contactNumber = "";
      if (userDetails.type === USER_TYPES.SME) {
      // bigchaindbType = BIGCHAINDB_TRANSACTION_TYPE.SME_VERIFIED;
        let aSme = {
          uennumber: "",
          phone: "",
        };
        console.log(aSme);
  
        switch (_model.company.type) {
          case SME.TECH_SME_IN_SINGAPORE:
            aSme.uennumber = _model.company.details.uenNumber;
            aSme.phone = _model.company.details.companyTelephoneNumber;
            break;
          case SME.TECH_SME_OUTSIDE_SINGAPORE:
            aSme.uennumber = _model.company.details.companyRegistrationNumber;
            aSme.phone = _model.company.details.telephoneNumber;
            break;
        }
        console.log(aSme);
        _uenNumber = aSme.uennumber;
        _companyName = _model.company.details.companyName;
        _contactNumber = aSme.phone;
    }

    //  console.log("companydetails", projectDetails._entityDetailsSponsor);
    console.log("userDetails", userDetails, bigchaindbkeys);
    // let username = model.firstName;
    const username = new driver.Ed25519Keypair();
    // console.log(username, model);
    let bigchaindbType = null;
    const API_PATH = BIGCHAINDB_API_PATH;
    bigchaindbType = BIGCHAINDB_TRANSACTION_TYPE.PROJECT_VERIFIED;
    let ProjectAssetData = {
      createdBy: "Cycloan",
      cifId: userDetails.formattedId,
      projectId: projectDetails.formattedId,
      creationdatetime: Date.now(),
      projectOwner: projectDetails.owner.name,
      fundsRequested: projectDetails.fundsRequired,
      projectValue: projectDetails.value,
      assetreferenceKey: bigchaindbkeys.transactionId,
      smeCompanyName: _companyName, //krishna send the sme company name here
    };
    console.log("ProjectAssetData", ProjectAssetData);
    /*
     */
    let projectStatus = "";
    switch (projectDetails.status) {
      case 0:
        projectStatus = "Pending";
        break;
      case 1:
        projectStatus = "Approved";
        break;
      case 2:
        projectStatus = "Funded";
        break;
      case 3:
        projectStatus = "Rejected";
        break;
      case 4:
        projectStatus: "Completed";
        break;
      case -1:
        projectStatus = "Deleted";
        break;
      default:
        console.log("Nostatus");
        break;
    }
    debugger;
    const milestoneDetails = projectDetails._milestoneDetails.map((o) => {
      return {
        name: o.name,
        complexity: o.complexity,
        number: o.number,
        completion: o.completion,
      };
    });
      console.log("ProjectAssetData", ProjectAssetData);
      /*
      */
      
      const documentsUpload =
        projectDetails._entityDetailsSponsor.documentsUpload;
      console.log(documentsUpload);
      console.log(milestoneDetails);
      let ProjectMetaData = {
        projectTenure: projectDetails.tenure,
        projectName: projectDetails.name,
        projectType: projectDetails.type,
        practiceArea: projectDetails.practiceArea,
        projectStatus: projectStatus,
        projectDescription: projectDetails.description,
        milestoneDetails: milestoneDetails,
        // documentsUpload:documentsUpload,
        //  type: _model.company.type,
      };
      console.log("ProjectMetaData", ProjectMetaData);
      // Construct a transaction payload
      const tx = await driver.Transaction.makeCreateTransaction(
        { ProjectAssetData },

        { ProjectMetaData },

        // A transaction needs an output
        [
          driver.Transaction.makeOutput(
            driver.Transaction.makeEd25519Condition(username.publicKey)
          ),
        ],
        username.publicKey
      );
      console.log("ttttxxxxx", tx);

      // Sign the transaction with private keys
      const txSigned = driver.Transaction.signTransaction(
        tx,
        username.privateKey
      );
      console.log("txSigneddddd", txSigned);
      // Send the transaction off to BigchainDB

      const conn = new driver.Connection(API_PATH);

      conn.postTransactionCommit(txSigned).then((retrievedTx) => {
        let keys = {
          type: bigchaindbType,
          publickey: username.publicKey,
          privatekey: username.privateKey,
          transactionId: retrievedTx.id,
          projectRef: projectDetails.code,
          isDummyTransaction: false
          //"id",
        };
        return new Promise(async (resolve, reject) => {
          // <--- this line
          let bcdb = await this.bigchaindbService.SaveKeys(keys, userDetails);
          console.log("Transaction", retrievedTx.id, "successfully posted.");
        });
      });
      //  return new Promise(async (resolve, reject) => { // <--- this line
      //  let bcdb = await this.bigchaindbService.SaveKeys(keys, model);
      ////   console.log("Transaction", retrievedTx.id, "successfully posted.");

      //console.log(result);
      //
      // console.log(result.id)
      //   console.log(username.publicKey)

      //---bbbbbbbbbb
    }
    
}
  async doTest() {}
}
