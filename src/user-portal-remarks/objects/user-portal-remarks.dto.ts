import { IsInt, Min, Max, IsDefined, IsNotEmpty, Allow, IsArray, ValidateNested, IsIn } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";
import { Creator, Reader } from "../../common/base/base.dto";
import { Expose, Type } from "class-transformer";
import { REMARKS, REMARKS_SUBTYPE } from "../../common/constants/enum";

class CreateSingleUserPortalRemarkDto extends Creator {
    constructor() {
      super(true);
    }
  

    @IsDefined()
    @IsNotEmpty()
    @IsIn(Object.values({ ...REMARKS}))
    @ApiModelProperty()
    readonly type: string;
  
    @IsDefined()
    @IsNotEmpty()
    @IsIn(Object.values({ ...REMARKS_SUBTYPE}))
    @ApiModelProperty()
    readonly subType: string;

    @IsDefined()
    @IsNotEmpty()
    @ApiModelProperty()
    readonly remark: string;

    //@IsDefined()
    //@IsNotEmpty()
    //@ApiModelProperty()
    //readonly createdBy: string;
  
}

export class CreateUserPortalRemarksDto {
    @Allow()
    @IsDefined()
    @IsNotEmpty()
    @IsArray()
    @Type(() => CreateSingleUserPortalRemarkDto)
    @ValidateNested()
    remarks: CreateSingleUserPortalRemarkDto[];
  
    @IsDefined()
    @IsNotEmpty()
    @ApiModelProperty()
    userId: string;
  }

  export class UserPortalRemarkDto extends Reader {
    @Expose()
    @ApiModelProperty()
    readonly type: string = "";

    @Expose()
    @ApiModelProperty()
    readonly subType: string = "";

    @Expose()
    @ApiModelProperty()
    readonly remark: string;

    @Expose()
    @ApiModelProperty()
    readonly userId: string = "";

    @Expose()
    @ApiModelProperty()
    readonly createdBy: string = "";
}