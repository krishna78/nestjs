import { Entity, IEntity, createModel } from "../../common/base/base.model";
import { Schema } from "mongoose";

export class UserPortalRemark extends Entity {
    userId: string;
    type: string;
    subType: string;
    remark: string;
    createdBy: string;
}

export interface IUserPortalRemark extends UserPortalRemark, IEntity {
    id: string;
  }

export const UserPortalRemarkSchema: Schema = createModel("UserPortalRemarks", {
 userId: { type: String },
 type: { type: String },
 subType: { type: String},
 remark: { type: String },
 createdBy:{ type: String }
})