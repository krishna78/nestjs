import { Test, TestingModule } from '@nestjs/testing';
import { UserPortalRemarksController } from './user-portal-remarks.controller';

describe('UserPortalRemarks Controller', () => {
  let controller: UserPortalRemarksController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserPortalRemarksController],
    }).compile();

    controller = module.get<UserPortalRemarksController>(UserPortalRemarksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
