import { Controller, NotFoundException, Body, Post, Query, Get, Request, UseGuards } from '@nestjs/common';
import { UserPortalRemarkDto, CreateUserPortalRemarksDto } from './objects/user-portal-remarks.dto';
import { abstractBaseControllerFactory } from '../common/base/base.controller';
import { BASEROUTES, USER_TYPES, REMARKS_SUBTYPE, ROLES_ACCESS_ACTION } from '../common/constants/enum';
import { UserPortalRemarksService } from './user-portal-remarks.service';
import { SmeProjectService } from '../sme-project/sme-project.service';
import { success } from '../common/base/httpResponse.interface';
import { plainToClass } from 'class-transformer';
import { RequestUser } from '../common/utils/controller.decorator';
import { AbstractClassTransformerPipe } from '../common/pipes/class-transformer.pipe';
import { UsersService } from '../users/users.service';
import { JwtAuthGuard } from '../auth/auth.guard';
import { RolesService } from '../roles/roles.service';


const BaseController = abstractBaseControllerFactory<any>({
    DTO: UserPortalRemarkDto,
    CreateDTO: CreateUserPortalRemarksDto,
    DisabledRoutes: [BASEROUTES.UPDATEONE,BASEROUTES.DETELEONE, BASEROUTES.PATCH],
  });

@UseGuards(JwtAuthGuard)
@Controller('user-portal-remarks')
export class UserPortalRemarksController  extends BaseController{
    constructor(private userPortalRemarksService: UserPortalRemarksService,
        private usersService: UsersService,
        private rolesservice: RolesService
        //private smeProjectService: SmeProjectService
        ) {
          super(userPortalRemarksService);
        }

        @Get()
        async findList(
         // @Param("projectId") projectId,
          @Request() req, 
          @Query() query, 
          @RequestUser() user) {
            console.log('user typessssss', user) 
            let userId = query.userId;
          let _user = await this.usersService.findOne(userId);
          let _user_ = await this.rolesservice.findOneByQuery({roleName: user.type});
        
          let hasAccess  = _user_.rolesAccessAction.some(
            (e) => e === ROLES_ACCESS_ACTION.USER_PORTAL_REMARKS_CONTROLLER_FINDLIST
          );
          let _userSme = await this.rolesservice.findOneByQuery({roleName: user.type});
         
          let hasAccessSme = _userSme.rolesAccessAction.some(
            (e) => e === ROLES_ACCESS_ACTION.USER_PORTAL_REMARKS_CONTROLLER_FINDLIST_SME
          );
          let _userSponsor = await this.rolesservice.findOneByQuery({roleName: user.type});
          
          let hasAccessSponsor = _userSponsor.rolesAccessAction.some(
            (e) => e === ROLES_ACCESS_ACTION.USER_PORTAL_REMARKS_CONTROLLER_FINDLIST_SPONSOR
          );
          console.log('userstype', user.type, '_user_, _userSme, _userSponsor, hasAccess, hasAccessSme, hasAccessSponsor', _user_, _userSme, _userSponsor, hasAccess, hasAccessSme, hasAccessSponsor)
  
         // let projectId = query.projectId
         // let smeProject = await this.smeProjectService.findOne(projectId)
         console.log('user typessssss', user) 
         if(_user){
            switch (_user.verification.isProfileCompleted){
              case 0:
                //if (user.type == USER_TYPES.SME) {
                  if(hasAccessSme){
                  let _query = { subType: REMARKS_SUBTYPE.STATUS_COLUMN, userId: _user.code }
                  console.log('_query,_user.verification.isProfileCompleted',_query,_user.verification.isProfileCompleted)
                  return await super.findList(req, { ..._query });
                } //else if(user.type == USER_TYPES.SPONSOR){
                  else if(hasAccessSponsor){
                throw new NotFoundException();
                }    else if(hasAccess){
                // else if(user.type == USER_TYPES.ADMIN){
                   // <--- only admin can see the user lists
                   let _query = { userId: _user.code }
                   return await super.findList(req, { ..._query });
                }
                break;
              case 1:
                //if (user.type == USER_TYPES.SME) {
                  if(hasAccessSme){
                  let _query = { subType: REMARKS_SUBTYPE.EDIT_COLUMN, userId: _user.code }
                  return await super.findList(req, { ..._query });
                } //else if(user.type == USER_TYPES.SPONSOR){
                  else if(hasAccessSponsor){
                let _query = { subType: REMARKS_SUBTYPE.EDIT_COLUMN, userId: _user.code }
                  return await super.findList(req, { ..._query });
                }  else if(hasAccess){
                //else if(user.type == USER_TYPES.ADMIN){
                  // <--- only admin can see the user lists
                  let _query = { userId: _user.code }
                  return await super.findList(req, { ..._query });
                }
                break;
              case 2:
                 // if (user.type == USER_TYPES.SME) {
                  if(hasAccessSme){
                    let _query = { subType: REMARKS_SUBTYPE.STATUS_COLUMN, userId: _user.code }
                    console.log('_query,_user.verification.isProfileCompleted', _query, _user.verification.isProfileCompleted)
                    return await super.findList(req, { ..._query });
                  } else if(hasAccessSponsor){
                  // else if(user.type == USER_TYPES.SPONSOR){
                         throw new NotFoundException();
                  } else if(hasAccess){
                  //else if(user.type == USER_TYPES.ADMIN){
                     // <--- only admin can see the user lists
                     let _query = { userId: _user.code }
                     return await super.findList(req, { ..._query });
                  }
                break;
            }
          }
          /*
         // switch (smeProject.status)
          if (user.type == USER_TYPES.ADMIN) {
            // <--- only admin can see the user lists
            return await super.findList(req, { ...query });
          }
         // throw new NotFoundException();
         */
        }
        
  
  
    @Post()
    public async create(
      @Request() req,
      @Body(AbstractClassTransformerPipe(CreateUserPortalRemarksDto))
      body: CreateUserPortalRemarksDto,
      @RequestUser() user
    ) {
  
      // TODO: verify if project exists with code
     // if (user.isAdmin) {
      let _user = await this.rolesservice.findOneByQuery({roleName: user.type});
      console.log('userstype', user.type, _user)
      let hasAccess = _user.rolesAccessAction.some(
        (e) => e ===  ROLES_ACCESS_ACTION.USER_PORTAL_REMARKS_CONTROLLER_CREATE
      );
      console.log('userstype', user.type, _user, hasAccess)
     if(hasAccess) {
          // <--- only admin can see the remarks lists
          let remarks = body.remarks.map((remark) => {
              return {
                //project: body.project,
                userId: body.userId,
                createdBy: user.email,
                ...remark,
              };
            });
            let createdItems = await this.userPortalRemarksService.create(remarks);
            createdItems.map((milestone) => {
              return plainToClass(UserPortalRemarkDto, milestone, {
                excludeExtraneousValues: true,
              });
            });
            return success(createdItems);
          
        }
        throw new NotFoundException();
   
    }  
}
