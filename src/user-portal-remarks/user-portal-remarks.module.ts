import { Module, forwardRef } from '@nestjs/common';
import { UserPortalRemarksController } from './user-portal-remarks.controller';
import { UserPortalRemarksService } from './user-portal-remarks.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserPortalRemarkSchema } from './objects/user-portal-remarks.schema';
import { UserSchema } from '../users/objects/user.schema';
import { OtpSchema } from '../auth/otp/otp.schema';
import { CrmSchema } from '../users/crm/crm.schema';
import { UsersModule } from '../users/users.module';
import { RolesSchema } from '../roles/objects/roles.schema';
import { RolesModule } from '../roles/roles.module';
import { RolesService } from '../roles/roles.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "UserPortalRemark", schema: UserPortalRemarkSchema },
                               //{ name: "SmeProject", schema: SmeProjectSchema }
                               { name: "User", schema: UserSchema },
                               { name: "Otp", schema: OtpSchema },
                               { name: "Crm", schema: CrmSchema },
                               { name: "AdminRoles", schema: RolesSchema }
                              ]),
                              UsersModule,
                              RolesModule,
                              // forwardRef(() => SmeProjectModule)
  ],
  controllers: [UserPortalRemarksController],
  providers: [UserPortalRemarksService, RolesService]
})
export class UserPortalRemarksModule {}
