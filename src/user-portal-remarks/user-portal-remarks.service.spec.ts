import { Test, TestingModule } from '@nestjs/testing';
import { UserPortalRemarksService } from './user-portal-remarks.service';

describe('UserPortalRemarksService', () => {
  let service: UserPortalRemarksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserPortalRemarksService],
    }).compile();

    service = module.get<UserPortalRemarksService>(UserPortalRemarksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
