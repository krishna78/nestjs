import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { IUserPortalRemark, UserPortalRemark } from './objects/user-portal-remarks.schema';
import { Model } from "mongoose";
import { BaseService } from '../common/base/base.service';

@Injectable()
export class UserPortalRemarksService extends BaseService<IUserPortalRemark>{
    constructor(
        @InjectModel("UserPortalRemark") private readonly userPortalRemarkModel: Model<IUserPortalRemark>
      ) {
        super(userPortalRemarkModel);
      }

      async create(createDto: UserPortalRemark[]) {
        return await this.baseModel.insertMany(createDto);
      }
}
