import { Creator, Reader } from "../../common/base/base.dto";
import { IsDefined } from "class-validator";
import { Expose } from "class-transformer";

export class CreateCrmDto extends Creator {
    constructor({
        crmId,
        createdBy,
       // email,
       // expiry,
       // isVerified
    }) {
      super(true);
      this.crmId = crmId;
      this.creator = createdBy
     // this.email = email;
     // this.expiry = expiry;
     // this.isVerified = isVerified;
    }

    @IsDefined()
    readonly crmId: string;

    @IsDefined()
    readonly creator: string;

    //@IsDefined()
    //readonly email: string;

    //@IsDefined()
    //readonly expiry: number;
    
    //@IsDefined()
    //readonly isVerified: boolean;
}

export class CrmDto extends Reader {
  @Expose()
  readonly crmId: string;
  
  //@Expose()
  //readonly email: string;
  
  //@Expose()
  //readonly expiry: number;
  
  //@Expose()
  //readonly isVerified: boolean;
}
