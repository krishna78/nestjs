import { createModel, Entity, IEntity } from "../../common/base/base.model";
import { Schema } from "mongoose";
import { OTP_EMAIL_VERIFICATION_STATUS } from "../../common/constants/enum";

export class Crm extends Entity {
    crmId: string;
    crmCustomer: {
        preUpdate: boolean;
        postUpdate: boolean;
        isComplete: boolean;
      };
      creator: string;
    //token: string; 
    //email: string;
    //expiry: number;
    //isVerified: boolean;
}

export interface ICrm extends Crm, IEntity {
    id: string;
  }

  export const CrmSchema: Schema = createModel("CrmTable", {
    crmId: { type: String },
    crmCustomer: {
        preUpdate:  {  type: Boolean, default: false },
        postUpdate:  {  type: Boolean, default: false },
        isComplete: { type: Boolean, default: false }
      },
      creator: { type: String }  
    //email: { type: String },
    //expiry: { type: Number},
    //isVerified: { type: Boolean, default: OTP_EMAIL_VERIFICATION_STATUS.FALSE}
  });