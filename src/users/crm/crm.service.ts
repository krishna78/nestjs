import { ICrm, Crm } from "./crm.schema";
import { BaseService } from "../../common/base/base.service";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CreateCrmDto } from "./crm.dto";


@Injectable()
export class CrmService extends BaseService<ICrm> {
  constructor(
    @InjectModel("Crm") private readonly crmModel: Model<Crm>,
   
  ) {
    super(crmModel);
  }

  async create(createDto: any) {
      let data = new CreateCrmDto(createDto);
      return super.create(data);
  }
}