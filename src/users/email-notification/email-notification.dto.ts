import { Creator } from "../../common/base/base.dto";
import { IsDefined, IsNotEmpty } from "class-validator";

export class CreateEmailNotification extends Creator {
  constructor({ creator }) {
    super(true);
    /*this.creator = creator;
    this.isOnboardingInitiated=isOnboardingInitiated,
    this.isKycApproved=isKycApproved,
    this.isKycRejected=isKycRejected,
    this.isKycPending=isKycPending,
    this.isDocumentsSubmitted=isDocumentsSubmitted*/
    this.creator = creator;

    debugger;
  }

  @IsDefined()
  @IsNotEmpty()
  readonly isOnboardingInitiated: boolean;

  @IsDefined()
  @IsNotEmpty()
  readonly isKycApproved: boolean;

  @IsDefined()
  @IsNotEmpty()
  readonly isKycRejected: boolean;

  @IsDefined()
  @IsNotEmpty()
  readonly isKycPending: boolean;
  @IsDefined()
  @IsNotEmpty()
  readonly isDocumentsSubmitted: boolean;

  @IsDefined()
  @IsNotEmpty()
  readonly creator: string;

  @IsDefined()
  @IsNotEmpty()
  project: {
    readonly isProjectCreated: boolean;
    readonly isProjectPending: boolean;
    readonly isProjectApproved: boolean;
    readonly isProjectRejected: boolean;
  };
}
