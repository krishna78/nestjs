import { Schema } from "mongoose";
import { createModel, Entity, IEntity } from "../../common/base/base.model";

export class EmailNotification extends Entity {
  isOnboardingInitiated: boolean;
  isKycApproved: boolean;
  isKycRejected: boolean;
  isKycPending: boolean;
  isDocumentsSubmitted: boolean;
  project: {
    isProjectCreated: boolean;
    isProjectPending: boolean;
    isProjectApproved: boolean;
    isProjectRejected: boolean;
  };
  //IsProjectCreated: boolean;
  creator: string;
}

export interface IEmailNotification extends EmailNotification, IEntity {
  id: string;
}

export const EmailNotificationSchema: Schema = createModel(
  "EmailNotification",
  {
    isOnboardingInitiated: { type: Boolean, default: false },
    isKycApproved: { type: Boolean, default: false },
    isKycRejected: { type: Boolean, default: false },
    isKycPending: { type: Boolean, default: false },
    isDocumentsSubmitted: { type: Boolean, default: false },
    project: {
      isProjectCreated: { type: Boolean, default: false },
      isProjectPending: { type: Boolean, default: false },
      isProjectApproved: { type: Boolean, default: false },
      isProjectRejected: { type: Boolean, default: false },
    },
    creator: { type: String },
  }
);
