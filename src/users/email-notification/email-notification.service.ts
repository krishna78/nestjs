import { Injectable } from "@nestjs/common";
import { BaseService } from "../../common/base/base.service";
import {
  IEmailNotification,
  EmailNotification,
} from "../email-notification/email-notification.schema";
import { CreateEmailNotification } from "../email-notification/email-notification.dto";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { from } from "rxjs";

@Injectable()
export class EmailNotificationService extends BaseService<IEmailNotification> {
  constructor(
    @InjectModel("EmailNotification")
    private readonly emailNoticationModel: Model<EmailNotification>
  ) {
    super(emailNoticationModel);
  }

  async create(createDto: any) {
    let data = new CreateEmailNotification(createDto);

    console.log(data);

    return super.create(data);
  }
}
