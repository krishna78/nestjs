import { Expose, Type } from "class-transformer";
import {
  IsNotEmpty,
  IsDefined,
  IsEmail,
  MinLength,
  MaxLength,
  IsIn,
  Allow,
  IsOptional,
  ValidateNested,
} from "class-validator";
import { Match } from "../../common/utils/match.decorator";
import { ApiModelProperty } from "@nestjs/swagger";
import { Creator, Updater, Reader } from "../../common/base/base.dto";
import {
  USER_TYPES,
  KYC_VERIFICATION_STATUS,
  TIMELINE_PREFERENCE_0,
  PRACTICE_AREA,
  PROJECT_TYPE,
} from "../../common/constants/enum";
import { FileUploadDto } from "../../file-upload/objects/file-upload.dto";
import {
  Owner,
  SmeProjectDto,
} from "../../sme-project/objects/sme-project.dto";
import { User } from "./user.schema";

export class VerificationDto {
  @Expose()
  readonly mobile: boolean = true;

  @Expose()
  readonly email: boolean = true;

  @IsIn(Object.values(KYC_VERIFICATION_STATUS))
  @Expose()
  readonly isProfileCompleted: number = 1;

  @Expose()
  readonly isOnboardingComplete: boolean = true;

  @Expose()
  readonly forcePasswordChange: boolean = true;
}

export class PreferenceDto {
  @IsIn(Object.values(PROJECT_TYPE))
  @Expose()
  readonly projectType: String;

  @IsIn(Object.values(PRACTICE_AREA))
  @Expose()
  readonly practiceArea: string;

  @Expose()
  readonly investmentRange: number;

  @Expose()
  readonly borrowingRange: number;

  @IsIn(Object.values(TIMELINE_PREFERENCE_0))
  @Expose()
  readonly timeline: string;
}

export class FundDetailsDto {
  @Expose()
  totalFundedAmount: number;

  @Expose()
  totalProjectFunded: number;

  @Expose()
  netAmount: number;

  @Expose()
  avgInterestRate: number;

  @Expose()
  netReturns: number;
}

export class CloseAccount {
  @Expose()
  reason: string;

  @Expose()
  description: string;
}

export class CrmCustomer {
  @Expose()
  _preUpdate: boolean;

  @Expose()
  _postUpdate: boolean;
}

export class CreateUserDto extends Creator {
  constructor() {
    super(true);
  }

  @IsIn(Object.values(USER_TYPES))
  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly type: string;

  @IsDefined()
  @IsNotEmpty()
  @MaxLength(50, { message: "FirstName is too long" })
  @ApiModelProperty()
  readonly firstName: string;

  @IsDefined()
  @IsNotEmpty()
  @MaxLength(100, { message: "LastName is too long" })
  @ApiModelProperty()
  readonly lastName: string;

  //@IsDefined()
  //@IsNotEmpty()
  // @MaxLength(100, { message: "LastName is too long" })
  //@ApiModelProperty()
  readonly userName: string;

  @IsEmail()
  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly email: string;

  @IsDefined()
  @IsNotEmpty()
  @MinLength(8, { message: "Password is too short" })
  @MaxLength(128, { message: "Password is too long" })
  @ApiModelProperty()
  readonly password: string;

  @Match("password", { message: "Password and confirmPassword must match" })
  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly confirmPassword: string;

  @IsDefined()
  @IsNotEmpty()
  @MinLength(9, { message: "Number is too short" })
  @MaxLength(15, { message: "Number is too long" })
  @ApiModelProperty()
  readonly mobileNumber: string;
  
  @IsOptional()
  //@IsDefined()
  //@IsNotEmpty()
  @ApiModelProperty()
  readonly concode: string;


  @IsDefined()
  @IsNotEmpty()
  readonly agreedTermsAndConditions: boolean;
}

export class UserDto extends Reader {
  @Expose()
  readonly type: string = "";

  @Expose()
  readonly firstName: string = "";

  @Expose()
  readonly lastName: string = "";

  @Expose()
  readonly userName: string = "";

  @Expose()
  readonly email: string = "";

  @Expose()
  readonly mobileNumber: string = "";
  
  @Expose()
  readonly concode: string = "";

  @Expose()
  readonly agreedTermsAndConditions: boolean;

  @Expose()
  readonly emailAlerts: boolean;

  @Type(() => VerificationDto)
  @Expose()
  readonly verification: VerificationDto = new VerificationDto();

  @Expose()
  readonly lastLogin: number;

  @Expose()
  readonly entityDetailCode: string;

  @Expose()
  readonly totalBalance: number;

  @Expose()
  readonly formattedId: string = "";

  @Expose()
  readonly crmId: string = "";
  /*
  @Type(() => CrmCustomer)
  @Expose()
  readonly crmCustomer: CrmCustomer;
  */
  @Type(() => PreferenceDto)
  @Expose()
  readonly preferences: PreferenceDto;

  @Type(() => FundDetailsDto)
  @Expose()
  readonly fundDetails: FundDetailsDto;

  @Expose()
  @Type(() => FileUploadDto)
  readonly profileDetails: FileUploadDto;

  @Expose()
  readonly profilePic: string;

  @Expose()
  readonly aboutCompany: string;

  @Expose()
  readonly favourites: string[];
  /* */
  @Expose() //Owner
  @Type(() => SmeProjectDto)
  readonly favouriteProjects: SmeProjectDto[];
  /*  */

  @Expose()
  @Type(() => CloseAccount)
  readonly closeAccount: CloseAccount;

  @Expose()
  readonly createdBy: string = "";

  @Expose()
  readonly userApprovedTime: Date;

  @Expose()
  readonly userApprovedBy: string = "";

  @Expose()
  readonly userApprovedAdminEmail: string = "";
}

export class UpdateUserDto extends Updater {
  @IsOptional()
  readonly firstName: string;

  @IsOptional()
  readonly lastName: string;

  @IsOptional()
  readonly mobileNumber: string;

  //@IsOptional()
  //readonly password: string;

  //@IsOptional()
  //readonly agreedTermsAndConditions: boolean;

  @IsOptional()
  readonly emailAlerts: boolean;

  @IsOptional()
  readonly verification: VerificationDto;

  @IsOptional()
  readonly aboutCompany: string;

  @IsOptional()
  readonly preferences: PreferenceDto;

  @IsOptional()
  readonly fundDetails: FundDetailsDto;

  @IsOptional()
  readonly profilePic: string;

  //@IsOptional()
  //@Type(() => ProjectContractDto)
  //readonly projectContract: ProjectContractDto [];
  //readonly favourites: string[];

  @IsOptional()
  readonly closeAccount: CloseAccount;
}
