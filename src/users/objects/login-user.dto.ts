import { Expose, Type } from "class-transformer";
import { IsNotEmpty, IsDefined } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";
import { VerificationDto } from "./create-user.dto"
export class LoginUserDto {
  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly email: string;

  @IsDefined()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly password: string;
}

export class LoggedInToken {
  @Expose()
  readonly expiresIn: number;

  @Expose()
  readonly token: string;

  @Expose()
  readonly type: string;

  @Type(() => VerificationDto)
  @Expose()
  readonly verification: VerificationDto;
}

