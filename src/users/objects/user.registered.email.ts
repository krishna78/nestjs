import {
  AbstractEmail,
  EmailTemplateAndSubject,
} from "../../email/objects/abstract.email";
import { EMAILSTATUS } from "../../common/constants/enum";
import { IUser } from "./user.schema";
import { Otp } from "src/auth/otp/otp.schema";
import { measureMemory } from "vm";

export class UserRegisteredEmailForSme extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Cycloan Account Registration";
  }
  protected emailTemplateCode = "welcome-email";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `<img src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" alt="cycloanlogo" height="auto" width="auto"> 

    <p>Dear <b>{{firstName}} {{lastName}}</b> ,</p>
    
    <p>Welcome to Cycloan, world's first P2P lending platform for tech by tech.</p>
    
    <p>Thank you for signing up as a SME with Cycloan </p>
    
    <p>You can create your account by filling the onboarding details and submitting relevant documents</p>
    
    
    <p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>
    
    <p>Regards,</p>
    <p>Cycloan Support Team</p>
    
    
    <img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      email: meta.user.email,
      firstName: meta.user.firstName,
      lastName: meta.user.lastName,
      // lastname:meta.user.lastname
    };
  }
}
export class UserRegisteredEmailForSponsor extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Cycloan Account Registration";
  }
  protected emailTemplateCode = "welcome-email";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `<img src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" alt="cycloanlogo" height="auto" width="auto"> 

    <p>Dear <b>{{firstName}} {{lastName}}</b> ,</p>
    
    <p>Welcome to Cycloan, world's first P2P lending platform for tech by tech.</p>
    
    <p>Thank you for signing up as a Sponsor with Cycloan </p>
    
    <p>You can create your account by filling the onboarding details and submitting relevant documents</p>
    
    
    <p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>
    
    <p>Regards,</p>
    <p>Cycloan Support Team</p>
    
    
    <img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      email: meta.user.email,
      firstName: meta.user.firstName,
      lastName: meta.user.lastName,
      // lastname:meta.user.lastname
    };
  }
}

export class OtpEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Your OTP for cycloan email ID verification-" + meta.token;
  }
  protected emailTemplateCode = "otp-email";

  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `<img src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" alt="cycloanlogo" height="auto" width="auto">

<p>Dear <b>{{firstName}} {{lastName}} <b>,</p>
 
<p>Verify your email address</p>

<p>You’re just a step away from accessing your Cycloan account.
We’re sharing a One Time Password with you to authenticate your login credentials. Please note that this code is valid for 15 minutes and can be used only once. Please don’t share your OTP with anyone.</p>


<p>Your OTP : </p>
<p style="color:blue;">{{otp}}</p>


<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>

<p>Regards,</p>
<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      email: meta.email,
      otp: meta.token,
      firstName: meta.firstName,
      lastName: meta.lastName,

      // name: meta.userToAttempt.firstName
    };
  }
}
export class ForgetPasswordOtpEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Your OTP for Password Reset " + meta.token;
  }
  protected emailTemplateCode = "forgetpassword otp-email";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

    <p>Dear {{firstName}} {{lastName}} ,</p>
    
    <p>Password Reset</p>
    
    
    <p>We’re sharing a One Time Password with you to reset your password. Please note that this code is valid for 15 minutes and can be used only once. </p>
    
    <p>If you dont want to change your password or din't request this, you can report to us at support@cycloan.io</p>
    
    
    <p>Your OTP : </p>
    <p style="color:blue;">{{otp}}</p>
    
    
    <p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>
    
    <p>Regards,</p>
    <p>Cycloan Support Team</p>
    
    <img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      email: meta.email,
      otp: meta.token,
      firstName: meta.firstName,
      lastName: meta.lastName,
      // name: meta.userToAttempt.firstName
    };
  }
}
export class PasswordChangedAlert extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Cycloan Security Alert - Your Password was changed  ";
  }
  protected emailTemplateCode = "passwordchange alert";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

    <p>Dear {{firstName}} {{lastName}} ,</p>
    
    <p>The password for your Cycloan account {{email}} was changed. If you dint change it, you can report to support@cycloan.io</p>
    
    <p>This is a system generated email, please do not reply</p>
    
    <p>Regards,</p>
    <p>Cycloan Support Team</p>
    
    <img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      email: meta.email,
      firstName: meta.firstName,
      lastName: meta.lastName,
      // name: meta.userToAttempt.firstName
    };
  }
}
export class OnBoardingEmailAlert extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Cycloan Onboarding - Next steps";
  }
  protected emailTemplateCode = "email-alert onboarding process";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

    <p>Dear {{firstName}} {{lastName}} ,</p>
    
    <p>Thank you for your application. We are from Cycloan Support Team for helping you in a smoother onboarding process.</p>
    
    <p>Next Steps:</p>
    <p>1. Customized based on applicants form</p>
    
    <p>2. Customized based on applicants form</p>

    <p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>
    
    <p>Regards,</p>
    <p>Cycloan Support Team</p>
    
    <img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      // email: meta.email,
      firstName: meta.user.firstName,
      lastName: meta.user.lastName,
      // name: meta.userToAttempt.firstName
    };
  }
}
export class OnBoardingCompletedForSme extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Congratulations ! Customer account created successfully ";
  }
  protected emailTemplateCode = "Welcome msg for onboarding completed";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

    <p>Dear <b>{{firstName}} {{lastName}}</b> ,</p>
    
    <p>Congratulations! You have successfully created an active SME account.</p>
    
    <p>Welcome to cycloan :) We are excited to have you on board and hope you will have an enriching experience with our application</p>
    
    <p>You're now part of a platform that connects tech sponsors and tech smes across the globe. First P2P lending platform for tech by tech</p>
    
    <p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>
    
    
    <p>Regards,</p>
    <p>Cycloan Support Team</p>
    
    <img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      // email: meta.email,
      firstName: meta.firstName,
      lastName: meta.lastName,
      // name: meta.userToAttempt.firstName
    };
  }
}
export class OnBoardingCompletedForSponsor extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Congratulations ! Customer account created successfully ";
  }
  protected emailTemplateCode = "Welcome msg for onboarding completed";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

    <p>Dear <b>{{firstName}} {{lastName}}</b> ,</p>
    
    <p>Congratulations! You have successfully created an active Sponsor account.</p>
    
    <p>Welcome to cycloan :) We are excited to have you on board and hope you will have an enriching experience with our application</p>
    
    <p>You're now part of a platform that connects tech sponsors and tech smes across the globe. First P2P lending platform for tech by tech</p>
    
    <p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>
    
    
    <p>Regards,</p>
    <p>Cycloan Support Team</p>
    
    <img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      // email: meta.email,
      firstName: meta.firstName,
      lastName: meta.lastName,
      // name: meta.userToAttempt.firstName
    };
  }
}
export class LoginCredentialEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Login Credentials ";
  }
  protected emailTemplateCode = "Admin CreatedUser logincredentials";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

<p>Dear <b> {{firstName}} {{lastName}} </b> ,</p>


<p>Thank you for your interest in Cycloan </p>

<p>You are invited to access our platform. Please click here http://161.117.176.141/auth/login to login to the portal, with the below credentials </p>

<p>Email ID: {{emailId}} </p>
<p>Password : 12345678</p>

<p>Please Note : We request you to change your password after logging in the first time</p>

<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>

<p>Regards,</p>
<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      emailId: meta.email,
      firstName: meta.firstName,
      lastName: meta.lastName,
      // name: meta.userToAttempt.firstName
    };
  }
}
export class KycRejectedforSme extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Revalidate your KYC details";
  }
  protected emailTemplateCode = "Kyc rejected-alert-sme";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

<p>Dear <b>{{firstName}} {{lastName}}</b> ,</p>

<p>We regret to inform you that the KYC details submitted by you in the account creation process is not valid</p>

<p>We will not be able to Verify your project until KYC verification is successfull.</p> 

<p>We request you to kindly update the KYC details at the earliest</p>


<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>


<p>Regards,</p>
<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      // email: meta.email,
      firstName: meta.model.firstName,
      lastName: meta.model.lastName,
      // name: meta.userToAttempt.firstName
    };
  }
}
export class KycRejectedforSponsor extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Revalidate your KYC details  ";
  }
  protected emailTemplateCode = "Kyc rejected-alert-sponosor";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

<p>Dear <b>{{firstName}} {{lastName}}</b> ,</p>

<p>We regret to inform you that the KYC details submitted by you in the account creation process is not valid</p>

<p>We will not be able to fund a project until KYC verification is successfull.</p> 

<p>We request you to kindly update the KYC details at the earliest</p>


<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>


<p>Regards,</p>
<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      // email: meta.email,
      firstName: meta.model.firstName,
      lastName: meta.model.lastName,
      // name: meta.userToAttempt.firstName
    };
  }
}
export class KycApprovedforSponsor extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Congratulations ! KYC Verification Successful ";
  }
  protected emailTemplateCode = "Kyc approved-alert-sponosor";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

<p>Dear <b>{{firstName}} {{lastName}}</b> ,</p>


<p>Congratulations! Your KYC details are verified successfully, now you have access to all the core functionalities you require to finance on tech projects </p>

<p>Here are a few resources to help you get started:</p>

<p>1. Video Tutorial</p>
<p>2. {{ notificationUrl }}</p>

<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>


<p>Regards,</p>
<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      // email: meta.email,
      firstName: meta.model.firstName,
      lastName: meta.model.lastName,
      notificationUrl: meta.url
      // name: meta.userToAttempt.firstName
    };
  }
}
export class KycApprovedforSme extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Congratulations ! KYC Verification Successful";
  }
  protected emailTemplateCode = "Kyc approved-alert-sme";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

<p>Dear <b>{{firstName}} {{lastName}}</b> ,</p>


<p>Congratulations! Your KYC details are verified successfully, now you have access to all the core functionalities you require to get funds for your projects </p>

<p>Here are a few resources to help you get started:</p>

<p>1. Video Tutorial</p>
<p>2.{{ notificationUrl }}</p>

<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>


<p>Regards,</p>
<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    debugger;
    return {
      // email: meta.email,
      firstName: meta.model.firstName,
      lastName: meta.model.lastName,
      notificationUrl: meta.url
      // name: meta.userToAttempt.firstName
    };
  }
}
export class KycPendingEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "KYC Verification - Documents Required";
  }
  protected emailTemplateCode = "kyc-pendingemail to-admin";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

<p> Dear <b>{{firstName}} {{lastName}}</b>, </p> 


<p>We request you to provide the below mentioned additional documents for successfull KYC verification</p> 

<p>1. < Customized document name > </p> 
<p>2. < Customized document name > </p> 
<p>3. < Customized document name > </p> 

<p>We kindly request you to share the required documents to support@cycloan.io at the earliest , so that we can proceed further with your KYC</p> 

<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>  


<p>  Regards,</p> 

<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    debugger;
    return {
      // email: meta.email,
      firstName: meta.user.firstName,
      lastName: meta.user.lastName,
      // name: meta.userToAttempt.firstName
    };
  }
}

export class ProjectCreationEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Acknowledgement - " + meta.emaildata.projectName + " Submitted";
  }
  protected emailTemplateCode = "sme-created project";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

<p> Dear {{firstName}} {{lastName}}, </p> 

<p> Thank you for creating a new project {{projectName}} on our platform </p>

<p> We would like to inform you that we received your project details and our Cycloan operations team is currently evaluating the project details. We will be updating the status of the project within 2 days </p>

<p> This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io </p>


<p>  Regards,</p>
<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    debugger;
    return {
      // email: meta.email,
      firstName: meta.emaildata.firstName,
      lastName: meta.emaildata.lastName,
      projectName: meta.emaildata.projectName,
    };
  }
}

export class TokensSubmittedSponsorEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Tokens Spent";
  }
  protected emailTemplateCode = "TokensSubmittedSponsor";

  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `<img src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" alt="cycloanlogo" height="auto" width="auto">
  

    <p>Dear <b>{{firstName}} {{lastName}}</b> ,</p>
    
    <p> Please find below the details of tokens spent for the project {{projectName}}</p>
    <p>ProjectId: {{projectId}}</p>
    <p>Project Name: {{projectName}}</p>
    <p>Equivalent bid points : {{equivalentBidPoints}}</p>
    <p>Tokens Required: {{tokensRequired}}</p>
    <p>Tokens Submitted: {{tokensSubmitted}}</p>
    <p>Remaining Tokens: {{remainingTokens}}</p>
    
    This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io
    
    
    Regards,
    Cycloan Support Team
    
    Cycloan logo
    

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      //email: meta.email,
      projectId: meta.project.formattedId,
      projectName: meta.project.name,
      equivalentBidPoints: meta.project.equivalentBidPoints,
      tokensRequired: meta.project.tokensRequired,
      tokensSubmitted: meta.bidModel.tokensForSubmission,
      remainingTokens: meta._user.totalBalance,
      firstName: meta._user.firstName,
      lastName: meta._user.lastName,

      // name: meta.userToAttempt.firstName
    };
  }
}
export class ProjectPendingEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Documents Required - " + meta.data.projectName;
  }
  protected emailTemplateCode = "project-pending-email to Admin";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
    <img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">
<p> Dear {{firstName}} {{lastName}}, </p> 

<p>Thank you for creating a new project {{projectName}} on our platform</p> 


<p>We request you to provide the below mentioned additional documents for successfull project approval {{projectName}}</p> 


<p>1. < Customized document name > </p> 

<p>2. < Customized document name > </p> 

<p>3. < Customized document name > </p> 


<p>We kindly request you to share the required documents to info@cycloan.io at the earliest , so that we can proceed further with your project approval</p> 


<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p> 


<p>  Regards,</p> 

<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    debugger;
    return {
      // email: meta.email,
      firstName: meta.data.firstName,
      lastName: meta.data.lastName,
      projectName: meta.data.projectName,
      // name: meta.userToAttempt.firstName
    };
  }
}

export class ProjectDocumentsRequiredEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Documents Required - " + meta.data.projectName;
  }
  protected emailTemplateCode = "Project Status Project Documents Required - SME";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
    <img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">
<p> Dear {{firstName}} {{lastName}}, </p> 

<p>Thank you for creating a new project {{projectName}} on our platform</p> 


<p>We request you to provide the below mentioned additional documents for successfull project approval {{projectName}}</p> 


<p>1. < Customized document name > </p> 

<p>2. < Customized document name > </p> 

<p>3. < Customized document name > </p> 


<p>We kindly request you to share the required documents to info@cycloan.io at the earliest , so that we can proceed further with your project approval</p> 


<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p> 


<p>  Regards,</p> 

<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    debugger;
    return {
      // email: meta.email,
      firstName: meta.data.firstName,
      lastName: meta.data.lastName,
      projectName: meta.data.projectName,
      // name: meta.userToAttempt.firstName
    };
  }
}

export class WalletBalanceSponsorEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return (
      "Cycloan Wallet Balance" + " " + "Transaction Date" + " " + meta._date
    );
  }
  protected emailTemplateCode = "TokensSubmittedSponsor";

  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `<img src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" alt="cycloanlogo" height="auto" width="auto">
  

    <p>Dear <b>{{firstName}} {{lastName}}</b> ,</p>
    Cycloan Logo 

    <p> Lets have a look at your wallet balance after the recent transaction </p>

    <p> Opening Balance : {{openingBalance}} tokens </p>
    <p>Recent Transaction : {{recentTransaction}} tokens </p>
    <p> Tokens added / Tokens spent : {{recentTransaction}} tokens </p>
    
    <p>Transaction type: {{transactionType}} </p>
    <p> Your wallet balance : {{walletBalance}} tokens </p>

    <p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>


    Regards,
    Cycloan Support Team

    Cycloan logo



<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      //email: meta.email,
      recentTransaction: meta.createDto.bidAmount,
      transactionType: meta.createDto.transactionType,
      openingBalance: meta._user.totalBalance,
      walletBalance: meta._userUpdate.totalBalance,
      //projectId: meta.project.formattedId,
      //projectName: meta.project.name,
      //equivalentBidPoints: meta.project.equivalentBidPoints,
      //tokensRequired: meta.project.tokensRequired,
      //tokensSubmitted: meta.bidModel.tokensForSubmission,
      //remainingTokens: meta._user.totalBalance,
      firstName: meta._user.firstName,
      lastName: meta._user.lastName,

      // name: meta.userToAttempt.firstName
    };
  }
}
export class ProjectApprovedEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Congratulations ! " + meta.data.projectName + " - Approved";
  }
  protected emailTemplateCode = "project-approvedemail by Admin";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

<p> Dear <b>{{firstName}} {{lastName}}</b>, </p> 

<p>Congratulations! Your project details for {{projectName}} is verified and your project is approved to be listed on the sponsor project marketplace </p> 

<p>We will keep you posted on any change in the status of the project in the marketplace</p> 


<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>  


<p>  Regards,</p> 

<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    debugger;
    return {
      // email: meta.email,
      firstName: meta.data.firstName,
      lastName: meta.data.lastName,
      projectName: meta.data.projectName,
      // name: meta.userToAttempt.firstName
    };
  }
}
export class ProjectRejectedEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Project details " + meta.data.projectName + " - Not Valid";
  }

  protected emailTemplateCode = "project-rejectedemail to Admin";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

<p> Dear <b>{{firstName}} {{lastName}}</b>, </p> 

<p>We regret to inform you that the project details submitted by you for {{projectName}} is not valid due to the following reasons</p> 

<p>1. < Customized reason 1 ></p> 
<p>2. < Customized reason 2 ></p> 

<p>We request you to kindly try again by creating a new project with the relevant details and submit for approval at the earliest. Please contact superadmin1@cycloan.io for any project creation related queries</p>  

<p>We will not be able to list the project on the Sponsors marketplace until the project is approved</p> 


<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>  


<p>  Regards,</p> 

<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    debugger;
    return {
      // email: meta.email,
      firstName: meta.data.firstName,
      lastName: meta.data.lastName,
      projectName: meta.data.projectName,
      // name: meta.userToAttempt.firstName
    };
  }
}


export class ProjectAllocationSponsorEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Congratulations - " + meta._project.name + " - successfully matched";
  }

  protected emailTemplateCode = "Project allocation - Sponsor";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

<p> Dear <b>{{firstName}} {{lastName}}</b>, </p> 

<p>Congratulations! Your interest for the project {{ project.name }} is approved by the Cycloan platform and you are successfully matched to {{ projectCreator.formattedId }} </p> 


<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>  


<p>  Regards,</p> 

<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    debugger;
    return {
      // email: meta.email,
      firstName: meta._user.firstName,
      lastName: meta._user.lastName,
      //projectName: meta.data.projectName,
      project: meta._project,
      bid: meta.element ,
      projectCreator: meta._u
      // name: meta.userToAttempt.firstName
    };
  }
}


export class ProjectAmountFundedForProjectEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Project" + " " + meta._project.name + " " + "funded" + " " +  meta.element.financingAmount;
  }

  protected emailTemplateCode = "Amount funded for project";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

<p> Dear <b>{{firstName}} {{lastName}}</b>, </p> 

<p> Congratulations, your project {{ project.name }} is successfully funded with {{ bid.financingAmount }} by the sponsor {{ sponsor.firstName}} {{ sponsor.lastName }} & {{ sponsor.formattedId }} </p> 


<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>  


<p>  Regards,</p> 

<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    debugger;
    return {
      // email: meta.email,
      firstName: meta._u.firstName,
      lastName: meta._u.lastName,
      //projectName: meta.data.projectName,
      project: meta._project,
      bid: meta.element,
      projectCreator: meta._u,
      sponsor: meta._user
      // name: meta.userToAttempt.firstName
    };
  }
}


export class UserCycloanAccountBlockedEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Cycloan Account - Blocked";
  }
  protected emailTemplateCode = "blocked Account";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `<img src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" alt="cycloanlogo" height="auto" width="auto"> 

    <p>Dear Sir or Madam,</p>
    
    <p>We regret to inform you that, your account {{ email }} has been blocked to due to successive failed OTP attempts.</p>
    
    <p> You can reactivate your account by contacting us at support@cycloan.io or call +65-********</p>
   
    <p>This is a system generated email, please do not reply.</p>
    
    <p>Regards,</p>
    <p>Cycloan Support Team</p>
    
    
    <img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    return {
      email: meta.email,
      //firstName: meta.user.firstName,
      //lastName: meta.user.lastName,
      // lastname:meta.user.lastname
    };
  }
}

export class ProjectCompletedEmail extends AbstractEmail {
  protected getEmailSubject(meta: any) {
    return "Project" + " " + meta.model.name + " " + "Completed";
  }

  protected emailTemplateCode = "Project Completion";
  protected defaultEmailStatusWhenSent = EMAILSTATUS.PENDING;

  protected defaultTemplate: EmailTemplateAndSubject = {
    view: `
<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">

<p> Dear <b>{{firstName}} {{lastName}}</b>, </p> 

<p> We are glad to inform you that, you have successfully completed the project {{ project.name }} in the specified duration of {{ financingTenure }} from {{ transactionDate }} to {{ currentLoginTime }}</p> 

<p> All the transactions related to {{ project.name }} have been closed successfully </p>

<p> We at Cycloan are very happy to have been working with your team. Looking forward for helping in many successful project executions. </p>

<p>This is a system generated email, please do not reply.If you are having any issues, please contact us at support@cycloan.io</p>  

<p>  Regards,</p> 

<p>Cycloan Support Team</p>

<img  src="https://res.cloudinary.com/dskzmamid/image/upload/v1595658517/cycloan-color.png" width="auto" height="auto">`,
  };

  protected getDataForTemplate(meta: any): any {
    debugger;
    return {
      // email: meta.email,
      firstName: meta.registerduser.firstName,
      lastName: meta.registerduser.lastName,
      //projectName: meta.data.projectName,
      project: meta.model,
      financingTenure: meta.financingTenure,
      transactionDate: meta.projectFinancingDate,
      currentLoginTime: meta._currentLoginTime
      //bid: meta.element,
      //projectCreator: meta._u,
      //sponsor: meta._user
      // name: meta.userToAttempt.firstName
    };
  }
}