import * as bcrypt from "bcrypt";
import { Schema } from "mongoose";
import { createModel, Entity, IEntity } from "../../common/base/base.model";
//import { USER_TYPES, PROJECT_STATUS, KYC_VERIFICATION_STATUS } from "../../common/constants/enum";
import { TOTAL_BALANCE } from "../../common/constants/config";
import {
  USER_TYPES,
  PROJECT_STATUS,
  KYC_VERIFICATION_STATUS,
  PROJECT_TYPE,
  PRACTICE_AREA,
  TIMELINE_PREFERENCE_0,
} from "../../common/constants/enum";
import { ProjectContract } from "../../sme-project/objects/sme-project.schema";
import { bool } from "aws-sdk/clients/signer";

export class User extends Entity {
  entityDetailCode: string;
  type: string;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  password: string;
  mobileNumber: string;
  concode: string;
  verification: {
    mobile: boolean;
    email: boolean;
    isProfileCompleted: number;
    isOnboardingComplete: boolean;
    forcePasswordChange: boolean;
  };
  /*
  crmCustomer: {
    preUpdate: boolean;
    postUpdate: boolean;
  };
  */
  agreedTermsAndConditions: boolean;
  emailAlerts: boolean;
  lastLogin: number;
  totalBalance: number;
  formattedId: string;
  crmId: string;
  preferences: {
    projectType: string;
    practiceArea: string;
    investmentRange: number;
    borrowingRange: number;
    timeline: string;
  };
  fundDetails: {
    totalFundedAmount: number;
    totalProjectFunded: number;
    netAmount: number;
    avgInterestRate: number;
    netReturns: number;
  };
  profilePic: string;
  aboutCompany: string;
  favourites: string[];
  //projectIds: ProjectContract
  closeAccount: {
    reason: string;
    description: string;
  };
  userApprovedTime: number;
  userApprovedBy: string;
  userApprovedAdminEmail: string;
}

export interface IUser extends User, IEntity {
  id: string;
}
export interface IPassword {
  //dollarSet: {
  $set: {
    password: string;
    updatedTime: number;
  };
}
export const UserSchema: Schema = createModel("Users", {
  entityDetailCode: { type: String },
  type: { type: String, enum: Object.values({ ...USER_TYPES }) },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  userName: { type: String },
  email: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  mobileNumber: { type: String, unique: true, required: true },
  concode: { type: String
    //, required: true 
  },
  verification: {
    mobile: {
      type: Boolean,
      default: true,
    },
    email: {
      type: Boolean,
      default: true,
    },
    isProfileCompleted: {
      type: Number,
      enum: Object.values({ ...KYC_VERIFICATION_STATUS }),
      default: KYC_VERIFICATION_STATUS.PENDING,
    },
    isOnboardingComplete: {
      type: Boolean,
      default: false,
    },
    forcePasswordChange: {
      type: Boolean,
      default: false,
    },
  },
  /*
  crmCustomer: {
    _preUpdate:  {  type: Boolean, default: false },
    _postUpdate:  {  type: Boolean, default: false }
  },
  */
  agreedTermsAndConditions: { type: Boolean, required: true },
  emailAlerts:  {
    type: Boolean,
    default: true,
  },
  lastLogin: { type: Number, default: Date.now },
  totalBalance: { type: Number, default: TOTAL_BALANCE },
  formattedId: { type: String },
  crmId: { type: String },
  preferences: {
    projectType: { type: String, enum: Object.values({ ...PROJECT_TYPE }) },
    practiceArea: { type: String, enum: Object.values({ ...PRACTICE_AREA }) },
    investmentRange: { type: Number },
    borrowingRange: { type: Number },
    timeline: {
      type: String,
      enum: Object.values({ ...TIMELINE_PREFERENCE_0 }),
    },
  },
  fundDetails: {
    totalFundedAmount: { type: Number },
    totalProjectFunded: { type: Number },
    netAmount: { type: Number },
    avgInterestRate: { type: Number },
    netReturns: { type: Number },
  },
  profilePic: { type: String },
  aboutCompany: { type: String },
  favourites: [
    {
      type: String,
      //type: Schema.Types.ObjectId
      //fileId: { type: String },
    },
  ],
  closeAccount: {
    reason: { type: String },
    description: { type: String },
  },
 userApprovedTime:  { type: Number },
 userApprovedBy:  { type: String },
 userApprovedAdminEmail:  { type: String },
});

UserSchema.pre<IUser>("save", function (next) {
  const user = this;
  if (!user.isModified("password")) return next();
  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});

UserSchema.pre<IUser>("updateOne", function (next) {
  const user = this;
  console.log(user._update);
  var p: IPassword = user._update;

  var a = p.$set.password;
  if (!a) {
    console.log("unsuccessfull !!!!!!!!!!!!!");
    return next();
  }
  bcrypt.genSalt(10, async (err, salt) => {
    if (err) return next(err);
    await bcrypt.hash(user._update.$set.password, salt, (err, hash) => {
      if (err) return next(err);
      user._update.$set.password = hash;
      user.password = hash;
      console.log("aaaaaaaaaa", user._update.$set.password, user.password);
      next();
    });
  });
});

UserSchema.methods.checkPassword = async function (attempt) {
  return await bcrypt.compare(attempt, this.password);
};

UserSchema.virtual("profileDetails", {
  ref: "FileUpload",
  localField: "profilePic",
  foreignField: "_id",
  justOne: true,
  options: {
    select: "mimetype + originalname",
  },
});
/* */
UserSchema.virtual("favouriteProjects", {
  ref: "SmeProject",
  localField: "favourites",
  foreignField: "code",
  justOne: false,
  //options: {
  // select: "owner"
  //}
});
/* */

export const isAdmin = (user: User): boolean => {
  return user.type === USER_TYPES.ADMIN;
};
