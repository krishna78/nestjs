import { Creator, Updater, Reader } from "../../common/base/base.dto";
import { Expose, Type } from "class-transformer";
/*
{
    "success": true,
    "result": {
        @Expose()
        "salutationtype": "";
        "firstname": "";
        "lastname": "SampleTest";
        "email": "";
        "phone": ";
        "mobile
        "homephone
        "birthday
        "otherphone": "",
        "fax": "",
        "account_id": "",
        "title": "",
        "department": "",
        "contact_id": "0",
        "leadsource": "",
        "secondaryemail": "",
        "assistant": "",
        "assigned_user_id": "19x1",
        "assistantphone": "",
        "donotcall": "0",
        "notify_owner": "0",
        "emailoptout": "0",
        "createdtime": "2020-06-28 17:20:55",
        "modifiedtime": "2020-06-28 17:20:55",
        "contact_no": "CON213",
        "modifiedby": "19x1",
        "isconvertedfromlead": "0",
        "created_user_id": "19x1",
        "primary_twitter": "",
        "source": "WEBSERVICE",
        "engagement_score": "0",
        "last_contacted_on": "",
        "last_contacted_via": "",
        "slaid": "0",
        "starred": "0",
        "tags": "",
        "contacttype": "Sales Qualified Lead",
        "contactstatus": "Cold",
        "happiness_rating": "",
        "record_currency_id": "",
        "record_conversion_rate": "",
        "profile_score": "0",
        "profile_rating": "0",
        "referred_by": "0",
        "emailoptin": "singleoptinuser",
        "emailoptin_requestcount": "0",
        "emailoptin_lastrequestedon": "",
        "smsoptin": "singleoptinuser",
        "language": "",
        "primary_phone_field": "",
        "primary_email_field": "",
        "cf_contacts_roletype": "",
        "cf_contacts_role": "SME",
        "cf_contacts_cifid": "SM000018",
        "cf_contacts_typeofsme": "Tech Company in Singapore",
        "cf_contacts_typeofsponsor": "Tech Company in Singapore",
        "cf_contacts_passportnumber": "",
        "cf_contacts_middlename": "Middle Name",
        "cf_contacts_designation": "",
        "cf_contacts_nationality": "",
        "cf_contacts_uennumber": "",
        "cf_contacts_registrationnumber": "",
        "cf_contacts_country": "",
        "cf_contacts_state": "",
        "cf_contacts_city": "",
        "cf_contacts_street": "",
        "cf_contacts_postalcode": "",
        "cf_contacts_companyname": "",
        "portal": "0",
        "support_start_date": "",
        "support_end_date": "",
        "mailingcountry": "",
        "othercountry": "",
        "mailingstreet": "",
        "otherstreet": "",
        "mailingpobox": "",
        "otherpobox": "",
        "mailingcity": "",
        "othercity": "",
        "mailingstate": "",
        "otherstate": "",
        "mailingzip": "",
        "otherzip": "",
        "mailing_gps_lat": "0.0000000",
        "mailing_gps_lng": "0.0000000",
        "cf_contacts_noofprojectscreated": "0",
        "cf_contacts_noofprojectsfunded": "0",
        "cf_contacts_noofprojectsbid": "0",
        "cf_contacts_noofprojectssponsored": "0",
        "description": "",
        "imagename": "",
        "consent_requested": "0",
        "consent_trash_data": "",
        "data_erased": "0",
        "consent_lock_data": "",
        "consent_track_email_engagement": "Not Applicable",
        "consent_track_shared_documents": "Not Applicable",
        "consents_last_requested_on": "",
        "primary_linkedin": "",
        "followers_linkedin": "0",
        "primary_facebook": "",
        "followers_facebook": "0",
        "id": "4x475",
        "isclosed": 0,
        "label": "SampleTest"
    }
}
*/

export class rDto {
    @Expose()
     readonly salutationtype: string;
     @Expose()
      readonly firstname: string;
      @Expose()
      readonly lastname: string;
      @Expose()
       readonly email: string;
       @Expose()
       readonly phone: string;
       @Expose()
       readonly mobile: string;
       @Expose()
       readonly homephone: string;
       @Expose()
       readonly birthday: string;
       @Expose()
       readonly otherphone: string;
       @Expose()
       readonly fax: string;
       @Expose()
       readonly account_id: string;
       @Expose()
       readonly title: string;
       @Expose()
       readonly department: string;
       @Expose()
       readonly contact_id: string;
       @Expose()
       readonly leadsource: string;
       
       @Expose()
       readonly secondaryemail: string;
       @Expose()
       readonly assistant: string;
       @Expose()
       readonly assigned_user_id: string;
       @Expose()
       readonly assistantphone: string;
       @Expose()
       readonly donotcall: string;
       @Expose()
       readonly notify_owner: string;
       @Expose()
       readonly emailoptout: string;
       @Expose()
       readonly createdtime: string;
       @Expose()
       readonly modifiedtime: string;
       @Expose()
       readonly contact_no: string;
       @Expose()
       readonly modifiedby: string;
       @Expose()
       readonly isconvertedfromlead: string;
       @Expose()
       readonly created_user_id: string;
       @Expose()
       readonly primary_twitter: string;
       @Expose()
       readonly source: string;
       @Expose()
       readonly engagement_score: string;
       @Expose()
       readonly last_contacted_on: string;
       @Expose()
       readonly last_contacted_via: string;
       @Expose()
       readonly slaid: string;
       @Expose()
       readonly starred: string;
       @Expose()
       readonly tags: string;
       @Expose()
       readonly contacttype: string;
       @Expose()
       readonly contactstatus: string;
       @Expose()
       readonly happiness_rating: string;
       @Expose()
       readonly record_currency_id: string;
       @Expose()
       readonly record_conversion_rate: string;
       @Expose()
       readonly profile_score: string;
       @Expose()
       readonly profile_rating: string;
       @Expose()
       readonly emailoptin: string;
       @Expose()
       readonly emailoptin_requestcount: string;
       @Expose()
       readonly emailoptin_lastrequestedon: string;
       @Expose()
       readonly smsoptin: string;
       @Expose()
       readonly language: string;
       @Expose()
       readonly primary_phone_field: string;
       @Expose()
       readonly primary_email_field: string;
       @Expose()
       readonly cf_contacts_roletype: string;
       @Expose()
       readonly cf_contacts_role: string;
       @Expose()
       readonly cf_contacts_cifid: string;
       @Expose()
       readonly cf_contacts_typeofsme: string;
       @Expose()
       readonly cf_contacts_typeofsponsor: string;
       @Expose()
       readonly cf_contacts_passportnumber: string;
       @Expose()
       readonly cf_contacts_middlename: string;
       @Expose()
       readonly cf_contacts_designation: string;
       @Expose()
       readonly cf_contacts_nationality: string;
       @Expose()
       readonly cf_contacts_uennumber: string;
       @Expose()
       readonly cf_contacts_registrationnumber: string;
       @Expose()
       readonly cf_contacts_country: string;
       @Expose()
       readonly cf_contacts_state: string;
       @Expose()
       readonly cf_contacts_city: string;
       @Expose()
       readonly cf_contacts_street: string;
       @Expose()
       readonly cf_contacts_postalcode: string;
       @Expose()
       readonly cf_contacts_companyname: string;
       @Expose()
       readonly portal: string;
       @Expose()
       readonly support_start_date: string;
       @Expose()
       readonly support_end_date: string;
       @Expose()
       readonly mailingcountry: string;
       @Expose()
       readonly othercountry: string;
       @Expose()
       readonly mailingstreet: string;
       @Expose()
       readonly otherstreet: string;
       @Expose()
       readonly mailingpobox: string;
       @Expose()
       readonly otherpobox: string;
       @Expose()
       readonly mailingcity: string;
       @Expose()
       readonly othercity: string;
       @Expose()
       readonly mailingstate: string;
       @Expose()
       readonly otherstate: string;
       @Expose()
       readonly mailingzip: string;
       @Expose()
       readonly otherzip: string;
       @Expose()
       readonly mailing_gps_lat: string;
       @Expose()
       readonly mailing_gps_lng: string;
       @Expose()
       readonly cf_contacts_noofprojectscreated: string;
       @Expose()
       readonly cf_contacts_noofprojectsfunded: string;
       @Expose()
       readonly cf_contacts_noofprojectsbid: string;
       @Expose()
       readonly cf_contacts_noofprojectssponsored: string;
       @Expose()
       readonly description: string;
       
       @Expose()
       readonly imagename: string;
       @Expose()
       readonly consent_requested: string;
       @Expose()
       readonly consent_trash_data: string;
       @Expose()
       readonly data_erased: string;
       @Expose()
       readonly consent_lock_data: string;
       @Expose()
       readonly consent_track_email_engagement: string;
       @Expose()
       readonly consent_track_shared_documents: string;
       @Expose()
       readonly consents_last_requested_on: string;
       @Expose()
       readonly primary_linkedin: string;
       @Expose()
       readonly followers_linkedin: string;
       @Expose()
       readonly primary_facebook: string;
       @Expose()
       readonly followers_facebook: string;
       @Expose()
       readonly id: string;
       @Expose()
       readonly isclosed: string;
       @Expose()
       readonly label: string;
}

export class crmDto extends Reader {
    @Expose()
    readonly success: boolean;
  
  @Type(() => rDto)
  @Expose()
  readonly result: rDto;
}