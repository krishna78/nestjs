import {
  Body,
  Controller,
  Param,
  Post,
  UseGuards,
  Get,
  Request,
  Query,
  Put,
  NotFoundException,
  Delete,
  BadRequestException,
} from "@nestjs/common";
import { UsersService } from "./users.service";
import {
  CreateUserDto,
  UpdateUserDto,
  UserDto,
  CloseAccount,
} from "./objects/create-user.dto";
import { abstractBaseControllerFactory } from "../common/base/base.controller";
import { LoggedInToken } from "./objects/login-user.dto";
import {
  BASEROUTES,
  USER_TYPES,
  KYC_VERIFICATION_STATUS,
  USER_STATUS,
  ROLES_ACCESS_ACTION,
} from "../common/constants/enum";
import { JwtAuthGuard } from "../auth/auth.guard";
import {
  RequestUser,
  LastUpdatedTime,
  IdOrCodeParser,
} from "../common/utils/controller.decorator";
import {
  UNKNOWN_PARAM,
  NOT_FOUND,
  PAGE_NOT_FOUND_404,
  NEW_PASSWORD_AND_CONFIRM_NEW_PASSWORD_ERROR,
  USERNAME_OR_PASSWORD_INCORRECT,
  CURRENT_PASSWORD_AND_NEW_PASSWORD_ERROR,
  KYC_PENDING_STATUS_CHANGE_ERROR,
  KYC_APPROVED_STATUS_CHANGE_ERROR,
  KYC_REJECTED_STATUS_CHANGE_ERROR,
  USER_ACTIVE_STATUS_CHANGE_ERROR,
  USER_CLOSED_STATUS_CHANGE_ERROR,
  USER_IN_REVIEW_STATUS_CHANGE_ERROR,
  USER_KYC_INCOMPLETE_STATUS_CHANGE_ERROR,
  ONLY_FOR_ADMIN,
} from "../common/constants/string";
import { plainToClass } from "class-transformer";
import { success } from "../common/base/httpResponse.interface";
import { AbstractClassTransformerPipe } from "../common/pipes/class-transformer.pipe";
import { normalizeObject } from "../common/utils/helper";
import * as _ from "lodash";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { IUser, User } from "./objects/user.schema";
import { CrmService } from "./crm/crm.service";
import { KycPendingEmail } from "./objects/user.registered.email";
import { EmailDto } from "../email/objects/email.dto";
import { normalizePaginateResult } from "../common/interfaces/pagination";
import { RolesService } from "../roles/roles.service";

const BaseController = abstractBaseControllerFactory<any>({
  DTO: UserDto,
  DisabledRoutes: [
    BASEROUTES.PATCH,
    //, BASEROUTES.DETELEONE
  ],
});

@Controller("users")
export class UsersController extends BaseController {
  constructor(
    private usersService: UsersService,
    private crmService: CrmService,
    @InjectModel("User") private readonly usersModel: Model<IUser>,
    private rolesservice: RolesService
  ) {
    super(usersService);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async findList(@Request() req, @Query() query, @RequestUser() user) {
   let _user = await this.rolesservice.findOneByQuery({roleName: user.type})
    // if(user.type == USER_TYPES.OPERATIONS_TEAM){
    let hasAccessOperations = _user.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.USERS_CONTROLLER_FINDLIST_OPERATIONS
    );
    let hasAccessAdmin = _user.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.USERS_CONTROLLER_FINDLIST_ADMIN
    );
    
    if(hasAccessOperations) {
      console.log('userstype', user.type, _user, hasAccessOperations, hasAccessAdmin)
      let t = { $or: [{ type: USER_TYPES.SME }, { type: USER_TYPES.SPONSOR }] };
      return await super.findList(req, { ...query, ...t, sort: { _id: -1 } });
      /*
      var options = {
        limit: 30,
        page: 1,
        sort: "_id",
        skip: query.page ? (query.page - 1) : 0
      };
     // return await super.findList(req, { ...query, ...t });
      let d = await this.usersModel.find(
        { ...query, ...t },
        {},
        { sort: { _id: 1 }, skip: options.skip * options.limit, limit: 30, projection: {} }
      );
      let dCount = await this.usersModel.count(
        { ...t },
        //{},
        //{ sort: { _id: 1 }, skip: options.skip * options.limit, limit: options.limit, projection: {} }
      );
      console.log(d.length, dCount);
      await d.map((data) => {
        return plainToClass(UserDto, data, { excludeExtraneousValues: true });
      });
      let pagination = normalizePaginateResult({
        total: dCount,//d.length,
        limit: options.limit,
        page: options.page,
        pages: d.pages,
      });

      //return success
      return success({ d, pagination });
      */
    }
    //if (user.isAdmin) {
      if (hasAccessAdmin){
      // <--- only admin can see the user lists
      return await super.findList(req, { ...query, sort: { _id: -1 } });
    }
    throw new NotFoundException();
  }

  @UseGuards(JwtAuthGuard)
  @Get("profile")
  async getProfile(@RequestUser() user) {
    return await super.findOne(user.code);
  }

  @UseGuards(JwtAuthGuard)
  @Get("PrivacySettingsUrl")
  async getPrivacySettingsUrl(@RequestUser() user) {
    let url = "https://www.cycloan.io/pre-launch/privacy-policy.html"
    //return await super.findOne(user.code);
    return success(url);
  }

  //@UseGuards(JwtAuthGuard)
  @Get("contentTerms&Condition")
  async getcontentsTermsAndConditionsUrl(@RequestUser() user) {
    let url: string[] = [];
    url[0] = "https://www.cycloan.io/terms_of_use_sme.html"
    url[1] = "https://www.cycloan.io/terms_of_use_investor.html"
    //return await super.findOne(user.code);
    return success(url);
  }
  
  
  
  
  @UseGuards(JwtAuthGuard)
  @Get("smeHelpCentre")
  async getsmeHelpCentreUrl(@RequestUser() user) {
    //let url: string[] = [];
    //let url = "https://cycloanpteltd.od2.vtiger.com/portal/index.php"
    let url = "https://cycloan.freshdesk.com/support/home"
   // url[1] = "https://www.cycloan.io/terms_of_use_investor.html"
    //return await super.findOne(user.code);
    return success(url);
  }
  
  
  @UseGuards(JwtAuthGuard)
  @Get(":idOrCode")
  async findOne(
    @IdOrCodeParser("idOrCode") idOrCode: string,
    @RequestUser() user
  ) {
    let _user = await this.rolesservice.findOneByQuery({roleName: user.type});
    console.log('userstype', user.type, _user)
    let hasAccess = _user.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.USERS_CONTROLLER_FIND_ONE
    );
  
    if (hasAccess || user.code === idOrCode || user.id === idOrCode) {
      // <--- only admin or the same person can view a profile
      return await super.findOne(idOrCode);
    }
    throw new NotFoundException();
  }

  @UseGuards(JwtAuthGuard)
  @Post("filter/kycFilter")
  async findListFilterKYCStatus(
    @Request() req,
    @Query() query,
    @RequestUser() user,
    @Body() body: { isProfileCompleted: number }
  ) {
    let t = { $or: [{ type: USER_TYPES.SME }, { type: USER_TYPES.SPONSOR }] };
    //if (user.type == USER_TYPES.ADMIN) {
      let _user = await this.rolesservice.findOneByQuery({roleName: user.type});
      console.log('userstype', user.type, _user)
      let hasAccess = _user.rolesAccessAction.some(
        (e) => e === ROLES_ACCESS_ACTION.USERS_CONTROLLER_KYC_FILTER
      );
     // console.log('userstype', user.type, _user, hasAccess)
   if(hasAccess) {
    var options = {
        limit: 30,
        page: 1,
        sort: "_id",
        skip: query.page ? (query.page - 1) : 0
      };
      
      // <--- only admin and Sponsor can see all the USER lists
      // const d = await this.findList(req, { ...query });
      console.log("filterrrrrrrrrrr", query, query.page, body.isProfileCompleted);
      //const d =  await this.findList(req,  query,  user);
      //let d = await this.usersService.findAll(query, options);
      //let d = await this.usersModel.find({"verification.isProfileCompleted": body.isProfileCompleted}, options);
      let d = await this.usersModel.find(
        { "verification.isProfileCompleted": body.isProfileCompleted, ...t },
        {},
        { sort: { _id: 1 }, skip: options.skip * options.limit, limit: options.limit, projection: {} }
      );
      let dCount = await this.usersModel.count(
        { "verification.isProfileCompleted": body.isProfileCompleted },
        //{},
        //{ sort: { _id: 1 }, skip: options.skip * options.limit, limit: options.limit, projection: {} }
      );
      console.log(d.length, dCount);
      await d.map((data) => {
        return plainToClass(UserDto, data, { excludeExtraneousValues: true });
      });
      let pagination = normalizePaginateResult({
        total: dCount,//d.length,
        limit: options.limit,
        page: options.page,
        pages: d.pages,
      });

      //return success
      return success({ d, pagination });
    }
    throw new NotFoundException();
  }

  @UseGuards(JwtAuthGuard)
  @Post("filter/userStatusFilter")
  async findListFilterUserStatus(
    @Request() req,
    @Query() query,
    @RequestUser() user,
    @Body() body: { status: number }
  ) {
    let t = { $or: [{ type: USER_TYPES.SME }, { type: USER_TYPES.SPONSOR }] };
  //  if (user.type == USER_TYPES.ADMIN) {
    let _user = await this.rolesservice.findOneByQuery({roleName: user.type});
  let hasAccess = _user.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.USERS_CONTROLLER_USER_STATUS_FILTER
    );
    console.log('userstype', user.type, _user)
    if(hasAccess) {
      var options = {
        limit: 30,
        page: 1,
        sort: "_id",
        skip: query.page ? (query.page - 1) : 0
      };
      // <--- only admin and Sponsor can see all the USER lists
      // const d = await this.findList(req, { ...query });
      console.log("filterrrrrrrrrrr", query, body.status);
      //const d =  await this.findList(req,  query,  user);
      //let d = await this.usersService.findAll(query, options);
      //let d = await this.usersModel.find({"verification.isProfileCompleted": body.isProfileCompleted}, options);
      let d = await this.usersModel.find(
        { status: body.status, ...t },
        {},
        { sort: { _id: 1 }, skip: options.skip * options.limit, limit: options.limit, projection: {} }
        //{ sort: { _id: 1 }, skip: 0, limit: 30, projection: {} }
      );
      await d.map((data) => {
        return plainToClass(UserDto, data, { excludeExtraneousValues: true });
      });
      console.log(d);
      let pagination = normalizePaginateResult({
        total: d.length,
        limit: options.limit,
        page: options.page,
        pages: d.pages,
      });

      //return success
      return success({ d, pagination });
      //return _data;
    }
    throw new NotFoundException();
  }

  @UseGuards(JwtAuthGuard)
  @Put(":idOrCode")
  public async update(
    @IdOrCodeParser("idOrCode") idOrCode: string,
    @LastUpdatedTime() lastUpdatedTime: number,
    @Body(AbstractClassTransformerPipe(UpdateUserDto)) updateDto,
    @RequestUser() user
  ) {
    let _user = await this.usersService.findOne(idOrCode);
    // console.log(updateDto); //  console.log(user);
    await this.usersService.checkForUpdateStatusError(_user, updateDto, user);
    debugger;
    updateDto.updatedBy = user.code;
    let _user_ = await this.rolesservice.findOneByQuery({roleName: user.type});
    console.log('userstype', user.type, _user_)
    let hasAccess = _user_.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.USERS_CONTROLLER_USER_UPDATE
    );
    console.log('hasAccess', hasAccess);
    if(!hasAccess) {
   // if (!user.isAdmin) {
    
      if (!(user.code === idOrCode || user.id === idOrCode)) {
        // if user is non admin and tries to update others account
        throw new NotFoundException();
      }

      updateDto = normalizeObject(
        plainToClass(UserDto, {
          // firstName: updateDto.firstName,
          //  lastName: updateDto.lastName,
          preferences: { ...updateDto.preferences },
          aboutCompany: updateDto.aboutCompany,
          profilePic: updateDto.profilePic,
          emailAlerts: updateDto.emailAlerts
          // projectIds: updateDto.projectIds,
          // favourites: updateDto.favourites,
        })
      ); // <--- if non admin wants to update, only preferences,aboutCompany or profilePic should be updated
    }

    let data = await this.usersService.edit(
      idOrCode,
      updateDto,
      Date.now(),
      user
    );
    debugger;

    let d = plainToClass(UserDto, data, { excludeExtraneousValues: true });
    //
    return success(d);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(":idOrCode")
  public async delete(
    @IdOrCodeParser("idOrCode") idOrCode: string,
    @Body()
    body: {
      closeAccount: {
        reason: string;
        description: string;
      };
    },
    //@Body(AbstractClassTransformerPipe(CloseAccount)) closeAccount,
    @RequestUser() user
  ) {
    let _user = await this.rolesservice.findOneByQuery({roleName: user.type});
    console.log('userstype', user.type, _user)
    let hasAccess = _user.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.USERS_CONTROLLER_DELETE
    );
    if(hasAccess) {
    
      //if (user.isAdmin) {
        let _data = await this.usersService.edit(
          idOrCode,
          { status: USER_STATUS.CLOSED },
          Date.now()
        );
        let data = await super.delete(idOrCode);
        return success(data);
    }
      // if (!user.isAdmin) {
    else {
        if (user.code === idOrCode || user.id === idOrCode) {
          console.log(body.closeAccount);
          let _data = await this.usersService.edit(
            idOrCode,
            { closeAccount: body.closeAccount, status: USER_STATUS.CLOSED },
            Date.now()
          );
          let data = await super.delete(idOrCode);
          return success(data);
        }
        throw new NotFoundException();
      }
  }

  @Post()
  public async create(
    @Request() req,
    @Body(AbstractClassTransformerPipe(CreateUserDto)) body: any,
    @Query() query
  ) {
    debugger;
    let data: any;
    body.userName = body.firstName + " " + body.lastName;

    let created = await this.usersService.create(body);

    let dto; // Define what is the response, login if source === register , user object if source is an admin
    switch (query.source) {
      case "admin":
        data = (created as unknown) as UserDto;
        dto = UserDto;
        data = data.user;
        break;
      default:
        data = (created as unknown) as LoggedInToken;
        dto = LoggedInToken;
    }
    const _data = plainToClass(dto, data, { excludeExtraneousValues: true });
    return success(_data);
  }
  @UseGuards(JwtAuthGuard)
  @Post("admin")
  public async createdByAdmin(
    @Request() req,
    @Body(AbstractClassTransformerPipe(CreateUserDto)) body: any,
    @Query() query,
    @RequestUser() user
  ) {
    debugger;
    let data: any;
    console.log(user.type);
    body.createdBy = user.code;
    body.userName = body.firstName + " " + body.lastName;
    let created = await this.usersService.create(body, user);

    let dto; // Define what is the response, login if source === register , user object if source is an admin
    switch (user.type) {
      case USER_TYPES.ADMIN:
        data = (created as unknown) as UserDto;
        dto = UserDto;
        data = data.user;
        break;
      case USER_TYPES.OPERATIONS_TEAM:
        data = (created as unknown) as UserDto;
        dto = UserDto;
        data = data.user;
        break;     
      default:
        data = (created as unknown) as LoggedInToken;
        dto = LoggedInToken;
    }
    const _data = plainToClass(dto, data, { excludeExtraneousValues: true });
    return success(_data);
  }

  @UseGuards(JwtAuthGuard)
  @Post("otp/:emailOrMobile")
  async verifyOTP(
    @Param("emailOrMobile") emailOrMobile,
    @Body() body: { otp: number },
    @RequestUser() user
  ) {
    let otp = body.otp;
    let update = {};
    switch (emailOrMobile) {
      case "mobile":
        update = { mobile: true };
        break;
      case "email":
        update = { email: true };
        break;
      default:
        throw new Error(UNKNOWN_PARAM(emailOrMobile));
    }
    let updated = await this.usersService.edit(user.id, {
      verification: update,
    });
    const _data = plainToClass(UserDto, updated, {
      excludeExtraneousValues: true,
    });
    return success(_data);
  }

  @UseGuards(JwtAuthGuard)
  @Post("changePassword")
  async changePassword(
    //@Param("emailOrMobile") emailOrMobile,
    @Body()
    body: {
      currentPassword: string;
      newPassword: string;
      confirmNewPassword: string;
    },
    @RequestUser() user
  ) {
    let update = {};
    let _updated: any = {};
    if (user) {
      let error = USERNAME_OR_PASSWORD_INCORRECT;
      const userToAttempt: any = await this.usersService.findOneByEmail(
        user.email
      );
      if (!userToAttempt) {
        throw new BadRequestException(error);
      }
      const isMatch = await userToAttempt.checkPassword(body.currentPassword);
      let isPasswordNew = !(body.newPassword == body.currentPassword);
      // const isMatch = await user.checkPassword(body.currentPassword);
      if (
        body.newPassword == body.confirmNewPassword &&
        isMatch &&
        isPasswordNew
      ) {
        let updated = await this.usersService.edit(user.id, {
          password: body.newPassword,
        });

        if (!(user.createdBy === "system")) {
          await this.usersService.forceChangePassword(user);
        }

        const _data = plainToClass(UserDto, updated, {
          excludeExtraneousValues: true,
        });
        return success(_data);
      }
      throw new BadRequestException(CURRENT_PASSWORD_AND_NEW_PASSWORD_ERROR);
    }
  }
}
