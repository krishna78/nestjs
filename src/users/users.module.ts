import { Module, forwardRef } from "@nestjs/common";
import { UsersController } from "./users.controller";
import { UsersService } from "./users.service";
import { MongooseModule } from "@nestjs/mongoose";
import { UserSchema } from "./objects/user.schema";
import { EmailModule } from "../email/email.module";
import { EntityDetailsModule } from "../entity-details/entity-details.module";
import { EntityDetailsService } from "../entity-details/entity-details.service";
import { JwtModule } from "@nestjs/jwt";
import { JWT_EXPIRY, JWT_SECRET_KEY } from "../common/constants/config";
import { PassportModule } from "@nestjs/passport";
import { OtpSchema } from "../auth/otp/otp.schema";
import { OtpService } from "../auth/otp/otp.service";
import { AuthModule } from "../auth/auth.module";
import { CrmSchema } from "./crm/crm.schema";
import { CrmService } from "./crm/crm.service";
import { EmailNotificationSchema } from "./email-notification/email-notification.schema";
import { EmailNotificationService } from "./email-notification/email-notification.service";
import { ActivityFeedModule } from "../activity-feed/activity-feed.module";
import { SmeActivityFeedModule } from "../sme-activity-feed/sme-activity-feed.module";
import { BigchainDbModule } from "../bigchain-db/bigchain-db.module";
import { RolesSchema } from "../roles/objects/roles.schema";
import { RolesModule } from "../roles/roles.module";
import { RolesService } from "../roles/roles.service";
import { BigchaindDbSchema } from "../bigchain-db/objects/bigchain-db.schema";

@Module({
  imports: [
    JwtModule.register({
      secretOrPrivateKey: JWT_SECRET_KEY,
      signOptions: {
        expiresIn: JWT_EXPIRY,
      },
    }),
    PassportModule.register({
      defaultStrategy: "jwt",
      session: false,
    }),
    MongooseModule.forFeature([
      { name: "User", schema: UserSchema },
      { name: "Otp", schema: OtpSchema },
      { name: "Crm", schema: CrmSchema },
      { name: "EmailNotification", schema: EmailNotificationSchema },
      { name: "AdminRoles", schema: RolesSchema },
      { name: "BigchainDb", schema: BigchaindDbSchema },
    ]),
    EmailModule,
    EntityDetailsModule,
    ActivityFeedModule,
    SmeActivityFeedModule,
    BigchainDbModule, 
    RolesModule,
    //forwardRef(() =>AuthModule)
  ],
  exports: [UsersService, EntityDetailsService, EmailNotificationService],
  controllers: [UsersController],
  providers: [
    UsersService,
    EntityDetailsService,
    OtpService,
    CrmService,
    EmailNotificationService,
    RolesService
  ],
})
export class UsersModule {}
