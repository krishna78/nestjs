import { Injectable, BadRequestException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { plainToClass } from "class-transformer";
import { BaseService } from "../common/base/base.service";
import { IUser, User } from "./objects/user.schema";
import { EmailService } from "../email/email.service";
import {
  HasPostCreate,
  HasPreCreate,
  HasPostUpdate,
  HasPreUpdate,
} from "../common/base/pre.post.action";
import { EmailDto } from "../email/objects/email.dto";
import {
  UserRegisteredEmailForSponsor,
  UserRegisteredEmailForSme,
  LoginCredentialEmail,
  KycApprovedforSme,
  KycApprovedforSponsor,
  KycRejectedforSme,
  KycRejectedforSponsor,
  KycPendingEmail,
} from "./objects/user.registered.email";
import { JwtService } from "@nestjs/jwt";
import {
  JWT_EXPIRY,
  CRM_URL_HEADERS,
  BIGCHAINDB_API_PATH,
} from "../common/constants/config";
import { EntityDetailsService } from "../entity-details/entity-details.service";
import {
  CreateEntityDetailsDto,
  EntityDetailsDto,
} from "../entity-details/objects/entity-details.dto";
import { IEntityDetails } from "../entity-details/objects/entity-details.schema";
import { UserDto, CreateUserDto } from "./objects/create-user.dto";
import { crmDto } from "./objects/userCrm.dto";
import {
  USER_TYPES,
  ACTIVITY_FEED_EVENTS_SPONSOR,
  ACTIVITY_FEED_EVENTS_SME,
  KYC_VERIFICATION_STATUS,
  USER_STATUS,
  SME,
  INVESTOR,
  BIGCHAINDB_TRANSACTION_TYPE,
  ROLES_ACCESS_ACTION,
} from "../common/constants/enum";
import { SetFormattedId } from "../common/base/set.formattedId";
import { OtpService } from "../auth/otp/otp.service";
import * as request from "request-promise-native";
import * as cloneDeep from "lodash.clonedeep";
import { AnyFilesInterceptor } from "@nestjs/platform-express";
import { CanLoadVirtual } from "../common/interfaces/can.load.virtual";
import { IsBoolean } from "class-validator";
import { json } from "express";
import { IamUser } from "aws-sdk/clients/macie2";
import { IamUserArn } from "aws-sdk/clients/codedeploy";
import { CRM_URL_HEADERS_UPDATE } from "../common/constants/config";
import { CrmService } from "./crm/crm.service";
import { EmailNotificationService } from "./email-notification/email-notification.service";
import { result } from "lodash";
import { ActivityFeedService } from "../activity-feed/activity-feed.service";
import { UpdateBlogDTO } from "../blog/objects/blog.dto";
import { SmeActivityFeedService } from "../sme-activity-feed/sme-activity-feed.service";
import {
  KYC_PENDING_STATUS_CHANGE_ERROR,
  KYC_PENDING_STATUS_CHANGE_ERROR_WITH_ONBOARDING_FALSE,
  KYC_APPROVED_STATUS_CHANGE_ERROR,
  KYC_REJECTED_STATUS_CHANGE_ERROR,
  USER_KYC_INCOMPLETE_STATUS_CHANGE_ERROR,
  USER_CLOSED_STATUS_CHANGE_ERROR,
  USER_IN_REVIEW_STATUS_CHANGE_ERROR,
  USER_ACTIVE_STATUS_CHANGE_ERROR,
  BIGCHAIN_SERVER_IS_DOWN,
} from "../common/constants/string";
const driver = require("bigchaindb-driver");
//import driver from 'bigchaindb-driver';
//import { delay } from "../bid-details/bid-details.controller";
//const driver = require('bigchaindb-driver')

import { generateKeyPair } from "crypto";
import { BigchainDbService } from "../bigchain-db/bigchain-db.service";
import { type } from "os";
import { RolesService } from "../roles/roles.service";
import { BigchainDb } from "../bigchain-db/objects/bigchain-db.schema";

export function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

@Injectable()
export class UsersService extends BaseService<IUser>
  implements
    HasPreCreate,
    HasPostCreate,
    HasPreUpdate,
    HasPostUpdate,
    CanLoadVirtual {
  constructor(
    @InjectModel("User") private readonly userModel: Model<User>,
    private emailService: EmailService,
    private jwtService: JwtService,
    private entityDetailsService: EntityDetailsService,
    private otpService: OtpService,
    private crmService: CrmService,
    private emailnotificationService: EmailNotificationService,
    private activityFeedService: ActivityFeedService,
    private smeactivityFeedService: SmeActivityFeedService,
    private bigchaindbService: BigchainDbService,
    private rolesservice: RolesService,
    @InjectModel("BigchainDb") private readonly bigchaindbmodel: Model<BigchainDb>
  ) {
    super(userModel);
  }

  formattedIdLength: number = 6;

  getQuery(createDto: CreateUserDto): Object {
    debugger;
    return { type: createDto.type };
  }

  getPrefixForId(createDto: CreateUserDto): string {
    debugger;
    switch (createDto.type) {
      case "Admin":
        return "AD";
      case "Sponsor":
        return "SP";
      case "Sme":
        return "SM"; 
      case "Operations":
        return "OP"
      default:
      //
    }
  }

  getVirtualForItem(): string[] {
    return ["profileDetails", "favouriteProjects"];
  }

  getVirtualForList(): string[] {
    return ["profileDetails"];
  }

  async checkForUpdateStatusError(_user, updateDto, user) {
    let userProfile = await this.rolesservice.findOneByQuery({roleName: user.type})
    // if(user.type == USER_TYPES.OPERATIONS_TEAM){
    let hasAccess = userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.USERS_SERVICE_CHECK_FOR_UPDATE_STATUS_ERROR
    );
    console.log('userstype', user.type, _user, hasAccess, userProfile)
    /*
    let hasAccessAdmin = userOperations.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.USERS_SERVICE_CHECK_FOR_UPDATE_STATUS_ERROR
    );
    */
    switch (_user.verification.isProfileCompleted) {
      case KYC_VERIFICATION_STATUS.PENDING:
        {
         // if (user.type == USER_TYPES.ADMIN) {
          if (hasAccess) {
           
            let s = updateDto.verification;
            if (updateDto.verification == undefined) {
              console.log(
                "__________________________No KYC status field___________________________"
              );
            } else {
              console.log("updateDTooooo.status", updateDto.verification, s);
              if((updateDto.verification.isProfileCompleted == KYC_VERIFICATION_STATUS.APPROVED) && (_user.verification.isOnboardingComplete == false)){
                  throw new BadRequestException(KYC_PENDING_STATUS_CHANGE_ERROR_WITH_ONBOARDING_FALSE);
                }
                
              if (
                !(
                  updateDto.verification.isProfileCompleted ==
                    KYC_VERIFICATION_STATUS.PENDING ||
                  updateDto.verification.isProfileCompleted ==
                    KYC_VERIFICATION_STATUS.APPROVED ||
                  updateDto.verification.isProfileCompleted ==
                    KYC_VERIFICATION_STATUS.REJECTED
                )
              ) {
                throw new BadRequestException(KYC_PENDING_STATUS_CHANGE_ERROR);
              }
              if((updateDto.verification.isProfileCompleted == KYC_VERIFICATION_STATUS.APPROVED) && (_user.verification.isOnboardingComplete == true) //&& (_user.verification.isProfileCompleted == KYC_VERIFICATION_STATUS.PENDING)
              ){
                if (_user.entityDetailCode !== undefined && _user.verification.isProfileCompleted == 0) {
                  let _model = await this.entityDetailsService.findOne(
                    _user.entityDetailCode
                  );  
                  let query = { creator: _user.code };
                  let data = await this.emailnotificationService.findOneByQuery(query);
                  if (
                   // _user.verification.isProfileCompleted === 1 &&
                    data.isKycApproved === false
                  ) {
                    await this.createDummyBigChainTransaction(_model, _user);
                  }
                  /*
                    let bigchaindbkeys =  await this.bigchaindbmodel.find({
                      createdBy: _user.code, isDummyTransaction: true
                    });
                  // let bigchaindbkeys = await this.bigchaindbService.findOneByQuery({
                  //   createdBy: _user.code, isDummyTransaction: true
                  // });
                    let assetreferenceKey = bigchaindbkeys[0].transactionId;
                    console.log('If transactionId is null? If transactionId is null? If transactionId is null? If transactionId is null?',bigchaindbkeys,  assetreferenceKey)
                  if(assetreferenceKey == null){
                    throw new BadRequestException(BIGCHAIN_SERVER_IS_DOWN)
                  }
                  */
                }
              
                updateDto.userApprovedTime = Date.now(),
                updateDto.userApprovedBy = user.code,
                updateDto.userApprovedAdminEmail = user.email
              }
            }
          }
        }
        break;
      case KYC_VERIFICATION_STATUS.APPROVED:
        {
         // if (user.type == USER_TYPES.ADMIN) {
          if (hasAccess) {
            let s = updateDto.verification;
            if (updateDto.verification == undefined) {
              console.log(
                "__________________________No KYC status field___________________________"
              );
            } else {
              console.log("updateDTooooo.status", updateDto.verification, s);
              if (
                !(
                  updateDto.verification.isProfileCompleted ==
                  KYC_VERIFICATION_STATUS.APPROVED
                )
              ) {
                throw new BadRequestException(KYC_APPROVED_STATUS_CHANGE_ERROR);
              }
            }
          }
        }
        break;
      case KYC_VERIFICATION_STATUS.REJECTED: {
        if (hasAccess) {
          //if (user.type == USER_TYPES.ADMIN) {
          let s = updateDto.verification;
          if (updateDto.verification == undefined) {
            console.log(
              "__________________________No KYC status field___________________________"
            );
          } else {
            console.log("updateDTooooo.status", updateDto.verification, s);
            if (
              !(
                updateDto.verification.isProfileCompleted ==
                KYC_VERIFICATION_STATUS.REJECTED
              )
            ) {
              throw new BadRequestException(KYC_REJECTED_STATUS_CHANGE_ERROR);
            }
          }
        }
      }
    }

    switch (_user.status) {
      case USER_STATUS.ACTIVE:
        {
          if (hasAccess) {
           // if (user.type == USER_TYPES.ADMIN) {
            let s = updateDto.status;
            if (updateDto.status == undefined) {
              console.log(
                "__________________________No User status field___________________________"
              );
            } else {
              console.log("updateDTooooo.status", updateDto.status, s);
              if (
                !(
                  updateDto.status == USER_STATUS.ACTIVE ||
                  updateDto.status == USER_STATUS.IN_REVIEW ||
                  updateDto.status == USER_STATUS.CLOSED ||
                  updateDto.status == USER_STATUS.KYC_INCOMPLETE
                )
              ) {
                throw new BadRequestException(USER_ACTIVE_STATUS_CHANGE_ERROR);
              }
            }
          }
        }
        break;
      case USER_STATUS.IN_REVIEW:
        {
          //if (user.type == USER_TYPES.ADMIN) {
          if (hasAccess) {
            let s = updateDto.status;
            if (updateDto.status == undefined) {
              console.log(
                "__________________________No User status field___________________________"
              );
            } else {
              console.log("updateDTooooo.status", updateDto.status, s);
              if (
                !(
                  updateDto.status == USER_STATUS.ACTIVE ||
                  updateDto.status == USER_STATUS.IN_REVIEW ||
                  updateDto.status == USER_STATUS.CLOSED ||
                  updateDto.status == USER_STATUS.KYC_INCOMPLETE
                )
              ) {
                throw new BadRequestException(
                  USER_IN_REVIEW_STATUS_CHANGE_ERROR
                );
              }
            }
          }
        }
        break;
      case USER_STATUS.CLOSED:
        {
          if (hasAccess) {
           //if (user.type == USER_TYPES.ADMIN) {
            let s = updateDto.status;
            if (updateDto.status == undefined) {
              console.log(
                "__________________________No User status field___________________________"
              );
            } else {
              console.log("updateDTooooo.status", updateDto.status, s);
              if (
                !(
                  updateDto.status == USER_STATUS.ACTIVE ||
                  updateDto.status == USER_STATUS.IN_REVIEW ||
                  updateDto.status == USER_STATUS.CLOSED ||
                  updateDto.status == USER_STATUS.KYC_INCOMPLETE
                )
              ) {
                throw new BadRequestException(USER_CLOSED_STATUS_CHANGE_ERROR);
              }
            }
          }
        }
        break;
      case USER_STATUS.KYC_INCOMPLETE:
        {
          if (hasAccess) {
           //if (user.type == USER_TYPES.ADMIN) {
            let s = updateDto.status;
            if (updateDto.status == undefined) {
              console.log(
                "__________________________No User status field___________________________"
              );
            } else {
              console.log("updateDTooooo.status", updateDto.status, s);
              if (
                !(
                  updateDto.status == USER_STATUS.ACTIVE ||
                  updateDto.status == USER_STATUS.IN_REVIEW ||
                  updateDto.status == USER_STATUS.CLOSED ||
                  updateDto.status == USER_STATUS.KYC_INCOMPLETE
                )
              ) {
                throw new BadRequestException(
                  USER_KYC_INCOMPLETE_STATUS_CHANGE_ERROR
                );
              }
            }
          }
        }
        break;
    }
  }

  async create(createDto: any, user?: User) {
    debugger;
    
    // if(user.type == USER_TYPES.OPERATIONS_TEAM){
   
   
    if (user) {
      // if (user.type == USER_TYPES.ADMIN) {
       // if(userProfile !== undefined){
          
        let userProfile = await this.rolesservice.findOneByQuery({roleName: user.type})
        let hasAccess = userProfile.rolesAccessAction.some(
          (e) => e === ROLES_ACCESS_ACTION.USERS_SERVICE_CREATE
        );
        console.log('userstype', user.type, userProfile, hasAccess);
        if(hasAccess){
         let _user: UserDto = await super.create(createDto);
         let user = ({
           email: createDto.email,
           password: createDto.password,
         } as unknown) as User;
         let jwt = this.jwtService.sign(
           {
             email: user.email,
           },
           { expiresIn: JWT_EXPIRY }
         );
         //    await this.doPostCreate(_user);
 
         return {
           expiresIn: JWT_EXPIRY,
           token: jwt,
           type: createDto.type,
           verification: _user.verification,
           user: _user,
         };
       }
    }
  
    let query = { email: createDto.email, isVerified: true };
    
    let _otp: any = await this.otpService.findOneByQuery(query);

    if (_otp) {
  //    if (_otp == null) {
      let _user: UserDto = await super.create(createDto);

      let user = ({
        email: createDto.email,
        password: createDto.password,
      } as unknown) as User;
      let jwt = this.jwtService.sign(
        {
          email: user.email,
        },
        { expiresIn: JWT_EXPIRY }
      );

      return {
        expiresIn: JWT_EXPIRY,
        token: jwt,
        type: createDto.type,
        verification: _user.verification,
        user: _user,
      };
    }
    return await this.validate(createDto);
  }

  async findOneByEmail(email: string): Promise<IUser> {
    return await this.findOneByQuery({ email });
  }

  async doPreCreate(createDto: Partial<IUser>): Promise<void> {
    debugger;
    delete createDto["confirmPassword"];
  }

  

  async activityFeedprofileChanges(editDto, model) {
    let createDto7: any = {
      _priority: 3,
      type: ACTIVITY_FEED_EVENTS_SPONSOR.PROFILE_CHANGES,
      setFields: editDto,
    };
    let _activityFeed1: any = await this.activityFeedService.profileChangesEditingTheProfile(
      createDto7,
      model
    );
  }

  async smeactivityFeedprofileChanges(editDto, model) {
    let createDto7: any = {
      _priority: 3,
      type: ACTIVITY_FEED_EVENTS_SPONSOR.PROFILE_CHANGES,
      setFields: editDto,
    };
    let _activityFeed1: any = await this.smeactivityFeedService.profileChangesEditingTheProfile(
      createDto7,
      model
    );
  }

  async activityFeedRegistrationAndWalletBalance(user) {
    let _user = await this.edit(user.code, { totalBalance: 100000 }, Date.now());
    //console.log(_user);
    let createDto2: any = {
      createdBy: _user.code,
      _priority: 2,
      type: ACTIVITY_FEED_EVENTS_SPONSOR.SPONSOR_REGISTRATION,
      itemId: _user.code,
      setFields: _user,
    };
    let createDto3: any = {
      _priority: 2,
      type: ACTIVITY_FEED_EVENTS_SPONSOR.WALLET_BALANCE_INITIAL_BALANCE,
      itemId: _user.code,
      setFields: { totalBalance: 100 },
    };

    let _activityFeed: any = await this.activityFeedService.sponsorRegistration(
      createDto2,
      _user
    );
    //WALLET_BALANCE_INITIAL_BALANCE
    let _activityFeed1: any = await this.activityFeedService.walletBalanceInitialBalance(
      createDto3,
      _user
    );
  }

  async smeActivityFeedRegistration(user) {
    let createDto2: any = {
      _priority: 2,
      type: ACTIVITY_FEED_EVENTS_SME.SME_REGISTRATION,
      itemId: user.code,
      setFields: user,
    };
    let _smeactivityFeed: any = await this.smeactivityFeedService.smeRegistration(
      createDto2,
      user
    );
  }

  async activityFeedkYCVerificationPending(user) {
    let createDto3: any = {
      _priority: 2,
      type: ACTIVITY_FEED_EVENTS_SPONSOR.KYC_VERIFICATION_NOTIFICATION_PENDING,
      itemId: user.code,
      setFields: { message: "kyc-pending" },
    };
    let _activityFeed: any = await this.activityFeedService.kYCVerificationNotificationsPendingAndRequestingForDocuments(
      createDto3,
      user
    );
  }

  async smeactivitykYCVerificationPending(user) {
    let createDto3: any = {
      _priority: 2,
      type: ACTIVITY_FEED_EVENTS_SME.KYC_VERIFICATION_NOTIFICATION_PENDING,
      itemId: user.code,
      setFields: { message: "kyc-pending" },
    };
    let _activityFeed: any = await this.smeactivityFeedService.kYCVerificationNotificationsPendingAndRequestingForDocuments(
      createDto3,
      user
    );
  }

  async sendKycPendingNotificationAllAdmins(user) {
    let condition = { $or: [{ type: USER_TYPES.ADMIN }, { type: USER_TYPES.OPERATIONS_TEAM }] };
    //let condition = { type: "Admin" };
    debugger;
    //usersModel
    let admins = await this.userModel.find(condition);
    /*
    let admins = await super.findAll(condition, {
      limit: 30,
      page: 1,
      sort: "_id",
    });
    */
    let notify = await this.emailnotificationService.findOneByQuery({
      creator: user.code,
    });
    console.log(
      "Adminssssss", //admins,
      //admins.total,
      admins.length
    ); //console.log(admins.docs[0]);

    if (notify.isKycPending === false) {
      for (let i = 0; i < admins.length; i++) {
        console.log(
          "email-integration sending pending emails users to all admins"
        );
        let email = admins[i].email;
        await this.emailService.sendEmail(
          new KycPendingEmail(
            new EmailDto({
              to: email,
              metaData: { user },
            })
          )
        );
      }
      let result = await this.emailnotificationService.edit(notify.code, {
        isKycApproved: false,
        isKycRejected: false,
        isKycPending: true,
      });
    }
  }

  async forceChangePassword(user) {
    let update = { forcePasswordChange: true };
    await this.edit(user.code, {
      verification: update,
    });
  }

  async createEmailNotificationObject(user) {
    let createDto: any = {
      creator: user.code,
    };
    //Creating EmailNotification Collection
    debugger;
    let endata = await this.emailnotificationService.create(createDto);
    console.log(endata);
  }

  async createCrmAccountAsRegisteredUser(user) {
    let crmId = "";
    var crmUser: any =
      '{"firstname":' +
      '"' +
      user.firstName +
      '"' +
      ',"lastname":' +
      '"' +
      user.lastName +
      '"' +
      ',"cf_contacts_role":' +
      '"' +
      user.type +
      '"' +
      ',"assigned_user_id":"19x1","contacttype":"Registered Leads"' +
      ',"email":' +
      '"' +
      user.email +
      '"' +
      ',"mobile":' +
      '"' +
      user.mobileNumber +
      '"' +
      "}";

    var options1 = {
      ...CRM_URL_HEADERS,
      form: {
        elementType: "Contacts",
        element: crmUser,
      },
    };

    //1st POST reuest to the Crm
    var response = await request.post(options1);
    crmId = JSON.parse(response).result.id;
    return crmId;
  }

  async createCrmObject(crmId, user) {
    let createDto1: any = {
      crmId: crmId,
      createdBy: user.code,
      // email: email,
      // expiry: updateTime + 300 * 1000,
    };
    let _crm1: any = await this.crmService.create(createDto1);
  }

  async updateUserCrmIdAndEntityDetailCode(crmId, user) {
   // if (user.type == USER_TYPES.SME || USER_TYPES.SPONSOR || USER_TYPES.ADMIN) {
    let userProfile = await this.rolesservice.findOneByQuery({roleName: user.type})
    // if(user.type == USER_TYPES.OPERATIONS_TEAM){
    let hasAccess = userProfile.rolesAccessAction.some(
      (e) => e === ROLES_ACCESS_ACTION.USERS_SERVICE_UPDATE_USER_CRMID_AND_ENTITYDETAILCODE
    );
    console.log('userstype', user.type, userProfile, hasAccess)
    if(hasAccess){
      let entity = await this.entityDetailsService.create(
          new CreateEntityDetailsDto(user.code)
        );
        console.log(CreateUserDto);
        await this.edit(user.code, {
          entityDetailCode: entity.code,
          crmId: crmId,
        });
    }
  }

  async createBigChainTransaction(_model, model) {
    // let username = model.firstName;
    const username = new driver.Ed25519Keypair();
    console.log(username, model);
    let bigchaindbType = null;
    const API_PATH = BIGCHAINDB_API_PATH;
    /*
     */
    let _uenNumber = "";
    let _companyName = "";
    let _contactNumber = "";
    bigchaindbType = BIGCHAINDB_TRANSACTION_TYPE.SPONSOR_VERFIED;
    if (model.type === USER_TYPES.SPONSOR) {
      let aSponsor = {
        uennumber: "",
        companyName: "",
        phone: "",
      };

      switch (_model.company.type) {
        case INVESTOR.TECH_COMPANY_IN_SINGAPORE:
          aSponsor.uennumber = _model.company.details.uenNumber;
          aSponsor.companyName = _model.company.details.name;
          aSponsor.phone = _model.company.details.telephoneNumber;
          break;
        case INVESTOR.TECH_COMPANY_OUTSIDE_SINGAPORE:
          aSponsor.uennumber = _model.company.details.registrationNumber;
          aSponsor.companyName = _model.company.details.name;
          aSponsor.phone = _model.company.details.telephoneNumber;
          break;
        case INVESTOR.FINANCIAL_INSTITUTIONS:
          aSponsor.uennumber = _model.company.details.registrationNumber;
          aSponsor.companyName = _model.company.details.companyName;
          aSponsor.phone = _model.company.details.telephoneNumber;
          break;
        case INVESTOR.HIGH_NET_WORTH_INDIVIDUALS:
          aSponsor.uennumber = "";
          aSponsor.companyName = "";
          aSponsor.phone = _model.company.details.contactNumber;
          break;
      }
      console.log(aSponsor);
      _uenNumber = aSponsor.uennumber;
      _companyName = aSponsor.companyName;
      _contactNumber = aSponsor.phone;
    } else if (model.type === USER_TYPES.SME) {
      bigchaindbType = BIGCHAINDB_TRANSACTION_TYPE.SME_VERIFIED;
      let aSme = {
        uennumber: "",
        phone: "",
      };
      console.log(aSme);

      switch (_model.company.type) {
        case SME.TECH_SME_IN_SINGAPORE:
          aSme.uennumber = _model.company.details.uenNumber;
          aSme.phone = _model.company.details.companyTelephoneNumber;
          break;
        case SME.TECH_SME_OUTSIDE_SINGAPORE:
          aSme.uennumber = _model.company.details.companyRegistrationNumber;
          aSme.phone = _model.company.details.telephoneNumber;
          break;
      }
      console.log(aSme);
      _uenNumber = aSme.uennumber;
      _companyName = _model.company.details.companyName;
      _contactNumber = aSme.phone;
    }

    let smeassetData = {
      //createdBy: model.createdBy,
      createdBy: "Cycloan",
      cifID: model.formattedId,
      email: model.email,
      uenNumber: _uenNumber,
      //RegistrationNo: entitydetails.company.details.RegistrationNo,
      passportNumber: _model.applicantDetails.passportNumber,
      companyName: _companyName,
      datetime: Date.now(),
    };
    console.log("FinalllllllsmeAsssssettttttDatata", smeassetData);
    /*
     */
    let smemetaData = {
      firstName: _model.applicantDetails.firstName,
      lastName: _model.applicantDetails.lastName,
      contactNumber: _contactNumber,
      //companyAddress: _model.company.address,
      type: _model.company.type,
    };
    console.log("SSSSSmeMeeetttaaaDatata", smeassetData);
    /*
          /*
        */
    //---bbbbbbbbbb
    // Construct a transaction payload
    const tx = await driver.Transaction.makeCreateTransaction(
      // Define the asset to store, in this example it is the current temperature
      // (in Celsius) for the city of Berlin.
      //assets data
      smeassetData,
      /* 
          
          {
              
              city: "Berlin, DE",
              temperature: 22,
              datetime: new Date().toString(),
              
            }
            
            /*
            */ // Metadata contains information about the transaction itself
      // (can be `null` if not needed)
      //
      smemetaData,
      /*
            
            { 
              
              what: "My first BigchainDB transaction"
              
            }
          
            */ // A transaction needs an output
      [
        driver.Transaction.makeOutput(
          driver.Transaction.makeEd25519Condition(username.publicKey)
        ),
      ],
      username.publicKey
    );
    console.log("ttttxxxxx", tx);
    // Sign the transaction with private keys
    const txSigned = driver.Transaction.signTransaction(
      tx,
      username.privateKey
    );
    console.log("txSigneddddd", txSigned);
    // Send the transaction off to BigchainDB
    debugger;
    const conn = new driver.Connection(API_PATH);
    conn.postTransactionCommit(txSigned).then((retrievedTx) => {
      let keys = {
        type: bigchaindbType,
        publickey: username.publicKey,
        privatekey: username.privateKey,
        transactionId: retrievedTx.id,
        projectRef:  null,
        isDummyTransaction: false
        //"id",
      };
      return new Promise(async (resolve, reject) => {
        // <--- this line
        let bcdb = await this.bigchaindbService.SaveKeys(keys, model);
        console.log("Transaction", retrievedTx.id, "successfully posted.");
      });
    });
/*
    conn
      .postTransactionCommit(txSigned)
      .then((retrievedTx) =>
        console.log("Transaction", retrievedTx.id, "successfully posted.")
      );
      */
    //---bbbbbbbbbb
  }

  async createDummyBigChainTransaction(_model, model) {
    // let username = model.firstName;
    const username = new driver.Ed25519Keypair();
    console.log(username, model);
    let bigchaindbType = null;
    const API_PATH = BIGCHAINDB_API_PATH;
    /*
     */
    let _uenNumber = "";
    let _companyName = "";
    let _contactNumber = "";
    bigchaindbType = BIGCHAINDB_TRANSACTION_TYPE.SPONSOR_VERFIED;
    if (model.type === USER_TYPES.SPONSOR) {
      let aSponsor = {
        uennumber: "",
        companyName: "",
        phone: "",
      };

      switch (_model.company.type) {
        case INVESTOR.TECH_COMPANY_IN_SINGAPORE:
          aSponsor.uennumber = _model.company.details.uenNumber;
          aSponsor.companyName = _model.company.details.name;
          aSponsor.phone = _model.company.details.telephoneNumber;
          break;
        case INVESTOR.TECH_COMPANY_OUTSIDE_SINGAPORE:
          aSponsor.uennumber = _model.company.details.registrationNumber;
          aSponsor.companyName = _model.company.details.name;
          aSponsor.phone = _model.company.details.telephoneNumber;
          break;
        case INVESTOR.FINANCIAL_INSTITUTIONS:
          aSponsor.uennumber = _model.company.details.registrationNumber;
          aSponsor.companyName = _model.company.details.companyName;
          aSponsor.phone = _model.company.details.telephoneNumber;
          break;
        case INVESTOR.HIGH_NET_WORTH_INDIVIDUALS:
          aSponsor.uennumber = "null";
          aSponsor.companyName = "null";
          aSponsor.phone = _model.company.details.contactNumber;
          _model.applicantDetails.firstName = "null";
          _model.applicantDetails.lastName = "null";
          _model.applicantDetails.passportNumber = _model.company.details.passportNumber
          break;
      }
      console.log(aSponsor);
      _uenNumber = aSponsor.uennumber;
      _companyName = aSponsor.companyName;
      _contactNumber = aSponsor.phone;
    } else if (model.type === USER_TYPES.SME) {
      bigchaindbType = BIGCHAINDB_TRANSACTION_TYPE.SME_VERIFIED;
      let aSme = {
        uennumber: "",
        phone: "",
      };
      console.log(aSme);

      switch (_model.company.type) {
        case SME.TECH_SME_IN_SINGAPORE:
          aSme.uennumber = _model.company.details.uenNumber;
          aSme.phone = _model.company.details.companyTelephoneNumber;
          break;
        case SME.TECH_SME_OUTSIDE_SINGAPORE:
          aSme.uennumber = _model.company.details.companyRegistrationNumber;
          aSme.phone = _model.company.details.telephoneNumber;
          break;
      }
      console.log(aSme);
      _uenNumber = aSme.uennumber;
      _companyName = _model.company.details.companyName;
      _contactNumber = aSme.phone;
    }

    let smeassetData = {
      //createdBy: model.createdBy,
      createdBy: "Cycloan",
      cifID: model.formattedId,
      email: model.email,
      uenNumber: _uenNumber,
      //RegistrationNo: entitydetails.company.details.RegistrationNo,
      passportNumber: _model.applicantDetails.passportNumber,
      companyName: _companyName,
      datetime: Date.now(),
    };
    console.log("FinalllllllsmeAsssssettttttDatata", smeassetData);
    /*
     */
    let smemetaData = {
      firstName: _model.applicantDetails.firstName,
      lastName: _model.applicantDetails.lastName,
      contactNumber: _contactNumber,
      //companyAddress: _model.company.address,
      type: _model.company.type,
    };
    console.log("SSSSSmeMeeetttaaaDatata", smemetaData);
    /*
          /*
        */
    //---bbbbbbbbbb
    // Construct a transaction payload
    const tx = await driver.Transaction.makeCreateTransaction(
      // Define the asset to store, in this example it is the current temperature
      // (in Celsius) for the city of Berlin.
      //assets data
   //   smeassetData,
      /* */
          
          {
              
              city: "Berlin, DE",
              temperature: 22,
              datetime: new Date().toString(),
              
            },
            
            /*
            */ // Metadata contains information about the transaction itself
      // (can be `null` if not needed)
      //
    //  smemetaData,
      /**/ 
            
            { 
              
              what: "My first BigchainDB transaction"
              
            },
          
           /* */ // A transaction needs an output
      [
        driver.Transaction.makeOutput(
          driver.Transaction.makeEd25519Condition(username.publicKey)
        ),
      ],
      username.publicKey
    );
    console.log("ttttxxxxx", tx);
    // Sign the transaction with private keys
    const txSigned = driver.Transaction.signTransaction(
      tx,
      username.privateKey
    );
    console.log("txSigneddddd", txSigned);
    // Send the transaction off to BigchainDB
    debugger;
    const conn = new driver.Connection(API_PATH);
    /* */
    conn.postTransactionCommit(txSigned).then((retrievedTx) => {
      let keys = {
        type: bigchaindbType,
        publickey: username.publicKey,
        privatekey: username.privateKey,
        transactionId: retrievedTx.id,
        projectRef:  null,
        isDummyTransaction: true
        //"id",
      };
      return new Promise((resolve, reject) => {
        // <--- this line
        let bcdb = this.bigchaindbService.SaveKeys(keys, model);
        console.log("Transaction", retrievedTx.id, "successfully posted.");
      });
    });
    /**/
   // let retrievedTxO = await conn.postTransactionCommit(txSigned);
   // console.log("Transaction", retrievedTxO.id, "successfully posted.");
/*
    conn
      .postTransactionCommit(txSigned)
      .then((retrievedTx) =>
        console.log("Transaction", retrievedTx.id, "successfully posted.")
      );
      */
    //---bbbbbbbbbb
  }

  async SendUserRegisteredEmailForSme(user) {
    console.log("email intiated");
    if(user.emailAlerts == true){
      await this.emailService.sendEmail(
        new UserRegisteredEmailForSme(
          new EmailDto({
            to: user.email,
            metaData: { user },
          })
        )
      );
    }
  }
  async SendUserRegisteredEmailForSponsor(user) {
    console.log("email intiated");
    if(user.emailAlerts == true){
      await this.emailService.sendEmail(
        new UserRegisteredEmailForSponsor(
          new EmailDto({
            to: user.email,
            metaData: { user },
          })
        )
      );
    }
  }

  async doPostCreate(user: IUser): Promise<void> {
    debugger;
    console.log(user);
    let query = { code: user.code };
    let userdata = await super.findOneByQuery(query);
    if (!(userdata.createdBy === "system")) {
      console.log("AdminCreated user Initiated----sending login credentials");
      let email = user.email;
      let firstName = user.firstName;
      let lastName = user.lastName;
      if(user.emailAlerts == true){
        await this.emailService.sendEmail(
          new LoginCredentialEmail(
            new EmailDto({
              to: user.email,
              metaData: { email, firstName, lastName },
            })
          )
        );
      }
    } else {
      debugger;

      //Set the forceChangePassword field to true for non-Admin created profiles.
      await this.forceChangePassword(user);
    }

    await this.createEmailNotificationObject(user);

    let crmId = await this.createCrmAccountAsRegisteredUser(user);

    await this.createCrmObject(crmId, user);

    await this.updateUserCrmIdAndEntityDetailCode(crmId, user);

    if (user.type == USER_TYPES.SPONSOR) {
      await this.SendUserRegisteredEmailForSponsor(user);
      await this.activityFeedRegistrationAndWalletBalance(user);
      await this.activityFeedkYCVerificationPending(user);
    }

    if (user.type == USER_TYPES.SME) {
      debugger;
      await this.SendUserRegisteredEmailForSme(user);
      await this.smeActivityFeedRegistration(user);
      await this.smeactivitykYCVerificationPending(user);
    }
    debugger;
    //sending pending email to all admin available when user registered
    await this.sendKycPendingNotificationAllAdmins(user);
  }
  //async createBigChainDbTransaction(){

  async findEntityByUser(createdBy: string): Promise<EntityDetailsDto> {
    let _entityDetails: EntityDetailsDto = await this.entityDetailsService.findOneByQuery(
      { createdBy }
    );
    return _entityDetails;
  }

  async doPreUpdate(idOrCode, editDto, model, user): Promise<void> {
    console.log('modelllUser',model,user)
    if (model.type == USER_TYPES.SPONSOR) {
      if (editDto.preferences || editDto.aboutCompany || editDto.profilePic) {
        await this.activityFeedprofileChanges(editDto, model);
      }
    } else if (model.type == USER_TYPES.SME) {
      if (editDto.preferences || editDto.aboutCompany || editDto.profilePic) {
        await this.smeactivityFeedprofileChanges(editDto, model);
      }
    }
    if(editDto.verification !== undefined){
      if (model.entityDetailCode !== undefined && editDto.verification.isProfileCompleted == 0) {
        let _model = await this.entityDetailsService.findOne(
          model.entityDetailCode
        );
        //await this.createDummyBigChainTransaction(_model, model);
      }
    }
   

  }

  async doPostUpdate(idOrCode, model) {
    debugger;

    console.log(
      "Modelllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll"
    );
    console.log(model);

    let query = { creator: model.code };
    let c = await this.crmService.findOneByQuery(query);
    console.log("DDDDDDDDDDD", c);
    let createDto: any = {
      creator: model.code,
    };
    // let q={createdBy:model.code};
    debugger;
    let url = "http://161.117.176.141/auth/login"
    let data = await this.emailnotificationService.findOneByQuery(query);
    console.log(
      "Model.EntityDetailCodeModel.EntityDetailCodeModel.EntityDetailCode",
      model.entityDetailCode
    );
    

    //await this.createBigChainTransaction(_model, model);
    debugger;
    //let email=model.email;
    if (
      model.verification.isProfileCompleted === 1 &&
      data.isKycApproved === false
    ) {
      if (model.verification.isOnboardingComplete == true) {
        // await this.createBigChainTransaction(_model, model);
        if (model.entityDetailCode !== undefined) {
          let _model = await this.entityDetailsService.findOne(
            model.entityDetailCode
          );
          //debugger hittttttttttttttted
          debugger;
          //console.log(_model)
          //console.log(_model)
          await this.createBigChainTransaction(_model, model);
        }
      }

      // let tcheck = model.verification.isProfileCompleted == 1;
      if (
        c.crmCustomer.isComplete == false
        //&& tcheck
      ) {
        //email integration

        // if(c.crmCustomer.isComplete == false){
        console.log("PostUpdateeeeeeeeffffffffff", model);
        var crmUserCustomer: any =
          '{"contacttype":"Customer","assigned_user_id":"19x1"' +
          ',"lastname":' +
          '"' +
          model.lastName +
          '"' +
          ',"cf_contacts_role":' +
          '"' +
          model.type +
          '"' +
          ',"email":' +
          '"' +
          model.email +
          '"' +
          ',"id":' +
          '"' +
          model.crmId +
          '"' +
          "}";

        var options5 = {
          ...CRM_URL_HEADERS_UPDATE,
          form: {
            //'elementType': 'Contacts',
            element: crmUserCustomer,
          },
        };
        const response = await request.post(options5);
        console.log(crmUserCustomer);
        console.log(response);
        console.log("Paassssssssssssssssssssssssssssed");

        let k = {
          preUpdate: c.crmCustomer.preUpdate,
          postUpdate: c.crmCustomer.postUpdate,
          isComplete: true,
        };

        let up = await this.crmService.edit(
          c.code,
          { crmCustomer: k },
          Date.now()
        );

        if (model.type === USER_TYPES.SPONSOR) {
          console.log(response);
          console.log("SponsorrrrrrrrrrrrrrPaassssssssssssssssssssssssssssed");
          await delay(3000);
          console.log("delay is completed");
        }
        //this.usersService.PostToCrmAsCustomer(user);
        //console.log("submitted3333333333", up);
      }

      //activity feeding for kyc approval

      if (model.type === USER_TYPES.SPONSOR) {
        let createDto3: any = {
          _priority: 2,
          type:
            ACTIVITY_FEED_EVENTS_SPONSOR.KYC_VERIFICATION_NOTIFICATION_SUCCESSFUL,
          itemId: model.code,
          setFields: { message: "admin approved" },
        };
        let _activityFeed: any = await this.activityFeedService.kYCVerificationNotificationsSuccessful(
          createDto3,
          model
        );
        if(model.emailAlerts == true){
          await this.emailService.sendEmail(
            new KycApprovedforSponsor(
              new EmailDto({
                to: model.email,
                metaData: { model, url },
              })
            )
          );
        }
      }
      if (model.type === USER_TYPES.SME) {
        let createDto3: any = {
          _priority: 2,
          type:
            ACTIVITY_FEED_EVENTS_SME.KYC_VERIFICATION_NOTIFICATION_SUCCESSFUL,
          itemId: model.code,
          setFields: { message: "admin approved" },
        };
        let _activityFeed: any = await this.smeactivityFeedService.kYCVerificationNotificationsSuccessful(
          createDto3,
          model
        );
        if(model.emailAlerts == true){
          await this.emailService.sendEmail(
            new KycApprovedforSme(
              new EmailDto({
                to: model.email,
                metaData: { model, url },
              })
            )
          );
        }
      }
      let result = await this.emailnotificationService.edit(data.code, {
        isKycApproved: true,
        isKycRejected: false,
        //isKycPending:false
      });

      console.log(result);
    }
    debugger;
    if (
      model.verification.isProfileCompleted === 2 &&
      data.isKycRejected === false
    ) {
      if (model.type === USER_TYPES.SPONSOR) {
        let createDto3: any = {
          _priority: 2,
          type:
            ACTIVITY_FEED_EVENTS_SPONSOR.KYC_VERIFICATION_NOTIFICATION_REJECTED,
          itemId: model.code,
          setFields: { message: "admin rejected" },
        };
        let _activityFeed: any = await this.activityFeedService.kYCVerificationNotificationsRejectedAndReasonForRejection(
          createDto3,
          model
        );
        if(model.emailAlerts == true){
          await this.emailService.sendEmail(
            new KycRejectedforSponsor(
              new EmailDto({
                to: model.email,
                metaData: { model },
              })
            )
          );
        }
      }
      if (model.type === USER_TYPES.SME) {
        let createDto3: any = {
          _priority: 2,
          type: ACTIVITY_FEED_EVENTS_SME.KYC_VERIFICATION_NOTIFICATION_REJECTED,
          itemId: model.code,
          setFields: { message: "admin rejected" },
        };
        let _activityFeed: any = await this.smeactivityFeedService.kYCVerificationNotificationsRejectedAndReasonForRejection(
          createDto3,
          model
        );
        if(model.emailAlerts == true){
          await this.emailService.sendEmail(
            new KycRejectedforSme(
              new EmailDto({
                to: model.email,
                metaData: { model },
              })
            )
          );
        }
      }

      let result = await this.emailnotificationService.edit(data.code, {
        isKycRejected: true,
        isKycApproved: false,
        //  isKycPending:false
      });

      console.log(result);
    }

    if (!(c == null)) {
      /*
      let tcheck = model.verification.isProfileCompleted == 1;
      await delay(1000);
      console.log("delay is completed");
      if (c.crmCustomer.isComplete == false && tcheck) {
        //email integration

        // if(c.crmCustomer.isComplete == false){
        console.log("PostUpdateeeeeeeeffffffffff", model);
        var crmUserCustomer: any =
          '{"contacttype":"Customer","assigned_user_id":"19x1"' +
          ',"lastname":' +
          '"' +ther open issu
          ',"cf_contacts_role":' +
          '"' +
          model.type +
          '"' +
          ',"email":' +
          '"' +
          model.email +
          '"' +
          ',"id":' +
          '"' +
          model.crmId +
          '"' +
          "}";

        var options5 = {
          ...CRM_URL_HEADERS_UPDATE,
          form: {
            //'elementType': 'Contacts',
            element: crmUserCustomer,
          },
        };
        const response = await request.post(options5);
        console.log(crmUserCustomer);
        console.log(response);
        console.log("Paassssssssssssssssssssssssssssed");
        console.log(crmUserCustomer);

        let k = {
          preUpdate: c.crmCustomer.preUpdate,
          postUpdate: c.crmCustomer.postUpdate,
          isComplete: true,
        };
        
        let up = await this.crmService.edit(
          c.code,
          { crmCustomer: k },
          Date.now()
        );
        //this.usersService.PostToCrmAsCustomer(user);
        console.log("submitted3333333333", up);
      
        */
      //}
    }
  }
}
