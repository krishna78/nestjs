import { Creator, Reader } from "../../common/base/base.dto";
import { IsDefined, IsNotEmpty, IsInt } from "class-validator";
import { Expose } from "class-transformer";

export class WalletTransactionDto extends Reader{
   
    @Expose()
    readonly projectId: string = "";

    @Expose()
    readonly bidAmount: number;

    @Expose()
    readonly sponserId: string = "";
   
    @Expose()
    readonly bidId: string = "";
    
    @Expose()
    readonly transactionType: string = "";
}


export  class CreateWalletTransactionDto extends Creator{
    constructor({
        projectId,
        bidId,
        bidAmount,
        sponserId,
        transactionType
    }){
        super(true);
        this.projectId = projectId;
        this.bidId = bidId;
        this.bidAmount = bidAmount;
        this.sponserId = sponserId;
        this.transactionType = transactionType;
    }
  
    @IsDefined()
    @IsNotEmpty()
    readonly projectId: string;
    
    @IsDefined()
    @IsNotEmpty()
    readonly bidId: string
    
    @IsDefined()
    @IsNotEmpty()
    @IsInt()
    readonly bidAmount: number;

    @IsDefined()
    @IsNotEmpty()
    readonly sponserId: string;

    @IsDefined()
    @IsNotEmpty()
    readonly transactionType: string;

}