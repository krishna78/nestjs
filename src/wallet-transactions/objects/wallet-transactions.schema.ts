import { Schema } from "mongoose";
import { Entity, IEntity, createModel } from "../../common/base/base.model";

export class WalletTransaction  extends Entity{
    projectId: string;
    bidId: string;
    sponserId: string;
    bidAmount: number;
    transactionType: string;
}
export interface IWalletTransaction extends WalletTransaction,IEntity{
   id :string;
}

export const WalletTransactionsSchema:Schema= createModel("WalletTransactions", {
    projectId:{ type: String, required: true },
    bidId:{ type: String, required: true },
    sponserId:{ type: String, required: true },
    bidAmount:{ type: Number, required: true },
    transactionType:{ type: String, required: true },
  
});