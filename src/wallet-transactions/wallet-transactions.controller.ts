import { Controller } from '@nestjs/common';
import { WalletTransactionsService } from './wallet-transactions.service';
import { abstractBaseControllerFactory } from '../common/base/base.controller';
import { WalletTransactionDto, CreateWalletTransactionDto } from './objects/wallet-transactions.dto';
import { BASEROUTES } from '../common/constants/enum';

const BaseController = abstractBaseControllerFactory<any>({
    DTO: WalletTransactionDto,
    CreateDTO: CreateWalletTransactionDto,
  //  UpdateDTO: UpdateBidDetailsDto,
    DisabledRoutes: [
        //BASEROUTES.FINDLIST,
        BASEROUTES.FINDONE,
        BASEROUTES.UPDATEONE,
        BASEROUTES.DETELEONE,
        BASEROUTES.PATCH,
    ]
  });

@Controller('wallet-transactions')
export class WalletTransactionsController extends BaseController {
    constructor(private walletTransactionsService: WalletTransactionsService) {
        super(walletTransactionsService);
      }
}
