import { Module, forwardRef } from '@nestjs/common';
import { WalletTransactionsController } from './wallet-transactions.controller';
import { WalletTransactionsService } from './wallet-transactions.service';
import { MongooseModule } from '@nestjs/mongoose';
import { WalletTransactionsSchema } from './objects/wallet-transactions.schema';
import { UsersModule } from '../users/users.module';
import { UserSchema } from '../users/objects/user.schema';
import { SmeProjectSchema } from '../sme-project/objects/sme-project.schema';
import { SmeProjectModule } from '../sme-project/sme-project.module';
import { EmailModule } from '../email/email.module';
import { ActivityFeedModule } from '../activity-feed/activity-feed.module';

@Module({ 
  imports: [
  MongooseModule.forFeature([
    { name: "WalletTransaction", schema: WalletTransactionsSchema },
    { name: "User", schema: UserSchema },
    { name: "SmeProject", schema: SmeProjectSchema },
    
  ]),forwardRef(() => UsersModule),
  forwardRef(() => SmeProjectModule),
  EmailModule,
  ActivityFeedModule
],
  controllers: [WalletTransactionsController],
  providers: [WalletTransactionsService]
})
export class WalletTransactionsModule {

}
