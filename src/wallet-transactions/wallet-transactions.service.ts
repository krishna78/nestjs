import { Injectable } from "@nestjs/common";
import { Model } from "mongoose";
import { IWalletTransaction } from "./objects/wallet-transactions.schema";
import { InjectModel } from "@nestjs/mongoose";
import { BaseService } from "../common/base/base.service";
import { UsersService } from "../users/users.service";
import { EmailService } from "../email/email.service";
import { WalletBalanceSponsorEmail } from "../users/objects/user.registered.email";
import { EmailDto } from "../email/objects/email.dto";
import {
  ACTIVITY_FEED_EVENTS_SPONSOR,
  TRANSACTION_TYPE,
} from "../common/constants/enum";
import { ActivityFeedService } from "../activity-feed/activity-feed.service";
import { SmeProjectService } from "../sme-project/sme-project.service";
import { HasPreCreate, HasPostCreate } from "../common/base/pre.post.action";

@Injectable()
export class WalletTransactionsService extends BaseService<IWalletTransaction>
  implements HasPreCreate, HasPostCreate {
  constructor(
    @InjectModel("WalletTransaction")
    private readonly walletTransactionModel: Model<IWalletTransaction>,
    private usersService: UsersService,
    private emailService: EmailService,
    private smeProjectService: SmeProjectService,
    private activityFeedService: ActivityFeedService
  ) {
    super(walletTransactionModel);
  }

  async doPreCreate(createDto) {
    //To do: Wallet transaction type :Credit/Debit.Add more functionality.
  }

  async doPostCreate(createDto) {
    let _user = await this.usersService.findOne(createDto.sponserId);
    let _project = await this.smeProjectService.findOne(createDto.projectId);
    var _date = new Date(createDto.createdTime);
    console.log(
      "doPostCreateeeedoPostCreateeeedoPostCreateeeedoPostCreateeee",
      createDto,
      _date
    );

    //WALLET MONEY ADDED
    if (createDto.transactionType == TRANSACTION_TYPE.CREDIT) {
      let obj = { createDto, _user };
      let createDto6: any = {
        _priority: 1,
        type: ACTIVITY_FEED_EVENTS_SPONSOR.WALLET_MONEY_ADDED,
        itemId: createDto.bidId,
        setFields: obj,
        optional: _project.formattedId,
      };
      let balance = _user.totalBalance + createDto.bidAmount;
      let _userUpdate = await this.usersService.edit(_user.code, {
        totalBalance: balance,
      });

      let _activityFeed6: any = await this.activityFeedService.walletMoneyAdded(
        createDto6,
        _userUpdate
      );
      if(_user.emailAlerts == true){
        await this.emailService.sendEmail(
          new WalletBalanceSponsorEmail(
            new EmailDto({
              to: _user.email,
              metaData: { createDto, _user, _userUpdate, _date },
            })
          )
        );
      }
    } else if (createDto.transactionType == TRANSACTION_TYPE.DEBIT) {
      //WALLET_MONEY_WITHDRAWN

      let balance = _user.totalBalance - createDto.bidAmount;
      let _userUpdate = await this.usersService.edit(createDto.sponserId, {
        totalBalance: balance,
      });
      console.log(createDto, _user.totalBalance, _userUpdate.totalBalance);
      let obj = { createDto, _userUpdate };
      let createDto5: any = {
        _priority: 1,
        type: ACTIVITY_FEED_EVENTS_SPONSOR.WALLET_MONEY_WITHDRAWN,
        itemId: createDto.code,
        setFields: obj,
        optional: _project.formattedId,
      };
      let _activityFeed5: any = await this.activityFeedService.walletMoneyWithdrawn(
        createDto5,
        _user
      );  
      if(_user.emailAlerts == true){
        await this.emailService.sendEmail(
          new WalletBalanceSponsorEmail(
            new EmailDto({
              to: _user.email,
              metaData: { createDto, _user, _userUpdate, _date },
            })
          )
        );
      }
    }
  }
}
